//
//  CommonUtil.h
//  iOS SDK
//
//  Created by Jacky Che on 19/3/11.
//  Copyright © 2019年 互盈. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <MapKit/MKMapItem.h>
#import <MapKit/MKTypes.h>
#import "JZLocationConverter.h"
@class FYBaseViewController;
@interface CommonUtil : NSObject
//获取单例
/**
 *  获取一个UUID
 *
 *  @return UUID
 */
+ (NSString *)createUUID;

/**
 *  获取当前版本号
 *
 *  @return 版本号字符串
 */
+ (NSString *)getVersionNmuber;

/**
 *  获取window
 *
 *  @return window
 */
+ (UIWindow *)window;

#pragma mark - 关于"NSUserDefaults"快捷方法

/**
 *  保存数据 - NSUserDefaults
 *  注：必须NSUserDefaults 识别的类型
 *
 *  @param key      key
 */
+ (void)setInfo:(id)info forKey:(NSString *)key;

/**
 *  获取数据 - NSUserDefaults
 *
 *  @param key key
 *
 *  @return value
 */
+ (id)getInfoWithKey:(NSString *)key;

/**
 *  删除数据 - NSUserDefaults
 *
 *  @param key key
 */
+ (void)removeInfoWithKey:(NSString *)key;

#pragma mark - 关于accesstoken

/**
 *  判断是否含有 accessToken
 *
 *  @return YES？NO
 */
+ (BOOL)isHaveAccessToken;

/**
 *  获取 accessToken
 *
 *  @return YES？NO
 */
+ (NSString *)getAccessToken;

/**
 *  移除 accessToken
 *
 */
+ (void)removeAccessToken;

#pragma mark - 关于时间

/** 知道时间，计算出是周几 */
+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate;

#pragma mark - 关于时间方法

/** ”时间戳“转成”Date“ */
+ (NSDate *)getDateForTimeIntervalString:(NSString *)interval;

/** Date转"时间戳"字符串（0.PHP类型-10位 1.Jave-13位） */
+ (NSString *)getTimeIntervalForDate:(NSDate *)date byType:(NSInteger)type;

/** 获取当前时间的“时间戳” -- 默认13位 */
+ (NSString *)getTimeIntervalStringByNew;

/** 时间戳 直接转成 NSString (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss") */
+ (NSString *)getStringForTimeIntervalString:(NSString *)interval format:(NSString *)format;

#pragma mark -

/**
 Date 转换 NSString
 (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDate:(NSDate *)date format:(NSString *)format;

/**
 Date 转换 NSString
 (默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDate:(NSDate *)date;

/** NSString 转换 Date
 (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSDate *)getDateForString:(NSString *)string format:(NSString *)format;

/**
 NSString 转换 Date
 (默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSDate *)getDateForString:(NSString *)string;

/**
 DateNSString 转换 NSString
 (默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDateString:(NSString *)string byFormat:(NSString *)byFormat toFormat:(NSString *)toFormat;

#pragma mark -

/** 获取时间差(秒) */
+ (NSInteger)getSecondForFromDate:(NSDate *)fromdate toDate:(NSDate *)todate;

//是否格式为 包含字母和数字，且只包含数字、字母、下划线
+(BOOL)checkIsHaveNumAndLetter:(NSString*)password;

/**
 根据code 返回相应的错误信息

 @param errorCode 错误代码编号
 @return 错误信息
 */
+ (NSString *)getErrorMessageWithCode:(NSInteger)errorCode;

/**
 md5 加密

 @param string 加密字符串
 @return 加密后字符串
 */
+ (NSString *)md5:(NSString *)string;

/**
 检测手机号码是否可用

 @param mobile 手机号
 @return 是否可以
 */
+ (BOOL)isValueMobile:(NSString *)mobile;

/**
  *  验证身份证号码是否正确的方法
  *
  *  @param IdNumber 身份证号码
  *
  *  @return 返回YES或NO表示该身份证号码是否符合国家标准
  */
+ (BOOL)isAvailableIdCardNumber:(NSString *)IdNumber;

/**
 验证银行卡号是否合法

 @param cardNo 银行卡号
 @return 返回是否合法
 */
+ (BOOL)isAvailableBankCardNumber:(NSString*)cardNo;

/**
 根据银行缩写返回银行名称

 @param logogram 银行缩写
 @return 返回银行名称
 */
+ (NSString *)bankNameWithLogogram:(NSString *)logogram;

/**
 jsonStr转Array

 @param jsonStr jsonStr
 @return  数组
 */
+  (id)toArrayOrNSDictionary:(NSString *)jsonStr;

/**
 计算距离
 @param distance 距离
 @return 计算后的距离
 */
+ (NSString *)countDistance:(double )distance;
+ (NSArray *)getInstalledMapAppWithEndLocation:(CLLocationCoordinate2D)endLocation title:(NSString *)title;
+ (void)navAppleMapWithEndLocation:(CLLocationCoordinate2D)endLocation title:(NSString *)title;
+ (NSMutableAttributedString *)addBlankWith:(NSString *)cardNo;
+ (BOOL)inputTextfield:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replaceString:(NSString *)rangeString number:(NSInteger)number VC:(FYBaseViewController *)ViewController;
+ (CGFloat)getMessageHeight:(NSString *)mess;
+ (NSMutableAttributedString *)replaceTelephone:(NSString *)phoneNum;
+ (NSInteger)statusWithType:(NSString *)type;
+ (NSString *)typeWithStatus:(NSInteger)status;
+ (BOOL)feiKong:(id)text;
+ (NSString *)fixNullText:(NSString *)nullText;
@end
