//
//  FeedbackViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/6/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FeedbackViewController.h"

@interface FeedbackViewController ()<UITextViewDelegate>
@property (nonatomic,strong)UITextView *textView;
@property (nonatomic,strong)UITextField *contactTF;
@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CAGradientLayer  *gradient = [CAGradientLayer layer];
    [self.customNavView.layer addSublayer:gradient];
    gradient.frame = self.customNavView.bounds;
    gradient.colors = @[(id)Color_Common.CGColor,(id)Color_CommonLight.CGColor];
    gradient.startPoint = POINT(0,0.5);
    gradient.endPoint = POINT(1,0.5);
    [self setNavTitle:@"问题反馈" Color:Color_White];
    [self setLeftButtonImage:@"icon_backUp"];
    [self navgationRightButtonTitle:@"提交" color:Color_White];
    [self layoutUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = Color_Common;
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    
     UILabel *tipLabel1 = [UILabel new];
    [tipLabel1 rect:RECT(10, 10+NavHeight, 80, 30) aligment:Left font:15 isBold:NO text:@"*问题描述:" textColor:Color_666666 superView:self.view];
    
    _textView = [[UITextView alloc]initWithFrame:RECT(90, 8+NavHeight, ScreenWidth-100, 150)];
    _textView.text = @"详细的问题描述(使用过程中遇到的问题，如：APP崩溃、异常信息等)";
    _textView.textColor = Color_999999;
    _textView.font = FONT(14);
    _textView.delegate = self;
    [self.view addSubview:_textView];
    
    
    UIView *lineView = [UIView createLineViewWithFrame:RECT(10, 170+NavHeight, ScreenWidth-20, .5)];
    lineView.backgroundColor = Color_Line;
    [self.view addSubview:lineView];
    
    
    UILabel *tipLabel2 = [UILabel new];
    [tipLabel2 rect:RECT(10, 180+NavHeight, 80, 30) aligment:Left font:15 isBold:NO text:@"*联系方式:" textColor:Color_666666 superView:self.view];
    
    _contactTF = [[UITextField alloc]initWithFrame:RECT(100, 180+NavHeight, ScreenWidth-110, 30)];
    _contactTF.borderStyle = 0;
    _contactTF.textColor = Color_666666;
    _contactTF.font = FONT(14);
    _contactTF.placeholder = @"您的联系方式？";
    [_contactTF addObserverTextFieldEditChangeWithLenth:32 inputType:ImportChatTypeEmail];
    [self.view addSubview:_contactTF];
    
    UIView *lineView2 = [UIView createLineViewWithFrame:RECT(10, 220+NavHeight, ScreenWidth-20, .5)];
    lineView2.backgroundColor = Color_Line;
    [self.view addSubview:lineView2];
}
#pragma mark ---------->>> textViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"详细的问题描述(使用过程中遇到的问题，如：APP崩溃、异常信息等)"]) {
        _textView.text = @"";
        _textView.textColor = Color_666666;
    }
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    if (_textView.text.length==0) {
        _textView.text = @"详细的问题描述(使用过程中遇到的问题，如：APP崩溃、异常信息等)";
        _textView.textColor = Color_999999;
    }
}
#pragma mark ---------->>> private
- (void)navgationRightButtonClick {
    if (_textView.text.length==0||[_textView.text isEqualToString:@"详细的问题描述(使用过程中遇到的问题，如：APP崩溃、异常信息等)"]) {
        [self showHUDMessage:@"请输入反馈的问题描述"];
        return;
    }
    if (_contactTF.text.length==0) {
        [self showHUDMessage:@"请输入您的联系方式"];
        return;
    }
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:_contactTF.text forKey:@"contact"];
    [params setValue:_textView.text forKey:@"content"];
    [params setValue:@"yw-ios" forKey:@"system"];
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/feedback" method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"反馈成功"];
                [self dismissViewControllerAnimated:YES completion:nil];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
@end
