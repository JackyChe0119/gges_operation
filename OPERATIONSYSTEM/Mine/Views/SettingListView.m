//
//  SettingListView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/18.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "SettingListView.h"
@interface SettingListView()
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UIImageView *accessTypeImage;
@end
@implementation SettingListView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Color_White;
        [self addSubview:self.itemLogoImageView];
        _titleLabel = [UILabel new];
        [_titleLabel rect:RECT(50, HEIGHT(self)/2.0-15, 100, 30) aligment:Left font:14 isBold:NO text:@"手机号" textColor:Color_333333 superView:self];
        
        _subTitleLabel = [UILabel new];
        [_subTitleLabel rect:RECT(170, HEIGHT(self)/2.0-15, WIDTH(self)-150, 30) aligment:Right font:14 isBold:NO text:@"" textColor:colorWithHexString(@"#4F5D83") superView:self];
        
        _accessTypeImage = [UIImageView createImageViewWithFrame:RECT(WIDTH(self)-30,HEIGHT(self)/2.0-10 , 20, 20) imageName:@"mine_accesstype_gray"];
        _accessTypeImage.contentMode = UIViewContentModeCenter;
        _accessTypeImage.tag = 100;
        [self addSubview:_accessTypeImage];
        
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
        [self addGestureRecognizer:tap];
    }
    return self;
}
- (void)setAlintment:(NSInteger)alintment {
    _subTitleLabel.textAlignment = alintment;
}
- (void)setLeft:(CGFloat)left {
    _subTitleLabel.frame = RECT(left, HEIGHT(self)/2.0-15, WIDTH(self)-left-30, 30);
}
#pragma mark private
- (void)tap {
    self.userInteractionEnabled = NO;
    if (_operationBlock) {
        _operationBlock();
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.userInteractionEnabled = YES;
    });
}
#pragma mark setting getting
- (void)setTitleString:(NSString *)titleString {
    _titleString = titleString;
    self.titleLabel.text = titleString;
}
- (void)setSubTiteString:(NSString *)subTiteString {
    _subTiteString = subTiteString;
    self.subTitleLabel.text = subTiteString;
}
- (void)setLogo:(NSString *)logo {
    _logo = logo;
    self.itemLogoImageView.image = IMAGE(logo);
}
- (void)setTitlecolor:(UIColor *)titlecolor {
    _titlecolor = titlecolor;
    self.titleLabel.textColor = titlecolor;
}
- (void)setTitleFontSize:(NSInteger)titleFontSize {
    _titleFontSize = titleFontSize;
    self.titleLabel.font = FONT(titleFontSize);
}
- (void)setSubTitleFontSize:(NSInteger)subTitleFontSize {
    _subTitleFontSize = subTitleFontSize;
    self.subTitleLabel.font = FONT(subTitleFontSize);
}
- (void)setShowLine:(BOOL)showLine {
    _showLine = showLine;
    self.lineView.hidden = !showLine;
}
- (void)setHiddenAccessType:(BOOL)hiddenAccessType {
    _hiddenAccessType = hiddenAccessType;
    if (hiddenAccessType) {
        self.subTitleLabel.frame = RECT(120, HEIGHT(self)/2.0-15, WIDTH(self)-136, 30);
        UIImageView *imageView = (UIImageView *)[self viewWithTag:100];
        imageView.hidden = YES;
    }
}
- (void)setAccessType:(NSString *)accessType {
    _accessType = accessType;
    _accessTypeImage.image = IMAGE(accessType);
}
- (void)setSubTitlecolor:(UIColor *)subTitlecolor {
    _subTitlecolor = subTitlecolor;
    self.subTitleLabel.textColor = subTitlecolor;
}
#pragma mark 懒加载
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView createViewWithFrame:RECT(0, HEIGHT(self)-.5, WIDTH(self), .5) color:Color_Line];
        [self addSubview:_lineView];
    }
    return _lineView;
}
- (UIImageView *)itemLogoImageView {
    if (!_itemLogoImageView) {
        _itemLogoImageView = [[UIImageView alloc]initWithFrame:RECT(15, 14, 22, 22)];
    }
    return _itemLogoImageView;
}
@end
