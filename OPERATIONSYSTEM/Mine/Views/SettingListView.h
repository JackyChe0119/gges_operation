//
//  SettingListView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/18.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingListView : UIView
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,copy)NSString *titleString;//标题
@property (nonatomic,copy)NSString *subTiteString;//副标题
@property (nonatomic,copy)NSString *accessType;
@property (nonatomic,strong)UILabel *subTitleLabel;
@property (nonatomic,strong)UIImageView *itemLogoImageView;
@property (nonatomic,copy)NSString *logo;
@property (nonatomic,assign)BOOL showLine;//是否需要划线
@property (nonatomic,assign)BOOL hiddenAccessType;//显示头像
@property (nonatomic,strong)void (^operationBlock)(void);//操作回调
@property (nonatomic,strong)UIColor *subTitlecolor;
@property (nonatomic,strong)UIColor *titlecolor;
@property (nonatomic,assign)NSInteger titleFontSize;
@property (nonatomic,assign)NSInteger subTitleFontSize;
@property (nonatomic,assign)NSInteger alintment;
@property (nonatomic,assign)CGFloat left;
@end

NS_ASSUME_NONNULL_END
