//
//  MineListView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/6/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "MineListView.h"

@implementation MineListView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.leftImageView];
        [self addSubview:self.inputTextField];
        [self addSubview:self.lineView];
    }
    return self;
}
- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        _leftImageView = [UIImageView createImageViewWithFrame:RECT(0, HEIGHT(self)/2.0-10, 20, 20) imageName:@"icon_mima_gray"];
    }
    return _leftImageView;
}
- (UITextField *)inputTextField {
    if (!_inputTextField) {
        _inputTextField = [[UITextField alloc]initWithFrame:RECT(30, HEIGHT(self)/2.0-15, WIDTH(self)-30, 30)];
        _inputTextField.borderStyle = 0;
        _inputTextField.textColor = Color_333333;
        _inputTextField.font = FONT(14);
        [_inputTextField addObserverTextFieldEditChangeWithLenth:32 inputType:ImportChatTypeNumberAndEnglish];
        _inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _inputTextField;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView createViewWithFrame:RECT(0, HEIGHT(self)-.5, WIDTH(self), .5) color:Color_Line];
    }
    return _lineView;
}
@end
