//
//  MineListView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/6/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineListView : UIView
@property (nonatomic,strong)UIImageView *leftImageView;
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,strong)UIView *lineView;
@end

NS_ASSUME_NONNULL_END
