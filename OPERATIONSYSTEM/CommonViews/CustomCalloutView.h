//
//  CustomCalloutView.h
//  ManggeekBaseProject
//
//  Created by 车杰 on 2017/6/20.
//  Copyright © 2017年 Jacky. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CustomCalloutView : UIView
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, copy) NSString *title; //商户名
@property (nonatomic, copy) NSString *subtitle; //副标题
@property (nonatomic, copy) NSString *numString;//数据
@property (nonatomic, assign)NSInteger type;//类型
- (id)initWithFrame:(CGRect)frame type:(NSInteger)type;
@end
