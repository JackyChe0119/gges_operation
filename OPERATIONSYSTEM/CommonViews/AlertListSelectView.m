//
//  AlertListSelectView.m
//  GEESYSTEM
//
//  Created by 车杰 on 2019/7/16.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "AlertListSelectView.h"

@implementation AlertListSelectView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    
        _titleLabel = [UILabel new];
        [_titleLabel rect:RECT(0, 10,60, HEIGHT(self)-20) aligment:Right font:13 isBold:NO text:@"处理线" textColor:Color_999999 superView:self];
        _titleLabel.adjustsFontSizeToFitWidth = YES;
        
        _baseView = [UIView createViewWithFrame:RECT(65, HEIGHT(self)/2.0-13, WIDTH(self)-75, 26) color:Color_BG];
        [_baseView addRoundedCornersWithRadius:4];
        _baseView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [_baseView addGestureRecognizer:tap];
        [self addSubview:_baseView];
        
        _infoLabel = [UILabel new];
        [_infoLabel rect:RECT(5, 0,WIDTH(_baseView)-15, HEIGHT(_baseView)) aligment:Left font:13 isBold:NO text:@"全部" textColor:Color_333333 superView:_baseView];
        
        _imageView = [UIImageView createImageViewWithFrame:RECT(WIDTH(_baseView)-12, HEIGHT(_baseView)-12, 24, 20) imageName:@"iocn_sanjiao"];
        [_baseView addSubview:_imageView];
    }
    return self;
}
- (void)setLeftWidth:(double)leftWidth {
    _titleLabel.frame = RECT(0, 10,leftWidth, HEIGHT(self)-20);
    _baseView.frame = RECT(RIGHT(_titleLabel), HEIGHT(self)/2.0-13, WIDTH(self)-RIGHT(_titleLabel)-10, 26);
    _infoLabel.frame = RECT(5, 0,WIDTH(_baseView)-15, HEIGHT(_baseView));
    _imageView.frame = RECT(WIDTH(_baseView)-12, HEIGHT(_baseView)-12, 24, 20);
    
}
- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = title;
}
- (void)setInfo:(NSString *)info {
    _info = info;
    if (![_info isKindOfClass:[NSNull class]]) {
        self.infoLabel.text = info;
    }else {
        self.infoLabel.text = @"";
    }
}
- (void)tap:(UITapGestureRecognizer *)tap {
    tap.view.userInteractionEnabled = NO;
    if (_operationBlock) {
        _operationBlock();
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        tap.view.userInteractionEnabled = YES;
    });
}
@end
