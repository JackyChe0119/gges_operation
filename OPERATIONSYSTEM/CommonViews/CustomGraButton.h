//
//  CustomGraButton.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/15.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomGraButton : UIButton

@property (nonatomic,strong)UIColor *titleColor;
@property (nonatomic,copy)NSString *buttonTitle;
@property (nonatomic,strong)CAGradientLayer *gradient;
- (void)setUnSelectState;
- (void)setSlectState;
/**
 自定义按钮

 @param frame 坐标
 @param taeget 目标
 @param selector 方法
 @param title 标题
 @param color 颜色
 @param fontSize 字体大小
 @return 按钮
 */
- (instancetype)initWithFrame:(CGRect)frame target:(id)taeget action:(SEL)selector title:(NSString *)title color:(UIColor *)color font:(CGFloat)fontSize type:(NSInteger)type;
@end

NS_ASSUME_NONNULL_END
