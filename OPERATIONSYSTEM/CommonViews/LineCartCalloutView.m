//
//  LineCartCalloutView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/7/10.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "LineCartCalloutView.h"

@implementation LineCartCalloutView

#define kArrorHeight        5

#define kPortraitMargin     5
#define kPortraitWidth      70
#define kPortraitHeight     50

#define kTitleWidth         120
#define kTitleHeight        20

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        [self initSubViews];
    }
    return self;
}
- (void)setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

- (void)setSubtitle:(NSString *)subtitle
{
    self.subtitleLabel.text = subtitle;
}
- (void)setNumString:(NSString *)numString {
    self.numLabel.text = numString;
}
- (void)initSubViews
{
    // 添加标题，即商户名
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 4, WIDTH(self)-4, 16)];
    self.titleLabel.font = [UIFont systemFontOfSize:12];
    self.titleLabel.textColor = Color_333333;
    self.titleLabel.text = @"title";
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    // 添加副标题，即商户地址
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(2 , 20, WIDTH(self)-4, 16)];
    self.subtitleLabel.font = [UIFont systemFontOfSize:10];
    self.subtitleLabel.textColor = Color_Common;
    self.subtitleLabel.text = @"subTitle";
    self.subtitleLabel.adjustsFontSizeToFitWidth = YES;
    self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.subtitleLabel];
    
    self.numLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 36, WIDTH(self)-4, 16)];
    self.numLabel.font = [UIFont systemFontOfSize:10];
    self.numLabel.textColor = Color_Common;
    self.numLabel.text = @"numLabel";
    self.numLabel.adjustsFontSizeToFitWidth = YES;
    self.numLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.numLabel];
    
}
- (void)drawRect:(CGRect)rect {
    [self drawInContext:UIGraphicsGetCurrentContext()];
}
- (void)drawInContext:(CGContextRef)context {
    CGContextSetLineWidth(context, 1);
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] colorWithAlphaComponent:.9].CGColor);
    [self getDrawPath:context];
    CGContextFillPath(context);
    CGContextSetStrokeColorWithColor(context, Color_Common.CGColor);
    [self getDrawPath:context];
    CGContextStrokePath(context);
}
- (void)getDrawPath:(CGContextRef)context
{
    CGRect rrect = self.bounds;
    CGFloat radius = 4.0;
    CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect),
    maxy = CGRectGetMaxY(rrect)-kArrorHeight;
    
    CGContextMoveToPoint(context, midx+kArrorHeight, maxy);
    CGContextAddLineToPoint(context,midx, maxy+kArrorHeight);
    CGContextAddLineToPoint(context,midx-kArrorHeight, maxy);
    
    CGContextAddArcToPoint(context, minx, maxy, minx, miny, radius);
    CGContextAddArcToPoint(context, minx, minx, maxx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, maxx, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextClosePath(context);
}
@end
