//
//  CustomGraButton.m
//  OPERATIONSYSTEM
//  颜色渐变button
//  Created by 车杰 on 2019/3/15.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "CustomGraButton.h"
@implementation CustomGraButton

- (instancetype)initWithFrame:(CGRect)frame target:(id)taeget action:(SEL)selector title:(NSString *)title color:(UIColor *)color font:(CGFloat)fontSize type:(NSInteger)type {
    self = [super initWithFrame:frame];
    if (self) {
        if (type==1) {
            [self setUnSelectState];
        }else {
            [self setSlectState];
        }
        _buttonTitle = title;
        _titleColor = color;
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:color forState:UIControlStateNormal];
        self.titleLabel.font = FONT(fontSize);
        [self addTarget:taeget action:selector forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)setUnSelectState {
    if (!_gradient) {
        _gradient = [CAGradientLayer layer];
        [self.layer addSublayer:_gradient];
    }
    _gradient.frame = self.bounds;
    _gradient.colors = @[(id)colorWithHexString(@"#dddddd").CGColor,(id)colorWithHexString(@"#dddddd").CGColor];
    _gradient.startPoint = POINT(0,0.5);
    _gradient.endPoint = POINT(1,0.5);
    self.userInteractionEnabled = NO;
}
- (void)setSlectState {
    if (!_gradient) {
        _gradient = [CAGradientLayer layer];
        [self.layer addSublayer:_gradient];
    }
    _gradient.frame = self.bounds;
    _gradient.colors = @[(id)Color_Common.CGColor,(id)Color_CommonLight.CGColor];
    _gradient.startPoint = POINT(0,0.5);
    _gradient.endPoint = POINT(1,0.5);
    [self setTitle:self.buttonTitle forState:UIControlStateNormal];
    [self setTitleColor:self.titleColor forState:UIControlStateNormal];
    self.userInteractionEnabled = YES;
}
@end
