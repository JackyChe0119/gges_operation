//
//  CustomCalloutView.m
//  ManggeekBaseProject
//
//  Created by 车杰 on 2017/6/20.
//  Copyright © 2017年 Jacky. All rights reserved.
//

#import "CustomCalloutView.h"
#define kArrorHeight        5

#define kPortraitMargin     5
#define kPortraitWidth      70
#define kPortraitHeight     50

#define kTitleWidth         120
#define kTitleHeight        20

@implementation CustomCalloutView

- (id)initWithFrame:(CGRect)frame type:(NSInteger)type
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _type = type;
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        [self initSubViews];
    }
    return self;
}
- (void)setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

- (void)setSubtitle:(NSString *)subtitle
{
    self.subtitleLabel.text = subtitle;
}

- (void)initSubViews
{
    // 添加标题，即商户名
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, WIDTH(self)-4, 20)];
    self.titleLabel.font = [UIFont systemFontOfSize:10];
    self.titleLabel.textColor = Color_333333;
    self.titleLabel.text = @"title";
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.titleLabel];
    
    // 添加副标题，即商户地址
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(2 , 22, WIDTH(self)-4, 16)];
    self.subtitleLabel.font = [UIFont systemFontOfSize:10];
    self.subtitleLabel.textColor = Color_333333;
    self.subtitleLabel.text = @"subTitle";
    self.subtitleLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.subtitleLabel];
    
}
- (void)drawRect:(CGRect)rect {
     [self drawInContext:UIGraphicsGetCurrentContext()];
}
- (void)drawInContext:(CGContextRef)context {
    CGContextSetLineWidth(context, 1);
    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] colorWithAlphaComponent:1].CGColor);
    [self getDrawPath:context];
    CGContextFillPath(context);
    CGContextSetStrokeColorWithColor(context, Color_Common.CGColor);
    [self getDrawPath:context];
    CGContextStrokePath(context);
}

- (void)getDrawPath:(CGContextRef)context {
    CGRect rrect = self.bounds;
    CGFloat radius = 4.0;
    CGFloat minx = CGRectGetMinX(rrect),
    maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect),
    maxy = CGRectGetMaxY(rrect)-kArrorHeight;
    
    CGContextMoveToPoint(context, maxx, maxy+kArrorHeight);
    CGContextAddLineToPoint(context,maxx-kArrorHeight, maxy);
    CGContextAddArcToPoint(context, minx, maxy, minx, miny, radius);
    CGContextAddArcToPoint(context, minx, minx, maxx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, maxx, radius);
    CGContextAddLineToPoint(context,maxx, maxy+kArrorHeight);
    
    CGContextClosePath(context);
}
@end
