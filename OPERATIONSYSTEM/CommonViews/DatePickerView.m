//
//  DatePickerView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/6/26.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "DatePickerView.h"

@implementation DatePickerView
- (instancetype)initWithFrame:(CGRect)frame type:(NSUInteger)type {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Color_White;
        UIButton *backUpButton = [UIButton createButtonWithFrame:RECT(0, 5, 50, 37) Title:@"取消" Target:self Selector:@selector(backUp)];
        [backUpButton setTitleColor:Color_999999 forState:UIControlStateNormal];
        backUpButton.titleLabel.font = FONT(15);
        [self addSubview:backUpButton];
        
        UIButton *sureButton = [UIButton createButtonWithFrame:RECT(WIDTH(self)-50, 5, 50, 37) Title:@"确定" Target:self Selector:@selector(sureButtonClick)];
        [sureButton setTitleColor:Color_Common forState:UIControlStateNormal];
        sureButton.titleLabel.font = FONT(15);
        [self addSubview:sureButton];
        
        _titleLabel = [UILabel new];
        [_titleLabel rect:RECT(60, 13, ScreenWidth-120, 22) aligment:Center font:14 isBold:NO text:@"选择时间" textColor:Color_666666 superView:self];
        
        CALayer *layer = [CALayer layer];
        layer.frame = RECT(0, 47.5, WIDTH(self), .5);
        layer.backgroundColor = Color_Line.CGColor;
        [self.layer addSublayer:layer];
        
        [self calculate];
        
        _datePicker = [[UIPickerView alloc]initWithFrame:RECT(0, 48, WIDTH(self), HEIGHT(self)-SafeAreaBottomHeight-48)];
        //设置地区: zh-中国
        _datePicker.delegate = self;
        [_datePicker selectRow:self.yearArray.count-1 inComponent:0 animated:YES];
        [_datePicker selectRow:self.currentYearMonth.count-1 inComponent:1 animated:YES];
        [self addSubview:_datePicker];
    }
    return self;
}
- (void)calculate {
    self.yearArray = [[NSMutableArray alloc]initWithCapacity:0];
    self.monthArray = [[NSMutableArray alloc]initWithCapacity:0];
    self.currentYearMonth = [[NSMutableArray alloc]initWithCapacity:0];
    NSDate *date = [NSDate date];
    NSString *year = [CommonUtil getStringForDate:date format:@"yyyy"];
    if ([year integerValue]>=1970) {
        for (int i = 1970; i<=[year integerValue]; i++) {
            [self.yearArray addObject:[NSString stringWithFormat:@"%i年",i]];
        }
    }
    NSString *month = [CommonUtil getStringForDate:date format:@"MM"];
    _currentYear = [NSString stringWithFormat:@"%@年",year];
    if ([month integerValue]>0) {
        for (int i = 1; i<=[month integerValue]; i++) {
            [self.currentYearMonth addObject:[NSString stringWithFormat:@"%i月",i]];
        }
    }
    _theSame = YES;
    _yearIndex = self.yearArray.count-1;
    _monthIndex = self.currentYearMonth.count-1;
    [self.monthArray addObjectsFromArray:@[@"01月",@"02月",@"03月",@"04月",@"05月",@"06月",@"07月",@"08月",@"09月",@"10月",@"11月",@"12月"]];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component==0) {
        return self.yearArray.count;
    }else {
        if (_theSame) {
            return self.currentYearMonth.count;
        }else {
            return self.monthArray.count;
        }
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component==0) {
        _yearIndex = row;
        UILabel *view = (UILabel *)[pickerView viewForRow:row forComponent:0];
        if ([view.text isEqualToString:_currentYear]) {
            _theSame = YES;
            if (_monthIndex>self.currentYearMonth.count-1) {
                _monthIndex = self.currentYearMonth.count-1;
            }
        }else {
            _theSame = NO;
        }
        [pickerView reloadComponent:1];
     
    }else {
        _monthIndex = row;
    }
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    //设置文字的属性
    UILabel *genderLabel = [[UILabel alloc] init];
    genderLabel.textAlignment = NSTextAlignmentCenter;
    if (component == 0) {
        genderLabel.text = self.yearArray[row];
    } else if (component == 1) {
        genderLabel.text = self.monthArray[row];
    }
    return genderLabel;
}
- (void)backUp {
    [UIView animateWithDuration:.3 animations:^{
        self.frame = RECT(0, ScreenHeight, WIDTH(self), HEIGHT(self));
    }completion:^(BOOL finished) {
        if (_cancelBlock) {
            _cancelBlock();
        }
    }];
}
- (void)sureButtonClick {
    [UIView animateWithDuration:.3 animations:^{
        self.frame = RECT(0, ScreenHeight, WIDTH(self), HEIGHT(self));
    }completion:^(BOOL finished) {
        if (_operationBlock) {
            NSString *pickerYear = ((UILabel *)[self.datePicker viewForRow:_yearIndex forComponent:0]).text;
            NSString *pickerMonth = ((UILabel *)[self.datePicker viewForRow:_monthIndex forComponent:1]).text;
            _operationBlock([NSString stringWithFormat:@"%@%@",pickerYear,pickerMonth]);
        }
    }];
}
@end
