//
//  DatePickerView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/6/26.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DatePickerView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,strong)UIPickerView *datePicker;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,copy)NSString *currentYear;
@property (nonatomic,assign)BOOL theSame;//是否一致
@property (nonatomic,assign)NSInteger yearIndex,monthIndex;
@property (nonatomic,strong)NSMutableArray *yearArray,*monthArray,*currentYearMonth;
@property (nonatomic,strong) void (^cancelBlock)(void);
@property (nonatomic,strong)void (^operationBlock)(NSString *date);

@end

NS_ASSUME_NONNULL_END
