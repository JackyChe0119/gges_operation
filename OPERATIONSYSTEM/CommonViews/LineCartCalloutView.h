//
//  LineCartCalloutView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/7/10.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LineCartCalloutView : UIView
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, copy) NSString *title; //商户名
@property (nonatomic, copy) NSString *subtitle; //副标题
@property (nonatomic, copy) NSString *numString;//数据
@end

NS_ASSUME_NONNULL_END
