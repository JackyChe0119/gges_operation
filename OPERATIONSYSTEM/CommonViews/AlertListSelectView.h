//
//  AlertListSelectView.h
//  GEESYSTEM
//
//  Created by 车杰 on 2019/7/16.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AlertListSelectView : UIView
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *infoLabel;
@property (nonatomic,copy)NSString *title,*info;
@property (nonatomic,assign)NSInteger alertType;
@property (nonatomic,copy)NSString *lineId;
@property (nonatomic,assign)double leftWidth;
@property (nonatomic,strong)UIImageView *imageView;
@property (nonatomic,strong)UIView *baseView;
@property (nonatomic,strong) void (^operationBlock)(void);
@end

NS_ASSUME_NONNULL_END
