//
//  CustomPageCtrl.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/14.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomPageCtrl : UIView

@property (nonatomic, assign) NSInteger numberOfPages;                 /** < 指示器页数 */
@property (nonatomic, strong) UIColor* pageIndicatorColor;            /** < 指示器颜色 */
@property (nonatomic, strong) UIColor* currentPageIndicatorColor;     /** < 当前指示器颜色 */
@property (nonatomic, assign) NSInteger currentPage;                   /** < 当前页 */

@end

NS_ASSUME_NONNULL_END
