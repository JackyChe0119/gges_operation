//
//  CompanyModel.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/29.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CompanyModel : HYBaseModel
//addressDetail = "浙江省杭州市西湖区西斗门路3号";
//        businessScope = "环境咨询，软件开发";
//        companyName = "浙江飞源环境工程有限公司";
//        createTime = "2019-09-29 16:05:47";
//        creditCode = 123456789987654321;
//        division = "杭州市西湖区";
//        divisionName = "<null>";
//        eiaBatchNumber = 9771467141JKFJA812133;
//        emissionTradingDocuments = "972198131E91E-1E78112E";
//        id = 338198210365493248;
//        industryClass = "环保工程";
//        industryClassName = "<null>";
//        latitude = "120.117494";
//        legalRepresentative = "赵庆卒";
//        licenseNumber = JAAY93YFSFH232IFKJHSFJ;
//        linkman = "冯小飞";
//        linkmanPhone = 18205625519;
//        linkmanTwo = "周琳";
//        linkmanTwoPhone = 15672518273;
//        longitude = "30.292010";
//        mainProducts = "绿金智能环保系统";
//        registerMoney = 1000;
//        status = 1;
//        updateTime = "2019-09-29 16:05:47";
@property (nonatomic,copy)NSString *addressDetail,*businessScope,*companyName,*createTime,*creditCode,*division,*eiaBatchNumber,*emissionTradingDocuments,*Id,*industryClass,*latitude,*legalRepresentative,*licenseNumber,*linkman,*linkmanPhone,*linkmanTwo,*linkmanTwoPhone,*longitude,*mainProducts,*registerMoney,*status,*updateTime,*divisionName,*industryClassName;
@end

NS_ASSUME_NONNULL_END
