//
//  AreaSelectViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/8.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AreaSelectViewController : FYBaseViewController
@property (nonatomic,copy)NSString *titleStr;
@property (nonatomic,strong)NSMutableArray *listArray;
@property (nonatomic,copy)NSString *parentCode,*parentName;
@property (nonatomic,strong) void (^areaBlock)(NSString *name,NSString *code);
@end

NS_ASSUME_NONNULL_END
