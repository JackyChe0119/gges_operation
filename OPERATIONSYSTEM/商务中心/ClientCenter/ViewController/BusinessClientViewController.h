//
//  BusinessClientViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"
#import "CompanyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessClientViewController : FYBaseViewController
@property (nonatomic,assign)BOOL isSelect;//是否是选择状态
@property (nonatomic,strong) void (^selectBlock)(CompanyModel *model);
@end

NS_ASSUME_NONNULL_END
