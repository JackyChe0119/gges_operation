//
//  AreaSelectViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/8.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "AreaSelectViewController.h"

@interface AreaSelectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation AreaSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    [self setNavTitle:self.titleStr Color:Color_3D3A39];
    [self setLeftButtonImage:BACKUP];
    _tableView = [[UITableView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight) style:0];
       _tableView.delegate = self;
       _tableView.dataSource = self;
       _tableView.rowHeight = 50;
       [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0 )];
       [_tableView setSeparatorColor:Color_Line];
       _tableView.showsVerticalScrollIndicator = NO;
       _tableView.noDataContent = @"暂无区域信息";
       _tableView.marginTop = HEIGHT(_tableView)/2.0-80;
       [self.view addSubview:_tableView];
    if (self.listArray.count==0) {
         NSArray *array = [UserManager shareManager].areaJosn;
         for (NSDictionary *dic in array) {
          [self.listArray addObject:dic];
        }
    }else {
        NSMutableDictionary *all = [[NSMutableDictionary alloc]initWithCapacity:0];
        [all setValue:@"全部" forKey:@"name"];
        [all setValue:self.parentCode forKey:@"parentId"];
        [self.listArray insertObject:all atIndex:0];
    }
    _tableView.dataArray = _listArray;
    [self.tableView reloadWithData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:0 reuseIdentifier:identify];
    }
    NSDictionary *result = self.listArray[indexPath.row];
    cell.textLabel.text = result[@"name"];
    NSArray *children = result[@"children"];
    if (children&&children.count>0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *result = self.listArray[indexPath.row];
    NSArray *children = result[@"children"];
    if (children&&children.count>0) {
        AreaSelectViewController *vc = [AreaSelectViewController new];
        vc.titleStr = result[@"name"];
        vc.listArray = [children mutableCopy];
        vc.parentCode = result[@"code"];
        vc.parentName = result[@"name"];
        vc.areaBlock = self.areaBlock;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        if ([result[@"name"] isEqualToString:@"全部"]) {
            if (_areaBlock) {
                _areaBlock(self.parentName,self.parentCode);
            }
        }else {
            if (_areaBlock) {
                _areaBlock(result[@"name"],result[@"code"]);
            }
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, .5) color:Color_Line];
}
- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _listArray;
}
- (void)navgationLeftButtonClick {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
