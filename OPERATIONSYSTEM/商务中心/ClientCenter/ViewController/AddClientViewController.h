//
//  AddClientViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddClientViewController : FYBaseViewController
@property (nonatomic,assign)BOOL isAdd;//是否是企业客户
@property (nonatomic,copy)NSString *companyId;
@property (nonatomic,strong) void (^addBlock)(void);
@end

NS_ASSUME_NONNULL_END
