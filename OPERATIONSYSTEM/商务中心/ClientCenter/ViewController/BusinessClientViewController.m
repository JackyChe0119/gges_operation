//
//  BusinessClientViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "BusinessClientViewController.h"
#import "MineViewController.h"
#import "AlertListSelectView.h"
#import "SearchView.h"
#import "TaskCenterListCell.h"
#import "TaskSenderViewController.h"
#import "AddClientViewController.h"
#import "AreaSelectViewController.h"
static NSString * const TASK_LIST = @"TaskCenterListCell";

@interface BusinessClientViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)AlertListSelectView *areaView;
@property (nonatomic,strong)SearchView *searchView;
@property (nonatomic,assign)BOOL isCompany;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *listArray;
@property (nonatomic,assign)NSInteger operationType;
@end

@implementation BusinessClientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    self.isCompany = YES;
        
    [self setRightButtonImage:@"icon_add"];
    
    [self addLineViewWithFrame:RECT(0, NavHeight, ScreenWidth, 4) Color:Color_BG supView:self.view];
    
    _searchView = [[SearchView alloc]initWithFrame:RECT(ScreenWidth/2.0, NavHeight+10, ScreenWidth/2.0, 40) type:1];
    _searchView.placeHold = @"搜索内容";
    _searchView.searchBlock = ^(NSString * _Nonnull search) {
        [weakSelf.tableView resetPageNum:1];
        [weakSelf requestCompanyList:1 show:YES];
    };
    [self.view addSubview:_searchView];
    
    _areaView = [[AlertListSelectView alloc]initWithFrame:RECT(0,NavHeight+8, ScreenWidth/2.0, 44)];
    _areaView.title = @"区  域：";
    _areaView.info = @"全部";
    _areaView.operationBlock = ^{
        NSArray  *array = [UserManager shareManager].areaJosn;
        if (!array) {
            [weakSelf requestDivisionCode:YES];
        }else {
            if (array.count==0) {
                [weakSelf showHUDMessage:@"您当前账号暂无区域可选"];
            }else {
                [weakSelf showCityPickerView];
            }
        }
    };
    [self.view addSubview:_areaView];
    
    [self addLineViewWithFrame:RECT(0, BOTTOM(_areaView)+4, ScreenWidth, 4) Color:Color_BG supView:self.view];
        
    _tableView = [[UITableView alloc]initWithFrame:RECT(0, BOTTOM(_areaView)+8, ScreenWidth, ScreenHeight-BOTTOM(_areaView)-8-TabBarHeight) style:0];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 64;
    [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0 )];
    [_tableView setSeparatorColor:Color_Line];
    [_tableView registerNib:[UINib nibWithNibName:@"TaskCenterListCell" bundle:nil] forCellReuseIdentifier:TASK_LIST];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.noDataContent = @"暂无企业客户";
    _tableView.marginTop = HEIGHT(_tableView)/2.0-80;
    [self.view addSubview:_tableView];
    [_tableView addHeaderRefreshWithAutomaticallyRefresh:YES refreshBlock:^(NSInteger pageIndex) {
        [weakSelf requestCompanyList:pageIndex show:NO];
    }];
    [_tableView addFootLoadMoreWithAutomaticallyLoad:YES loadMoreBlock:^(NSInteger pageIndex) {
        if (weakSelf.noMoreData) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf showHUDMessage:@"暂无更多数据了"];
                [weakSelf.tableView endFootLoadMore];
            });
            return;
        }
        [weakSelf requestCompanyList:pageIndex show:NO];
    }];
    [self.view addSubview:_tableView];
    
    if (self.isSelect) {
        [self setLeftButtonImage:BACKUP];
        _tableView.frame = RECT(0, BOTTOM(_areaView)+8, ScreenWidth, ScreenHeight-BOTTOM(_areaView)-8);
        [self setNavTitle:@"选择企业客户" Color:Color_3D3A39];
    }else {
        [self setLeftButtonTitle:@"商"];
        [self setNavTitle:@"企业客户" Color:Color_3D3A39];
    }
    [self requestCompanyList:1 show:YES];
    
    NSArray  *array = [UserManager shareManager].areaJosn;
    if (!array.count) {
        dispatch_queue_t queue = dispatch_queue_create("loadAreaTree", DISPATCH_QUEUE_CONCURRENT);
        dispatch_async(queue, ^{
            [self requestDivisionCode:NO];
        });
    }
    [self requestUpdate];
}
#pragma mark ---------->>> method
- (void)change:(UISegmentedControl *)seg {
        self.isCompany = !seg.selectedSegmentIndex;
}
- (void)navgationLeftButtonClick {
    if (self.isSelect) {
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        MineViewController *vc = [MineViewController new];
        [self pushWithViewController:vc hidden:YES];
    }
}
- (void)navgationRightButtonClick {
    AddClientViewController *addvc = [AddClientViewController new];
    addvc.isAdd = YES;
    addvc.addBlock = ^{
        [_tableView resetPageNum:1];
        [self requestCompanyList:1 show:YES];
    };
    [self pushWithViewController:addvc hidden:YES];
}
#pragma mark ---------->>> delegate
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, .5) color:Color_Line];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskCenterListCell *cell = [tableView dequeueReusableCellWithIdentifier:TASK_LIST];
    CompanyModel *model = self.listArray[indexPath.row];
    cell.nameLabel.text = model.companyName;
    cell.areaLabel.text = [NSString stringWithFormat:@"区域:%@",model.divisionName];
    cell.typeLabel.text = [NSString stringWithFormat:@"联系人:%@",model.linkman];
    cell.legenLabel.text = [NSString stringWithFormat:@"联系电话:%@",model.linkmanPhone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isSelect) {
        if (_selectBlock) {
            _selectBlock(self.listArray[indexPath.row]);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        CompanyModel *model = self.listArray[indexPath.row];
        AddClientViewController *addvc = [AddClientViewController new];
        addvc.companyId = model.Id;
        [self pushWithViewController:addvc hidden:YES];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    //娄凤翔说都可以删
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}
//修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}
//点击删除
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //在这里实现删除操作
    [AlertViewUtil showSelectAlertViewWithVC:self Title:@"删除该企业客户" Message:@"删除后该企业将无法查看，是否确定删除？" LeftTitle:@"确定" RightTitle:@"取消" callBack:^(NSInteger type) {
        if (type==1) {
            CompanyModel *model = self.listArray[indexPath.row];
            NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
            [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/business/company/%@",model.Id] method:DELETE params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([responseMessage isRequestSuccessful]) {
                        [self showHUDMessage:@"删除成功"];
                        [self.listArray removeObjectAtIndex:indexPath.row];
                        _tableView.dataArray = _listArray;
                        [_tableView reloadWithData];
                    }else {
                        [self showHUDMessage:responseMessage.errorMessage];
                    }
                });
            }];
        }
    }];
}
#pragma mark ---------->>> request
- (void)requestCompanyList:(NSInteger)pageNum show:(BOOL)show{
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[NSNumber numberWithInteger:pageNum] forKey:@"pageNum"];
    [params setValue:[NSNumber numberWithInteger:20] forKey:@"pageSize"];
    if (_searchView.searchTextField.text.length>0) {
        [params setValue:_searchView.searchTextField.text forKey:@"companyName"];
    }
    if (![_areaView.lineId isEqualToString:@"0"]) {
        [params setValue:_areaView.lineId forKey:@"division"];
    }
    [params setValue:[UserManager shareManager].Id forKey:@"loginUserId"];
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/business/company" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }else {
                [_tableView endLoading];
            }
            if ([responseMessage isRequestSuccessful]) {
                if (pageNum==1) {
                    [self.listArray removeAllObjects];
                }
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                [_listArray addObjectsFromArray:[CompanyModel mj_objectArrayWithKeyValuesArray:array]];
                if (array.count==0) {
                   [_tableView resetPageNum:pageNum];
                    self.noMoreData = YES;
                }else {
                    self.noMoreData = NO;
                }
                _tableView.dataArray = _listArray;
                [_tableView reloadWithData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestDivisionCode:(BOOL)show {
//    商务中心  001003001
//    实施中心  001003002
//    运维中心  001003003
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[UserManager shareManager].deptCode forKey:@"deptCode"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/division/divisionTreeByDeptCode" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }
            if ([responseMessage isRequestSuccessful]) {
                 NSArray *data = responseMessage.data[@"data"];
                             if (data.count>0) {
                                      NSArray *treeArray = data;
                                      NSMutableArray *totalArray = [[NSMutableArray alloc]initWithCapacity:0];
                                      NSMutableDictionary *all = [[NSMutableDictionary alloc]initWithCapacity:0];
                                      [all setValue:@"全部" forKey:@"name"];
                                      [all setValue:@"0" forKey:@"parentId"];
                                      [totalArray addObject:all];
                                      [totalArray addObjectsFromArray:treeArray];
                                      [UserManager shareManager].areaJosn = (NSArray *)totalArray;
                                      if (show) {
                                         [self showCityPickerView];
                                      }
                             }else{
                                 if (show) {
                                     [self showHUDMessage:@"您当前账号暂无区域可选"];
                                 }
                             }
            }else {
                if (show) {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            }
        });
    }];
}
#pragma mark ---------->>> 懒加载
- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _listArray;
}
- (void)showCityPickerView {
    __weak typeof(self)weakSelf = self;
    AreaSelectViewController *vc = [[AreaSelectViewController alloc]init];
    vc.titleStr = @"选择区域";
    vc.parentName = @"全部";
    vc.parentCode = @"0";
    vc.areaBlock = ^(NSString * _Nonnull name, NSString * _Nonnull code) {
        weakSelf.areaView.info = name;
        weakSelf.areaView.lineId= code;
        [weakSelf.tableView resetPageNum:1];
        [weakSelf requestCompanyList:1 show:YES];
    };
    [self presentWithViewController:vc animated:YES];
}
- (void)requestUpdate {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *localVersion = [CommonUtil getInfoWithKey:@"localVersion"];
    if ([version isEqualToString:localVersion]&&localVersion) {
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/mobile/update/iosdev" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *versionInfo = responseMessage.data[@"data"];
                if (!versionInfo||![CommonUtil feiKong:version]) {
                    return ;
                }
                NSString *requestVersion = versionInfo[@"apkVersion"]; //请求的版本
                if (![version isEqualToString:requestVersion]) { //版本是否一致 不一致 执行是否更新操作
                    [CommonUtil setInfo:requestVersion forKey:@"localVersion"];
                    if ([versionInfo[@"prop"] integerValue]==1) { //有需要弹出
                        if ([versionInfo[@"forceUpdate"] integerValue]==1) { //强制更新
                            BOOL TemporarilyNotUpdate = [[NSUserDefaults standardUserDefaults] boolForKey:@"TemporarilyNotUpdate"];
                            if (TemporarilyNotUpdate) { //暂不更新是否点击过
                                return ;
                            }
                            [self showAlert:0];
                        }else { //不强制更新
                            [UserManager shareManager].isForce = YES;
                            [self showAlert:1];
                        }
                    }else {
                        //不需要弹出  不用处理
                    }
                }
            }else {
                if ([UserManager shareManager].isForce) {
                    [self showAlert:1];
                }
            }
        });
    }];
}
- (void)showAlert:(NSInteger)type {
    if (type==1) {
        [AlertViewUtil showCancelAlertViewWithVC:MainWindow.rootViewController Title:@"温馨提示！" Message:@"版本已更新，如不更新，当前版本暂时无法使用" LeftTitle:@"更新" callbackBlock:^{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TemporarilyNotUpdate"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1484906853"]];
        }];
    }else {
        [AlertViewUtil showSelectAlertViewWithVC:MainWindow.rootViewController Title:@"温馨提示！" Message:@"版本已更新，为了您的使用体验请前往更新" LeftTitle:@"暂不更新" RightTitle:@"去更新" callBack:^(NSInteger type) {
            if (type==2) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TemporarilyNotUpdate"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1484906853"]];
            }else {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TemporarilyNotUpdate"]; //暂不更新
            }
        }];
    }
}
@end
