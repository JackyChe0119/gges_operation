//
//  AddClientViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "AddClientViewController.h"
#import "TaskCompanyInfoView.h"
#import "TaskMarkView.h"
#import "LocationView.h"
#import "GovernmentInfoView.h"
#import "CompanyModel.h"
#import "DivisionSelectViewController.h"
@interface AddClientViewController ()
@property (nonatomic,strong)TaskCompanyInfoView *companyInfoView;
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)TaskMarkView *jyfwView;
@property (nonatomic,strong)TaskMarkView *zycpView;
@property (nonatomic,strong)LocationView *locationView;
@property (nonatomic,strong)GovernmentInfoView *governmentView;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UIButton *commitButton;
@property (nonatomic,strong)CompanyModel *model;
@property (nonatomic,strong)NSMutableArray *industryArray;
@end

@implementation AddClientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftButtonImage:BACKUP];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [self.view addSubview:_baseScrollView];
    
    _companyInfoView = [[TaskCompanyInfoView alloc]initWithFrame:RECT(0, 10, ScreenWidth, 500) type:3];
    _companyInfoView.operationBlock = ^(NSInteger type) {
        if (type==0) {//选择企业
            
        }else if (type==3) {//校验企业名称
            [weakSelf checkCompany:weakSelf.companyInfoView.qymcIView.inputTextfield.text];
        }
    };
    _companyInfoView.xzqhView.operationBlock = ^{
        [weakSelf requestDivisionCode:YES];
    };
    _companyInfoView.hylbView.operationBlock = ^{
        [weakSelf requestCategory];
    };
    [_baseScrollView addSubview:_companyInfoView];
    
    _jyfwView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_companyInfoView), ScreenWidth, 150)];
    _jyfwView.titleLabel.text = @"经营范围：";
    _jyfwView.tipStr = @"请输入企业的经营范围";
    [_baseScrollView addSubview:_jyfwView];
    
    _zycpView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_jyfwView), ScreenWidth, 150)];
    _zycpView.titleLabel.text = @"主要产品：";
    _zycpView.tipStr = @"请输入企业的主要产品";
    [_baseScrollView addSubview:_zycpView];
    
    _locationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(_zycpView), ScreenWidth, 340)];
    [_baseScrollView addSubview:_locationView];
    
    _commitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0-80, BOTTOM(_locationView)+30,160, 40) target:self action:@selector(commitButtonClick) title:@"保    存" color:Color_White font:17 type:2];
     [_commitButton addRoundedCornersWithRadius:5];
    [_baseScrollView addSubview:_commitButton];
    
    [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(_commitButton)+20)];
    
    if (self.isAdd) {
        [self setNavTitle:@"新增企业客户" Color:Color_3D3A39];
        _locationView.needlocation = YES;
    }else {
        [self setNavTitle:@"企业客户详情" Color:Color_3D3A39];
        [self requestCompanyDetail];
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightButton setTitle:@"编辑" forState:UIControlStateNormal];
        [_rightButton setTitle:@"取消" forState:UIControlStateSelected];
        _rightButton.frame = CGRectMake(ScreenWidth-50, StatusBarHeight, 45, 44);
        [_rightButton setTitleColor:color_Red forState:UIControlStateSelected];
        [_rightButton setTitleColor:Color_Common forState:UIControlStateNormal];
        _rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_rightButton addTarget:self action:@selector(navgationRightButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.customNavView addSubview:_rightButton];
        _commitButton.hidden  = YES;
        _locationView.needlocation = NO;
        [self setEditStatus:NO];
    }
}
- (void)navgationRightButtonClick {
    _rightButton.selected = !_rightButton.selected;
    _commitButton.hidden = !_rightButton.selected;
    if (_rightButton.selected) {
        _locationView.needlocation = YES;
        [_locationView removePoint];
    }else {
        _locationView.needlocation = NO;
        if (_model) { //如果存在 恢复原样
            [self layoutUIWithModel];
        }
    }
    [self setEditStatus:_rightButton.selected];
}
- (void)setEditStatus:(BOOL)select {
    _companyInfoView.qymcIView.inputTextfield.enabled = select;
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.fzView.inputTextfield.enabled = select;
    _companyInfoView.lxrView.inputTextfield.enabled = select;
    _companyInfoView.sjhmView.inputTextfield.enabled = select;
    _companyInfoView.bylxrView.inputTextfield.enabled = select;
    _companyInfoView.bysjhmView.inputTextfield.enabled = select;
    _companyInfoView.xzqhView.baseView.userInteractionEnabled = select;
    _companyInfoView.hylbView.baseView.userInteractionEnabled = select;
    _companyInfoView.zczjView.inputTextfield.enabled = select;
    _companyInfoView.pwxkzView.inputTextfield.enabled = select;
    _companyInfoView.pwjyhView.inputTextfield.enabled = select;
    _companyInfoView.hppfView.inputTextfield.enabled = select;
    _jyfwView.textView.editable = select;
    _zycpView.textView.editable = select;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    _locationView.addressView.editable = select;
}
- (void)layoutUIWithModel {
    _companyInfoView.qymcIView.inputTextfield.text = _model.companyName;
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.tyxydmView.inputTextfield.text = _model.creditCode;
    _companyInfoView.fzView.inputTextfield.text = _model.legalRepresentative;
    _companyInfoView.lxrView.inputTextfield.text = _model.linkman;
    _companyInfoView.sjhmView.inputTextfield.text = _model.linkmanPhone;
    _companyInfoView.bylxrView.inputTextfield.text = _model.linkmanTwo;
    _companyInfoView.bysjhmView.inputTextfield.text = _model.linkmanTwoPhone;
    _companyInfoView.xzqhView.info = _model.divisionName;
    _companyInfoView.xzqhView.lineId = _model.division;
    _companyInfoView.hylbView.info = _model.industryClassName;
    _companyInfoView.hylbView.lineId = _model.industryClass;
    _companyInfoView.zczjView.inputTextfield.text = _model.registerMoney;
    _companyInfoView.pwxkzView.inputTextfield.text = _model.licenseNumber;
    _companyInfoView.pwjyhView.inputTextfield.text = _model.emissionTradingDocuments;
    _companyInfoView.hppfView.inputTextfield.text = _model.eiaBatchNumber;
    _jyfwView.textView.text = _model.businessScope;
    _zycpView.textView.text = _model.mainProducts;
    _locationView.jdView.inputTextfield.text = _model.longitude;
    _locationView.wdView.inputTextfield.text = _model.latitude;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([_model.latitude doubleValue], [_model.longitude doubleValue]);
    [_locationView.mapView setCenterCoordinate:location animated:YES];
    [_locationView addPoint:location title:_model.companyName];
    _locationView.addressView.text = _model.addressDetail;
}
#pragma mark ---------->>> private
- (void)commitButtonClick {
    if (_companyInfoView.qymcIView.inputTextfield.text.length==0) {[self showHUDMessage:@"请输入企业名称"];return;}
    if (_companyInfoView.lxrView.inputTextfield.text.length==0) {[self showHUDMessage:@"请输入联系人姓名"];return;}
    if ( _companyInfoView.xzqhView.info.length==0) {[self showHUDMessage:@"请选择行政区划"];return;}
    if ( _companyInfoView.hylbView.info.length==0) {[self showHUDMessage:@"请选择行业类别"];return;}
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:_companyInfoView.qymcIView.inputTextfield.text forKey:@"companyName"];
    if (_companyInfoView.tyxydmView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.tyxydmView.inputTextfield.text forKey:@"creditCode"];
    }
    if (_companyInfoView.fzView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.fzView.inputTextfield.text forKey:@"legalRepresentative"];
    }
    [params setValue:_companyInfoView.lxrView.inputTextfield.text forKey:@"linkman"];
    [params setValue:_companyInfoView.sjhmView.inputTextfield.text forKey:@"linkmanPhone"];
    if (_companyInfoView.bylxrView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.bylxrView.inputTextfield.text forKey:@"linkmanTwo"];
    }
    if (_companyInfoView.bysjhmView.inputTextfield.text .length>0) {
         [params setValue:_companyInfoView.bysjhmView.inputTextfield.text  forKey:@"linkmanTwoPhone"];
    }
    [params setValue:_companyInfoView.xzqhView.info forKey:@"divisionName"];
    [params setValue:_companyInfoView.xzqhView.lineId forKey:@"division"];
    [params setValue:_companyInfoView.hylbView.info  forKey:@"industryClassName"];
    [params setValue:_companyInfoView.hylbView.lineId  forKey:@"industryClass"];
    if (_companyInfoView.zczjView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.zczjView.inputTextfield.text forKey:@"registerMoney"];
    }
    if (_companyInfoView.pwxkzView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.pwxkzView.inputTextfield.text forKey:@"licenseNumber"];
    }
    if (_companyInfoView.pwjyhView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.pwjyhView.inputTextfield.text forKey:@"emissionTradingDocuments"];
    }
    if (_companyInfoView.hppfView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.hppfView.inputTextfield.text forKey:@"eiaBatchNumber"];
    }
    if (![_jyfwView.textView.text isEqualToString:@"请输入企业的经营范围"]) {
        [params setValue:_jyfwView.textView.text forKey:@"businessScope"];
    }
    if (![_zycpView.textView.text isEqualToString:@"请输入企业的主要产品"]) {
        [params setValue:_zycpView.textView.text forKey:@"mainProducts"];
    }
    [params setValue:_locationView.jdView.inputTextfield.text forKey:@"longitude"];
    [params setValue:_locationView.wdView.inputTextfield.text forKey:@"latitude"];
    [params setValue:_locationView.addressView.text forKey:@"addressDetail"];
    [params setValue:[NSNumber numberWithInteger:1] forKey:@"status"];
    NSString *url;
    if (self.isAdd) {
        url = @"devops/api/v1/business/company";
    }else {
        url = [NSString stringWithFormat:@"devops/api/v1/business/company/%@",self.companyId];
    }
    [self showProgressHud];
    [RequestTool requestDataWithUrlSuffix:url method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                if (self.isAdd) {
                    [self showHUDMessage:@"新增成功"];
                }else {
                    [self showHUDMessage:@"编辑成功"];
                }
                if (_addBlock) {
                    _addBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestCompanyDetail {
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/business/company/%@",self.companyId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *result = responseMessage.data[@"data"];
                _model = [CompanyModel mj_objectWithKeyValues:result];
                [self layoutUIWithModel];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
                [self.navigationController popViewControllerAnimated:YES];
            }
        });
    }];
}
- (void)checkCompany:(NSString *)companyName {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:companyName forKey:@"companyName"];
    if (!self.isAdd) {
        [params setValue:self.companyId forKey:@"id"];
    }
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/business/company/check/name" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *result = responseMessage.responseData[@"body"];
                NSInteger type = [result[@"result"] integerValue];
                if (type==0) {
                    [self showHUDMessage:@"该企业已存在"];
                    _companyInfoView.qymcIView.inputTextfield.text = @"";
                }else {
                    NSLog(@"企业不存在，可以添加");
                }
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestDivisionCode:(BOOL)show {
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[UserManager shareManager].deptCode forKey:@"deptCode"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/division/divisionTreeByDeptCode" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }
            if ([responseMessage isRequestSuccessful]) {
                 NSArray *data = responseMessage.data[@"data"];
                             if (data.count>0) {
                                      NSArray *treeArray = data;
                                      NSMutableArray *totalArray = [[NSMutableArray alloc]initWithCapacity:0];
                                      [totalArray addObjectsFromArray:treeArray];
                                      if (show) {
                                          [self showCityPickerView:totalArray];
                                      }
                             }else{
                                 if (show) {
                                     [self showHUDMessage:@"您当前账号暂无区域可选"];
                                 }
                             }
            }else {
                if (show) {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            }
        });
    }];
}
- (void)showCityPickerView:(NSArray *)totalArray {
    __weak typeof(self)weakSelf = self;
    DivisionSelectViewController *vc = [[DivisionSelectViewController alloc]init];
    vc.titleStr = @"选择区域";
    vc.listArray = [totalArray mutableCopy];
    vc.areaBlock = ^(NSString * _Nonnull name, NSString * _Nonnull code) {
        weakSelf.companyInfoView.xzqhView.info = name;
        weakSelf.companyInfoView.xzqhView.lineId = code;
    };
    [self presentWithViewController:vc animated:YES];
}
- (void)requestCategory {
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/code/category/010201" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    [self.industryArray addObject:dic[@"code"]];
                }];
                [self showActionSheetWithArray:nameArray title:@"行业类别"];

            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (NSMutableArray *)industryArray {
    if (!_industryArray) {
        _industryArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _industryArray;
}
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    _companyInfoView.hylbView.info = title;
   _companyInfoView.hylbView.lineId = self.industryArray[index];

}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}
@end
