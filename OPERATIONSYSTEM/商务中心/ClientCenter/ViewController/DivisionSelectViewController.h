//
//  DivisionSelectViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/10.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DivisionSelectViewController : FYBaseViewController
@property (nonatomic,copy)NSString *titleStr;
@property (nonatomic,strong)NSMutableArray *listArray;
@property (nonatomic,copy)NSString *parentName,*parentId;
@property (nonatomic,strong) void (^areaBlock)(NSString *name,NSString *code);
@end

NS_ASSUME_NONNULL_END
