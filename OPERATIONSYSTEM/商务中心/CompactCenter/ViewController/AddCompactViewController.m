//
//  AddCompactViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "AddCompactViewController.h"
#import "TaskInfoView.h"
#import "TaskServiceView.h"
#import "TaskCompanyInfoView.h"
#import "LocationView.h"
#import "BusinessClientViewController.h"
#import "ChooseDatePickerView.h"
#import "CZHDatePickerView.h"

@interface AddCompactViewController ()<ChooseDatePickerViewDelegate>
{
    UIButton *saveButton;
    UIButton *commitButton;
}
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)TaskInfoView *taskView;
@property (nonatomic,strong)TaskServiceView *serviceView;
@property (nonatomic,strong)TaskCompanyInfoView *companyInfoView;
@property (nonatomic,strong)LocationView *locationView;
@property (nonatomic,assign)NSInteger operationType;
@property (nonatomic,strong)CompanyModel *currentModel;
@property (nonatomic,assign)BOOL isCommit;
@property (nonatomic,copy)NSString *divisionLeader,*maintainDivisionLeader;
@property (nonatomic,strong)UIDatePicker *datePicker;
@property (nonatomic,strong)UIView *baseView;
@end

@implementation AddCompactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftButtonImage:BACKUP];
    [self layoutUI];
}

#pragma mark ---------->>> 布局视图
- (void)layoutUI {
       __weak typeof(self)weakSelf = self;
       _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
       _baseScrollView.showsVerticalScrollIndicator = NO;
       _baseScrollView.backgroundColor = Color_White;
        [self.view addSubview:_baseScrollView];
 
        UILabel  *statusLabel = [UILabel new];
        [statusLabel rect:RECT(10, 10, ScreenWidth/2.0, 20) aligment:Left font:13 isBold:NO text:@"*合同状态：未提交" textColor:Color_333333 superView:_baseScrollView];
    
        _taskView = [[TaskInfoView alloc]initWithFrame:RECT(0, BOTTOM(statusLabel)+5, ScreenWidth, 240)];
        _taskView.principal = [NSString stringWithFormat:@"*商务负责人：%@",[UserManager shareManager].name];
        _taskView.principalId = [CommonUtil fixNullText:[UserManager shareManager].Id];
       _taskView.principalName = [CommonUtil fixNullText:[UserManager shareManager].name];
        _taskView.implementView.title = @"*回款状态：";
        _taskView.implementView.leftWidth = 70;
        _taskView.implementView.info = @"未回款";
        _taskView.serviceTimeView.info = @"3";
        _taskView.operationBlock = ^(NSInteger type) {
            weakSelf.operationType = type;
            if (type==0) {//回款状态
                [weakSelf showActionSheetWithArray:@[@"未回款",@"回款中",@"已回款"] title:@"回款状态"];
            }else if (type==1) {//服务年限
                [weakSelf showActionSheetWithArray:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"] title:@"服务年限"];
            }else if (type==2) {//服务起始时间
                  [CZHDatePickerView sharePickerViewWithCurrentDate:weakSelf.taskView.timeQuanTum.fromTimeView.info DateBlock:^(NSString *dateString) {
                      weakSelf.taskView.timeQuanTum.fromTimeView.info = dateString;
                      [weakSelf.taskView.timeQuanTum setToTime:dateString];
                  }];
            }
         };
        [_baseScrollView addSubview:_taskView];
           
        _serviceView = [[TaskServiceView alloc]initWithFrame:RECT(10, BOTTOM(_taskView)+10, ScreenWidth-20, 300)];
        [_serviceView addMarView];
        [_baseScrollView addSubview:_serviceView];
           
        _companyInfoView = [[TaskCompanyInfoView alloc]initWithFrame:RECT(0, BOTTOM(_serviceView)+20, ScreenWidth, 500) type:1];
        _companyInfoView.operationBlock = ^(NSInteger type) {
            if (type==0) { //选择企业
                BusinessClientViewController *vc = [BusinessClientViewController new];
                vc.isSelect = YES;
                vc.selectBlock = ^(CompanyModel * model) {
                    weakSelf.currentModel = model;
                    [weakSelf.locationView removePoint];
                    [weakSelf layoutUIWithModel:model];
                    [weakSelf request:model.division show:YES];
                };
                [weakSelf pushWithViewController:vc hidden:YES];
            }
         };
        [self setEditStatus:NO];
        [_baseScrollView addSubview:_companyInfoView];
           
        _locationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(_companyInfoView), ScreenWidth, 340)];
        [_baseScrollView addSubview:_locationView];
           
         saveButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0-160, BOTTOM(_locationView)+30,150, 40) target:self action:@selector(saveButtonClick) title:@"暂  存" color:Color_White font:17 type:2];
         [saveButton addRoundedCornersWithRadius:5];
         [_baseScrollView addSubview:saveButton];

         commitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0+10, BOTTOM(_locationView)+30,150, 40) target:self action:@selector(commitButtonClick) title:@"提  交" color:Color_White font:17 type:2];
        [commitButton addRoundedCornersWithRadius:5];
           
        [_baseScrollView addSubview:commitButton];
           
        [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(commitButton)+20)];
    if (self.isAdd) {
         [self setNavTitle:@"新增合同" Color:Color_3D3A39];
    }else {
        [self setNavTitle:@"合同详情" Color:Color_3D3A39];
        if (_model.status==1) {
            statusLabel.text = @"*合同状态：已提交";
            if ([UserManager shareManager].roleGroup!=1) {//运维管理员负责人可以编辑删除
                saveButton.hidden = YES;
                commitButton.hidden = YES;
            }
            [self requestCompactDeatil:NO];
        }else {
            statusLabel.text = @"*合同状态：未提交";
            [self requestCompactDeatil:YES];
        }
    }
}
#pragma mark ---------->>> requestCompactDeatil
- (void)requestCompactDeatil:(BOOL)nextRequest {
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/contract/contract/%@",_model.Id] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *result = responseMessage.data[@"data"];
                [self relayoutWithDeatil:result];
                if (nextRequest) {
                    [self request:result[@"division"] show:NO];
                }else {
                    [self hiddenProgressHud];
                }
            }else {
                [self hiddenProgressHud];
                [self showHUDMessage:responseMessage.errorMessage];
                [self.navigationController popViewControllerAnimated:YES];
            }
        });
    }];
}
- (void)request:(NSString *)divisionId show:(BOOL)show{
            if (show) {
                 [self showProgressHud];
            }
            NSMutableDictionary *params2 = [[NSMutableDictionary alloc]initWithCapacity:0];
            [params2 setValue:@"001003002" forKey:@"deptCode"];
            [params2 setValue:divisionId forKey:@"divisionId"];
            [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/operation/user/chargeUser" method:GET params:params2 callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                       if ([responseMessage isRequestSuccessful]) {
                            NSDictionary *dic = responseMessage.data[@"data"];
                            if (!dic||[dic isKindOfClass:[NSNull class]]) {
                                [self hiddenProgressHud];
                                [AlertViewUtil showCancelAlertViewWithVC:self Title:@"温馨提示！" Message:@"该区域暂无负责人，可能无法新建合同，如有需要请联系管理员" LeftTitle:@"好的" callbackBlock:^{
                                        [self.navigationController popViewControllerAnimated:YES];
                                }];
                            }else {
                                _divisionLeader = dic[@"id"];
                                [self requestmaintainDivisionLeader:divisionId];
                            }
                        }else {
                            [self hiddenProgressHud];
                            [self showHUDMessage:responseMessage.errorMessage];
                        }
                });
    }];
}
- (void)requestmaintainDivisionLeader:(NSString *)divisionId {
            NSMutableDictionary *params2 = [[NSMutableDictionary alloc]initWithCapacity:0];
            [params2 setValue:@"001003003" forKey:@"deptCode"];
            [params2 setValue:divisionId forKey:@"divisionId"];
            [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/operation/user/chargeUser" method:GET params:params2 callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                        [self hiddenProgressHud];
                        dispatch_async(dispatch_get_main_queue(), ^{
                    if ([responseMessage isRequestSuccessful]) {
                            NSDictionary *dic = responseMessage.data[@"data"];
                            if (!dic||[dic isKindOfClass:[NSNull class]]) {
                                [AlertViewUtil showCancelAlertViewWithVC:self Title:@"温馨提示！" Message:@"该区域暂无运维负责人，可能无法新建合同，如有需要请联系管理员" LeftTitle:@"好的" callbackBlock:^{
                                    [self.navigationController popViewControllerAnimated:YES];
                                }];
                            }else {
                                _maintainDivisionLeader = dic[@"id"];
                            }
                    }else {
                            [self showHUDMessage:responseMessage.errorMessage];
                    }
            });
    }];
}
#pragma mark ---------->>> 弹出视图
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    if (_operationType==0) {//回款状态
        _taskView.implementView.info = title;
        if (_isCommit) {
            [self changeCompactStatus];//提交完成状态 修改回款状态 请求保存
        }
    }else if(_operationType==1){//服务年限
        _taskView.serviceTimeView.info = title;
        _taskView.timeQuanTum.serviceYear = [title integerValue];
    }
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}

- (void)layoutUIWithModel:(CompanyModel *)model {
    _companyInfoView.qymcView.info = model.companyName;
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.tyxydmView.inputTextfield.text = model.creditCode;
    _companyInfoView.fzView.inputTextfield.text = model.legalRepresentative;
    _companyInfoView.lxrView.inputTextfield.text = model.linkman;
    _companyInfoView.sjhmView.inputTextfield.text = model.linkmanPhone;
    if (![model.linkmanTwo isKindOfClass:[NSNull class]]) {
        _companyInfoView.bylxrView.inputTextfield.text = model.linkmanTwo;
    }
    if (![model.linkmanTwoPhone isKindOfClass:[NSNull class]]) {
        _companyInfoView.bysjhmView.inputTextfield.text = model.linkmanTwoPhone;
    }
    _companyInfoView.xzqhView.info = model.divisionName;
    _companyInfoView.xzqhView.lineId = model.division;
    _companyInfoView.hylbView.lineId = model.industryClass;
    _companyInfoView.hylbView.info = model.industryClassName;
    if (![model.licenseNumber isKindOfClass:[NSNull class]]) {
         _companyInfoView.pwxkzView.inputTextfield.text = model.licenseNumber;
     }
    if (![ model.emissionTradingDocuments isKindOfClass:[NSNull class]]) {
         _companyInfoView.pwjyhView.inputTextfield.text = model.emissionTradingDocuments;
     }
    if (![model.eiaBatchNumber isKindOfClass:[NSNull class]]) {
         _companyInfoView.hppfView.inputTextfield.text = model.eiaBatchNumber;
     }
    _locationView.jdView.inputTextfield.text = model.longitude;
    _locationView.wdView.inputTextfield.text = model.latitude;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    [_locationView setCanEdit:NO];
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([model.latitude doubleValue], [model.longitude doubleValue]);
    [_locationView.mapView setCenterCoordinate:location animated:YES];
    [_locationView addPoint:location title:model.companyName];
    _locationView.addressView.text = model.addressDetail;
}
- (void)setEditStatus:(BOOL)select {
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.fzView.inputTextfield.enabled = NO;
    _companyInfoView.lxrView.inputTextfield.enabled = NO;
    _companyInfoView.sjhmView.inputTextfield.enabled = NO;
    _companyInfoView.bylxrView.inputTextfield.enabled = NO;
    _companyInfoView.bysjhmView.inputTextfield.enabled = NO;
    _companyInfoView.xzqhView.baseView.userInteractionEnabled = NO;
    _companyInfoView.hylbView.baseView.userInteractionEnabled = NO;
    _companyInfoView.zczjView.inputTextfield.enabled = NO;
    _companyInfoView.pwxkzView.inputTextfield.enabled = NO;
    _companyInfoView.pwjyhView.inputTextfield.enabled = NO;
    _companyInfoView.hppfView.inputTextfield.enabled = NO;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    _locationView.needlocation = NO;
    [_locationView setCanEdit:NO];
    _locationView.addressView.editable = NO;
}
#pragma mark ---------->>> private
- (void)commitButtonClick {
    [AlertViewUtil showSelectAlertViewWithVC:self Title:@"提交合同内容" Message:@"提交后，合同内容将无法再次修改" LeftTitle:@"取消" RightTitle:@"确定" callBack:^(NSInteger type) {
        if (type==2) {
            [self checkCompactNumber:_taskView.htbhView.inputTextfield.text callBack:^(NSInteger type) {
                if (type==1) {
                    [self commitCompact];
                }else {
                    [self hiddenProgressHud];
                    [self showHUDMessage:@"合同编号已存在，请检查"];
                }
            }];
        }
    }];
}
- (void)saveButtonClick {
    [AlertViewUtil showSelectAlertViewWithVC:self Title:@"暂存合同内容" Message:@"暂存后，合同内容后续可继续编辑" LeftTitle:@"取消" RightTitle:@"确定" callBack:^(NSInteger type) {
        if (type==2) {
            [self saveCompact];
        }
    }];
}
- (void)commitCompact {
    if ([_companyInfoView.qymcView.info isEqualToString:@"请选择"]) {
          [self showHUDMessage:@"请选择甲方信息"];
          return;
      }
    if (!_divisionLeader||[_divisionLeader isKindOfClass:[NSNull class]]) {
        [self showHUDMessage:@"该区域暂无负责人信息，请稍后重试或重新选择企业信息"];
        return;
    }
    if (!_maintainDivisionLeader||[_maintainDivisionLeader isKindOfClass:[NSNull class]]) {
        [self showHUDMessage:@"该区域暂无运维负责人信息，请稍后重试或重新选择企业信息"];
        return;
    }
    if (_taskView.hetongView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入合同名称"];
        return;
    }
//    if (_taskView.htbhView.inputTextfield.text.length==0) {
//        [self showHUDMessage:@"请输入合同编号"];
//               return;
//    }
      if (_taskView.yiFangView.inputTextfield.text.length==0) {
          [self showHUDMessage:@"请输入乙方公司"];
          return;
      }
     if (_serviceView.banciView.inputTextfield.text.length==0) {
         [self showHUDMessage:@"请输入生产班次"];
         return;
      }
      if (_serviceView.timeView.inputTextfield.text==0) {
          [self showHUDMessage:@"请输入生产时间"];
          return;
      }
       if (_serviceView.dianweiView.inputTextfield.text==0) {
           [self showHUDMessage:@"请输入点位数量"];return;
      }
       if (_serviceView.htjeView.inputTextfield.text.length==0) {
           [self showHUDMessage:@"请输入合同金额"];return;
       }
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       if ([_taskView.implementView.info isEqualToString:@"未回款"]) {
         [params setValue:[NSNumber numberWithInteger:0] forKey:@"moneyBackStatus"];//回款状态
       }else if ([_taskView.implementView.info isEqualToString:@"回款中"]) {
           [params setValue:[NSNumber numberWithInteger:1] forKey:@"moneyBackStatus"];//回款状态
       }else {
         [params setValue:[NSNumber numberWithInteger:2] forKey:@"moneyBackStatus"];//回款状态
        }
       if (_taskView.htbhView.inputTextfield.text.length>0) {
        [params setValue:_taskView.htbhView.inputTextfield.text forKey:@"number"];
       }
        [params setValue:_taskView.hetongView.inputTextfield.text forKey:@"name"];
        [params setValue:_taskView.yiFangView.inputTextfield.text forKey:@"bcompany"];
        [params setValue:[NSNumber numberWithInteger:[_taskView.serviceTimeView.info integerValue]] forKey:@"serviceYear"];
        [params setValue:_taskView.timeQuanTum.fromTimeView.lineId forKey:@"serviceStart"];
        [params setValue:_taskView.timeQuanTum.toTimeView.lineId forKey:@"serviceEnd"];
        [params setValue:@"用电监管" forKey:@"service"];
        [params setValue:_serviceView.banciView.inputTextfield.text forKey:@"productShifts"];
        [params setValue:_serviceView.timeView.inputTextfield.text forKey:@"productTime"];
       [params setValue:_serviceView.htjeView.inputTextfield.text forKey:@"money"];
        [params setValue:[NSNumber numberWithInteger:[_serviceView.dianweiView.inputTextfield.text integerValue]] forKey:@"pointCount"];
    [params setValue:_divisionLeader forKey:@"divisionLeader"];
    [params setValue:_maintainDivisionLeader forKey:@"maintainDivisionLeader"];
    [params setValue:[UserManager shareManager].Id forKey:@"businessLeader"];
    [params setValue:[UserManager shareManager].name forKey:@"businessLeaderName"];
      if (_serviceView.uploadView.imageArray.count>0) {
          NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
          [_serviceView.uploadView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
              if (obj.length>0) {
                  NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                     [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                     [dic setValue:obj forKey:@"ossName"];
                     [uploadImageArray addObject:dic];
              }
          }];
          if (uploadImageArray.count>0) {
              [params setValue:uploadImageArray forKey:@"pointImageAttachment"];
          }
      }
      if (![_serviceView.markView2.textView.text isEqualToString:@"请输入回款状态备注"]) {
          [params setValue:_serviceView.markView2.textView.text forKey:@"moneyBackComment"];
      }
      if (![_serviceView.markView.textView.text isEqualToString:@"详细的情况备注(如回款、实施过程中遇到的问题)"]) {
          [params setValue:_serviceView.markView.textView.text forKey:@"comment"];
       }
      [params setValue:[NSNumber numberWithInteger:1] forKey:@"status"];// 1提交
      [self setParams:params];
      NSString *url = @"devops/api/v1/contract/";
      if (_model) {
          url = [NSString stringWithFormat:@"devops/api/v1/contract/%@",_model.Id];
      }
    [self showProgressHud];
    [RequestTool requestDataWithUrlSuffix:url method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"提交成功"];
                if (_addBlock) {
                    _addBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)changeCompactStatus {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    if ([_taskView.implementView.info isEqualToString:@"未回款"]) {
      [params setValue:[NSNumber numberWithInteger:0] forKey:@"status"];//回款状态
    }else if ([_taskView.implementView.info isEqualToString:@"回款中"]) {
        [params setValue:[NSNumber numberWithInteger:1] forKey:@"status"];//回款状态
    }else {
      [params setValue:[NSNumber numberWithInteger:2] forKey:@"status"];//回款状态
     }
    [params setValue:_model.Id forKey:@"id"];
    [self showProgressHud];
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/contract/change/moneyBack" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"回款状态已修改"];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)saveCompact {//保存合同
       if ([_companyInfoView.qymcView.info isEqualToString:@"请选择"]) {
            [self showHUDMessage:@"请选择甲方信息"];
            return;
        }
    if (!_divisionLeader||[_divisionLeader isKindOfClass:[NSNull class]]) {
        [self showHUDMessage:@"该区域暂无负责人信息，请稍后重试或重新选择企业信息"];
        return;
    }
    if (!_maintainDivisionLeader||[_maintainDivisionLeader isKindOfClass:[NSNull class]]) {
        [self showHUDMessage:@"该区域暂无运维负责人信息，请稍后重试或重新选择企业信息"];
        return;
    }
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
        if (_taskView.yiFangView.inputTextfield.text.length>0) {
            [params setValue:_taskView.yiFangView.inputTextfield.text forKey:@"bcompany"];
        }
        if (_taskView.hetongView.inputTextfield.text.length>0) {
            [params setValue:_taskView.hetongView.inputTextfield.text forKey:@"name"];
        }
        if (_taskView.htbhView.inputTextfield.text.length>0) {
            [params setValue:_taskView.htbhView.inputTextfield.text forKey:@"number"];
        }
        if ([_taskView.implementView.info isEqualToString:@"未回款"]) {
          [params setValue:[NSNumber numberWithInteger:0] forKey:@"moneyBackStatus"];//回款状态
        }else if ([_taskView.implementView.info isEqualToString:@"回款中"]) {
            [params setValue:[NSNumber numberWithInteger:1] forKey:@"moneyBackStatus"];//回款状态
        }else {
          [params setValue:[NSNumber numberWithInteger:2] forKey:@"moneyBackStatus"];//回款状态
         }
        [params setValue:[NSNumber numberWithInteger:[_taskView.serviceTimeView.info integerValue]] forKey:@"serviceYear"];
        [params setValue:_taskView.timeQuanTum.fromTimeView.lineId forKey:@"serviceStart"];
        [params setValue:_taskView.timeQuanTum.toTimeView.lineId forKey:@"serviceEnd"];
        [params setValue:@"用电监管" forKey:@"service"];
        if (_serviceView.banciView.inputTextfield.text.length>0) {
            [params setValue:_serviceView.banciView.inputTextfield.text forKey:@"productShifts"];
        }
        if (_serviceView.timeView.inputTextfield.text>0) {
            [params setValue:_serviceView.timeView.inputTextfield.text forKey:@"productTime"];
        }
         if (_serviceView.dianweiView.inputTextfield.text>0) {
            [params setValue:[NSNumber numberWithInteger:[_serviceView.dianweiView.inputTextfield.text integerValue]] forKey:@"pointCount"];
        }
         if (_serviceView.htjeView.inputTextfield.text.length>0) {
              [params setValue:_serviceView.htjeView.inputTextfield.text forKey:@"money"];
         }
        if (_serviceView.uploadView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_serviceView.uploadView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"pointImageAttachment"];
            }
        }
        if (![_serviceView.markView2.textView.text isEqualToString:@"请输入回款状态备注"]) {
            [params setValue:_serviceView.markView2.textView.text forKey:@"moneyBackComment"];
        }
        if (![_serviceView.markView.textView.text isEqualToString:@"详细的情况备注(如回款、实施过程中遇到的问题)"]) {
            [params setValue:_serviceView.markView.textView.text forKey:@"comment"];
         }
        [params setValue:[NSNumber numberWithInteger:0] forKey:@"status"];// 0 暂存
    [params setValue:_divisionLeader forKey:@"divisionLeader"];
    [params setValue:_maintainDivisionLeader forKey:@"maintainDivisionLeader"];
        [self setParams:params];
        NSString *url = @"devops/api/v1/contract/";
        if (_model) {
            url = [NSString stringWithFormat:@"devops/api/v1/contract/%@",_model.Id];
        }
    [self showProgressHud];
        [RequestTool requestDataWithUrlSuffix:url method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hiddenProgressHud];
                if ([responseMessage isRequestSuccessful]) {
                    [self showHUDMessage:@"暂存成功"];
                    if (_addBlock) {
                        _addBlock();
                    }
                    [self.navigationController popViewControllerAnimated:YES];
                }else {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            });
        }];
    }
- (NSMutableDictionary *)setParams:(NSMutableDictionary *)params {
    if ([UserManager shareManager].roleGroup==1) {
        [params setValue:_taskView.principalId forKey:@"businessLeader"];//商务负责人
        [params setValue:_taskView.principalName forKey:@"businessLeaderName"];//商务负责姓名
    }else {
        [params setValue:[UserManager shareManager].Id forKey:@"businessLeader"];//商务负责人
        [params setValue:[UserManager shareManager].name forKey:@"businessLeaderName"];//商务负责姓名
    }
    [params setValue:_companyInfoView.qymcView.info forKey:@"companyName"];
    if (_companyInfoView.tyxydmView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.tyxydmView.inputTextfield.text forKey:@"creditCode"];
    }
    if (_companyInfoView.fzView.inputTextfield.text.length>0) {
        [params setValue:_companyInfoView.fzView.inputTextfield.text forKey:@"legalRepresentative"];
    }
    [params setValue:_companyInfoView.lxrView.inputTextfield.text forKey:@"linkman"];
    [params setValue:_companyInfoView.sjhmView.inputTextfield.text forKey:@"linkmanPhone"];
    [params setValue:_companyInfoView.bylxrView.inputTextfield.text forKey:@"linkmanTwo"];
    [params setValue:_companyInfoView.bysjhmView.inputTextfield.text  forKey:@"linkmanTwoPhone"];
    [params setValue:_companyInfoView.xzqhView.info forKey:@"divisionName"];
    [params setValue:_companyInfoView.xzqhView.lineId forKey:@"division"];
    [params setValue:_companyInfoView.hylbView.info  forKey:@"industryClass"];
    [params setValue:_companyInfoView.hylbView.lineId  forKey:@"industryClassId"];
    [params setValue:_companyInfoView.pwxkzView.inputTextfield.text forKey:@"licenseNumber"];
    [params setValue:_companyInfoView.pwjyhView.inputTextfield.text forKey:@"emissionTradingDocuments"];
    [params setValue:_companyInfoView.hppfView.inputTextfield.text forKey:@"eiaBatchNumber"];
    [params setValue:_locationView.jdView.inputTextfield.text forKey:@"longitude"];
    [params setValue:_locationView.wdView.inputTextfield.text forKey:@"latitude"];
    [params setValue:_locationView.addressView.text forKey:@"addressDetail"];
    [params setValue:_currentModel.Id forKey:@"companyId"];
    return params;
}

- (void)checkCompactNumber:(NSString *)number  callBack:(void(^)(NSInteger type))callBackBlock {
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:number forKey:@"number"];
    [params setValue:self.model.Id forKey:@"id"];
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/contract/check/number" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *dic = responseMessage.responseData[@"body"];
                NSInteger type = [dic[@"result"] integerValue];
                callBackBlock(type);
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)relayoutWithDeatil:(NSDictionary *)result {
    _companyInfoView.qymcView.info= result[@"companyName"];
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _divisionLeader = result[@"divisionLeader"];
    _maintainDivisionLeader = result[@"maintainDivisionLeader"];
    _companyInfoView.tyxydmView.inputTextfield.text = result[@"creditCode"];
    _companyInfoView.fzView.inputTextfield.text = result[@"legalRepresentative"];
    _companyInfoView.lxrView.inputTextfield.text = result[@"linkman"];
    _companyInfoView.sjhmView.inputTextfield.text = result[@"linkmanPhone"];
    _companyInfoView.bylxrView.inputTextfield.text = result[@"linkmanTwo"];
    _companyInfoView.bysjhmView.inputTextfield.text = result[@"linkmanTwoPhone"];
    _companyInfoView.xzqhView.info = result[@"divisionName"];
    _companyInfoView.xzqhView.lineId = result[@"division"];
    _companyInfoView.hylbView.info = result[@"industryClass"];
    _companyInfoView.hylbView.lineId = result[@"industryClassId"];
    _companyInfoView.pwxkzView.inputTextfield.text = result[@"licenseNumber"];
    _companyInfoView.pwjyhView.inputTextfield.text = result[@"emissionTradingDocuments"];
    _companyInfoView.hppfView.inputTextfield.text = result[@"eiaBatchNumber"];
    _locationView.jdView.inputTextfield.text = result[@"longitude"];
    _locationView.wdView.inputTextfield.text = result[@"latitude"];
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([result[@"latitude"] doubleValue], [result[@"longitude"] doubleValue]);
    [_locationView.mapView setCenterCoordinate:location animated:YES];
    [_locationView addPoint:location title:result[@"companyName"]];
    _locationView.addressView.text = result[@"addressDetail"];
    _currentModel = [[CompanyModel alloc]init];
    _currentModel.Id = result[@"companyId"];
    
    _taskView.principal = [NSString stringWithFormat:@"商务负责人：%@",[CommonUtil fixNullTextWithLine:result[@"businessLeaderName"]]];
    _taskView.principalId = [CommonUtil fixNullText:result[@"businessLeader"]];
    _taskView.principalName = [CommonUtil fixNullText:result[@"businessLeaderName"]];
    
    if ([result[@"moneyBackStatus"] integerValue]==0) {
        _taskView.implementView.info = @"未回款";
    }else if ([result[@"moneyBackStatus"] integerValue]==1){
        _taskView.implementView.info = @"回款中";
    }else {
        _taskView.implementView.info = @"已回款";
    }
    _taskView.hetongView.inputTextfield.text = result[@"name"];
    _taskView.htbhView.inputTextfield.text = [CommonUtil fixNullText:result[@"number"]];
    _taskView.yiFangView.inputTextfield.text = result[@"bcompany"];
    _taskView.timeQuanTum.serviceYear = [result[@"serviceYear"] integerValue];
    _taskView.serviceTimeView.info = [NSString stringWithFormat:@"%@",result[@"serviceYear"]];
    _taskView.timeQuanTum.fromTimeView.info = [result[@"serviceStart"] substringWithRange:RANGE(0, 10)];
    _taskView.timeQuanTum.fromTimeView.lineId = result[@"serviceStart"];
    _taskView.timeQuanTum.toTimeView.info = [result[@"serviceEnd"] substringWithRange:RANGE(0, 10)];
    _taskView.timeQuanTum.toTimeView.lineId = result[@"serviceEnd"] ;

    _serviceView.banciView.inputTextfield.text = result[@"productShifts"];
    _serviceView.fuwuView.info = result[@"service"];
    _serviceView.timeView.inputTextfield.text = result[@"productTime"];
    _serviceView.dianweiView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"pointCount"]];
    _serviceView.htjeView.inputTextfield.text = result[@"money"];
    _serviceView.markView2.textView.text = result[@"moneyBackComment"];
    _serviceView.markView.textView.text = result[@"comment"];
    
    if ([result[@"status"] integerValue]==1) {//已提交
        if ([UserManager shareManager].roleGroup==1) {//管理员身份  已提交状态下 编辑 只能提交  不能暂存
            saveButton.hidden = YES;
            commitButton.frame = RECT(ScreenWidth/2.0-75, BOTTOM(_locationView)+30,150, 40);
            _companyInfoView.qymcView.userInteractionEnabled = NO;//已提交 甲方不可选择
        }else {
            _companyInfoView.qymcView.userInteractionEnabled = NO;//已提交 甲方不可选择
            _isCommit = YES;//是已提交状态 只能修改回款状态
            _taskView.hetongView.inputTextfield.enabled = NO;
            _taskView.htbhView.inputTextfield.enabled = NO;
            _taskView.yiFangView.inputTextfield.enabled = NO;
            _taskView.serviceTimeView.userInteractionEnabled = NO;
            _taskView.timeQuanTum.fromTimeView.userInteractionEnabled = NO;
            _serviceView.fuwuView.userInteractionEnabled = NO;
            _serviceView.banciView.inputTextfield.enabled = NO;
            _serviceView.timeView.inputTextfield.enabled = NO;
            _serviceView.dianweiView.inputTextfield.enabled = NO;
            _serviceView.htjeView.inputTextfield.enabled = NO;
            _serviceView.markView.canEdit = NO;
            _serviceView.markView2.canEdit = NO;
            _serviceView.uploadView.isCommit = YES;
            [_serviceView.uploadView removeUploadButton];//提交状态图片不可编辑
        }
    }
    NSArray *array = result[@"pointImageAttachment"];
    if (array&&array.count>0) {
         _serviceView.uploadView.imageUrlArray = [array mutableCopy];
     }
}
@end
