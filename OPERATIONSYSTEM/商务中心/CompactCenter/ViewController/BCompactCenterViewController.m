//
//  BCompactCenterViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "BCompactCenterViewController.h"
#import "AlertListSelectView.h"
#import "SearchView.h"
#import "TaskCenterListCell.h"
#import "TaskSenderViewController.h"
#import "AddClientViewController.h"
#import "MineViewController.h"
#import "AddCompactViewController.h"
#import "CompactModel.h"
#import "AreaSelectViewController.h"
#import "LegenSelectViewController.h"
static NSString * const TASK_LIST = @"TaskCenterListCell";
@interface BCompactCenterViewController ()<UITableViewDelegate,UITableViewDataSource,GYZActionSheetDelegate>
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)AlertListSelectView *areaView;
@property (nonatomic,strong)AlertListSelectView *swfzrView;
@property (nonatomic,strong)AlertListSelectView *htztView;
@property (nonatomic,strong)SearchView *searchView;
@property (nonatomic,assign)BOOL isCompany;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,assign)NSInteger operationType;
@property (nonatomic,strong)NSMutableArray *listArray;

@end

@implementation BCompactCenterViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    
    _listArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    self.isCompany = YES;
    
    [self setNavTitle:@"企业合同" Color:Color_3D3A39];

    [self setLeftButtonTitle:@"商"];

    [self setRightButtonImage:@"icon_add"];
    
    [self addLineViewWithFrame:RECT(0, NavHeight, ScreenWidth, 4) Color:Color_BG supView:self.view];
    
    _areaView = [[AlertListSelectView alloc]initWithFrame:RECT(0,NavHeight+8, ScreenWidth/2.0, 40)];
    _areaView.title = @"区  域：";
    _areaView.info = @"全部";
    _areaView.operationBlock = ^{
        NSArray  *array = [UserManager shareManager].areaJosn;
           if (!array.count) {
               [weakSelf requestDivisionCode:YES];
           }else {
               [weakSelf showCityPickerView];
           }
    };
    [self.view addSubview:_areaView];

    _swfzrView = [[AlertListSelectView alloc]initWithFrame:RECT(ScreenWidth/2.0,NavHeight+8, ScreenWidth/2.0, 40)];
    _swfzrView.title = @"商务负责人：";
    _swfzrView.info = @"全部";
    _swfzrView.leftWidth = 85;
    _swfzrView.operationBlock = ^{
        LegenSelectViewController *vc = [[LegenSelectViewController alloc]init];
        vc.url = @"devops/api/v1/contract/businessLeader/list";
        vc.selectBlock = ^(NSString * _Nonnull name) {
            weakSelf.swfzrView.info = name;
            [weakSelf.tableView resetPageNum:1];
            [weakSelf requestBusinessList:1 show:YES];
        };
        [weakSelf presentWithViewController:vc animated:YES];
    };
    [self.view addSubview:_swfzrView];
    
    _htztView = [[AlertListSelectView alloc]initWithFrame:RECT(0,BOTTOM(_swfzrView), ScreenWidth/2.0, 40)];
    _htztView.title = @"合同状态：";
    _htztView.info = @"全部";
    _htztView.operationBlock = ^{
        weakSelf.operationType = 1;//合同状态
        [weakSelf showActionSheetWithArray:@[@"全部",@"未提交",@"已提交"] title:@"合同状态"];
      };
    [self.view addSubview:_htztView];
    
    _searchView = [[SearchView alloc]initWithFrame:RECT(ScreenWidth/2.0,BOTTOM(_swfzrView), ScreenWidth/2.0, 40) type:1];
      _searchView.placeHold = @"搜索内容";
      _searchView.searchBlock = ^(NSString * _Nonnull search) {
          [weakSelf.tableView resetPageNum:1];
          [weakSelf requestBusinessList:1 show:YES];
      };
      [self.view addSubview:_searchView];
    
    [self addLineViewWithFrame:RECT(0, BOTTOM(_searchView)+4, ScreenWidth, 4) Color:Color_BG supView:self.view];
        
    _tableView = [[UITableView alloc]initWithFrame:RECT(0, BOTTOM(_searchView)+8, ScreenWidth, ScreenHeight-BOTTOM(_searchView)-8-TabBarHeight) style:0];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 64;
    _tableView.noDataContent = @"暂无合同清单";
    _tableView.marginTop = HEIGHT(_tableView)/2.0-80;
    [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0 )];
    [_tableView registerNib:[UINib nibWithNibName:@"TaskCenterListCell" bundle:nil] forCellReuseIdentifier:TASK_LIST];
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
    
    [_tableView addHeaderRefreshWithAutomaticallyRefresh:YES refreshBlock:^(NSInteger pageIndex) {
        [weakSelf requestBusinessList:pageIndex show:NO];
    }];
    [_tableView addFootLoadMoreWithAutomaticallyLoad:YES loadMoreBlock:^(NSInteger pageIndex) {
        if (weakSelf.noMoreData) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf showHUDMessage:@"暂无更多数据了"];
                [weakSelf.tableView endFootLoadMore];
            });
            return;
        }
        [weakSelf requestBusinessList:pageIndex show:NO];
    }];
    [weakSelf requestBusinessList:1 show:YES];
    NSArray  *array = [UserManager shareManager].areaJosn;
    if (!array.count) {
        dispatch_queue_t queue = dispatch_queue_create("loadAreaTree", DISPATCH_QUEUE_CONCURRENT);
        dispatch_async(queue, ^{
            [self requestDivisionCode:NO];
        });
    }
}
#pragma mark ---------->>> method
- (void)change:(UISegmentedControl *)seg {
        self.isCompany = !seg.selectedSegmentIndex;
}
- (void)navgationLeftButtonClick {
    MineViewController *vc = [MineViewController new];
    [self pushWithViewController:vc hidden:YES];
}
- (void)navgationRightButtonClick {
    AddCompactViewController *addvc = [AddCompactViewController new];
    addvc.isAdd = YES;
    addvc.addBlock = ^{
        [self.tableView resetPageNum:1];
        [self requestBusinessList:1 show:YES];
    };
    [self pushWithViewController:addvc hidden:YES];
}
#pragma mark ---------->>> delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskCenterListCell *cell = [tableView dequeueReusableCellWithIdentifier:TASK_LIST];
    CompactModel *model = self.listArray[indexPath.row];
    if (model.status==0) {
        cell.typeLabel.text = @"合同状态:未提交";
    }else {
        cell.typeLabel.text = @"合同状态:已提交";
    }
    cell.nameLabel.text = model.companyName;
    cell.areaLabel.text = [NSString stringWithFormat:@"区域:%@",model.division];
    cell.legenLabel.text = [NSString stringWithFormat:@"商务负责人:%@",[CommonUtil fixNullTextWithLine:model.businessLeaderName]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AddCompactViewController *addvc = [AddCompactViewController new];
    CompactModel *model = self.listArray[indexPath.row];
    addvc.model = model;
    addvc.addBlock = ^{
        [self.tableView resetPageNum:1];
        [self requestBusinessList:1 show:YES];
    };
    [self pushWithViewController:addvc hidden:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, .5) color:Color_Line];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([UserManager shareManager].roleGroup==1) {
        return YES;
    }else {
        CompactModel *model = self.listArray[indexPath.row];
        if (model.status==0) {
            return YES;
        }else {
            return NO;
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}
//修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}
//点击删除
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //在这里实现删除操作
    [AlertViewUtil showSelectAlertViewWithVC:self Title:@"删除该企业合同" Message:@"删除后该合同将无法查看，是否确定删除？" LeftTitle:@"确定" RightTitle:@"取消" callBack:^(NSInteger type) {
        if (type==1) {
            CompactModel *model = self.listArray[indexPath.row];
            NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
            [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/contract/%@",model.Id] method:DELETE params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([responseMessage isRequestSuccessful]) {
                        [self showHUDMessage:@"删除成功"];
                        [self.listArray removeObjectAtIndex:indexPath.row];
                        _tableView.dataArray = _listArray;
                        [_tableView reloadWithData];
                    }else {
                        [self showHUDMessage:responseMessage.errorMessage];
                    }
                });
            }];
        }
    }];
}
#pragma mark ---------->>> 弹出视图
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    _htztView.info = title;
    [self.tableView resetPageNum:1];
    [self requestBusinessList:1 show:YES];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}
#pragma mark ---------->>> request
- (void)requestBusinessList:(NSInteger)pageNum show:(BOOL)show {
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[NSNumber numberWithInteger:pageNum] forKey:@"pageNum"];
    [params setValue:[NSNumber numberWithInteger:20] forKey:@"pageSize"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    if (_areaView.lineId&&![_areaView.lineId isEqualToString:@"0"]) {
        [params setValue:_areaView.lineId forKey:@"division"];
    }
    if (![_swfzrView.info isEqualToString:@"全部"]) {
        [params setValue:_swfzrView.info forKey:@"businessLeaderName"];
    }
    if (![_htztView.info isEqualToString:@"全部"]) {
        if ([_htztView.info isEqualToString:@"未提交"]) {
            [params setValue:[NSNumber numberWithInteger:0] forKey:@"status"];
        }else {
            [params setValue:[NSNumber numberWithInteger:1] forKey:@"status"];
        }
    }
    if (_searchView.searchTextField.text.length>0) {
            [params setValue:_searchView.searchTextField.text forKey:@"companyName"];
    }
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/contract/" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
                if (show) {
                     [self hiddenProgressHud];
                 }else {
                     [_tableView endLoading];
                 }
                 if ([responseMessage isRequestSuccessful]) {
                     if (pageNum==1) {
                         [self.listArray removeAllObjects];
                     }
                     NSDictionary *result = responseMessage.data;
                     NSArray *array = result[@"data"];
                     [_listArray addObjectsFromArray:[CompactModel mj_objectArrayWithKeyValuesArray:array]];
                     if (array.count==0) {
                        [_tableView resetPageNum:pageNum];
                         self.noMoreData = YES;
                     }else {
                         self.noMoreData = NO;
                     }
                     _tableView.dataArray = _listArray;
                     [_tableView reloadWithData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestDivisionCode:(BOOL)show {
//    商务中心  001003001
//    实施中心  001003002
//    运维中心  001003003
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[UserManager shareManager].deptCode forKey:@"deptCode"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/division/divisionTreeByDeptCode" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }
            if ([responseMessage isRequestSuccessful]) {
                  NSArray *data = responseMessage.data[@"data"];
                             if (data.count>0) {
                                      NSArray *treeArray = data;
                                      NSMutableArray *totalArray = [[NSMutableArray alloc]initWithCapacity:0];
                                      NSMutableDictionary *all = [[NSMutableDictionary alloc]initWithCapacity:0];
                                      [all setValue:@"全部" forKey:@"name"];
                                      [all setValue:@"0" forKey:@"parentId"];
                                      [totalArray addObject:all];
                                      [totalArray addObjectsFromArray:treeArray];
                                      [UserManager shareManager].areaJosn = (NSArray *)totalArray;
                                      if (show) {
                                         [self showCityPickerView];
                                      }
                             }else{
                                 if (show) {
                                     [self showHUDMessage:@"您当前账号暂无区域可选"];
                                 }
                             }
            }else {
                if (show) {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            }
        });
    }];
}
- (void)showCityPickerView {
    __weak typeof(self)weakSelf = self;
    AreaSelectViewController *vc = [[AreaSelectViewController alloc]init];
    vc.titleStr = @"选择区域";
    vc.parentName = @"全部";
    vc.parentCode = @"0";
    vc.areaBlock = ^(NSString * _Nonnull name, NSString * _Nonnull code) {
        weakSelf.areaView.info = name;
        weakSelf.areaView.lineId= code;
        [weakSelf.tableView resetPageNum:1];
        [weakSelf requestBusinessList:1 show:YES];
    };
    [self presentWithViewController:vc animated:YES];
}
@end
