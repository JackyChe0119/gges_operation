//
//  LegenSelectViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/9.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "LegenSelectViewController.h"
#import "SearchView.h"
@interface LegenSelectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *listArray;
@property (nonatomic,strong)SearchView *searchView;
@end

@implementation LegenSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    [self setLeftButtonImage:BACKUP];
    [self addLineViewWithFrame:RECT(0, NavHeight, ScreenWidth, 4) Color:Color_BG supView:self.view];
    if (_navTitle) {
        [self setNavTitle:self.navTitle Color:Color_3D3A39];
    }else {
        [self setNavTitle:@"商务负责人" Color:Color_3D3A39];
    }
      _searchView = [[SearchView alloc]initWithFrame:RECT(0,NavHeight+4, ScreenWidth, 40) type:1];
      _searchView.placeHold = @"搜索内容";
      _searchView.searchBlock = ^(NSString * _Nonnull search) {
         [weakSelf requestLegenList:search];
      };
      [self.view addSubview:_searchView];
    
    [self addLineViewWithFrame:RECT(0, BOTTOM(_searchView), ScreenWidth, 4) Color:Color_BG supView:self.view];
    _tableView = [[UITableView alloc]initWithFrame:RECT(0, BOTTOM(_searchView)+4, ScreenWidth, ScreenHeight-NavHeight-BOTTOM(_searchView)-4) style:0];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 50;
    [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0 )];
    [_tableView setSeparatorColor:Color_Line];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.noDataContent = @"暂无负责人信息";
    _tableView.marginTop = HEIGHT(_tableView)/2.0-80;
    [self.view addSubview:_tableView];
    [self requestLegenList:@""];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:0 reuseIdentifier:identify];
    }
    NSDictionary *result = self.listArray[indexPath.row];
    cell.textLabel.text = result[@"name"];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *result = self.listArray[indexPath.row];
    if ([self.url containsString:@"api/v1/contract/applyLeader/list"]||[self.url containsString:@"/api/v1/operation/user/underlingUser"]||[self.url containsString:@"devops/api/v1/contract/maintainLeader/list"]) {
        if (_selectWithIdBlock) {
            _selectWithIdBlock(result[@"name"],result[@"id"]);
        }
    }else {
        if (_selectBlock) {
            _selectBlock(result[@"name"]);
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, .5) color:Color_Line];
}
- (NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _listArray;
}
- (void)navgationLeftButtonClick {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)requestLegenList:(NSString *)name {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    if (self.userId) {
        [params setValue:self.userId forKey:@"userId"];
    }else{
        [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    }
    if ([self.url containsString:@"/api/v1/operation/user/underlingUser"]) {
        [params setValue:[UserManager shareManager].deptCode forKey:@"deptCode"];
        if (name.length>0) {
            [params setValue:name forKey:@"searchKey"];
        }
        [params setValue:self.divisionId forKey:@"divisionId"];
    }else {
        if (name.length>0) {
            [params setValue:name forKey:@"name"];
        }
    }
    [self showProgressHud];
    [RequestTool requestDataWithUrlSuffix:self.url method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                NSArray *array = responseMessage.data[@"data"];
                [self.listArray removeAllObjects];
                if (![self.url containsString:@"/api/v1/operation/user/underlingUser"]) {//实施负责负责人分配时
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];
                    [dic setValue:@"0" forKey:@"id"];
                    [dic setValue:@"全部" forKey:@"name"];
                    [self.listArray addObject:dic];
                }
                [self.listArray addObjectsFromArray:array];
                _tableView.dataArray = _listArray;
                [_tableView reloadWithData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        });
    }];
}
@end
