//
//  LegenSelectViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/9.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LegenSelectViewController : FYBaseViewController
@property (nonatomic,copy)NSString *url;
@property (nonatomic,copy)NSString *navTitle;
@property (nonatomic,copy)NSString *userId;
@property (nonatomic,copy)NSString *divisionId;
@property (nonatomic,strong) void (^selectBlock)(NSString *name);
@property (nonatomic,strong) void (^selectWithIdBlock)(NSString *name,NSString *Id);
@end

NS_ASSUME_NONNULL_END
