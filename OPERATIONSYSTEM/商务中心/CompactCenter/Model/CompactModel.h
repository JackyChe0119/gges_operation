//
//  CompactModel.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/8.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CompactModel : HYBaseModel
//businessLeader = "超级管理员";
//             companyName = 1;
//             division = "杭州/西湖";
//             id = 322650634081931265;
//             status = 1;
@property (nonatomic,strong)NSString *businessLeaderName,*division,*Id,*companyName,*applyLeader;
@property (nonatomic,assign)NSInteger status,applyStatus,moneyBackStatus;
@end

NS_ASSUME_NONNULL_END
