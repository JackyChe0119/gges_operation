//
//  ShuCaiYiView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ShuCaiYiView.h"

@implementation ShuCaiYiView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _scyxhView = [[AlertListSelectView alloc]initWithFrame:RECT(10, 10, WIDTH(self)-10, 26)];
        _scyxhView.titleLabel.textColor = Color_333333;
        _scyxhView.title = @"*数采仪型号：";
        _scyxhView.info = @"请选择";
        _scyxhView.leftWidth = 85;
        _scyxhView.titleLabel.textAlignment = Left;
        [self addSubview:_scyxhView];
        
        _scymnmView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_scyxhView)+10, WIDTH(self)-20, 40)];
        _scymnmView.tipTitle = @"*数采仪MN码：";
        _scymnmView.placeHoldTtitle = @"请输入数采仪MN码";
        _scymnmView.inputTextfield.text = @"FFFF20190422000000000001";
        [_scymnmView.inputTextfield addObserverTextFieldEditChangeWithLenth:24 inputType:ImportChatTypeNumberAndEnglish];
        _scymnmView.titleLabel.textAlignment = Left;
        _scymnmView.leftMargin = 95;
        [self addSubview:_scymnmView];
        
        _scymcView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_scymnmView), WIDTH(self)-20, 40)];
        _scymcView.tipTitle = @"*数采仪名称：";
        _scymcView.placeHoldTtitle = @"请输入数采仪名称";
        _scymcView.leftMargin = 90;
        [_scymcView.inputTextfield addObserverTextFieldEditChangeWithLenth:20 inputType:ImportChatTypeNumberEnglishChinesePunctuation];
        _scymcView.titleLabel.textAlignment = Left;
        [self addSubview:_scymcView];
        
        _jrxyView= [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_scymcView)+10, WIDTH(self)-10, 26)];
        _jrxyView.titleLabel.textColor = Color_333333;
        _jrxyView.title = @"*接入协议：";
        _jrxyView.info = @"请选择";
        _jrxyView.leftWidth = 75;
        _jrxyView.titleLabel.textAlignment = Left;
        [self addSubview:_jrxyView];
        
        _syztView= [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_jrxyView)+10, WIDTH(self)-10, 26)];
        _syztView.titleLabel.textColor = Color_333333;
        _syztView.title = @"*使用状态：";
        _syztView.info = @"请选择";
        _syztView.leftWidth = 75;
        _syztView.titleLabel.textAlignment = Left;
        [self addSubview:_syztView];
        
        CGRect rect = self.frame;
        rect.size.height = BOTTOM(_syztView);
        self.frame = rect;
    }
    return self;
}
@end
