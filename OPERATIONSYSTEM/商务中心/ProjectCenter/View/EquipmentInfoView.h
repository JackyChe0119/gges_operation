//
//  EquipmentInfoView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertListSelectView.h"
#import "CustomTitleInputView.h"
NS_ASSUME_NONNULL_BEGIN

@interface EquipmentInfoView : UIView
@property (nonatomic,strong)AlertListSelectView *dbxhView;//电表型号
@property (nonatomic,strong)AlertListSelectView *sblxView;//设备类型
@property (nonatomic,strong)AlertListSelectView *zyflView;//作用分类
@property (nonatomic,strong)CustomTitleInputView *sbmcView;//设备名称
@property (nonatomic,strong)CustomTitleInputView *djglView;//待机功率
@property (nonatomic,strong)CustomTitleInputView *edglView;//额定功率
@property (nonatomic,strong)AlertListSelectView *ssscyView;//所属数采仪

@end

NS_ASSUME_NONNULL_END
