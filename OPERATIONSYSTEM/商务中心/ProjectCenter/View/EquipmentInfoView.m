//
//  EquipmentInfoView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "EquipmentInfoView.h"

@implementation EquipmentInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _dbxhView = [[AlertListSelectView alloc]initWithFrame:RECT(10, 10, WIDTH(self)-10, 26)];
        _dbxhView.titleLabel.textColor = Color_333333;
        _dbxhView.title = @"*电表型号：";
        _dbxhView.leftWidth = 75;
        _dbxhView.info = @"请选择";
        _dbxhView.titleLabel.textAlignment = Left;
        [self addSubview:_dbxhView];
            
        _sbmcView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_dbxhView)+10, WIDTH(self)-20, 40)];
        _sbmcView.tipTitle = @"*设备名称：";
        _sbmcView.placeHoldTtitle = @"请输入设备名称";
        [_djglView.inputTextfield addObserverTextFieldEditChangeWithLenth:20 inputType:ImportChatTypeAll];
        _sbmcView.titleLabel.textAlignment = Left;
        [self addSubview:_sbmcView];
    
        _sblxView= [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_sbmcView)+10, WIDTH(self)-10, 26)];
        _sblxView.titleLabel.textColor = Color_333333;
        _sblxView.title = @"*设备类型：";
        _sblxView.leftWidth = 75;
        _sblxView.info = @"请选择";
        _sblxView.titleLabel.textAlignment = Left;
        [self addSubview:_sblxView];
        
        _zyflView= [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_sblxView)+10, WIDTH(self)-10, 26)];
        _zyflView.titleLabel.textColor = Color_333333;
        _zyflView.title = @"*作用分类：";
        _zyflView.info = @"请选择";
        _zyflView.leftWidth = 75;
        _zyflView.titleLabel.textAlignment = Left;
        [self addSubview:_zyflView];
        
        _djglView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_zyflView)+10, WIDTH(self)/2.0+80, 40)];
          _djglView.tipTitle = @"*待机功率：";
          _djglView.placeHoldTtitle = @"请输入待机功率";
          _djglView.titleLabel.textAlignment = Left;
        _djglView.showRightTitle = YES;
        _djglView.rightMargin = 30;
        _djglView.subTitleLabel.text = @"KW";
        [_djglView.inputTextfield addObserverTextFieldEditChangeWithLenth:10 inputType:ImportChatTypeNumberAndPunctuation];
          [self addSubview:_djglView];
        
        _edglView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_djglView), WIDTH(self)/2+80, 40)];
        _edglView.tipTitle = @"额定功率：";
        _edglView.placeHoldTtitle = @"请输入额定功率";
        [_edglView.inputTextfield addObserverTextFieldEditChangeWithLenth:10 inputType:ImportChatTypeNumberAndPunctuation];
        _edglView.titleLabel.textAlignment = Left;
        _edglView.showRightTitle = YES;
        _edglView.rightMargin = 30;
        _edglView.subTitleLabel.text = @"KW";
        [self addSubview:_edglView];
        
        _ssscyView= [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_edglView)+10, WIDTH(self)-10, 26)];
          _ssscyView.titleLabel.textColor = Color_333333;
          _ssscyView.title = @"*所属数采仪：";
          _ssscyView.info = @"请选择";
          _ssscyView.leftWidth = 82;
          _ssscyView.titleLabel.textAlignment = Left;
          [self addSubview:_ssscyView];
        
        CGRect rect = self.frame;
        rect.size.height = BOTTOM(_ssscyView);
        self.frame = rect;
            
    }
    return self;
}
@end
