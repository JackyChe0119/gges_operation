//
//  ShuCaiYiView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertListSelectView.h"
#import "CustomTitleInputView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ShuCaiYiView : UIView
@property (nonatomic,strong)AlertListSelectView *scyxhView;//数采仪型号
@property (nonatomic,strong)AlertListSelectView *jrxyView;//接入协议
@property (nonatomic,strong)AlertListSelectView *syztView;//使用状态
@property (nonatomic,strong)CustomTitleInputView *scymnmView;//数采仪MN码
@property (nonatomic,strong)CustomTitleInputView *scymcView;//数采仪名称
@end

NS_ASSUME_NONNULL_END
