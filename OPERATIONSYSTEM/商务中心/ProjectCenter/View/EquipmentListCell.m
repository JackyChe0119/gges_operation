//
//  EquipmentListCell.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "EquipmentListCell.h"

@implementation EquipmentListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.width.constant = ScreenWidth/2.0-15;
    self.suoshuLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
