//
//  EquipmentDatailView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleInputView.h"

NS_ASSUME_NONNULL_BEGIN

@interface EquipmentDatailView : UIView
@property (nonatomic,strong)CustomTitleInputView *inputView;//统一应用
@property (nonatomic,strong)CustomTitleInputView *axdlView;//A相电流
@property (nonatomic,strong)CustomTitleInputView *zygglView;//总有功功率
@property (nonatomic,strong)CustomTitleInputView *bxdlView;//B相电流
@property (nonatomic,strong)CustomTitleInputView *zwgglView;//总无功功率
@property (nonatomic,strong)CustomTitleInputView *cxdlView;//C相电流
@property (nonatomic,strong)CustomTitleInputView *zygdlView;//总有功电量
@property (nonatomic,strong)CustomTitleInputView *dxdlView;//单相电流
@property (nonatomic,strong)CustomTitleInputView *zwgdlView;//总无功电流
@property (nonatomic,strong)CustomTitleInputView *axdyView;//A相电压
@property (nonatomic,strong)CustomTitleInputView *cxdyView;//C相电压
@property (nonatomic,strong)CustomTitleInputView *bxdyView;//B相电流
@property (nonatomic,strong)CustomTitleInputView *zglyzView;//总功率因数
@property (nonatomic,strong)CustomTitleInputView *tqscView;//通气时长
- (void)setInfoWith:(NSDictionary *)result;
- (void)noEdit;
@end

NS_ASSUME_NONNULL_END
