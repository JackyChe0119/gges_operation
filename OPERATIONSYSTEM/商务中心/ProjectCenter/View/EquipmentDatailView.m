//
//  EquipmentDatailView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "EquipmentDatailView.h"

@implementation EquipmentDatailView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = Color_BG;
    UILabel  *titleLabel = [UILabel new];
    [titleLabel rect:RECT(0, 0, WIDTH(self), 30) aligment:Left font:13 isBold:NO text:@" *因子编码" textColor:Color_333333 superView:self];
        titleLabel.backgroundColor = Color_White;
        
        _inputView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, 30, (WIDTH(self)-20)/2.0-10, 40)];
        _inputView.tipTitle = @"快捷编辑：";
        _inputView.leftMargin = 68;
        [_inputView.inputTextfield addObserverTextFieldEditChangeWithLenth:2 inputType:ImportChatTypeNumber];
        _inputView.placeHoldTtitle = @"因子后两位";
        _inputView.inputTextfield.keyboardType = UIKeyboardTypeNumberPad;
        _inputView.titleLabel.textAlignment = Left;
        _inputView.backgroundColor = Color_BG;
        _inputView.subTitleLabel.textAlignment = Center;
        [self addSubview:_inputView];
        
        UIButton *sureButton = [UIButton createButtonWithFrame:RECT(ScreenWidth/2.0, 38, 50, 24) Title:@"应用" Target:self Selector:@selector(sureButtonClick)];
        [sureButton setTitleColor:Color_White forState:UIControlStateNormal];
        sureButton.titleLabel.font = FONT(13);
        [sureButton setBackgroundColor:Color_Common];
        [sureButton addRoundedCornersWithRadius:4];
        [self addSubview:sureButton];
        
        _axdlView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, 70, (WIDTH(self)-20)/2.0-10, 40)];
          _axdlView.tipTitle = @"A相电流：";
        _axdlView.leftMargin = 68;
        [_axdlView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
          _axdlView.placeHoldTtitle = @"请输入";
          _axdlView.titleLabel.textAlignment = Left;
        _axdlView.backgroundColor = Color_BG;
        _axdlView.subTitleLabel.textAlignment = Center;
          [self addSubview:_axdlView];
        
        _zygglView = [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, 70, (WIDTH(self)-20)/2.0, 40)];
        _zygglView.backgroundColor = Color_BG;
          _zygglView.tipTitle = @"总有功功率：";
          _zygglView.placeHoldTtitle = @"请输入";
        _zygglView.leftMargin = 80;
          _zygglView.titleLabel.textAlignment = Left;
        [_zygglView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _zygglView.subTitleLabel.textAlignment = Center;
        [self addSubview:_zygglView];
        
        _bxdlView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_zygglView), (WIDTH(self)-20)/2.0-10, 40)];
        _bxdlView.backgroundColor = Color_BG;
         _bxdlView.tipTitle = @"B相电流：";
        _bxdlView.leftMargin = 68;
        _bxdlView.placeHoldTtitle = @"请输入";
        _bxdlView.titleLabel.textAlignment = Left;
        [_bxdlView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _bxdlView.subTitleLabel.textAlignment = Center;
          [self addSubview:_bxdlView];
        
        _zwgglView = [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_zygglView), (WIDTH(self)-20)/2.0, 40)];
        _zwgglView.backgroundColor = Color_BG;
          _zwgglView.tipTitle = @"总无功功率：";
          _zwgglView.placeHoldTtitle = @"请输入";
        _zwgglView.leftMargin = 80;
        _zwgglView.titleLabel.textAlignment = Left;
        [_zwgglView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _zwgglView.subTitleLabel.textAlignment = Center;
        [self addSubview:_zwgglView];
        
        _cxdlView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_zwgglView), (WIDTH(self)-20)/2.0-10, 40)];
        _cxdlView.backgroundColor = Color_BG;
           _cxdlView.tipTitle = @"C相电流：";
        _cxdlView.leftMargin = 68;
        [_cxdlView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
           _cxdlView.placeHoldTtitle = @"请输入";
           _cxdlView.titleLabel.textAlignment = Left;
        _cxdlView.subTitleLabel.textAlignment = Center;
           [self addSubview:_cxdlView];
         
         _zygdlView = [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_zwgglView), (WIDTH(self)-20)/2.0, 40)];
        _zygdlView.backgroundColor = Color_BG;
           _zygdlView.tipTitle = @"总有功电量：";
           _zygdlView.placeHoldTtitle = @"请输入";
         _zygdlView.leftMargin = 80;
        [_zygdlView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
           _zygdlView.titleLabel.textAlignment = Left;
        _zygdlView.subTitleLabel.textAlignment = Center;
         [self addSubview:_zygdlView];
        
        _dxdlView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_zygdlView), (WIDTH(self)-20)/2.0-10, 40)];
        _dxdlView.backgroundColor = Color_BG;
           _dxdlView.tipTitle = @"单相电流：";
        _dxdlView.leftMargin = 68;
           _dxdlView.placeHoldTtitle = @"请输入";
           _dxdlView.titleLabel.textAlignment = Left;
        [_dxdlView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _dxdlView.subTitleLabel.textAlignment = Center;
           [self addSubview:_dxdlView];
         
        _zwgdlView = [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_zygdlView), (WIDTH(self)-20)/2.0, 40)];
        _zwgdlView.backgroundColor = Color_BG;
        _zwgdlView.tipTitle = @"总无功电量：";
        _zwgdlView.placeHoldTtitle = @"请输入";
        _zwgdlView.leftMargin = 80;
        _zwgdlView.titleLabel.textAlignment = Left;
        [_zwgdlView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _zwgdlView.subTitleLabel.textAlignment = Center;
         [self addSubview:_zwgdlView];
        
        _axdyView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_zwgdlView), (WIDTH(self)-20)/2.0-10, 40)];
        _axdyView.backgroundColor = Color_BG;
        _axdyView.leftMargin = 68;
        [_axdyView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
           _axdyView.tipTitle = @"A相电压：";
           _axdyView.placeHoldTtitle = @"请输入";
           _axdyView.titleLabel.textAlignment = Left;
        _axdyView.subTitleLabel.textAlignment = Center;
           [self addSubview:_axdyView];
         
         _cxdyView = [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_zwgdlView), (WIDTH(self)-20)/2.0, 40)];
        _cxdyView.backgroundColor = Color_BG;
        [_cxdyView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
           _cxdyView.tipTitle = @"C相电压：";
           _cxdyView.placeHoldTtitle = @"请输入";
         _cxdyView.leftMargin = 80;
           _cxdyView.titleLabel.textAlignment = Left;
        _cxdyView.subTitleLabel.textAlignment = Center;
         [self addSubview:_cxdyView];
        
        _bxdyView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_cxdyView), (WIDTH(self)-20)/2.0-10, 40)];
        _bxdyView.backgroundColor = Color_BG;
        _bxdyView.tipTitle = @"B相电压：";
        [_bxdyView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _bxdyView.leftMargin = 68;
        _bxdyView.placeHoldTtitle = @"请输入";
        _bxdyView.titleLabel.textAlignment = Left;
        _bxdyView.subTitleLabel.textAlignment = Center;
        [self addSubview:_bxdyView];
              
        _zglyzView = [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_cxdyView), (WIDTH(self)-20)/2.0, 40)];
        _zglyzView.backgroundColor = Color_BG;
        _zglyzView.tipTitle = @"总有功因数：";
        _zglyzView.placeHoldTtitle = @"请输入";
        [_zglyzView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _zglyzView.leftMargin = 80;
        _zglyzView.titleLabel.textAlignment = Left;
        [self addSubview:_zglyzView];
        
        _tqscView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_zglyzView), WIDTH(self)-20, 40)];
        _tqscView.backgroundColor = Color_BG;
        _tqscView.tipTitle = @"通气时长(用气设备)：";
        _tqscView.placeHoldTtitle = @"请输入";
        _tqscView.leftMargin = 140;
        [_tqscView.inputTextfield addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumberAndEnglish];
        _tqscView.titleLabel.textAlignment = Left;
        [self addSubview:_tqscView];
        
        CGRect rect  = self.frame;
        rect.size.height = BOTTOM(_tqscView);
        self.frame = rect;
        
    }
    return self;
}
- (void)sureButtonClick {
    if (_inputView.inputTextfield.text.length!=2) {
        [CommonToastHUD showTips:@"请输入因子编码后两位"];return;
    }
    if (_axdlView.inputTextfield.text.length==6) {
        _axdlView.inputTextfield.text = [_axdlView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_bxdlView.inputTextfield.text.length==6) {
         _bxdlView.inputTextfield.text = [_bxdlView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
     }
    if (_cxdlView.inputTextfield.text.length==6) {
        _cxdlView.inputTextfield.text = [_cxdlView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
     }
    if (_axdyView.inputTextfield.text.length==6) {
          _axdyView.inputTextfield.text = [_axdyView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_bxdyView.inputTextfield.text.length==6) {
           _bxdyView.inputTextfield.text = [_bxdyView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_cxdyView.inputTextfield.text.length==6) {
            _cxdyView.inputTextfield.text = [_cxdyView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_zygglView.inputTextfield.text.length==6) {
             _zygglView.inputTextfield.text = [_zygglView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_zwgglView.inputTextfield.text.length==6) {
              _zwgglView.inputTextfield.text = [_zwgglView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_zygdlView.inputTextfield.text.length==6) {
               _zygdlView.inputTextfield.text = [_zygdlView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_zwgdlView.inputTextfield.text.length==6) {
                _zwgdlView.inputTextfield.text = [_zwgdlView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_dxdlView.inputTextfield.text.length==6) {
                 _dxdlView.inputTextfield.text = [_dxdlView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_zglyzView.inputTextfield.text.length==6) {
                _zglyzView.inputTextfield.text = [_zglyzView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
    if (_tqscView.inputTextfield.text.length==6) {
                 _tqscView.inputTextfield.text = [_tqscView.inputTextfield.text stringByReplacingCharactersInRange:RANGE(4, 2) withString:_inputView.inputTextfield.text];
    }
}
- (void)setInfoWith:(NSDictionary *)result {
    _axdlView.inputTextfield.text = [CommonUtil fixNullText:result[@"aPhaseCurrent"]];
    _bxdlView.inputTextfield.text = [CommonUtil fixNullText:result[@"bPhaseCurrent"]];
    _cxdlView.inputTextfield.text =[CommonUtil fixNullText:result[@"cPhaseCurrent"]];
    _axdyView.inputTextfield.text = [CommonUtil fixNullText:result[@"aPhaseVoltage"]];
    _bxdyView.inputTextfield.text =[CommonUtil fixNullText:result[@"bPhaseVoltage"]];
    _cxdyView.inputTextfield.text =[CommonUtil fixNullText:result[@"cPhaseVoltage"]];
    _zygglView.inputTextfield.text =[CommonUtil fixNullText:result[@"activePower"]];
    _zwgglView.inputTextfield.text =[CommonUtil fixNullText:result[@"reactivePower"]];
    _zygdlView.inputTextfield.text = [CommonUtil fixNullText:result[@"activeEnergyValue"]];
    _zwgdlView.inputTextfield.text = [CommonUtil fixNullText:result[@"reactiveEnergyValue"]];
    _dxdlView.inputTextfield.text =[CommonUtil fixNullText:result[@"singleSequenceCurrent"]];
    _zglyzView.inputTextfield.text = [CommonUtil fixNullText:result[@"totalPowerFactor"]];
    _tqscView.inputTextfield.text =[CommonUtil fixNullText:result[@"throughAirTime"]];
    
//}else if (indexPath.item==1) {
//        label.text = [self backString:@"aPhaseCurrent" result:result];
//    }else if (indexPath.item==2){
//        label.text = [self backString:@"bPhaseCurrent" result:result];
//    }else if (indexPath.item==3){
//        label.text = [self backString:@"cPhaseCurrent" result:result];
//    }else if (indexPath.item==4){
//        label.text = [self backString:@"aPhaseVoltage" result:result];
//    }else if (indexPath.item==5){
//        label.text = [self backString:@"bPhaseVoltage" result:result];
//    }else if (indexPath.item==6){
//        label.text = [self backString:@"cPhaseVoltage" result:result];
//    }else if (indexPath.item==7){
//        label.text = [self backString:@"activePower" result:result];
//    }else if (indexPath.item==8) {
//        label.text = [self backString:@"reactivePower" result:result];
//    }else if (indexPath.item==9){
//        label.text = [self backString:@"totalPowerFactor" result:result];
//    }else if (indexPath.item==10){
//        label.text = [self backString:@"activeEnergyValue" result:result];
//    }else if (indexPath.item==11){
//        label.text = [self backString:@"reactiveEnergyValue" result:result];
//    }}
}
- (void)noEdit {
       _axdlView.inputTextfield.enabled = NO;
       _bxdlView.inputTextfield.enabled = NO;
       _cxdlView.inputTextfield.enabled = NO;
       _axdyView.inputTextfield.enabled = NO;
       _bxdyView.inputTextfield.enabled = NO;
       _cxdyView.inputTextfield.enabled = NO;
       _zygglView.inputTextfield.enabled = NO;
       _zwgglView.inputTextfield.enabled = NO;
       _zygdlView.inputTextfield.enabled = NO;
       _zwgdlView.inputTextfield.enabled = NO;
       _dxdlView.inputTextfield.enabled = NO;
       _zglyzView.inputTextfield.enabled = NO;
       _tqscView.inputTextfield.enabled = NO;
}
@end
