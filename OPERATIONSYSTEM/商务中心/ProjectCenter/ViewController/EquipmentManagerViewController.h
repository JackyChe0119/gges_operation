//
//  EquipmentManagerViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EquipmentManagerViewController : FYBaseViewController
@property (nonatomic,copy)NSString *Id;
@property (nonatomic,assign)BOOL noEdit;
@end

NS_ASSUME_NONNULL_END
