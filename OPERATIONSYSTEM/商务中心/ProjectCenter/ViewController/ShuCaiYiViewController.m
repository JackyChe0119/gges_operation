//
//  ShuCaiYiViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ShuCaiYiViewController.h"
#import "ShuCaiYiView.h"
#import "TaskMarkView.h"
@interface ShuCaiYiViewController ()
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)ShuCaiYiView *shucaiyiView;
@property (nonatomic,strong)TaskMarkView *markView;
@property (nonatomic,assign)NSInteger operationType;//操作类型
@property (nonatomic,strong)NSMutableArray *jyxyArray,*shucaiyiArray,*syztArray;
@end

@implementation ShuCaiYiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        [self setLeftButtonImage:BACKUP];
        [self layoutUI];
    }

#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    [self setNavTitle:@"数采仪配置" Color:Color_3D3A39];
    
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.showsHorizontalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [self.view addSubview:_baseScrollView];
    
    _shucaiyiView = [[ShuCaiYiView alloc]initWithFrame:RECT(0, 20, ScreenWidth, 200)];
    _shucaiyiView.scyxhView.operationBlock = ^{
        weakSelf.operationType = 0;
        [weakSelf requestScyxh];
    };
    _shucaiyiView.jrxyView.operationBlock = ^{
        weakSelf.operationType = 1;
        [weakSelf requestjrxy];
    };
    _shucaiyiView.syztView.operationBlock = ^{
        weakSelf.operationType = 2;
        [weakSelf requestsszt];
    };
    [_baseScrollView addSubview:_shucaiyiView];
    
    _markView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_shucaiyiView)+10, ScreenWidth, 200)];
    _markView.titleLabel.text = @"备注：";
    _markView.tipStr = @"数采仪相关配置的备注";
    [_baseScrollView addSubview:_markView];
    
    UIButton *xcCommitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0-80, BOTTOM(_markView)+30,160, 40) target:self action:@selector(commitButtonClick) title:@"保    存" color:Color_White font:17 type:2];
     [xcCommitButton addRoundedCornersWithRadius:5];
    [_baseScrollView addSubview:xcCommitButton];
    
    [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(xcCommitButton)+20)];
    if (!self.isAdd) {
        [self requestInstrumentDetail];
        if (self.noEdit) {
            xcCommitButton.hidden = YES;
            [self setStatus];
        }
    }
}

#pragma mark ---------->>> private
- (void)commitButtonClick {
    if ([_shucaiyiView.scyxhView.info isEqualToString:@"请选择"]) {
        [self showHUDMessage:@"请选择数采仪型号"];return;
    }
    if (_shucaiyiView.scymnmView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入数采仪MN码"];return;
    }
    if (_shucaiyiView.scymcView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入数采仪名称"];
    }
    if ([_shucaiyiView.jrxyView.info isEqualToString:@"请选择"]) {
        [self showHUDMessage:@"请选择接入协议"];return;
    }
    if ([_shucaiyiView.syztView.info isEqualToString:@"请选择"]) {
        [self showHUDMessage:@"请选择使用状态"];return;
    }
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[NSDictionary dictionaryWithObject:self.Id forKey:@"id"] forKey:@"company"];
    [params setValue:[NSDictionary dictionaryWithObject:_shucaiyiView.scyxhView.lineId forKey:@"id"] forKey:@"model"];
    [params setValue:[NSDictionary dictionaryWithObject:_shucaiyiView.jrxyView.lineId forKey:@"id"] forKey:@"protocol"];
    [params setValue:[NSDictionary dictionaryWithObject:_shucaiyiView.syztView.lineId forKey:@"id"] forKey:@"status"];
    [params setValue:_shucaiyiView.scymnmView.inputTextfield.text forKey:@"mn"];
    [params setValue:_shucaiyiView.scymcView.inputTextfield.text forKey:@"name"];
    if (![_markView.textView.text isEqualToString:@"数采仪相关配置的备注"]) {
        [params setValue:_markView.textView.text forKey:@"comment"];
    }
    NSString *url = [NSString stringWithFormat:@"basics/api/v1/data/unit/%@",self.unitId];
    if (self.isAdd) {
        url = @"basics/api/v1/data/unit/";
    }
    [RequestTool requestDataWithUrlSuffix:url method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                if (self.isAdd) {
                    [self showHUDMessage:@"保存成功"];
                }else {
                    [self showHUDMessage:@"编辑成功"];
                }
                if (_addBlock) {
                    _addBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestScyxh {//数采仪型号
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/code/category/010105" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self.shucaiyiArray removeAllObjects];
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    [self.shucaiyiArray addObject:dic[@"id"]];
                }];
                [self showActionSheetWithArray:nameArray title:@"数采仪型号"];

            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestjrxy {
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/code/category/010102" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self.jyxyArray removeAllObjects];
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    [self.jyxyArray addObject:dic[@"id"]];
                }];
                [self showActionSheetWithArray:nameArray title:@"接入协议"];

            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestsszt {
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/code/category/010103" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self.syztArray removeAllObjects];
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    [self.syztArray addObject:dic[@"id"]];
                }];
                [self showActionSheetWithArray:nameArray title:@"使用状态"];

            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestInstrumentDetail  {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"basics/api/v1/data/unit/%@",self.unitId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                [self layoutInfoWith:responseMessage.data[@"data"]];
            }else {
                [self.navigationController popViewControllerAnimated:YES];
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
#pragma mark ---------->>> 弹出视图
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    if (_operationType==0) {
        _shucaiyiView.scyxhView.info = title;
        _shucaiyiView.scyxhView.lineId  = self.shucaiyiArray[index];
    }else if (_operationType==1) {
        _shucaiyiView.jrxyView.info = title;
        _shucaiyiView.jrxyView.lineId  = self.jyxyArray[index];
    }else {
       _shucaiyiView.syztView.info = title;
       _shucaiyiView.syztView.lineId  = self.syztArray[index];
    }
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}
#pragma mark ---------->>> 懒加载
- (NSMutableArray *)shucaiyiArray {
    if (!_shucaiyiArray) {
        _shucaiyiArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _shucaiyiArray;
}
- (NSMutableArray *)jyxyArray {
    if (!_jyxyArray) {
        _jyxyArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _jyxyArray;
}
- (NSMutableArray *)syztArray {
    if (!_syztArray) {
        _syztArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _syztArray;
}
- (void)layoutInfoWith:(NSDictionary *)result {
    NSDictionary *model = result[@"model"];
    if (model&&[CommonUtil feiKong:model]) {
        _shucaiyiView.scyxhView.info = result[@"model"][@"name"];
        _shucaiyiView.scyxhView.lineId = result[@"model"][@"id"];
    }
    _shucaiyiView.scymnmView.inputTextfield.text = result[@"mn"];
    _shucaiyiView.scymcView.inputTextfield.text = result[@"name"];
    _shucaiyiView.jrxyView.info = result[@"protocol"][@"name"];
    _shucaiyiView.jrxyView.lineId = result[@"protocol"][@"id"];
    if ([CommonUtil feiKong:result[@"status"]]&&result[@"status"]) {
        _shucaiyiView.syztView.info = [CommonUtil fixNullText:result[@"status"][@"name"]];
        _shucaiyiView.syztView.lineId = [CommonUtil fixNullText:result[@"status"][@"id"]];
    }
    if (![result[@"comment"] isKindOfClass:[NSNull class]]) {
        _markView.textView.text = result[@"comment"];
    }
}
- (void)setStatus {
    _shucaiyiView.scyxhView.userInteractionEnabled = NO;
    _shucaiyiView.scymnmView.inputTextfield.enabled = NO;
    _shucaiyiView.scymcView.inputTextfield.enabled = NO;;
    _shucaiyiView.jrxyView.userInteractionEnabled = NO;
    _shucaiyiView.syztView.userInteractionEnabled = NO;
    [_markView setCanEdit:NO];
    _markView.textView.editable = NO;
}
@end
