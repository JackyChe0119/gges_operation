//
//  AddEquipmentViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddEquipmentViewController : FYBaseViewController
@property (nonatomic,assign)BOOL isAdd;
@property (nonatomic,assign)BOOL isShiSi;
@property (nonatomic,copy)NSString *Id;//公司id
@property (nonatomic,strong) void (^addBlock)(void);
@property (nonatomic,copy)NSString *equipmentId;//设备ID
@property (nonatomic,assign)BOOL noEdit;
@end

NS_ASSUME_NONNULL_END
