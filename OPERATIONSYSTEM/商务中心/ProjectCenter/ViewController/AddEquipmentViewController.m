//
//  AddEquipmentViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "AddEquipmentViewController.h"
#import "EquipmentInfoView.h"
#import "EquipmentDatailView.h"
#import "YWExcelView.h"
#import "AlertListSelectView.h"
#import "ShuCaiYiViewController.h"
#import "InstrumentModel.h"
#import "ChooseDatePickerView.h"
@interface AddEquipmentViewController ()<YWExcelViewDataSource,YWExcelViewDelegate,ChooseDatePickerViewDelegate>
@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)EquipmentInfoView *equipmentView;
@property (nonatomic,strong)EquipmentDatailView *detailView;
@property (nonatomic,strong)AlertListSelectView *timeView;
@property (nonatomic,strong)YWExcelView *exceView;
@property (nonatomic,assign)NSInteger operationType;//操作类型
@property (nonatomic,strong)NSMutableArray *dbxhArray,*sblxArray,*shucaiyiArray,*zylxArray;
@property (nonatomic,strong) UILabel *namelabel;
@property (nonatomic,strong)NSMutableArray *runTimeArray;
@property (nonatomic,assign)BOOL isLoad;

@end

@implementation AddEquipmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftButtonImage:BACKUP];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    _scrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
     _scrollView.showsVerticalScrollIndicator = NO;
     _scrollView.showsHorizontalScrollIndicator = NO;
     _scrollView.backgroundColor = Color_White;
     _scrollView.scrollEnabled = NO;
     [self.view addSubview:_scrollView];
    
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, 0, ScreenWidth, HEIGHT(_scrollView))];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.showsHorizontalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [_scrollView addSubview:_baseScrollView];
    
    _equipmentView = [[EquipmentInfoView alloc]initWithFrame:RECT(0, 0, ScreenWidth, 150)];
    _equipmentView.dbxhView.operationBlock = ^{
        weakSelf.operationType = 0;
        [weakSelf requestDbxh];
    };
    _equipmentView.sblxView.operationBlock = ^{
        weakSelf.operationType = 1;
        [weakSelf requestSblx];
    };
    _equipmentView.zyflView.operationBlock = ^{
        _operationType = 2;
        [weakSelf requestZylx:NO];
    };
    _equipmentView.ssscyView.operationBlock = ^{
        weakSelf.operationType = 3;
        [weakSelf requestssscy];
    };
    [_baseScrollView addSubview:_equipmentView];
    
    _detailView = [[EquipmentDatailView alloc]initWithFrame:RECT(0, BOTTOM(_equipmentView)+10, ScreenWidth, 300)];
    [_baseScrollView addSubview:_detailView];
    
    UIButton *xcCommitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0-80, BOTTOM(_detailView)+30,160, 40) target:self action:@selector(commitButtonClick) title:@"保    存" color:Color_White font:17 type:2];
     [xcCommitButton addRoundedCornersWithRadius:5];
    [_baseScrollView addSubview:xcCommitButton];
    [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(xcCommitButton)+20)];
    if (self.isAdd) {
        [self setNavTitle:@"新增设备" Color:Color_3D3A39];
    }else {
          
            [self requestZylx:YES];
        
            NSArray *array = [NSArray arrayWithObjects:@"基础数据",@"实时数据",nil];
            //初始化UISegmentedControl
            _segment = [[UISegmentedControl alloc]initWithItems:array];
            //设置frame
            _segment.frame = CGRectMake(ScreenWidth/2.0-80, StatusBarHeight+9, 160, 26);
            //添加到视图
            _segment.tintColor = Color_CommonLight;
            [_segment setSelectedSegmentIndex:0];
            [_segment addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
            [self.view addSubview:_segment];
     
            [_scrollView setContentSize:CGSizeMake(ScreenWidth*2, HEIGHT(_scrollView))];
            
            _namelabel = [UILabel new];
            [_namelabel rect:RECT(10+ScreenWidth, 10, ScreenWidth-20, 20) aligment:Left font:13 isBold:NO text:@"设备名称：--" textColor:Color_3D3A39 superView:_scrollView];
            
            _timeView = [[AlertListSelectView alloc]initWithFrame:RECT(10+ScreenWidth, 40, 200, 26)];
            _timeView.title = @"时间筛选：";
            _timeView.info = [CommonUtil getStringForDate:[NSDate date] format:@"yyyy-MM-dd"];
            _timeView.titleLabel.textColor = Color_3D3A39;
            _timeView.leftWidth = 65;
            _timeView.operationBlock = ^{
                ChooseDatePickerView *chooseDataPicker = [[ChooseDatePickerView alloc] initWithFrame:weakSelf.view.bounds];
                chooseDataPicker.delegate = weakSelf;
                chooseDataPicker.data = [NSDate date];//最大日期为当日
                [chooseDataPicker show];
            };
            [_scrollView addSubview:_timeView];
            
            YWExcelViewMode *mode = [YWExcelViewMode new];
            mode.style = YWExcelViewStyleDefalut;
            mode.headTexts = @[@"时间",@"A相电流",@"B相电流",@"C相电流",@"A相电压",@"B相电压",@"C相电压",@"总有功功率",@"总无功功率",@"总有功因数",@"总有功电量",@"总无功电量"];
            mode.defalutHeight = 30;
            //推荐使用这样初始化
            YWExcelView *exceView = [[YWExcelView alloc] initWithFrame:CGRectMake(ScreenWidth+10, 80, CGRectGetWidth(self.view.frame) - 20, HEIGHT(_scrollView)-80-SafeAreaBottomHeight) mode:mode];
            _exceView = exceView;
            exceView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            exceView.dataSource = self;
            exceView.showBorder = YES;
            [_scrollView addSubview:exceView];
        if (self.noEdit) {
            xcCommitButton.hidden = YES;
            [self setStatus];
        }
            
    }
}
#pragma mark ---------->>> private
- (void)commitButtonClick {
    if ([_equipmentView.dbxhView.info isEqualToString:@"请选择"]) {
        [self showHUDMessage:@"请选择电表型号"];return;
    }
    if (_equipmentView.sbmcView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入设备类型"];return;
    }
    if ([_equipmentView.sblxView.info isEqualToString:@"请选择"]) {
        [self showHUDMessage:@"请选择设备类型"];return;
    }
    if ([_equipmentView.zyflView.info isEqualToString:@"请选择"]) {
        [self showHUDMessage:@"请选择作用分类"];return;
    }
    if (_equipmentView.djglView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入待机功率"];return;
    }
    if ([_equipmentView.ssscyView.info isEqualToString:@"请选择"]) {
        [self showHUDMessage:@"请选择所属数采仪"];return;
    }
    NSMutableDictionary *equipment = [NSMutableDictionary dictionary];
    [equipment setValue:[NSDictionary dictionaryWithObject:_equipmentView.dbxhView.lineId forKey:@"id"] forKey:@"model"];
    [equipment setValue:_equipmentView.sbmcView.inputTextfield.text forKey:@"name"];
    [equipment setValue:self.Id forKey:@"companyId"];
    [equipment setValue:_equipmentView.sblxView.lineId forKey:@"type"];
    [equipment setValue:_equipmentView.zyflView.lineId forKey:@"effect"];
    [equipment setValue:_equipmentView.ssscyView.lineId forKey:@"unitId"];
    [equipment setValue:[NSNumber numberWithInteger:0] forKey:@"status"];

    NSMutableDictionary *equipmentFactor = [NSMutableDictionary dictionary];
    [equipmentFactor setValue:_detailView.axdlView.inputTextfield.text forKey:@"aPhaseCurrent"];
    [equipmentFactor setValue:_detailView.bxdlView.inputTextfield.text forKey:@"bPhaseCurrent"];
    [equipmentFactor setValue:_detailView.cxdlView.inputTextfield.text forKey:@"cPhaseCurrent"];
    [equipmentFactor setValue:_detailView.axdyView.inputTextfield.text forKey:@"aPhaseVoltage"];
    [equipmentFactor setValue:_detailView.bxdyView.inputTextfield.text forKey:@"bPhaseVoltage"];
    [equipmentFactor setValue:_detailView.cxdyView.inputTextfield.text forKey:@"cPhaseVoltage"];
    [equipmentFactor setValue:_detailView.zygglView.inputTextfield.text forKey:@"activePower"];
    [equipmentFactor setValue:_detailView.zwgglView.inputTextfield.text forKey:@"reactivePower"];
    [equipmentFactor setValue:_detailView.zygdlView.inputTextfield.text forKey:@"activeEnergyValue"];
    [equipmentFactor setValue:_detailView.zwgdlView.inputTextfield.text forKey:@"reactiveEnergyValue"];
    [equipmentFactor setValue:_detailView.dxdlView.inputTextfield.text forKey:@"singleSequenceCurrent"];
    [equipmentFactor setValue:_detailView.zglyzView.inputTextfield.text forKey:@"totalPowerFactor"];
    [equipmentFactor setValue:_detailView.tqscView.inputTextfield.text forKey:@"throughAirTime"];
    [equipmentFactor setValue:_equipmentView.djglView.inputTextfield.text forKey:@"standbyPower"];
    
    if (_equipmentView.edglView.inputTextfield.text.length>0) {
        [equipmentFactor setValue:_equipmentView.edglView.inputTextfield.text forKey:@"ratedPower"];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:equipment forKey:@"equipment"];
    [params setValue:equipmentFactor forKey:@"equipmentFactor"];
    NSString *url = @"basics/api/v1/equipment/dtu";
    if (_equipmentId) {
        url = [NSString stringWithFormat:@"basics/api/v1/equipment/%@",self.equipmentId];
    }
    [self showProgressHud];
    [RequestTool requestDataWithUrlSuffix:url method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                if (_equipmentId) {
                    [self showHUDMessage:@"编辑成功"];
                }else {
                    [self showHUDMessage:@"新增成功"];
                }
                if (_addBlock) {
                    _addBlock();
                }
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)change:(UISegmentedControl *)seg {
    [_scrollView setContentOffset:CGPointMake(ScreenWidth*seg.selectedSegmentIndex, 0) animated:YES];
    if (seg.selectedSegmentIndex==1) {
        if (!self.isLoad) {
               _isLoad = YES;
               [self requestRuntimeData];
           }
    }
}
//多少行
- (NSInteger)excelView:(YWExcelView *)excelView numberOfRowsInSection:(NSInteger)section{
    return self.runTimeArray.count;
}
//多少列
- (NSInteger)itemOfRow:(YWExcelView *)excelView{
    return 12;
}
- (void)excelView:(YWExcelView *)excelView label:(UILabel *)label textAtIndexPath:(YWIndexPath *)indexPath{
       NSDictionary *result = self.runTimeArray[indexPath.row];
        if (indexPath.item == 0) {
            label.text = result[@"dateTime"];
        }else if (indexPath.item==1) {
            label.text = [self backString:@"aPhaseCurrent" result:result];
        }else if (indexPath.item==2){
            label.text = [self backString:@"bPhaseCurrent" result:result];
        }else if (indexPath.item==3){
            label.text = [self backString:@"cPhaseCurrent" result:result];
        }else if (indexPath.item==4){
            label.text = [self backString:@"aPhaseVoltage" result:result];
        }else if (indexPath.item==5){
            label.text = [self backString:@"bPhaseVoltage" result:result];
        }else if (indexPath.item==6){
            label.text = [self backString:@"cPhaseVoltage" result:result];
        }else if (indexPath.item==7){
            label.text = [self backString:@"activePower" result:result];
        }else if (indexPath.item==8) {
            label.text = [self backString:@"reactivePower" result:result];
        }else if (indexPath.item==9){
            label.text = [self backString:@"totalPowerFactor" result:result];
        }else if (indexPath.item==10){
            label.text = [self backString:@"activeEnergyValue" result:result];
        }else if (indexPath.item==11){
            label.text = [self backString:@"reactiveEnergyValue" result:result];
        }}

- (void)requestDbxh {//电表型号
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/code/category/010106" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                [self.dbxhArray removeAllObjects];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    [self.dbxhArray addObject:dic[@"id"]];
                }];
                [self showActionSheetWithArray:nameArray title:@"电表型号"];

            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestZylx:(BOOL)first {//作用类型
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/code/category/010108" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                [self.zylxArray removeAllObjects];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    NSMutableDictionary *info = [NSMutableDictionary dictionary];
                    [info setObject:dic[@"name"] forKey:@"name"];
                    [info setObject:dic[@"id"] forKey:@"id"];
                    [self.zylxArray addObject:info];
                }];
                if (first) {
                    [self requestEquipmentDetail];
                }else {
                    [self hiddenProgressHud];
                    [self showActionSheetWithArray:nameArray title:@"作用类型"];
                }
            }else {
                [self hiddenProgressHud];
                [self showHUDMessage:responseMessage.errorMessage];
                if (first) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        });
    }];
}
- (void)requestSblx {//设备类型
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/equip/type/nameList" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                NSArray *array = responseMessage.responseData[@"body"];
                [self.sblxArray removeAllObjects];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    [self.sblxArray addObject:dic];
                }];
                [self showActionSheetWithArray:nameArray title:@"设备类型"];

            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestssscy {//数采仪列表
      [self showProgressHud];
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
        [params setValue:self.Id forKey:@"company.id"];
        [params setValue:[UserManager shareManager].Id forKey:@"userId"];
        [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/data/unit/all" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hiddenProgressHud];
                if ([responseMessage isRequestSuccessful]) {
                    [self.shucaiyiArray removeAllObjects];
                    NSDictionary *result = responseMessage.responseData;
                    NSArray *array = result[@"body"];
                    [_shucaiyiArray addObjectsFromArray:[InstrumentModel mj_objectArrayWithKeyValuesArray:array]];
                    if (array.count==0) {
                        [AlertViewUtil showSelectAlertViewWithVC:self Title:@"暂无数采仪信息" Message:@"当前企业暂无数采仪配置，如有需要请添加" LeftTitle:@"取消" RightTitle:@"添加" callBack:^(NSInteger type) {
                            if (type==2) {
                                ShuCaiYiViewController *vc = [ShuCaiYiViewController new];
                                vc.isAdd = YES;
                                vc.Id = self.Id;
                                [self pushWithViewController:vc hidden:YES];
                            }
                        }];
                    }else {
                        NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                        [_shucaiyiArray enumerateObjectsUsingBlock:^(InstrumentModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            [nameArray addObject:obj.name];
                        }];
                        [self showActionSheetWithArray:nameArray title:@"所属数采仪"];
                    }
                }else {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            });
        }];
}
- (void)requestEquipmentDetail  {//设备详情
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"basics/api/v1/equipment/%@",self.equipmentId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self layoutInfoWith:responseMessage.responseData[@"body"]];
            }else {
                [self.navigationController popViewControllerAnimated:YES];
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestRuntimeData {//实时数据
    [self hiddenProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"basics/api/v1/equipment/runData/%@/%@",self.equipmentId,_timeView.info] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self.runTimeArray removeAllObjects];
                NSDictionary *result = responseMessage.responseData;
                NSArray *array = result[@"body"];
                [self.runTimeArray addObjectsFromArray:array];
                [_exceView reloadData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
#pragma mark ---------->>> 弹出视图
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    if (_operationType==0) {
        _equipmentView.dbxhView.info = title;
        _equipmentView.dbxhView.lineId = self.dbxhArray[index];
    }else if (_operationType==1) {
        _equipmentView.sblxView.info = title;
        NSDictionary *result = self.sblxArray[index];
        _equipmentView.sblxView.lineId = result[@"id"];
        if (self.isAdd) {
            NSDictionary *dic = self.sblxArray[index];
            _equipmentView.djglView.inputTextfield.text = dic[@"standbyPower"];
            _equipmentView.edglView.inputTextfield.text = dic[@"ratedPower"];
            [_detailView setInfoWith:self.sblxArray[index]];
        }
    }else if (_operationType==2) {
        _equipmentView.zyflView.info = title;
        _equipmentView.zyflView.lineId = self.zylxArray[index][@"id"];
        NSLog(@"id=====%@",_equipmentView.zyflView.lineId);
    }else {
        _equipmentView.ssscyView.info = title;
        InstrumentModel *model = self.shucaiyiArray[index];
        _equipmentView.ssscyView.lineId = model.Id;
    }
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}
#pragma mark ---------->>> 懒加载
- (NSMutableArray *)dbxhArray {
    if (!_dbxhArray) {
        _dbxhArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _dbxhArray;
}
- (NSMutableArray *)sblxArray {
    if (!_sblxArray) {
        _sblxArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _sblxArray;
}
- (NSMutableArray *)shucaiyiArray {
    if (!_shucaiyiArray) {
        _shucaiyiArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _shucaiyiArray;
}
- (NSMutableArray *)runTimeArray {
    if (!_runTimeArray) {
        _runTimeArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _runTimeArray;
}
- (NSMutableArray *)zylxArray {
    if (!_zylxArray) {
        _zylxArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _zylxArray;
}
- (void)layoutInfoWith:(NSDictionary *)result {
    NSDictionary *model = result[@"equipment"][@"model"];
    if (model&&[CommonUtil feiKong:model]) {
        _equipmentView.dbxhView.info = result[@"equipment"][@"model"][@"name"];
         _equipmentView.dbxhView.lineId = result[@"equipment"][@"model"][@"id"];
    }
    _equipmentView.sbmcView.inputTextfield.text = result[@"equipment"][@"name"];
    _namelabel.text = [NSString stringWithFormat:@"设备名称：%@",result[@"equipment"][@"name"]];
    _equipmentView.sblxView.info = result[@"equipment"][@"typeName"];
    _equipmentView.sblxView.lineId = result[@"equipment"][@"type"];
    [self.zylxArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj[@"id"] integerValue]==[result[@"equipment"][@"effect"] integerValue]) {
             _equipmentView.zyflView.info = obj[@"name"];
             _equipmentView.zyflView.lineId = obj[@"id"];
        }
    }];
    NSString *unitName = result[@"equipment"][@"unitName"];
    if ([CommonUtil feiKong:unitName]) {
        _equipmentView.ssscyView.info = unitName;
        _equipmentView.ssscyView.lineId = result[@"equipment"][@"unitId"];
    }else {
        _equipmentView.ssscyView.info = @"请选择";
        _equipmentView.ssscyView.lineId = @"0";
    }
    _equipmentView.djglView.inputTextfield.text = result[@"equipmentFactor"][@"standbyPower"];
    NSString *ratedPower =result[@"equipmentFactor"][@"ratedPower"];
    if (ratedPower&&![ratedPower isKindOfClass:[NSNull class]]) {
        _equipmentView.edglView.inputTextfield.text = result[@"equipmentFactor"][@"ratedPower"];
    }
    [_detailView setInfoWith:result[@"equipmentFactor"]];

}
- (NSString *)backString:(NSString *)name result:(NSDictionary *)result{
    if (![result[name] isKindOfClass:[NSNull class]]) {
        return result[name];
    }else {
        return @"-";
    }
}
- (void)setStatus {
    _equipmentView.dbxhView.userInteractionEnabled = NO;
    _equipmentView.sbmcView.inputTextfield.enabled = NO;
    _equipmentView.sblxView.userInteractionEnabled = NO;
    _equipmentView.zyflView.userInteractionEnabled = NO;
    _equipmentView.djglView.inputTextfield.enabled = NO;
    _equipmentView.edglView.inputTextfield.enabled = NO;
    _equipmentView.ssscyView.userInteractionEnabled = NO;
    [_detailView noEdit];
}
- (void)finishSelectDate:(NSDate *)date {
    _timeView.info = [CommonUtil getStringForDate:date format:@"yyyy-MM-dd"];
    [self requestRuntimeData];
}
@end
