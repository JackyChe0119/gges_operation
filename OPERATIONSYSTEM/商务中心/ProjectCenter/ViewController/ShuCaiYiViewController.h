//
//  ShuCaiYiViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShuCaiYiViewController : FYBaseViewController
@property (nonatomic,assign)BOOL isAdd;
@property (nonatomic,strong) void (^addBlock)(void);
@property (nonatomic,copy)NSString *Id;//公司id
@property (nonatomic,copy)NSString *unitId;//数采仪Id
@property (nonatomic,assign)BOOL noEdit;
@end

NS_ASSUME_NONNULL_END
