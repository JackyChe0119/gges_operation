//
//  ProjectDeatilViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProjectDeatilViewController : FYBaseViewController
@property (nonatomic,assign)BOOL noEdit;
@property (nonatomic,assign)BOOL isFromFZR;
@property (nonnull,copy)NSString *taskId;
@end

NS_ASSUME_NONNULL_END
