//
//  EquipmentManagerViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "EquipmentManagerViewController.h"
#import "AlertListSelectView.h"
#import "SearchView.h"
#import "EquipmentListCell.h"
#import "AddEquipmentViewController.h"
#import "ShuCaiYiViewController.h"
#import "InstrumentModel.h"
#import "EquipmentModel.h"
static NSString * const EQUIPMENT_LIST = @"EquipmentListCell";

@interface EquipmentManagerViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)AlertListSelectView *areaView;
@property (nonatomic,strong)AlertListSelectView *taskTypeView;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)UITableView *scyTableView;
@property (nonatomic,strong)SearchView *searchView;
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)NSMutableArray *equipmentArray;
@property (nonatomic,strong)NSMutableArray *shucaiyiArray;
@property (nonatomic,strong)NSMutableArray *sblxArray;
@property (nonatomic,strong)NSMutableArray *scyArray;
@property (nonatomic,assign)NSInteger operationType;
@property (nonatomic,assign)BOOL isShucaiyi;//是否是数采仪界面
@property (nonatomic,assign)BOOL isLoad;
@end

@implementation EquipmentManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftButtonImage:BACKUP];
    [self layoutUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_isShucaiyi) {
        [_baseScrollView setContentOffset:CGPointMake(ScreenWidth*1, 0)];
    }
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    
    __weak typeof(self)weakSelf = self;
    
    NSArray *array = [NSArray arrayWithObjects:@"数采仪配置",@"设备清单",nil];
        //初始化UISegmentedControl
    _segment = [[UISegmentedControl alloc]initWithItems:array];
        //设置frame
    _segment.frame = CGRectMake(ScreenWidth/2.0-90, StatusBarHeight+9, 180, 26);
        //添加到视图
    _segment.tintColor = Color_CommonLight;
    [_segment setSelectedSegmentIndex:0];
    [_segment addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_segment];
    
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.scrollEnabled = NO;
    _baseScrollView.showsHorizontalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [self.view addSubview:_baseScrollView];
           
    [self addLineViewWithFrame:RECT(ScreenWidth, 0, ScreenWidth, 4) Color:Color_BG supView:_baseScrollView];
       
    _searchView = [[SearchView alloc]initWithFrame:RECT(ScreenWidth, 8, ScreenWidth, 40) type:1];
    _searchView.placeHold = @"请输入搜索关键字";
    _searchView.searchBlock = ^(NSString * _Nonnull search) {
        [weakSelf.tableView resetPageNum:1];
        [weakSelf requestEquipmentList:1 show:YES];
    };
    [_baseScrollView addSubview:_searchView];
       
    _areaView = [[AlertListSelectView alloc]initWithFrame:RECT(ScreenWidth, BOTTOM(_searchView), ScreenWidth/2.0, 40)];
    _areaView.title = @"设备类型：";
    _areaView.info = @"全部";
    _areaView.operationBlock = ^{
        weakSelf.operationType = 0;
        [weakSelf requestSblx];
    };
    [_baseScrollView addSubview:_areaView];
       
    _taskTypeView = [[AlertListSelectView alloc]initWithFrame:RECT(ScreenWidth/2.0+ScreenWidth, BOTTOM(_searchView), ScreenWidth/2.0, 40)];
    _taskTypeView.title = @"所属数采仪：";
    _taskTypeView.info = @"全部";
    _taskTypeView.leftWidth = 80;
    _taskTypeView.operationBlock = ^{
        weakSelf.operationType = 1;
        [weakSelf requestssscy];
    };
    [_baseScrollView addSubview:_taskTypeView];
       
    [self addLineViewWithFrame:RECT(ScreenWidth, BOTTOM(_taskTypeView), ScreenWidth, 4) Color:Color_BG supView:_baseScrollView];
    
    _tableView = [[UITableView alloc]initWithFrame:RECT(ScreenWidth, BOTTOM(_taskTypeView)+4, ScreenWidth, HEIGHT(_baseScrollView)-BOTTOM(_taskTypeView)-4) style:0];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 64;
    _tableView.noDataContent = @"暂无设备清单";
    _tableView.marginTop = HEIGHT(_tableView)/2.0-80;
    [_tableView setSeparatorInset:UIEdgeInsetsMake(10, 0, 0, 0 )];
    [_tableView registerNib:[UINib nibWithNibName:@"EquipmentListCell" bundle:nil] forCellReuseIdentifier:EQUIPMENT_LIST];
    _tableView.showsVerticalScrollIndicator = NO;
    [_baseScrollView addSubview:_tableView];
    
    [_tableView addHeaderRefreshWithAutomaticallyRefresh:YES refreshBlock:^(NSInteger pageIndex) {
          [weakSelf requestEquipmentList:pageIndex show:NO];
      }];
      [_tableView addFootLoadMoreWithAutomaticallyLoad:YES loadMoreBlock:^(NSInteger pageIndex) {
          if (weakSelf.noMoreData) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [weakSelf showHUDMessage:@"暂无更多数据了"];
                  [weakSelf.tableView endFootLoadMore];
              });
              return;
          }
          [weakSelf requestEquipmentList:pageIndex show:NO];
      }];
    
    _scyTableView = [[UITableView alloc]initWithFrame:RECT(0, 0, ScreenWidth, HEIGHT(_baseScrollView)) style:0];
    _scyTableView.delegate = self;
    _scyTableView.dataSource = self;
    _scyTableView.rowHeight = 64;
    _scyTableView.noDataContent = @"暂无数采仪";
    _scyTableView.marginTop = HEIGHT(_tableView)/2.0-80;
    [_scyTableView setSeparatorInset:UIEdgeInsetsMake(10, 0, 0, 0 )];
    [_scyTableView registerNib:[UINib nibWithNibName:@"EquipmentListCell" bundle:nil] forCellReuseIdentifier:EQUIPMENT_LIST];
    _scyTableView.showsVerticalScrollIndicator = NO;
    [_baseScrollView addSubview:_scyTableView];
    
    [_scyTableView addHeaderRefreshWithAutomaticallyRefresh:YES refreshBlock:^(NSInteger pageIndex) {
          [weakSelf requestShuCaiYiList:pageIndex show:NO];
      }];
      [_scyTableView addFootLoadMoreWithAutomaticallyLoad:YES loadMoreBlock:^(NSInteger pageIndex) {
          if (weakSelf.noMoreData) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [weakSelf showHUDMessage:@"暂无更多数据了"];
                  [weakSelf.scyTableView endFootLoadMore];
              });
              return;
          }
          [weakSelf requestShuCaiYiList:pageIndex show:NO];
      }];
    [weakSelf requestShuCaiYiList:1 show:YES];

    if (!self.noEdit) { //不可添加状态
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *rightImage = [UIImage imageNamed:@"icon_add"];
        _rightButton.frame = CGRectMake(ScreenWidth-49, StatusBarHeight,44, 44);
        [_rightButton setImage:rightImage forState:UIControlStateNormal];
        [_rightButton addTarget:self action:@selector(navgationRightButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.customNavView addSubview:_rightButton];
    }
}
#pragma mark ---------->>> PRIVATE
- (void)change:(UISegmentedControl *)seg {
    [_baseScrollView setContentOffset:CGPointMake(ScreenWidth*seg.selectedSegmentIndex, 0) animated:YES];
    if (seg.selectedSegmentIndex==1) {
        if (!self.isLoad) {
            _isLoad = YES;
            [self requestEquipmentList:1 show:YES];
        }
    }
    _isShucaiyi = seg.selectedSegmentIndex;
}
- (void)navgationRightButtonClick {
    if (_segment.selectedSegmentIndex==1) {
        AddEquipmentViewController *vc = [AddEquipmentViewController new];
        vc.isAdd = YES;
        vc.Id = self.Id;
        vc.addBlock = ^{
            [_tableView resetPageNum:1];
            [self requestEquipmentList:1 show:YES];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        ShuCaiYiViewController *vc = [ShuCaiYiViewController new];
        vc.isAdd = YES;
        vc.Id = self.Id;
        vc.addBlock = ^{
            [_scyTableView resetPageNum:1];
            [self requestShuCaiYiList:1 show:YES];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark ---------->>> delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==_tableView) {
        return self.equipmentArray.count;
    }else {
        return self.shucaiyiArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EquipmentListCell *cell = [tableView dequeueReusableCellWithIdentifier:EQUIPMENT_LIST];
    if (tableView==_scyTableView) {
        InstrumentModel *model = self.shucaiyiArray[indexPath.row];
        cell.titleLabel.text = [NSString stringWithFormat:@"数采仪名称：%@",model.name];
        if (model.model) {
            cell.typeLabel.text = [NSString stringWithFormat:@"数采仪型号：%@",model.model[@"name"]];
        }else {
            cell.typeLabel.text = @"数采仪型号：--";
        }
        cell.suoshuLabel.text = [NSString stringWithFormat:@"MN码：%@",model.mn];
    }else {
        EquipmentModel *model = self.equipmentArray[indexPath.row];
        cell.titleLabel.text = [NSString stringWithFormat:@"设备名称：%@",model.name];
        if ([CommonUtil feiKong:model.type]) {
            cell.typeLabel.text = [NSString stringWithFormat:@"设备类型：%@",model.type];
        }else {
            cell.typeLabel.text = [NSString stringWithFormat:@"设备类型：%@",@"--"];
        }
        cell.suoshuLabel.text = [NSString stringWithFormat:@"所属数采仪：%@",model.unitName];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
      if (tableView==_tableView) {
          EquipmentModel *model = self.equipmentArray[indexPath.row];
          AddEquipmentViewController *vc = [AddEquipmentViewController new];
          vc.Id = self.Id;
          vc.equipmentId = model.Id;
          vc.addBlock = ^{
            [_tableView resetPageNum:1];
            [self requestEquipmentList:1 show:YES];
          };
          vc.noEdit = self.noEdit;
          [self.navigationController pushViewController:vc animated:YES];
    }else {
        InstrumentModel *model = self.shucaiyiArray[indexPath.row];
        ShuCaiYiViewController *vc = [ShuCaiYiViewController new];
        vc.unitId = model.Id;
        vc.Id = self.Id;
        vc.addBlock = ^{
              [_scyTableView resetPageNum:1];
              [self requestShuCaiYiList:1 show:YES];
        };
        vc.noEdit = self.noEdit;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.noEdit) {
        return NO;
    }else{
        return YES;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}
//修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}
//点击删除
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //在这里实现删除操作
    if (tableView == _tableView) {
        [AlertViewUtil showSelectAlertViewWithVC:self Title:@"删除设备" Message:@"删除后该设备将无法查看，是否确定删除？" LeftTitle:@"确定" RightTitle:@"取消" callBack:^(NSInteger type) {
               if (type==1) {
                   [self showProgressHud];
                   EquipmentModel *model = self.equipmentArray[indexPath.row];
                   NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
                   [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"basics/api/v1/equipment/%@",model.Id] method:DELETE params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [self hiddenProgressHud];
                           if ([responseMessage isRequestSuccessful]) {
                               [self showHUDMessage:@"删除成功"];
                               [self.equipmentArray removeObjectAtIndex:indexPath.row];
                               _tableView.dataArray = _equipmentArray;
                               [_tableView reloadWithData];
                           }else {
                               [self showHUDMessage:responseMessage.errorMessage];
                           }
                       });
                   }];
               }
           }];
    }else {
        [AlertViewUtil showSelectAlertViewWithVC:self Title:@"删除该数采仪户" Message:@"删除后该数采仪将无法查看，是否确定删除？" LeftTitle:@"确定" RightTitle:@"取消" callBack:^(NSInteger type) {
               if (type==1) {
                   [self showProgressHud];
                   InstrumentModel *model = self.shucaiyiArray[indexPath.row];
                   NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
                   [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"basics/api/v1/data/unit/%@",model.Id] method:DELETE params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [self hiddenProgressHud];
                           if ([responseMessage isRequestSuccessful]) {
                               [self showHUDMessage:@"删除成功"];
                               [self.shucaiyiArray removeObjectAtIndex:indexPath.row];
                               _scyTableView.dataArray = _shucaiyiArray;
                               [_scyTableView reloadWithData];
                           }else {
                               [self showHUDMessage:responseMessage.errorMessage];
                           }
                       });
                   }];
               }
           }];
      }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, .5) color:Color_Line];
}
#pragma mark ---------->>> request
- (void)requestEquipmentList:(NSInteger)pageNum show:(BOOL)show {
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:self.Id forKey:@"companyId"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    [params setValue:[NSNumber numberWithInteger:pageNum] forKey:@"offset"];
    [params setValue:[NSNumber numberWithInteger:20] forKey:@"limit"];
    if (![_areaView.info isEqualToString:@"全部"]) {
        [params setValue:_areaView.lineId forKey:@"type"];
    }
    if (![_taskTypeView.info isEqualToString:@"全部"]) {
        [params setValue:_taskTypeView.lineId forKey:@"unitId"];
    }
    if (_searchView.searchTextField.text.length > 0) {
        [params setValue:_searchView.searchTextField.text forKey:@"name"];
    }
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/equipment/" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }else {
                [_tableView endLoading];
            }
            if ([responseMessage isRequestSuccessful]) {
                    if (pageNum==1) {
                        [self.equipmentArray removeAllObjects];
                    }
                    NSDictionary *result = responseMessage.data;
                    NSArray *array = result[@"data"];
                    [_equipmentArray addObjectsFromArray:[EquipmentModel mj_objectArrayWithKeyValuesArray:array]];
                    if (array.count==0) {
                    [_tableView resetPageNum:pageNum];
                        self.noMoreData = YES;
                    }else {
                        self.noMoreData = NO;
                    }
                    _tableView.dataArray = _equipmentArray;
                    [_tableView reloadWithData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestShuCaiYiList:(NSInteger)pageNum show:(BOOL)show {
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:self.Id forKey:@"company.id"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    [params setValue:[NSNumber numberWithInteger:pageNum] forKey:@"offset"];
    [params setValue:[NSNumber numberWithInteger:20] forKey:@"limit"];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/data/unit/" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }else {
                [_scyTableView endLoading];
            }
            if ([responseMessage isRequestSuccessful]) {
                    if (pageNum==1) {
                        [self.shucaiyiArray removeAllObjects];
                    }
                    NSDictionary *result = responseMessage.data;
                    NSArray *array = result[@"data"];
                    [_shucaiyiArray addObjectsFromArray:[InstrumentModel mj_objectArrayWithKeyValuesArray:array]];
                    if (array.count==0) {
                    [_tableView resetPageNum:pageNum];
                        self.noMoreData = YES;
                    }else {
                        self.noMoreData = NO;
                    }
                    _scyTableView.dataArray = _shucaiyiArray;
                    [_scyTableView reloadWithData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (NSMutableArray *)equipmentArray {
    if (!_equipmentArray) {
        _equipmentArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _equipmentArray;
}
- (NSMutableArray *)shucaiyiArray {
    if (!_shucaiyiArray) {
        _shucaiyiArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _shucaiyiArray;
}
- (NSMutableArray *)sblxArray {
    if (!_sblxArray) {
        _sblxArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _sblxArray;
}
- (NSMutableArray *)scyArray {
    if (!_scyArray) {
        _scyArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _scyArray;
}
- (void)requestSblx {//设备类型
    _operationType = 0;
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/base/equip/type/nameList" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                NSArray *array = responseMessage.responseData[@"body"];
                NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                [array enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL * _Nonnull stop) {
                    [nameArray addObject:dic[@"name"]];
                    [self.sblxArray addObject:dic];
                }];
                [self showActionSheetWithArray:nameArray title:@"设备类型"];

            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestssscy {//数采仪列表
      [self showProgressHud];
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
        [params setValue:self.Id forKey:@"company.id"];
        [params setValue:[UserManager shareManager].Id forKey:@"userId"];
        [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/data/unit/all" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hiddenProgressHud];
                if ([responseMessage isRequestSuccessful]) {
                    [self.scyArray removeAllObjects];
                    NSDictionary *result = responseMessage.responseData;
                    NSArray *array = result[@"body"];
                    [_scyArray addObjectsFromArray:[InstrumentModel mj_objectArrayWithKeyValuesArray:array]];
                    if (array.count==0) {
                        [AlertViewUtil showSelectAlertViewWithVC:self Title:@"暂无数采仪信息" Message:@"当前企业暂无数采仪配置，如有需要请添加" LeftTitle:@"取消" RightTitle:@"添加" callBack:^(NSInteger type) {
                            if (type==2) {
                                ShuCaiYiViewController *vc = [ShuCaiYiViewController new];
                                vc.isAdd = YES;
                                vc.Id = self.Id;
                                [self pushWithViewController:vc hidden:YES];
                            }
                        }];
                    }else {
                        NSMutableArray *nameArray = [NSMutableArray arrayWithCapacity:0];
                        [_scyArray enumerateObjectsUsingBlock:^(InstrumentModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            [nameArray addObject:obj.name];
                        }];
                        [self showActionSheetWithArray:nameArray title:@"所属数采仪"];
                    }
                }else {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            });
        }];
}
#pragma mark ---------->>> 弹出视图
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    if (_operationType==0) {
        _areaView.info = title;
        NSDictionary *dic = self.sblxArray[index];
        _areaView.lineId = dic[@"id"];
    }else if (_operationType==1) {
        _taskTypeView.info = title;
        InstrumentModel *model = self.scyArray[index];
        _taskTypeView.lineId = model.Id;
    }
    [_tableView resetPageNum:1];
    [self requestEquipmentList:1 show:YES];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}
@end
