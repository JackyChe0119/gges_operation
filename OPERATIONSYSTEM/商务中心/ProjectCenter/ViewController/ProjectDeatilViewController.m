//
//  ProjectDeatilViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ProjectDeatilViewController.h"
#import "TaskCompanyInfoView.h"
#import "LocationView.h"
#import "TaskInfoView.h"
#import "TaskServiceView.h"
#import "ProjectTotalView.h"
#import "ProductInfoView.h"
#import "TaskMarkView.h"
#import "ImageUploadView.h"
#import "EquipmentManagerViewController.h"
@interface ProjectDeatilViewController ()
@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)UIScrollView *xcScrollView;
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)TaskCompanyInfoView *companyInfoView;
@property (nonatomic,strong)LocationView *locationView;
@property (nonatomic,strong)TaskInfoView *taskView;
@property (nonatomic,strong)TaskServiceView *serviceView;
@property (nonatomic,strong)ProjectTotalView *totalView;

@property (nonatomic,strong)ProductInfoView *productView;
@property (nonatomic,strong)ImageUploadView *dwfhtView;//点位复核图
@property (nonatomic,strong)ImageUploadView *czwgxtView;//产治污关系图
@property (nonatomic,strong)ImageUploadView *czwsjwdView;//产治污设计文档
@property (nonatomic,strong)TaskMarkView *scgyView;//生产工艺
@property (nonatomic,strong)TaskMarkView *zywrwView;//主要污染物
@property (nonatomic,strong)TaskMarkView *zysbView;//主要设备
@property (nonatomic,strong)LocationView *xcLocationView;
@property (nonatomic,assign)BOOL isloadXCInfo;
@end

@implementation ProjectDeatilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftButtonImage:BACKUP];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    
    [self navgationRightButtonTitle:@"设备管理" color:Color_3D3A39];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.backgroundColor = Color_White;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.scrollEnabled = NO;
    [_scrollView setContentSize:CGSizeMake(ScreenWidth*2, HEIGHT(_scrollView))];
    [self.view addSubview:_scrollView];
    
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, 0, ScreenWidth, ScreenHeight-NavHeight)];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [_scrollView addSubview:_baseScrollView];
    
        //先生成存放标题的数据
    NSArray *array = [NSArray arrayWithObjects:@"任务概况",@"现场概况",nil];
        //初始化UISegmentedControl
    _segment = [[UISegmentedControl alloc]initWithItems:array];
        //设置frame
    _segment.frame = CGRectMake(ScreenWidth/2.0-80, StatusBarHeight+9, 160, 26);
        //添加到视图
    _segment.tintColor = Color_CommonLight;
    [_segment setSelectedSegmentIndex:0];
    [_segment addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_segment];

    _taskView = [[TaskInfoView alloc]initWithFrame:RECT(0, 0, ScreenWidth, 240)];
    [_baseScrollView addSubview:_taskView];
    
    _totalView = [[ProjectTotalView alloc]initWithFrame:RECT(0, BOTTOM(_taskView)+10, ScreenWidth, 90)];
    _totalView.swfzr = @"商务负责人：-- ";
    _totalView.ssfzr = @"实施负责人：-- ";
    _totalView.ywfzr = @"运维负责人：-- ";
    _totalView.hkzt = @"回款状态：--";
    _totalView.sszt = @"实施状态：--";
    _totalView.ywzt = @"运维状态：--";
    [_baseScrollView addSubview:_totalView];
    
    _serviceView = [[TaskServiceView alloc]initWithFrame:RECT(10, BOTTOM(_totalView)+10, ScreenWidth-20, 300)];
    [_serviceView hiddenHTJE];//隐藏合同金额
    [_baseScrollView addSubview:_serviceView];
    
    _companyInfoView = [[TaskCompanyInfoView alloc]initWithFrame:RECT(0, BOTTOM(_serviceView)+10, ScreenWidth, 500) type:2];
    [_baseScrollView addSubview:_companyInfoView];
        
    _locationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(_companyInfoView), ScreenWidth, 340)];
    [_baseScrollView addSubview:_locationView];
        
    [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(_locationView)+20)];

    //现场概况
    _xcScrollView = [[UIScrollView alloc]initWithFrame:RECT(ScreenWidth, 0, ScreenWidth, ScreenHeight-NavHeight)];
    _xcScrollView.showsVerticalScrollIndicator = NO;
    _xcScrollView.backgroundColor = Color_White;
    [self.view addSubview:_xcScrollView];
    [_scrollView addSubview:_xcScrollView];
    
    _productView = [[ProductInfoView alloc]initWithFrame:RECT(0, 10, ScreenWidth, 240)];
    if (self.noEdit) {
        [_productView hiddenBottomView];
    }
    [_xcScrollView addSubview:_productView];
    
    _dwfhtView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_productView)+10, ScreenWidth, 140)  max:9];
    _dwfhtView.titleLabel.text = @"*点位复核图：";
    [_xcScrollView addSubview:_dwfhtView];
    
    _czwgxtView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_dwfhtView), ScreenWidth, 140) max:9];
    _czwgxtView.titleLabel.text = @"*产治污关系图：";
    [_xcScrollView addSubview:_czwgxtView];
    
    _czwsjwdView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_czwgxtView), ScreenWidth, 140) max:30];
    _czwsjwdView.titleLabel.text = @"产治污设计文档：";
    [_xcScrollView addSubview:_czwsjwdView];
    
    _scgyView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_czwsjwdView)+10, ScreenWidth, 150)];
    _scgyView.titleLabel.text = @"生产工艺：";
    [_xcScrollView addSubview:_scgyView];
    
    _zywrwView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_scgyView)+10, ScreenWidth, 150)];
    _zywrwView.titleLabel.text = @"主要污染物：";
    [_xcScrollView addSubview:_zywrwView];
    
    _zysbView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_zywrwView)+10, ScreenWidth, 150)];
    _zysbView.titleLabel.text = @"主要设备：";
    [_xcScrollView addSubview:_zysbView];
    
   UILabel *titleLabel = [UILabel new];
   [titleLabel rect:RECT(10, BOTTOM(_zysbView)+10, 100, 20) aligment:Left font:15 isBold:NO text:@"地址复核：" textColor:Color_333333 superView:_xcScrollView];
    
    _xcLocationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(titleLabel), ScreenWidth, 340)];
    [_xcScrollView addSubview:_xcLocationView];
    
    [_xcScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(_xcLocationView)+20)];
    
    [self setEditStatus];
    [self requestDetail];
    
}
#pragma mark ---------->>> method
- (void)change:(UISegmentedControl *)seg {
    [_scrollView setContentOffset:CGPointMake(ScreenWidth*seg.selectedSegmentIndex, 0) animated:YES];
    if (!_isloadXCInfo) {
        [self requestXcDetail];
    }
}
- (void)navgationRightButtonClick {
    EquipmentManagerViewController *vc = [EquipmentManagerViewController new];
    vc.Id = _companyInfoView.qymcView.lineId;
    if (!_isFromFZR) {
        vc.noEdit = YES;
    }
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)requestXcDetail {
    [self showProgressHud];
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/survey/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [self hiddenProgressHud];
               if ([responseMessage isRequestSuccessful]) {
                   self.isloadXCInfo = YES;
                   NSDictionary *result = responseMessage.data[@"data"];
                   [self relayoutWithXcDeatil:result];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
                   [self.navigationController popViewControllerAnimated:YES];
               }
           });
       }];
}
#pragma mark ---------->>> request
- (void)requestDetail {
    [self showProgressHud];
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [self hiddenProgressHud];
               if ([responseMessage isRequestSuccessful]) {
                   NSDictionary *result = responseMessage.data[@"data"];
                   [self relayoutWithDeatil:result];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
                   [self.navigationController popViewControllerAnimated:YES];
               }
           });
       }];
}
- (void)relayoutWithDeatil:(NSDictionary *)result {
    _companyInfoView.qymcView.info= result[@"companyName"];
    _companyInfoView.qymcView.lineId= result[@"companyId"];
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.tyxydmView.inputTextfield.text = result[@"creditCode"];
    _companyInfoView.fzView.inputTextfield.text = result[@"legalRepresentative"];
    _companyInfoView.lxrView.inputTextfield.text = result[@"linkman"];
    _companyInfoView.sjhmView.inputTextfield.text = result[@"linkmanPhone"];
    _companyInfoView.bylxrView.inputTextfield.text = result[@"linkmanTwo"];
    _companyInfoView.bysjhmView.inputTextfield.text = result[@"linkmanTwoPhone"];
    _companyInfoView.xzqhView.info = result[@"divisionName"];
    _companyInfoView.xzqhView.lineId = result[@"division"];
    _companyInfoView.hylbView.info = result[@"industryClass"];
    _companyInfoView.pwxkzView.inputTextfield.text = result[@"licenseNumber"];
    _companyInfoView.pwjyhView.inputTextfield.text = result[@"emissionTradingDocuments"];
    _companyInfoView.hppfView.inputTextfield.text = result[@"eiaBatchNumber"];
    _locationView.jdView.inputTextfield.text = result[@"longitude"];
    _locationView.wdView.inputTextfield.text = result[@"latitude"];
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([result[@"latitude"] doubleValue], [result[@"longitude"] doubleValue]);
    [_locationView.mapView setCenterCoordinate:location animated:YES];
    [_locationView addPoint:location title:result[@"companyName"]];
    _locationView.addressView.text = result[@"addressDetail"];
    _taskView.principal = [NSString stringWithFormat:@"区域负责人：%@",[CommonUtil fixNullTextWithLine:result[@"divisionLeaderName"]]];
    _taskView.implementView.info = [CommonUtil fixNullTextWithLine:result[@"applyLeaderName"]];
    _taskView.hetongView.inputTextfield.text = result[@"name"];
    _taskView.htbhView.inputTextfield.text = [CommonUtil fixNullText:result[@"number"]];
    _taskView.yiFangView.inputTextfield.text = result[@"bcompany"];
    _taskView.serviceTimeView.info = [NSString stringWithFormat:@"%@",result[@"serviceYear"]];
    _taskView.timeQuanTum.fromTimeView.info = [result[@"serviceStart"] substringWithRange:RANGE(0, 10)];
    _taskView.timeQuanTum.toTimeView.info = [result[@"serviceEnd"] substringWithRange:RANGE(0, 10)];

    _totalView.swfzr =[NSString stringWithFormat:@"商务负责人：%@",[CommonUtil fixNullTextWithLine:result[@"businessLeaderName"]]];
    
    if ([result[@"moneyBackStatus"] integerValue]==0) {
           _totalView.hkzt = @"回款状态：未回款 ";
    }else if ([result[@"moneyBackStatus"] integerValue]==1){
        _totalView.hkzt = @"回款状态：回款中 ";
    }else {
        _totalView.hkzt = @"回款状态：已回款 ";
    }
    _totalView.swPhone = result[@"businessLeaderPhone"];
    
    if ([result[@"applyStatus"] integerValue]==1) {
        _totalView.sszt = @"实施状态：进行中";
        _totalView.ssfzr = [NSString stringWithFormat:@"实施负责人：%@",[CommonUtil fixNullTextWithLine:result[@"applyLeaderName"]]];
        _totalView.ssPhone = result[@"applyLeaderPhone"];
    }else if ([result[@"applyStatus"] integerValue]==2){
        _totalView.sszt = @"实施状态：已完成";
        _totalView.ssfzr = [NSString stringWithFormat:@"实施负责人：%@",[CommonUtil fixNullTextWithLine:result[@"applyLeaderName"]]];
        _totalView.ssPhone = result[@"applyLeaderPhone"];
        if ([CommonUtil feiKong:result[@"maintainLeaderName"]]) {
            _totalView.ywfzr = [NSString stringWithFormat:@"运维负责人：%@",[CommonUtil fixNullTextWithLine:result[@"maintainLeaderName"]]];
            _totalView.ywPhone = result[@"maintainLeaderPhone"];
               switch ([result[@"maintainStatus"] integerValue]) {
                case 0:
                    _totalView.ywzt = @"运维状态：未分配";
                    break;
                 case 1:
                     _totalView.ywzt = @"运维状态：未创建";
                    break;
                case 2:
                    _totalView.ywzt = @"运维状态：已创建";
                    break;
                default:
                     _totalView.ywzt = @"运维状态：未知";
                    break;
            }
        }
    }
    _serviceView.banciView.inputTextfield.text = result[@"productShifts"];
    _serviceView.fuwuView.info = [CommonUtil fixNullText:result[@"service"]];
    _serviceView.timeView.inputTextfield.text = result[@"productTime"];
    _serviceView.dianweiView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"pointCount"]];
    if (![result[@"comment"] isKindOfClass:[NSNull class]]) {
              _serviceView.markView.textView.text = result[@"comment"];
    }else {
        _serviceView.markView.textView.text = @"暂无现场情况备注";
    }
    NSArray *array = result[@"pointImageAttachment"];
    if (array&&array.count>0) {
         _serviceView.uploadView.imageUrlArray = [array mutableCopy];
     }
}
- (void)setEditStatus {
    _companyInfoView.qymcView.userInteractionEnabled = NO;//甲方不可选择
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.fzView.inputTextfield.enabled = NO;
    _companyInfoView.lxrView.inputTextfield.enabled = NO;
    _companyInfoView.sjhmView.inputTextfield.enabled = NO;
    _companyInfoView.bylxrView.inputTextfield.enabled = NO;
    _companyInfoView.bysjhmView.inputTextfield.enabled = NO;
    _companyInfoView.xzqhView.baseView.userInteractionEnabled = NO;
    _companyInfoView.hylbView.baseView.userInteractionEnabled = NO;
    _companyInfoView.zczjView.inputTextfield.enabled = NO;
    _companyInfoView.pwxkzView.inputTextfield.enabled = NO;
    _companyInfoView.pwjyhView.inputTextfield.enabled = NO;
    _companyInfoView.hppfView.inputTextfield.enabled = NO;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    _locationView.needlocation = NO;
    [_locationView setCanEdit:NO];
    _locationView.addressView.editable = NO;
    _taskView.implementView.userInteractionEnabled = NO;
    _taskView.hetongView.inputTextfield.enabled = NO;
    _taskView.htbhView.inputTextfield.enabled = NO;
    _taskView.yiFangView.inputTextfield.enabled = NO;
    _taskView.serviceTimeView.userInteractionEnabled = NO;
    _taskView.timeQuanTum.fromTimeView.userInteractionEnabled = NO;
    _serviceView.fuwuView.userInteractionEnabled = NO;
    _serviceView.banciView.inputTextfield.enabled = NO;
    _serviceView.timeView.inputTextfield.enabled = NO;
    _serviceView.dianweiView.inputTextfield.enabled = NO;
    _serviceView.htjeView.inputTextfield.enabled = NO;
    _serviceView.markView.canEdit = NO;
    _serviceView.markView2.canEdit = NO;
    _serviceView.uploadView.isCommit = YES;
    [_serviceView.uploadView removeUploadButton];//提交状态图片不可编辑
    
    _xcLocationView.jdView.inputTextfield.enabled = NO;
    _xcLocationView.wdView.inputTextfield.enabled = NO;
    _xcLocationView.needlocation = NO;
    [_xcLocationView setCanEdit:NO];
    _xcLocationView.addressView.editable = NO;
    _productView.nydlView.inputTextfield.enabled = NO;
    _dwfhtView.isCommit = YES;
    [_dwfhtView removeUploadButton];
    _czwgxtView.isCommit = YES;
    [_czwgxtView removeUploadButton];
    _czwsjwdView.isCommit = YES;
     [_czwsjwdView removeUploadButton];
    _scgyView.textView.editable = NO;
    _zywrwView.textView.editable = NO;
    _zysbView.textView.editable = NO;
    _productView.nclView.inputTextfield.enabled = NO;
    _productView.npflView.inputTextfield.enabled = NO;
    _productView.nczView.inputTextfield.enabled = NO;
    _productView.sjslView.inputTextfield.enabled = NO;
    _productView.dianweiView.inputTextfield.enabled = NO;
    _productView.timeView.inputTextfield.enabled = NO;
    _productView.banciView.inputTextfield.enabled = NO;
    _productView.tyxydmView.inputTextfield.enabled = NO;
    _productView.fzView.inputTextfield.enabled = NO;

}
- (void)relayoutWithXcDeatil:(NSDictionary *)result {
    _productView.banciView.inputTextfield.text = result[@"productShifts"];
    _productView.timeView.inputTextfield.text = result[@"productTime"];
    _productView.dianweiView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"pointCount"]];
    _productView.sjslView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"realPointCount"]];
    _productView.nczView.inputTextfield.text = result[@"annualOutputValue"];
    _productView.nclView.inputTextfield.text = result[@"annualOutput"];
    _productView.npflView.inputTextfield.text = result[@"vocs"];
    _productView.nydlView.inputTextfield.text = result[@"yearPower"];
    _productView.tyxydmView.inputTextfield.text = result[@"creditCode"];
    _productView.fzView.inputTextfield.text =result[@"legalRepresentative"];
    NSArray *array = result[@"pointReviewAttachment"];
    if (array&&array.count>0) {
         _dwfhtView.imageUrlArray = [array mutableCopy];
     }
    NSArray *array2 = result[@"relationAttachment"];
    if (array2&&array2.count>0) {
         _czwgxtView.imageUrlArray = [array2 mutableCopy];
     }
 
    NSArray *array3 = result[@"designAttachment"];
     if (array3&&array3.count>0) {
          _czwsjwdView.imageUrlArray = [array3 mutableCopy];
      }
    if ([CommonUtil feiKong:result[@"productProcess"]]) {
        _scgyView.textView.text = result[@"productProcess"];
    }else {
        _scgyView.textView.text = @"无";
    }
    if ([CommonUtil feiKong:result[@"mainPollute"]]) {
          _zywrwView.textView.text = result[@"mainPollute"];
      }else {
          _zywrwView.textView.text = @"无";
      }
    if ([CommonUtil feiKong:result[@"mainEquip"]]) {
          _zysbView.textView.text = result[@"mainEquip"];
      }else {
          _zysbView.textView.text = @"无";
      }
    if (![result[@"longitudeReal"] isKindOfClass:[NSNull class]]) {
        _xcLocationView.jdView.inputTextfield.text = result[@"longitudeReal"];
         _xcLocationView.wdView.inputTextfield.text = result[@"latitudeReal"];
         CLLocationCoordinate2D location = CLLocationCoordinate2DMake([result[@"latitudeReal"] doubleValue], [result[@"longitudeReal"] doubleValue]);
         [_xcLocationView.mapView setCenterCoordinate:location];
         _xcLocationView.currentLocation = location;
         [_xcLocationView addPoint:location title:result[@"addressDetailReal"]];
         _xcLocationView.addressView.text = result[@"addressDetailReal"];
    }
 
}
@end
