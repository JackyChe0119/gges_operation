//
//  InstrumentModel.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InstrumentModel : HYBaseModel
@property (nonatomic,copy)NSString *comment,*createTime,*Id,*mn,*name;
@property (nonatomic,strong)NSDictionary *protocol,*model;
@end

NS_ASSUME_NONNULL_END
