//
//  EquipmentModel.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EquipmentModel : HYBaseModel

@property (nonatomic,copy)NSString *companyName,*Id,*name,*unitName,*unitId,*type,*mn,*equipId;
@property (nonatomic,assign)NSInteger effect,status;
@end

NS_ASSUME_NONNULL_END
