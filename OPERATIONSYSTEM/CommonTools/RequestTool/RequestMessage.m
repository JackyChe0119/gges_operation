//
//  RequestMessage.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "RequestMessage.h"
@implementation RequestMessage

- (instancetype)initWithUrl:(NSString *)url requestMethod:(HttpMethod)method params:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        // code : 新增默认请求字段
//        NSString *accessSession =[[NSUserDefaults standardUserDefaults] objectForKey:REQUEST_SESSION];
//        if (accessSession) {
//            [params setObject:accessSession forKey:REQUEST_SESSION];//请求权限 登录状态下获取
//        }
//        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
//        [params setObject:version forKey:REQUEST_VERSION];//当前请求版本号
//        [params setValue:@"1" forKey:REQUEST_ROUTE];//请求接口字段 1苹果 2为安卓
//        [params setValue:[CommonUtil getTimeIntervalStringByNew] forKey:REQUEST_TICKS];//请求时间
//        [params setValue:[self codeSign:params] forKey:@"sign"];

        _params = params;
        _method = method;
        _url = url;
    }
    return self;
}
- (instancetype)initWithPayUrl:(NSString *)url requestMethod:(HttpMethod)method params:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
//        // code : 新增默认请求字段
//        NSString *accessSession =[[NSUserDefaults standardUserDefaults] objectForKey:REQUEST_SESSION];
//        if (accessSession) {
//            [params setObject:accessSession forKey:REQUEST_SESSION];//请求权限 登录状态下获取
//        }
//        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
//        [params setObject:version forKey:REQUEST_VERSION];//当前请求版本号
//        [params setValue:@"1" forKey:REQUEST_ROUTE];//请求接口字段 1苹果 2为安卓
//        [params setValue:[CommonUtil getTimeIntervalStringByNew] forKey:REQUEST_TICKS];//请求时间
//        [params setValue:[self codeSign:params] forKey:@"sign"];
        
        _params = params;
        _method = method;
        _url = url;
    }
    return self;
}
- (NSString *)codeSign:(NSMutableDictionary *)params {
    //先 针对keys 去排序
   NSArray *sortaAfterArray  = [params.allKeys sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSComparisonResult result = [obj1 compare:obj2];
        return result;
    }];
//    NSLog(@"排序后的数据为%@",sortaAfterArray);
    // 根据排序后的keys  取values  拼接
    NSMutableString *signStr = [NSMutableString string];
    [sortaAfterArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [signStr appendFormat:@"%@", [NSString stringWithFormat:@"%@=%@|",obj,params[obj]]];
    }];
    [signStr appendString:@"123456"];
//    NSLog(@"signString==%@",signStr);
    NSString *md5string = [CommonUtil md5:signStr];
    return md5string;
}
@end
