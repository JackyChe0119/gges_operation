//
//  RequestResponseMessage.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestResponseMessage : NSObject
@property (nonatomic,copy)NSString *errorMessage;//错误信息
@property (nonatomic,strong) id responseData;//响应数据
@property (nonatomic,strong) NSDictionary *data;//响应有效数据
@property (nonatomic,strong) NSArray *list;//响应有效数据(数组)
@property (nonatomic,copy) NSString *responseCode;// 响应的code
@property (nonatomic,assign) NSInteger pageno;//页数
@property (nonatomic,assign) NSInteger total;//总页数
@property (nonatomic,copy) NSString *sessionid;
/**
 判断请求是否成功

 @return YES 表示请求成功  NO 表示请求失败或者链接失败
 */
- (BOOL)isRequestSuccessful;

@end

NS_ASSUME_NONNULL_END
