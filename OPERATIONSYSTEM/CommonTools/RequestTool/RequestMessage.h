//
//  RequestMessage.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    POST, //POST请求 数据安全
    GET,  //GET请求
    DELETE  //DELETE请求
} HttpMethod; //请求方法

NS_ASSUME_NONNULL_BEGIN

@interface RequestMessage : NSObject

@property (nonatomic,strong)NSDictionary *params;//请求参数体
@property (nonatomic,assign)HttpMethod method;//请求方式
@property (nonatomic,copy) NSString *url;//请求地址后缀
/**
 数据请求封装请求头

 @param url 请求地址后缀
 @param method 请求方式 POST 或GET请求
 @param params 请求参数
 @return 返回说句类型
 */
- (id)initWithUrl:(NSString *)url requestMethod:(HttpMethod)method params:(NSDictionary *)params;

- (instancetype)initWithPayUrl:(NSString *)url requestMethod:(HttpMethod)method params:(NSMutableDictionary *)params;
@end

NS_ASSUME_NONNULL_END
