//
//  AliyunOSSEngine.m
//  KongGeekSample
//
//  Created by Robin on 16/7/6.
//  Copyright © 2016年 KongGeek. All rights reserved.
//

#import "AliyunOSSEngine.h"
#import "OSSService.h"
#import "OSSCompat.h"
#import "InterfaceMacro.h"

@implementation AliyunOSSEngine

+(void) uploadWithFilePath:(NSString *)filePath fileData:(NSData *)fileData inView:(UIView *)view delete:(BOOL)candelete progressBlock:(NetworkProgressCallback)progressBlock callbackBlock:(void (^)(NSString *fileUrl))callbackBlock{
    OSSClient *ossClient = [self initOSSClient2];
    OSSPutObjectRequest *putRequest = [OSSPutObjectRequest new];

    UIView * shadow = [[UIView alloc] init];
    shadow.frame = view.bounds;
    shadow.backgroundColor = [UIColor blackColor];
    shadow.alpha = 0.6;
    [view addSubview:shadow];
        //显示上传百分比
    UILabel * labelProgress = [[UILabel alloc] init];
    labelProgress.frame = CGRectMake(0, 0, view.frame.size.width, 15);
    labelProgress.textAlignment = NSTextAlignmentCenter;
    labelProgress.textColor = [UIColor whiteColor];
    labelProgress.font = [UIFont systemFontOfSize:14];
    [shadow addSubview:labelProgress];
    labelProgress.center = shadow.center;
    
//    __block typeof (self) WeakSelf = self;
    putRequest.uploadProgress=^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
        dispatch_async(dispatch_get_main_queue(), ^{
            labelProgress.text = [NSString stringWithFormat:@"%.0f%@",(totalBytesSent / (totalBytesExpectedToSend / 1.0)) * 100,@"%"];
            if (totalBytesSent / totalBytesExpectedToSend == 1) {
                [shadow removeFromSuperview];
            }
        });
        if (progressBlock) {
            progressBlock(bytesSent,totalBytesSent,totalBytesExpectedToSend);
        }
    };
    putRequest.bucketName = ALIYUN_OSS_BUCKET_NAME;
    NSString *imagePath = [NSString stringWithFormat:@"FYHJ%@%@%u%u.jpg",filePath,[CommonUtil getStringForDate:[NSDate date] format:@"yyyy-MM-dd-HHmmss"],arc4random()%10000,arc4random()%10000];
    putRequest.objectKey = imagePath;
    putRequest.uploadingData = fileData;
    OSSTask * putTask = [ossClient putObject:putRequest];
    [putTask continueWithBlock:^id(OSSTask *task) {
        if (!task.error) {
            NSString *fileUrl = imagePath;
            NSLog(@"上传阿里云成功，fileUrl：%@",fileUrl);
            callbackBlock(fileUrl);
        } else {
            [shadow removeFromSuperview];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"checkTimeStamp"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"上传阿里云失败, error: %@" , task.error);
            NSString *str = [NSString stringWithFormat:@"%@",task.error];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"errorInfo" object:str];
            callbackBlock(nil);
        }
        return nil;
    }];
}
+ (OSSClient *)initOSSClient2 {
    NSString * const endPoint = ALIYUN_OSS_SERVER;
    id<OSSCredentialProvider> credential;
    credential = [[OSSPlainTextAKSKPairCredentialProvider alloc]initWithPlainTextAccessKey:@"LTAI3d9Nj71QcpfV" secretKey:@"z410VbYA36eTUjY7evnpPOAK0VYDkY"];
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.maxRetryCount = 2;
    conf.timeoutIntervalForRequest = 30;
    conf.timeoutIntervalForResource = 24 * 60 * 60;
    return [[OSSClient alloc] initWithEndpoint:endPoint credentialProvider:credential clientConfiguration:conf];

}
//OSS 初始化client
+ (OSSClient *)initOSSClient {
    NSString * const endPoint = ALIYUN_OSS_SERVER;
    NSString * checkTimeStamp = [[NSUserDefaults standardUserDefaults] valueForKey:@"checkTimeStamp"];
    NSString * currentTimeStamp = [self obtainCurrentTimeStamp];
    id<OSSCredentialProvider> credential2;
    if (!checkTimeStamp || [currentTimeStamp integerValue] - [checkTimeStamp integerValue] > 300) {
        credential2 = [[OSSFederationCredentialProvider alloc] initWithFederationTokenGetter:^OSSFederationToken * {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            NSString *accessSession =[[NSUserDefaults standardUserDefaults] objectForKey:REQUEST_SESSION];
            if (accessSession) {
                [params setObject:accessSession forKey:REQUEST_SESSION];//请求权限 登录状态下获取
            }
            NSString *urlStr = @"http://api.bizcaicai.org/common/alioss/distribute_token.htm";
            NSURL * url = [NSURL URLWithString:urlStr];
            NSURLRequest * request = [NSURLRequest requestWithURL:url];
            OSSTaskCompletionSource * tcs = [OSSTaskCompletionSource taskCompletionSource];
            NSURLSession * session = [NSURLSession sharedSession];
            NSURLSessionTask * sessionTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                [tcs setError:error];
                                                                return;
                                                            }
                                                            [tcs setResult:data];
                                                        }];
            [sessionTask resume];
            [tcs.task waitUntilFinished];
            if (tcs.task.error) {
                NSLog(@"get token error: %@", tcs.task.error);
                return nil;
            } else {
                NSDictionary * object = [NSJSONSerialization JSONObjectWithData:tcs.task.result options:kNilOptions error:nil];
                NSLog(@"=====%@",object[@"message"]);
                OSSFederationToken * token = [OSSFederationToken new];
                NSDictionary * data = [object objectForKey:@"data"];
                token.tAccessKey = [data objectForKey:@"accessKeyId"];
                token.tSecretKey = [data objectForKey:@"accessKeySecret"];
                token.tToken = [data objectForKey:@"securityToken"];
                token.expirationTimeInMilliSecond = [[data objectForKey:@"expireTime"] integerValue];
                NSLog(@"get token: %@",object);
                
                // 存入本地 获取当前的时间戳 做半个小时的校验
                [[NSUserDefaults standardUserDefaults] setValue:token.tAccessKey forKey:@"accessKeyId"];
                [[NSUserDefaults standardUserDefaults] setValue:token.tSecretKey forKey:@"accessKeySecret"];
                [[NSUserDefaults standardUserDefaults] setValue:token.tToken forKey:@"securityToken"];
                [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithInteger:[[data objectForKey:@"expireTime"] integerValue]] forKey:@"expireTime"];
                [[NSUserDefaults standardUserDefaults] setValue:data[@"dirPath"] forKey:@"dirPath"];

                // 存时间戳
                NSString * checkStamp = [self obtainCurrentTimeStamp];
                [[NSUserDefaults standardUserDefaults] setValue:checkStamp forKey:@"checkTimeStamp"];
                return token;
            }
        }];
        
    } else {
        credential2 = [[OSSFederationCredentialProvider alloc] initWithFederationTokenGetter:^OSSFederationToken *{
            OSSFederationToken * token = [OSSFederationToken new];
            token.tAccessKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"accessKeyId"];
            token.tSecretKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"accessKeySecret"];
            token.tToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"securityToken"];
            token.expirationTimeInMilliSecond = [[[NSUserDefaults standardUserDefaults] valueForKey:@"expireTime"] integerValue];
            return token;
        }];
    }
    
    OSSClientConfiguration * conf = [OSSClientConfiguration new];
    conf.maxRetryCount = 2;
    conf.timeoutIntervalForRequest = 30;
    conf.timeoutIntervalForResource = 24 * 60 * 60;
    return [[OSSClient alloc] initWithEndpoint:endPoint credentialProvider:credential2 clientConfiguration:conf];
}

/**
 *    @brief    获取当前的时间戳
 */
+ (NSString *)obtainCurrentTimeStamp {
    NSDate * date = [NSDate date];
    long currentDate = [date timeIntervalSince1970];
    NSString * currentTimeStamp = [NSString stringWithFormat:@"%ld",currentDate];
    return currentTimeStamp;
}
//@end
@end

