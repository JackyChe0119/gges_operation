//
//  RequestTool.m
//  OPERATIONSYSTEM
//  数据请求类
//  Created by 车杰 on 2019/3/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "RequestTool.h"
#import <AFHTTPSessionManager.h>

@implementation RequestTool
+ (void)requestLoginWithUrlSuffix:(NSString *)urlSuffix method:(HttpMethod)method params:(NSDictionary *)params callBack:(networkResponseBlock)callBack  {
    RequestMessage *message = [[RequestMessage alloc]initWithUrl:[NSString stringWithFormat:@"%@%@",URL_LoginRootAPIServer,urlSuffix] requestMethod:method params:params];
    message.method = method;
    [self requestDataWithRequestMessage:message callBack:^(RequestResponseMessage *responseMessage) {
        callBack(responseMessage);
    }];
}
+ (void)requestDataWithUrlSuffix:(NSString *)urlSuffix method:(HttpMethod)method params:(NSDictionary *)params callBack:(networkResponseBlock)callBack  {
    RequestMessage *message = [[RequestMessage alloc]initWithUrl:[NSString stringWithFormat:@"%@%@",URL_RootAPIServer,urlSuffix] requestMethod:method params:params];
    message.method = method;
    [self requestDataWithRequestMessage:message callBack:^(RequestResponseMessage *responseMessage) {
        callBack(responseMessage);
    }];
}
+ (void)requestPayUrlSuffix:(NSString *)urlSuffix method:(HttpMethod)method params:(NSDictionary *)params callBack:(networkResponseBlock)callBack  {
    RequestMessage *message = [[RequestMessage alloc]initWithUrl:[NSString stringWithFormat:@"%@%@",@"http://192.168.0.200:8800/",urlSuffix] requestMethod:method params:params];
    message.method = method;
    [self requestDataWithRequestMessage:message callBack:^(RequestResponseMessage *responseMessage) {
        callBack(responseMessage);
    }];
}
+ (void)requestDataWithRequestMessage:(RequestMessage *)message callBack:(networkResponseBlock)callBack {
    NSMutableDictionary *requestHeader = [NSMutableDictionary dictionary];
    if (message.params) {
        [requestHeader setValuesForKeysWithDictionary:message.params];
    }
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 30;
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    AFSecurityPolicy *security = [AFSecurityPolicy defaultPolicy];
    security.allowInvalidCertificates = YES;
    security.validatesDomainName = NO;
    manager.securityPolicy = security;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain",@"application/json",nil];
    NSString *accessSession =[[NSUserDefaults standardUserDefaults] objectForKey:REQUEST_SESSION];
    if (accessSession) {
        [manager.requestSerializer setValue:accessSession forHTTPHeaderField:@"token"];
    }
    if (message.method == GET) {
       //GET请求
        [manager GET:message.url parameters:requestHeader progress:^(NSProgress * _Nonnull downloadProgress) {
            //获取接口请求进度
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self processWithResponse:message resopnseObject:responseObject error:nil callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                callBack(responseMessage);
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self processWithResponse:message resopnseObject:nil error:error callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                callBack(responseMessage);
            }];
        }];
    }else if (message.method == DELETE) {
        [manager DELETE:message.url parameters:requestHeader success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self processWithResponse:message resopnseObject:responseObject error:nil callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                               callBack(responseMessage);
                           }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self processWithResponse:message resopnseObject:nil error:error callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                callBack(responseMessage);
            }];
        }];
    }else {
        if ([message.url containsString:@"sso/sso/login"]) {
            //       POST请求  默认
            [manager POST:message.url parameters:requestHeader progress:^(NSProgress * _Nonnull uploadProgress) {
                //获取接口请求进度
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self processWithResponse:message resopnseObject:responseObject error:nil callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                    callBack(responseMessage);
                }];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self processWithResponse:message resopnseObject:nil error:error callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                    callBack(responseMessage);
                }];
            }];
        }else {
            [self startRequestWithUrl:message callBack:callBack];
        }
    }
    message.url = [message.url stringByAppendingString:@"?"];
    [[requestHeader allKeys] enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx==0) {
            message.url = [message.url stringByAppendingString:[NSString stringWithFormat:@"%@=%@",obj,[requestHeader objectForKey:obj]]];
        }else {
            message.url = [message.url stringByAppendingString:[NSString stringWithFormat:@"&%@=%@",obj,[requestHeader objectForKey:obj]]];
        }
    }];
    NSLog(@"============>>>>%@<<<<===========",message.url);
}
+ (void)startRequestWithUrl:(RequestMessage *)message callBack:(networkResponseBlock)callBack {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:message.params options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        //method 为时post请求还是get请求
        NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:message.url parameters:nil error:nil];
        //设置超时时长
        request.timeoutInterval= 30;
        //设置上传数据type
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        //设置接受数据type
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        NSString *accessSession =[[NSUserDefaults standardUserDefaults] objectForKey:REQUEST_SESSION];
       if (accessSession) {
        [request setValue:accessSession forHTTPHeaderField:@"token"];
       }
        //将对象设置到requestbody中 ,主要是这不操作
        [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        //进行网络请求
        [[manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
            
        } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
            
        } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if (!error) {
                NSLog(@"Reply JSON: %@", responseObject);
                [self processWithResponse:message resopnseObject:responseObject error:nil callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                    callBack(responseMessage);
                }];
            } else {
                NSLog(@"Error: %@, %@, %@", error, response, responseObject);
                [self processWithResponse:message resopnseObject:responseObject error:nil callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                    callBack(responseMessage);
                }];
            }
            
        }] resume];
    
}
+ (void)processWithResponse:(RequestMessage *)requestMessage resopnseObject:(id)responseObject error:(NSError *)error callBack:(networkResponseBlock)callBlock {
    RequestResponseMessage *message = [[RequestResponseMessage alloc]init];
    if (error) { //若出现错误
        message.responseCode = [NSString stringWithFormat:@"%ld",error.code];
        message.errorMessage = [CommonUtil getErrorMessageWithCode:error.code];
    }else { //请求成功数据解析
        if (responseObject&&[responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *result = (NSDictionary *)responseObject;
            message.responseData = responseObject;
            message.responseCode = [result[@"head"] objectForKey:@"respCode"];
            message.errorMessage = [result[@"head"] objectForKey:@"respContent"];
            message.sessionid = result[@"sessionid"];
            message.data = result[@"body"];
            if ([message.responseCode isEqualToString:@"0000005"]) {
                [CommonUtil removeInfoWithKey:AUTO_LOGIN];
                [CommonUtil removeInfoWithKey:REQUEST_SESSION];
                if (![UserManager shareManager].needLogin) {
                    [UserManager shareManager].needLogin = YES;
                    [[NSNotificationCenter defaultCenter] postNotificationName:REQUEST_SESSION_OUT object:nil];
                }
            }
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
            HLLog(@"请求结果===>>>%@",responseObject);
    });
    callBlock(message);
}
@end
