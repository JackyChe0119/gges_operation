//
//  MJLocationPickerView.m
//  KONGGEEK
//
//  Created by 车杰 on 16/8/3.
//  Copyright © 2016年 车杰. All rights reserved.
//

#import "MJLocationPickerView.h"
@implementation MJLocationPickerView
- (instancetype)initWithFrame:(CGRect)frame array:(NSArray *)array {
    self = [super initWithFrame:frame];
    if (self) {
        _array = array;
        [self data];
        [self baseView];
    }
    return self;
}
/*
 ***  数据源 ****
 */
- (void)data {
    for (NSDictionary *dic in _array) {
        [self.allProrince addObject:dic];
    }
    if (self.allProrince.count>0) {
        NSDictionary *dic = self.allProrince[0];
        NSArray *array = (NSArray *)dic[@"citys"];
        [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.allCity addObject:obj];
        }];
    }

}
/*
 ***  布局基础视图 ****
 */
- (void)baseView {
    
    self.backgroundColor = [UIColor colorWithHexString:@"#f5f6f7"];
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, 39.5, ScreenWidth, 0.5);
    layer.backgroundColor = [UIColor colorWithHexString:@"#e8e8e8"].CGColor;
    [self.layer addSublayer:layer];
    layer.frame = CGRectMake(0, 39.5, ScreenWidth, 0.5);
    layer.backgroundColor = [UIColor colorWithHexString:@"#e8e8e8"].CGColor;
    [self.layer addSublayer:layer];
    
    // 取消按钮
    UIButton *buttonCancel = [[UIButton alloc] init];
    buttonCancel.frame = CGRectMake(0, 0, 70, 40);
    [buttonCancel setTitle:@"取消" forState:UIControlStateNormal];
    [buttonCancel setTitleColor:Color_666666 forState:UIControlStateNormal];
    buttonCancel.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonCancel addTarget:self action:@selector(buttonClickLocation:) forControlEvents:UIControlEventTouchUpInside];
    buttonCancel.tag = 1;
    [self addSubview:buttonCancel];
    
   UILabel *titleLabel = [UILabel new];
    [titleLabel rect:RECT(70, 0,WIDTH(self)-140, 40) aligment:Center font:13 isBold:NO text:@"支持查询的城市" textColor:Color_666666 superView:self];
    
    // 确定按钮
    UIButton *buttonSure = [[UIButton alloc] init];
    buttonSure.frame = CGRectMake(ScreenWidth - 70, 0, 70, 40);
    [buttonSure setTitle:@"确定" forState:UIControlStateNormal];
    [buttonSure setTitleColor:Color_Common forState:UIControlStateNormal];
    buttonSure.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonSure addTarget:self action:@selector(buttonClickLocation:) forControlEvents:UIControlEventTouchUpInside];
    buttonSure.tag = 2;
    [self addSubview:buttonSure];
    _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, ScreenWidth, HEIGHT(self)-40-SafeAreaBottomHeight)];
    _picker.backgroundColor = [UIColor whiteColor];
    _picker.delegate = self;
    _picker.dataSource = self;
    [_picker selectedRowInComponent:0];
    [self addSubview: _picker];
    
}
- (void)buttonClickLocation:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            if (_cancleBlock) {
                _cancleBlock ();
            }
            break;
        case 2:
        {
            if (self.allCity.count==0||self.allCity.count==0) {
                if (_cancleBlock) {
                    _cancleBlock ();
                }
                break;
            }
            NSInteger provinceIndex = [self.picker selectedRowInComponent: PROVINCE_COMPONENT];
            NSInteger cityIndex = [self.picker selectedRowInComponent: CITY_COMPONENT];

            NSDictionary *provinceDic = [self.allProrince objectAtIndex: provinceIndex];
            NSDictionary *cityDic  = [self.allCity objectAtIndex: cityIndex];

            NSString *showMsgStrring = [NSString stringWithFormat: @"%@ %@", [provinceDic objectForKey:@"province"], [cityDic objectForKey:@"city_name"]];
            if (_sureBlock) {
                _sureBlock (provinceDic,cityDic,nil,showMsgStrring);
            }
            break;
        }
        default:
            break;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}
#pragma mark- Picker Delegate Methods

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == PROVINCE_COMPONENT) {
        return [[self.allProrince objectAtIndex: row] objectForKey:@"province"];
    }
    else{
        return [[self.allCity objectAtIndex: row] objectForKey:@"city_name"];
    }
   
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
  
    if (component == PROVINCE_COMPONENT) {
        
        NSDictionary *dic = [self.allProrince objectAtIndex:row];
        [self.allCity removeAllObjects];
        NSArray *arrays  = (NSArray *)dic[@"citys"];
        [arrays enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.allCity addObject:obj];
        }];
        
        [pickerView selectRow: 0 inComponent: CITY_COMPONENT animated: NO];
        [pickerView reloadComponent: CITY_COMPONENT];

        
    }
    
}


- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
        return ScreenWidth/2.0;
}

#pragma mark =====  UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == PROVINCE_COMPONENT) {
        return [self.allProrince count];
    }
    else {
        return [self.allCity count];
    }
 
}

- (NSMutableArray *)allProrince {
    if (!_allProrince) {
        _allProrince = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _allProrince;
}
- (NSMutableArray *)allCity {
    if (!_allCity) {
        _allCity = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _allCity;
}


@end

