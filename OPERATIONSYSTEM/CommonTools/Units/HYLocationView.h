//
//  HYLocationView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/4/29.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define PROVINCE_COMPONENT  0
#define CITY_COMPONENT      1
#define DISTRICT_COMPONENT  2
typedef void (^ cancleBlock) (void);
typedef void (^ sureBtnBlock) (NSDictionary *province,NSDictionary *city,NSDictionary *district,NSString *showMsg);
@interface HYLocationView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,strong) NSMutableArray *allProrince;//省
@property (nonatomic,strong) NSMutableArray *allCity;//市
@property (nonatomic,strong) NSMutableArray *allDistrist;//区
@property (nonatomic,strong)  UIPickerView *picker;
@property (nonatomic,strong)NSArray *areaTreeArray;
@property (nonatomic,strong) cancleBlock  cancleBlock;
@property (nonatomic,strong) sureBtnBlock  sureBlock;
- (instancetype)initWithFrame:(CGRect)frame data:(NSArray *)araeData;
@end

NS_ASSUME_NONNULL_END
