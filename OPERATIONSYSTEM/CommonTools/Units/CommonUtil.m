//
//  CommonUtil.m
//  iOS SDK
//
//  Created by 王振 on 16/1/6.
//  Copyright © 2016年 杭州空极科技有限公司. All rights reserved.
//

#import "CommonUtil.h"
#import <CommonCrypto/CommonDigest.h>
#import "YYText.h"
@implementation CommonUtil

/** 获取对象的字符串类型 */
+ (NSString *)stringForId:(id)object {
    NSString *str = (NSString *)object;
    
    if (str == nil) return @"";
    if (str == NULL) return @"";
    if ([str isKindOfClass:[NSNull class]]) return @"";
    
    str = [NSString stringWithFormat:@"%@",str];
    return str;
}
/**
 *  获取一个UUID
 *
 *  @return UUID
 */
+ (NSString *)createUUID {
    NSString *uuidStr = [[UIDevice currentDevice].identifierForVendor UUIDString];
    return uuidStr;
}

/**
 获取系统版本号

 @return 版本号
 */
+ (NSString *)getVersionNmuber {
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    NSString *appCurVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    return appCurVersion;
}
/**
 *  获取window
 *
 *  @return window
 */
+ (UIWindow *)window {
    return [UIApplication sharedApplication].delegate.window;
}

/**
 *  保存数据 - NSUserDefaults
 *  注：必须NSUserDefaults 识别的类型
 *
 *  @param key      key
 */
+ (void)setInfo:(id)info forKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setObject:info forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 *  获取数据 - NSUserDefaults
 *
 *  @param key key
 *
 *  @return value
 */
+ (id)getInfoWithKey:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

/**
 *  删除数据 - NSUserDefaults
 *
 *  @param key key
 */
+ (void)removeInfoWithKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - 关于accessToken

/**
 *  判断是否含有 accessToken
 *
 *  @return YES？NO
 */
+ (BOOL)isHaveAccessToken {
    NSString *token = [CommonUtil getInfoWithKey:REQUEST_SESSION];
    if (token == (NSString *)nil) {
        return NO;
    } else  {
        return YES;
    }
}
/**
 *  获取 accessToken
 *
 *  @return YES？NO
 */
+ (NSString *)getAccessToken {
    NSString *token = [CommonUtil getInfoWithKey:REQUEST_SESSION];
    if ([self isHaveAccessToken]) {
        return token;
    } else {
        return nil;
    }
}
/**
 *  移除 accessToken
 *
 */
+ (void)removeAccessToken {
    [CommonUtil removeInfoWithKey:REQUEST_SESSION];
}
/** 知道时间，计算出是周几 */
+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate {
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"星期日", @"星期一", @"星期二", @"星期三", @"星期四", @"星期五", @"星期六", nil];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    [calendar setTimeZone: timeZone];
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    return [weekdays objectAtIndex:theComponents.weekday];
}
#pragma mark - 关于时间方法

/** ”时间戳“转成”Date“ */
+ (NSDate *)getDateForTimeIntervalString:(NSString *)interval {
    interval = [CommonUtil stringForId:interval];  // 强转字符串
    
    if (interval.length == 13) {            // Jave类型时间戳
        NSTimeInterval timeInterval = [interval doubleValue]/1000;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        return date;
    }
    else if (interval.length == 10) {       // PHP类型时间戳
        NSTimeInterval timeInterval = [interval doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        return date;
    }
    return nil;
}

/** Date转"时间戳"字符串（0.PHP类型-10位 1.Jave-13位） */
+ (NSString *)getTimeIntervalForDate:(NSDate *)date byType:(NSInteger)type {
    NSTimeInterval time;
    if (type == 1) {
        time = [date timeIntervalSince1970]*1000;
    }
    else {
        time = [date timeIntervalSince1970];
    }
    NSString *returnTime = [NSString stringWithFormat:@"%f",time];
    if (returnTime.length > 13) {
        returnTime = [returnTime substringToIndex:13];
    }
    return returnTime;
}

/** 获取当前时间的“时间戳” -- 默认13位 */
+ (NSString *)getTimeIntervalStringByNew {
    NSDate *date = [NSDate date];
    NSString *returnTime = [self getTimeIntervalForDate:date byType:1];
    return returnTime;
}

/** 时间戳 直接转成 NSString (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss") */
+ (NSString *)getStringForTimeIntervalString:(NSString *)interval format:(NSString *)format {
    NSDate *date = [self getDateForTimeIntervalString:interval];
    return [self getStringForDate:date format:format];
}

#pragma mark -

/**
 Date 转换 NSString
 (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDate:(NSDate *)date format:(NSString *)format {
    if (format == nil) format = @"yyyy-MM-dd HH:mm";
    // 实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // 设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:format];
    // 用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:date];
    return currentDateStr;
}

/**
 Date 转换 NSString
 (默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDate:(NSDate *)date {
    return [self getStringForDate:date format:nil];
}

/** NSString 转换 Date
 (自定义 默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSDate *)getDateForString:(NSString *)string format:(NSString *)format {
    if (format == nil) format = @"yyyy-MM-dd HH:mm:ss";
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:format];
    return [dateFormatter dateFromString:string];
}

/**
 NSString 转换 Date
 (默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSDate *)getDateForString:(NSString *)string {
    return [self getDateForString:string format:nil];
}

/**
 DateNSString 转换 NSString
 (默认格式：@"yyyy-MM-dd HH:mm:ss")
 */
+ (NSString *)getStringForDateString:(NSString *)string byFormat:(NSString *)byFormat toFormat:(NSString *)toFormat {
    NSDate *date = [self getDateForString:string format:byFormat];
    NSString *dateString = [self getStringForDate:date format:toFormat];
    return dateString;
}

#pragma mark -

/** 获取时间差(秒) */
+ (NSInteger)getSecondForFromDate:(NSDate *)fromdate toDate:(NSDate *)todate {
    NSTimeInterval fromInt = [fromdate timeIntervalSince1970];  // 获取离1970年间隔
    NSTimeInterval toInt = [todate timeIntervalSince1970];
    NSTimeInterval interval = toInt - fromInt;  // 获取时间差值
    return interval*1;
}

/*
 获取指定时间间隔“秒”后的新时间
 “interval”:单位“秒”，如果想要分钟传“X*60”，其他同理
 */
+ (NSDate *)getDateForIntervalTime:(NSInteger)interval fromDate:(NSDate *)oldDate {
    return [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([oldDate timeIntervalSinceReferenceDate] + interval)];
}

/**
 获取与当前时间间隔“秒”后的新时间
 “interval”:单位“秒”，如果想要分钟传“X*60”，其他同理
 */
+ (NSDate *)getDateForIntervalTime:(NSInteger)interval {
    return [self getDateForIntervalTime:interval fromDate:[NSDate new]];
}

/** 取得时间差
 fromDate.开始时间
 toDate.结束时间
 type.“yyyy”-年 “MM”-月 “dd”-天 “HH”-小时 "mm"-分 “ss”-秒
 */
+ (NSInteger)getTimeDifference:(NSDate *)fromdate toDate:(NSDate *)todate byType:(NSString *)type {
    // 获取开始时间
    NSTimeZone *fromzone = [NSTimeZone systemTimeZone];
    NSInteger frominterval = [fromzone secondsFromGMTForDate:fromdate];
    NSDate *fromDate = [fromdate dateByAddingTimeInterval:frominterval];
    if (fromDate == nil) return 0;
    
    // 获取结束时间
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:todate];
    NSDate *toDate = [todate dateByAddingTimeInterval:interval];
    
    // 获取时间差
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSUInteger unitFlags = NSCalendarUnitMonth | NSCalendarUnitDay;
    
    NSDateComponents *components = [gregorian components:unitFlags fromDate:fromDate toDate:toDate options:0];
    
    if ([@"yyyy" isEqualToString:type]) {
        return [components year];
    }
    if ([@"MM" isEqualToString:type]) {
        return [components month];
    }
    if ([@"dd" isEqualToString:type]) {
        return [components day];
    }
    if ([@"HH" isEqualToString:type]) {
        return [components hour];
    }
    if ([@"mm" isEqualToString:type]) {
        return [components minute];
    }
    if ([@"ss" isEqualToString:type]) {
        return [components second];
    }
    
    return 0;
}


/**
 将秒数变成“00:00”格式的显示
 format. "mm分ss秒"/"m分ss秒"/"m分s秒"/“00:00”
 */
+ (NSString *)timeFormatted:(int)totalSeconds format:(NSString *)format {
    if (totalSeconds < 0)  totalSeconds = 0;
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    
    if ([@"00:00" isEqualToString:format]) {
        return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    }
    else if ([@"mm分ss秒" isEqualToString:format]) {
        return [NSString stringWithFormat:@"%02d分%02d秒", minutes, seconds];
    }
    else if ([@"m分ss秒" isEqualToString:format]) {
        return [NSString stringWithFormat:@"%d分%02d秒", minutes, seconds];
    }
    else if ([@"m分s秒" isEqualToString:format]) {
        return [NSString stringWithFormat:@"%d分%d秒", minutes, seconds];
    }
    
    return @"";
}

//是否格式为 包含字母和数字，且只包含数字、字母、下划线
+(BOOL)checkIsHaveNumAndLetter:(NSString*)password{
    //数字条件
    NSRegularExpression *tNumRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:NSRegularExpressionCaseInsensitive error:nil];
    
    //符合数字条件的有几个字节
    NSUInteger tNumMatchCount = [tNumRegularExpression numberOfMatchesInString:password
                                                                       options:NSMatchingReportProgress
                                                                         range:NSMakeRange(0, password.length)];
    
    //英文字条件
    NSRegularExpression *tLetterRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
    
    //符合英文字条件的有几个字节
    NSUInteger tLetterMatchCount = [tLetterRegularExpression numberOfMatchesInString:password options:NSMatchingReportProgress range:NSMakeRange(0, password.length)];
    
    if (tNumMatchCount == 0 || tLetterMatchCount == 0) {
        return NO;
    }
    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_"];
    s = [s invertedSet];
    NSRange r = [password rangeOfCharacterFromSet:s];
    if (r.location != NSNotFound){
        return NO;
    }
    return YES;
}
+ (NSString *)getErrorMessageWithCode:(NSInteger)errorCode  {
    NSString *errorMsg;
    switch (errorCode) {
        case 400:
            errorMsg = @"服务器不理解请求的语法---code:400---";
            break;
        case 401:
            errorMsg = @"请求要求身份验证---code:401---";
            break;
        case 403:
            errorMsg = @"服务器请求拒绝---code:403---";
            break;
        case 404:
            errorMsg = @"请求地址出错---code:404---";
            break;
        case 500:
            errorMsg = @"服务器异常---code:500---";
            break;
        case 502:
            errorMsg = @"错误网关---code:502---";
            break;
        case -1001:
            errorMsg = @"请求超时，请检查网络是否异常";
            break;
        default:
            errorMsg = @"网络异常，请求失败";
            break;
    }
    NSLog(@"请求出现错误，errorCode:%ld",errorCode);
    return errorMsg;
}
+ (NSString *)md5:(NSString *)string {
    const char *input = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(input, (CC_LONG)strlen(input), result);
    
    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [digest appendFormat:@"%02x", result[i]];
    }
    return digest;
}
+ (BOOL)isValueMobile:(NSString *)mobile {
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,147,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|4[7]|5[017-9]|8[2378])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,175,176,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|7[56]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,177,180,181,189
     22         */
    NSString * CT = @"^1((33|53|77|8[019])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    if(([regextestmobile evaluateWithObject:mobile] == YES)
       || ([regextestcm evaluateWithObject:mobile] == YES)
       || ([regextestct evaluateWithObject:mobile] == YES)
       || ([regextestcu evaluateWithObject:mobile] == YES)){
        return YES;
    }else{
        return NO;
    }
}
/**
  *  验证身份证号码是否正确的方法
  *
  *  @param IdNumber 身份证号码
  *
  *  @return 返回YES或NO表示该身份证号码是否符合国家标准
 */
+ (BOOL)isAvailableIdCardNumber:(NSString *)IdNumber {
    
    NSString* value = [IdNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    int length = 0;
    if (!value) {
        return NO;
    }
    else {
        length = (int)value.length;
        
        if (length !=15 && length !=18) {
            return NO;
        }
    }
    // 省份代码
    NSArray *areasArray =@[@"11",@"12", @"13",@"14", @"15",@"21", @"22",@"23", @"31",@"32", @"33",@"34", @"35",@"36", @"37",@"41",@"42",@"43", @"44",@"45", @"46",@"50", @"51",@"52", @"53",@"54", @"61",@"62", @"63",@"64", @"65",@"71", @"81",@"82", @"91"];
    NSString *valueStart2 = [value substringToIndex:2];
    BOOL areaFlag =NO;
    for (NSString *areaCode in areasArray) {
        if ([areaCode isEqualToString:valueStart2]) {
            areaFlag =YES;
            break;
        }
    }
    if (!areaFlag) {
        return NO;
    }
    NSRegularExpression *regularExpression;
    NSUInteger numberofMatch;
    int year =0;
    switch (length) {
        case 15:{
            year = [value substringWithRange:NSMakeRange(6,2)].intValue +1900;
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$" options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            if(numberofMatch >0) {
                return YES;
            }else {
                return NO;
            }
        }
        case 18:
            year = [value substringWithRange:NSMakeRange(6,4)].intValue;
            if (year %4 ==0 || (year %100 ==0 && year %4 ==0)) {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}((19[0-9]{2})|(2[0-9]{3}))((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$" options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }else {
                regularExpression = [[NSRegularExpression alloc]initWithPattern:@"^[1-9][0-9]{5}((19[0-9]{2})|(2[0-9]{3}))((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$"
                                                                        options:NSRegularExpressionCaseInsensitive
                                                                          error:nil];//测试出生日期的合法性
            }
            numberofMatch = [regularExpression numberOfMatchesInString:value
                                                               options:NSMatchingReportProgress
                                                                 range:NSMakeRange(0, value.length)];
            
            if(numberofMatch >0) {
                int S = ([value substringWithRange:NSMakeRange(0,1)].intValue + [value substringWithRange:NSMakeRange(10,1)].intValue) *7 + ([value substringWithRange:NSMakeRange(1,1)].intValue + [value substringWithRange:NSMakeRange(11,1)].intValue) *9 + ([value substringWithRange:NSMakeRange(2,1)].intValue + [value substringWithRange:NSMakeRange(12,1)].intValue) *10 + ([value substringWithRange:NSMakeRange(3,1)].intValue + [value substringWithRange:NSMakeRange(13,1)].intValue) *5 + ([value substringWithRange:NSMakeRange(4,1)].intValue + [value substringWithRange:NSMakeRange(14,1)].intValue) *8 + ([value substringWithRange:NSMakeRange(5,1)].intValue + [value substringWithRange:NSMakeRange(15,1)].intValue) *4 + ([value substringWithRange:NSMakeRange(6,1)].intValue + [value substringWithRange:NSMakeRange(16,1)].intValue) *2 + [value substringWithRange:NSMakeRange(7,1)].intValue *1 + [value substringWithRange:NSMakeRange(8,1)].intValue *6 + [value substringWithRange:NSMakeRange(9,1)].intValue *3;
                int Y = S %11;
                NSString *M =@"F";
                NSString *JYM =@"10X98765432";
                M = [JYM substringWithRange:NSMakeRange(Y,1)];// 判断校验位
                if ([M isEqualToString:[value substringWithRange:NSMakeRange(17,1)]]) {
                    return YES;// 检测ID的校验位
                }else {
                    return NO;
                }
            }else {
                return NO;
            }
        default:
            return NO;
    }
}
+ (BOOL)isAvailableBankCardNumber:(NSString*)cardNo{
    
    if (cardNo.length < 15) {
        return NO;
    }
    int oddsum = 0;//奇数求和
    int evensum = 0;//偶数求和
    int allsum = 0;
    int cardNoLength = (int)[cardNo length];
    int lastNum = [[cardNo substringFromIndex:cardNoLength-1] intValue];
    cardNo = [cardNo substringToIndex:cardNoLength - 1];
    for (int i = cardNoLength -1 ; i>=1;i--) {
        NSString *tmpString = [cardNo substringWithRange:NSMakeRange(i-1, 1)];
        int tmpVal = [tmpString intValue];
        if (cardNoLength % 2 ==1 ) {
            if((i % 2) == 0){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }else{
            if((i % 2) == 1){
                tmpVal *= 2;
                if(tmpVal>=10)
                    tmpVal -= 9;
                evensum += tmpVal;
            }else{
                oddsum += tmpVal;
            }
        }
    }
    allsum = oddsum + evensum;
    allsum += lastNum;
    if((allsum % 10) == 0)
        return YES;
    else
        return NO;
}
+ (NSString *)bankNameWithLogogram:(NSString *)logogram {
    NSString *bankName = @"未知银行";
    if ([logogram isEqualToString:@"ABC"]) {
        bankName = @"中国农业银行";
    }else if ([logogram isEqualToString:@"ICBC"]) {
        bankName = @"中国工商银行";
    }else if ([logogram isEqualToString:@"CCB"]) {
        bankName = @"中国建设银行";
    }else if ([logogram isEqualToString:@"BOC"]) {
        bankName = @"中国银行";
    }else if ([logogram isEqualToString:@"BCM"]) {
        bankName = @"交通银行";
    }else if ([logogram isEqualToString:@"PSBC"]) {
        bankName = @"中国邮政储蓄银行";
    }
    return bankName;
}
+  (id)toArrayOrNSDictionary:(NSString *)jsonStr{
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    if (jsonObject != nil && error == nil){
        return jsonObject;
    }else{
        // 解析错误
        return nil;
    }
}
+ (NSString *)countDistance:(double )distance {
    NSString *distanceStr;
    if (distance<1000) {
        distanceStr  = [NSString stringWithFormat:@"%.0fm",distance];
    }else {
        distanceStr = [NSString stringWithFormat:@"%.1fkm",distance/1000.0];
    }
    return distanceStr;
}
+ (NSArray *)getInstalledMapAppWithEndLocation:(CLLocationCoordinate2D)endLocation title:(NSString *)title {
    NSMutableArray *maps = [NSMutableArray array];
    //苹果地图
    NSMutableDictionary *iosMapDic = [NSMutableDictionary dictionary];
    iosMapDic[@"title"] = @"苹果地图";
    [maps addObject:iosMapDic];
    //百度地图 Bd09坐标系  需要进行转换
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]) {
        CLLocationCoordinate2D exChangeLocation = [JZLocationConverter gcj02ToBd09:endLocation];
        NSMutableDictionary *baiduMapDic = [NSMutableDictionary dictionary];
        baiduMapDic[@"title"] = @"百度地图";
        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin={{我的位置}}&destination=latlng:%f,%f|name=%@&mode=driving&coord_type=bd09ll",exChangeLocation.latitude,exChangeLocation.longitude,title] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        baiduMapDic[@"url"] = urlString;
        [maps addObject:baiduMapDic];
    }
    //高德地图 GCJ-02火星坐标系
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]) {
        NSMutableDictionary *gaodeMapDic = [NSMutableDictionary dictionary];
        gaodeMapDic[@"title"] = @"高德地图";
        NSString *urlString = [[NSString stringWithFormat:@"iosamap://navi?sourceApplication=%@&backScheme=%@&poiname=%@&lat=%f&lon=%f&dev=0&style=2",@"飞源",@"com.OPERATIONSYSTEM",title,endLocation.latitude,endLocation.longitude] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        gaodeMapDic[@"url"] = urlString;
        [maps addObject:gaodeMapDic];
    }
    //腾讯地图 GCJ-02火星坐标系
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"qqmap://"]]) {
        NSMutableDictionary *qqMapDic = [NSMutableDictionary dictionary];
        qqMapDic[@"title"] = @"腾讯地图";
        NSString *urlString = [[NSString stringWithFormat:@"qqmap://map/routeplan?from=我的位置&type=drive&tocoord=%f,%f&to=%@&coord_type=1&policy=0",endLocation.latitude, endLocation.longitude,title] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        qqMapDic[@"url"] = urlString;
        [maps addObject:qqMapDic];
    }
    return maps;
}
+ (void)navAppleMapWithEndLocation:(CLLocationCoordinate2D)endLocation title:(NSString *)title  {
    // 苹果地图为高德地图 GCJ-02火星坐标系
    CLLocationCoordinate2D gps = endLocation;
    MKMapItem *currentLoc = [MKMapItem mapItemForCurrentLocation];
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:gps addressDictionary:nil]];
    toLocation.name =title;
    NSArray *items = @[currentLoc,toLocation];
    NSDictionary *dic = @{
                          MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving,
                          MKLaunchOptionsMapTypeKey : @(MKMapTypeStandard),
                          MKLaunchOptionsShowsTrafficKey : @(YES)
                          };
    [MKMapItem openMapsWithItems:items launchOptions:dic];
}
+ (NSMutableAttributedString *)addBlankWith:(NSString *)cardNo {
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:cardNo];
    NSInteger blankCount = 0;
    for (int i = 1; i<cardNo.length; i++) {
        if (i%4==0) {
            [att insertAttributedString:[[NSMutableAttributedString alloc] initWithString:@" "] atIndex:i+blankCount];
            blankCount++;
        }
    }
    return att;
}
+ (BOOL)inputTextfield:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replaceString:(NSString *)rangeString number:(NSInteger)number VC:(FYBaseViewController *)ViewController {
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        ViewController.hasDecimal = NO;
    }
    if ([rangeString length] > 0) {
        unichar single = [rangeString characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            //首字母不能为0和小数点
            if([textField.text length] == 0){
                if(single == '.') {
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            if ([textField.text isEqualToString:@"0"]) {
                if (single != '.') {
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }
            //输入的字符是否是小数点
            if (single == '.') {
                if(!ViewController.hasDecimal)//text中还没有小数点
                {
                    ViewController.hasDecimal = YES;
                    return YES;
                    
                }else{
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (ViewController.hasDecimal) {//存在小数点
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= number) {
                        return YES;
                    }else{
                        return NO;
                    }
                }else{
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else {
        return YES;
    }
}
+ (CGFloat)getMessageHeight:(NSString *)mess {
    NSMutableAttributedString *introText = [[NSMutableAttributedString alloc] initWithString:mess];
    introText.yy_font = FONT(13);
    introText.yy_lineSpacing = 3;
    CGSize introSize = CGSizeMake(ScreenWidth-32, CGFLOAT_MAX);
    YYTextLayout *layout = [YYTextLayout layoutWithContainerSize:introSize text:introText];
    CGFloat introHeight = layout.textBoundingSize.height;
    return introHeight;
}
+ (NSMutableAttributedString *)replaceTelephone:(NSString *)phoneNum {
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:phoneNum];
    if (phoneNum.length==11) {
        [att replaceCharactersInRange:RANGE(3, 4) withString:@"****"];
    }
    return att;
}
+ (NSInteger)statusWithType:(NSString *)type {
    if ([type isEqualToString:@"全部"]) {
        return -1;
    }else if ([type isEqualToString:@"正常"]){
        return 0;
    }else if ([type isEqualToString:@"限产"]){
        return 1;
    }else {
        return 2;
    }
}
+ (NSString *)typeWithStatus:(NSInteger)status {
    if (status==0) {
        return @"正常";
    }else if (status==1) {
        return @"限产";
    }else {
        return @"停产";
    }
}
+ (BOOL)feiKong:(id)text {
    if (![text isKindOfClass:[NSNull class]]) {
        return YES;
    }else {
        return NO;
    }
}
+ (NSString *)fixNullText:(NSString *)nullText {
    if ([nullText isKindOfClass:[NSNull class]]) {
        return @"";
    }
    return nullText;
}
+ (NSString *)fixNullTextWithLine:(NSString *)nullText {
    if ([nullText isKindOfClass:[NSNull class]]||!nullText) {
        return @"--";
    }
    return nullText;
}
@end
