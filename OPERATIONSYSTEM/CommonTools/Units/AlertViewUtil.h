//
//  AlertViewUtil.h
//  ManggeekBaseProject
//
//  Created by 车杰 on 2017/8/10.
//  Copyright © 2017年 Jacky. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^HYVoidCallbackBlock) (void);
typedef void (^HYTypeCallbackBlock)(NSInteger type);

@interface AlertViewUtil : NSObject
/*
 * 参数：取消提示框 只有提示效果  不做任何处理
 */
+ (void)showCancelAlertViewWithVC:(UIViewController *)ViewController Title:(NSString *)title Message:(NSString *)message LeftTitle:(NSString *)LeftTitle  callbackBlock:(HYVoidCallbackBlock)callbackBlock;
/*
 * 参数：选择提示框 callbackBlock 执行右边action  callbackBlock2执行左边action
 */
+ (void)showSelectAlertViewWithVC:(UIViewController *)ViewController Title:(NSString *)title Message:(NSString *)message LeftTitle:(NSString *)LeftTitle RightTitle:(NSString *)rightTitle  callBack:(HYTypeCallbackBlock)callBackBlock;

/**
 sheetView

 @param ViewController 需要弹出的对应控制器
 @param title 标题
 @param array 数据源
 @param callBackBlock 回调
 */
+ (void)showSlectSheetViewWithVC:(UIViewController *)ViewController Title:(NSString *)title array:(NSArray*)array callBack:(HYTypeCallbackBlock)callBackBlock;
@end
