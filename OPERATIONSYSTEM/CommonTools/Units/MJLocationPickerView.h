//
//  MJLocationPickerView.h
//  ManggeekBaseProject
//
//  Created by 车杰 on 2017/7/31.
//  Copyright © 2017年 车杰. All rights reserved.
//

#import <UIKit/UIKit.h>
#define PROVINCE_COMPONENT  0
#define CITY_COMPONENT      1
#define DISTRICT_COMPONENT  2
typedef void (^ cancleBlock) (void);
typedef void (^ sureBtnBlock) (NSDictionary *province,NSDictionary *city,NSDictionary *district,NSString *showMsg);
@interface MJLocationPickerView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,strong) NSMutableArray *allProrince;//省
@property (nonatomic,strong) NSMutableArray *allCity;//市
@property (nonatomic,strong) NSMutableArray *allDistrist;//区
@property (nonatomic,strong)  UIPickerView *picker;
@property (nonatomic,strong) cancleBlock  cancleBlock;
@property (nonatomic,strong) sureBtnBlock  sureBlock;
@property (nonatomic,strong)NSArray *array;
- (instancetype)initWithFrame:(CGRect)frame array:(NSArray *)array;
@end

