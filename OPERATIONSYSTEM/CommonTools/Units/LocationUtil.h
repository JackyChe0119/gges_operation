//
//  LocationUtil.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/28.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationUtil : NSObject<CLLocationManagerDelegate>
@property (nonatomic,strong)CLLocationManager *manager;
@property (nonatomic, strong) CLGeocoder *geoC;
@property (nonatomic,strong)FYBaseViewController *visibleVC;
/**
 开始定位
 */
 - (void)startLocation;
@end

NS_ASSUME_NONNULL_END
