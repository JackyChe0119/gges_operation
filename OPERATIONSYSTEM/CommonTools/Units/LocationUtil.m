//
//  LocationUtil.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/28.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "LocationUtil.h"

@implementation LocationUtil
- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (void)setVisibleVC:(FYBaseViewController *)visibleVC {
    _visibleVC = visibleVC;
}
- (void)startLocation {
    if ([CLLocationManager locationServicesEnabled]) {
        _manager = [[CLLocationManager alloc] init];
        _manager.delegate = self;
        _manager.desiredAccuracy = kCLLocationAccuracyBest;
        /** 由于IOS8中定位的授权机制改变 需要进行手动授权
         * 获取授权认证，两个方法：
         * [self.locationManager requestWhenInUseAuthorization];
         * [self.locationManager requestAlwaysAuthorization];
         */
        [_manager requestWhenInUseAuthorization];
        //开始定位，不断调用其代理方法
        [_manager startUpdatingLocation];
    }else {
        [AlertViewUtil showCancelAlertViewWithVC:_visibleVC Title:@"定位服务不可用，请在设置的隐私服务中开启定位权限" Message:nil LeftTitle:@"知道了" callbackBlock:^{
            NSLog(@"隐私定位被用户关闭，无法访问");
        }];
    }
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    // 1.获取用户位置的对象
    CLLocation *location = [locations lastObject];
    CLLocationCoordinate2D coordinate = location.coordinate;
    NSLog(@"纬度:%f 经度:%f", coordinate.latitude, coordinate.longitude);
    // 2.停止定位
    [manager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc]init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        for (CLPlacemark *place in placemarks) {
            NSLog(@"当前定位位置：%@", place.locality);
        }
    }];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"定位失败原因: %@",[error localizedDescription]);
    switch([error code]) {
        case kCLErrorLocationUnknown:
            // do something...
            break;
        case kCLErrorDenied: //用户拒绝
        {
            [AlertViewUtil showSelectAlertViewWithVC:self.visibleVC Title:@"无法定位到您所在的城市(默认为杭州市)，如需更改，请前去开启GPS定位" Message:nil LeftTitle:@"暂不开启" RightTitle:@"去开启" callBack:^(NSInteger type) {
                if (type==2) {
                    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }
            }];
            [self getAddress:@"杭州市"];
            NSLog(@"用户拒绝定位  ---->默认城市杭州市");
        }
            break;
    }
}

/**
 根据城市名返回经纬度

 @param address 城市名
 */
- (void)getAddress:(NSString *)address {
    if([address length] == 0) {
        return;
    }
    [self.geoC geocodeAddressString:address completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        // CLPlacemark : 地标
        // location : 位置对象
        // addressDictionary : 地址字典
        // name : 地址详情
        // locality : 城市
        if(error == nil) {
//            CLPlacemark *pl = [placemarks firstObject];
//            UserManager *user = [UserManager shareManager];
       
        }else {
            NSLog(@"解析城市出现错误");
        }
    }];
}
#pragma mark -懒加载
-(CLGeocoder *)geoC {
    if (!_geoC) {
        _geoC = [[CLGeocoder alloc] init];
    }
    return _geoC;
}
@end
