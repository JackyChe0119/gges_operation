//
//  HYLocationView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/4/29.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYLocationView.h"

@implementation HYLocationView
- (instancetype)initWithFrame:(CGRect)frame data:(NSArray *)araeData {
    self = [super initWithFrame:frame];
    if (self) {
        _areaTreeArray = araeData;
        [self data:araeData];
        [self baseView];
    }
    return self;
}
/*
 ***  数据源 ****
 */
- (void)data:(NSArray *)aratTree {

    for (NSDictionary *dic in aratTree) {
        NSString *parentId = dic[@"parentId"];
        if ([[dic objectForKey:@"parentId"] isEqualToString:parentId]) {
            [self.allProrince addObject:dic];
        }
    }
    NSDictionary *selectPro = [self.allProrince objectAtIndex:(self.allProrince.count-2)];
    NSArray *cityArray = selectPro[@"children"];
    for (NSDictionary *dic in cityArray) {
          [self.allCity addObject:dic];
    }
    
    if (self.allCity.count>0) {
        NSDictionary *selectCity = [self.allCity objectAtIndex:1];
        NSArray *areaArray = selectCity[@"children"];
        for (NSDictionary *dic in areaArray) {
                [self.allDistrist addObject:dic];
        }
    }
   
}
/*
 ***  布局基础视图 ****
 */
- (void)baseView {
    
    self.backgroundColor = Color_White;
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, 39.5, ScreenWidth, 0.5);
    layer.backgroundColor = [UIColor colorWithHexString:@"#e8e8e8"].CGColor;
    [self.layer addSublayer:layer];
    layer.frame = CGRectMake(0, 39.5, ScreenWidth, 0.5);
    layer.backgroundColor = [UIColor colorWithHexString:@"#e8e8e8"].CGColor;
    [self.layer addSublayer:layer];
    
    // 取消按钮
    UIButton *buttonCancel = [[UIButton alloc] init];
    buttonCancel.frame = CGRectMake(0, 0, 70, 40);
    [buttonCancel setTitle:@"取消" forState:UIControlStateNormal];
    [buttonCancel setTitleColor:Color_666666 forState:UIControlStateNormal];
    buttonCancel.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonCancel addTarget:self action:@selector(buttonClickLocation:) forControlEvents:UIControlEventTouchUpInside];
    buttonCancel.tag = 1;
    [self addSubview:buttonCancel];
    
    UILabel *titleLabel = [UILabel new];
    [titleLabel rect:RECT(70, 0,WIDTH(self)-140, 40) aligment:Center font:13 isBold:NO text:@"行政区域" textColor:Color_666666 superView:self];
    
    // 确定按钮
    UIButton *buttonSure = [[UIButton alloc] init];
    buttonSure.frame = CGRectMake(ScreenWidth - 70, 0, 70, 40);
    [buttonSure setTitle:@"确定" forState:UIControlStateNormal];
    [buttonSure setTitleColor:Color_Common forState:UIControlStateNormal];
    buttonSure.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonSure addTarget:self action:@selector(buttonClickLocation:) forControlEvents:UIControlEventTouchUpInside];
    buttonSure.tag = 2;
    [self addSubview:buttonSure];
    
    _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, ScreenWidth, HEIGHT(self)-40-SafeAreaBottomHeight)];
    _picker.backgroundColor = [UIColor whiteColor];
    _picker.delegate = self;
     [_picker selectRow:0 inComponent:0 animated:NO];
    [_picker selectRow:0 inComponent:1 animated:NO];
    _picker.dataSource = self;
    [self addSubview: _picker];
    
}
- (void)buttonClickLocation:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
        {
            [UIView animateWithDuration:.3 animations:^{
                self.frame = RECT(0, ScreenHeight, WIDTH(self), HEIGHT(self));
            }completion:^(BOOL finished) {
                if (_cancleBlock) {
                    _cancleBlock ();
                }
            }];
        }
            break;
        case 2:
        {
            NSInteger provinceIndex = [self.picker selectedRowInComponent: PROVINCE_COMPONENT];
            NSInteger cityIndex = [self.picker selectedRowInComponent: CITY_COMPONENT];
            NSInteger districtIndex = [self.picker selectedRowInComponent: DISTRICT_COMPONENT];
            
                      NSDictionary *provinceDic = [self.allProrince objectAtIndex: provinceIndex];
            NSDictionary *cityDic;
            NSDictionary *districtDic;
            NSString *showMsgStrring;
            if ([[provinceDic objectForKey:@"name"] isEqualToString:@"全部"]) {
                showMsgStrring = @"全部";
            }else {
                if (self.allCity.count>0) {
                    cityDic = [self.allCity objectAtIndex: cityIndex];
                }
                if (self.allDistrist.count>0) {
                    districtDic = [self.allDistrist objectAtIndex:districtIndex];
                }
            }
            [UIView animateWithDuration:.3 animations:^{
                self.frame = RECT(0, ScreenHeight, WIDTH(self), HEIGHT(self));
            }completion:^(BOOL finished) {
                if (_sureBlock) {
                    _sureBlock (provinceDic,cityDic,districtDic,@"");
                }
            }];
            break;
        }
        default:
            break;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}
#pragma mark- Picker Delegate Methods

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == PROVINCE_COMPONENT) {
        return [[self.allProrince objectAtIndex: row] objectForKey:@"name"];
    }
    else if (component == CITY_COMPONENT) {
        return [[self.allCity objectAtIndex: row] objectForKey:@"name"];
    }
    else {
        return [[self.allDistrist objectAtIndex: row] objectForKey:@"name"];
    }
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

    if (component == PROVINCE_COMPONENT) {
        
        NSDictionary *dic = [self.allProrince objectAtIndex:row];
        [self.allCity removeAllObjects];
        NSArray *cityarray = dic[@"children"];
        [cityarray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.allCity addObject:obj];
        }];
        
        [pickerView selectRow: 0 inComponent: CITY_COMPONENT animated: NO];
        [pickerView reloadComponent: CITY_COMPONENT];
        if (self.allCity.count>0) {
            NSDictionary *city = [self.allCity objectAtIndex:0];
            [self.allDistrist removeAllObjects];
            NSArray *array = city[@"children"];
            [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [self.allDistrist addObject:obj];
            }];
      
        }else {
            [self.allDistrist removeAllObjects];
        }
        [pickerView selectRow: 0 inComponent: DISTRICT_COMPONENT animated: NO];
        [pickerView reloadComponent: DISTRICT_COMPONENT];
    }
    else if (component == CITY_COMPONENT) {
        if (self.allCity.count>0) {
            NSDictionary *city = [self.allCity objectAtIndex:row];
            [self.allDistrist removeAllObjects];
            NSArray *array = city[@"children"];
            [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.allDistrist addObject:obj];
            }];
        }else {
            [self.allDistrist removeAllObjects];
        }
        [pickerView selectRow: 0 inComponent: DISTRICT_COMPONENT animated: YES];
        [pickerView reloadComponent: DISTRICT_COMPONENT];
    }
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == PROVINCE_COMPONENT) {
        return 80;
    }
    else if (component == CITY_COMPONENT) {
        return 100;
    }
    else {
        return 115;
    }
}

#pragma mark =====  UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == PROVINCE_COMPONENT) {
        return [self.allProrince count];
    }
    else if (component == CITY_COMPONENT) {
        return [self.allCity count];
    }
    else {
        return [self.allDistrist count];
    }
}

- (NSMutableArray *)allProrince {
    if (!_allProrince) {
        _allProrince = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _allProrince;
}
- (NSMutableArray *)allCity {
    if (!_allCity) {
        _allCity = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _allCity;
}
- (NSMutableArray *)allDistrist {
    if (!_allDistrist) {
        _allDistrist = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _allDistrist;
}


@end
