//
//  UIScrollView+Refresh.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "UIScrollView+Refresh.h"
#import <objc/message.h>
#import "MJRefresh.h"
@implementation UIScrollView (Refresh)

static void *pageIndexKey = &pageIndexKey;
- (void)setPageIndex:(NSInteger)pageIndex {
    objc_setAssociatedObject(self, &pageIndexKey, @(pageIndex), OBJC_ASSOCIATION_ASSIGN);
}
- (NSInteger)pageIndex {
    return [objc_getAssociatedObject(self, &pageIndexKey) integerValue];
}

static void *refreshKey = &refreshKey;
- (void)setRefreshBlcok:(RefreshBlock)refreshBlcok {
    objc_setAssociatedObject(self, &refreshKey, refreshBlcok, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (RefreshBlock)refreshBlcok {
    return objc_getAssociatedObject(self, &refreshKey);
}

static void *loadMoreKey = &loadMoreKey;
- (void)setLoadMoreBlock:(LoadMoreBlock)loadMoreBlock {
    objc_setAssociatedObject(self, &loadMoreKey, loadMoreBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (LoadMoreBlock)loadMoreBlock {
    return objc_getAssociatedObject(self, &loadMoreKey);
}

/**
 *   新增上拉刷新 下拉加载方法
 **/
- (void)addHeaderRefreshWithAutomaticallyRefresh:(BOOL)autoRefresh refreshBlock:(void (^)(NSInteger))RefreshBlock {
    __weak typeof(self) weakSelf = self;
    self.refreshBlcok = RefreshBlock;
    self.pageIndex = 1;
    [self addHeaderWithCallback:^{
        weakSelf.pageIndex = 1;
        if (weakSelf.refreshBlcok) {
            weakSelf.refreshBlcok(weakSelf.pageIndex);
        }
    }];
    self.headerPullToRefreshText = @"下拉刷新数据";
    self.headerReleaseToRefreshText = @"松开马上刷新";
    self.headerRefreshingText = @"正在刷新中";
    
}
- (void)addFootLoadMoreWithAutomaticallyLoad:(BOOL)autoLoad loadMoreBlock:(void (^)(NSInteger))LoadMoreBlock {
    __weak typeof(self) weakSelf = self;
    self.loadMoreBlock = LoadMoreBlock;
    [self addFooterWithCallback:^{
        weakSelf.pageIndex += 1;
        if (weakSelf.loadMoreBlock) {
            weakSelf.loadMoreBlock(weakSelf.pageIndex);
        }
    }];
    self.footerPullToRefreshText = @"上拉加载更多数据";
    self.footerReleaseToRefreshText = @"松开加载更多数据";
    self.footerRefreshingText = @"正在加载数据中";
}
- (void)endHeaderRefresh {
    [self.header endRefreshing];
}
- (void)endFootLoadMore {
    [self.footer endRefreshing];
}
- (void)endLoading {
    if (self.pageIndex==1) {
        [self.header endRefreshing];
    }else {
        [self.footer endRefreshing];
    }
}
- (void)resetPageNum:(NSInteger)pageNum {
    if (pageNum<=1) {
        self.pageIndex = 1;
    }else {
        self.pageIndex = pageNum-1;
    }
}
@end
