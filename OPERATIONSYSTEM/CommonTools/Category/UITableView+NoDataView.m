//
//  UITableView+NoDataView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/13.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "UITableView+NoDataView.h"
#import <objc/message.h>

@implementation UITableView (NoDataView)
/**
 *   利用runtime 添加属性
 **/
static void *noDataImageStringKey = &noDataImageStringKey;
- (void)setNoDataImageString:(NSString *)noDataImageString {
    objc_setAssociatedObject(self, &noDataImageStringKey, noDataImageString, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (NSString *)noDataImageString {
   return  objc_getAssociatedObject(self, &noDataImageStringKey);
}
static void *marginTopKey = &marginTopKey;

- (void)setMarginTop:(NSInteger)marginTop {
    objc_setAssociatedObject(self, &marginTopKey, @(marginTop), OBJC_ASSOCIATION_ASSIGN);
}
- (NSInteger)marginTop {
    return [objc_getAssociatedObject(self, &marginTopKey) integerValue];
}

static void *noDataContentKey = &noDataContentKey;
- (void)setNoDataContent:(NSString *)noDataContent {
    objc_setAssociatedObject(self, &noDataContentKey, noDataContent, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (NSString *)noDataContent {
    return  objc_getAssociatedObject(self, &noDataContentKey);
}

static void *noNetImageStringKey = &noNetImageStringKey;
- (void)setNoNetImageString:(NSString *)noNetImageString {
    objc_setAssociatedObject(self, &noNetImageStringKey, noNetImageString, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (NSString *)noNetImageString {
    return objc_getAssociatedObject(self, &noNetImageStringKey);
}

static void *noNetContentKey = &noNetContentKey;
- (void)setNoNetContent:(NSString *)noNetContent {
    objc_setAssociatedObject(self, &noNetContentKey, noNetContent, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (NSString *)noNetContent {
    return objc_getAssociatedObject(self, &noNetContentKey);
}

static void *baseViewKey = &baseViewKey;
- (void)setBaseView:(UIView *)baseView {
    objc_setAssociatedObject(self, &baseViewKey, baseView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIView *)baseView {
   return objc_getAssociatedObject(self, &baseViewKey);
}
static void *tipLabelKey = &tipLabelKey;
- (void)setTipLabel:(UILabel *)tipLabel {
    objc_setAssociatedObject(self, &tipLabelKey, tipLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UILabel *)tipLabel {
    return objc_getAssociatedObject(self, &tipLabelKey);
}
static void *noDataImageViewKey = &noDataImageViewKey;
- (void)setNoDataImageView:(UIImageView *)noDataImageView {
    objc_setAssociatedObject(self, &noDataImageViewKey, noDataImageView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIImageView  *)noDataImageView {
    return objc_getAssociatedObject(self, &noDataImageViewKey);
}
static void *dataArrayKey = &dataArrayKey;
- (void)setDataArray:(NSMutableArray *)dataArray {
    objc_setAssociatedObject(self, &dataArrayKey, dataArray, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSMutableArray *)dataArray {
    return objc_getAssociatedObject(self, &dataArrayKey);
}
#pragma mark 新增方法
- (void)reloadWithData {
    __weak typeof(self)weakSelf = self;
    if (self.dataArray.count==0) {
        if (!weakSelf.baseView) {
            self.baseView = [UIView createViewWithFrame:RECT(0, 0, WIDTH(self), HEIGHT(self)) color:[UIColor clearColor]];
            [self addSubview:self.baseView];
            self.noDataImageView = [UIImageView createImageViewWithFrame:RECT(ScreenWidth/2.0-50, self.marginTop, 100, 80) imageName:@"icon_noData"];
            [self.baseView addSubview:self.noDataImageView];
            
            self.tipLabel = [UILabel new];
            [self.tipLabel rect:RECT(10, BOTTOM(self.noDataImageView)+10, ScreenWidth-20, 30) aligment:Center font:13 isBold:YES text:self.noDataContent textColor:colorWithHexString(@"#cdcdcd") superView:self.baseView];
        }
        self.baseView.hidden = NO;
        self.tipLabel.text = self.noDataContent;
        [self sendSubviewToBack:self.baseView];
    }else {
        self.baseView.hidden = YES;
    }
    [self reloadData];
}
@end
