//
//  UIView+Create.h
//  H3CMagic
//
//  Created by mc on 15/12/14.
//  Copyright © 2015年 KongGeek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Create)
/**
 快速给View添加4边阴影
 参数:阴影透明度，默认0
 */
- (void)addProjectionWithShadowOpacity:(CGFloat)shadowOpacity;
/**
 快速给View添加4边框
 参数:边框宽度
 */
- (void)addBorderWithWidth:(CGFloat)width;
/**
 快速给View添加4边框
 width:边框宽度
 borderColor:边框颜色
 */
- (void)addBorderWithWidth:(CGFloat)width borderColor:(UIColor *)borderColor;
/**
 快速给View添加圆角
 参数:圆角半径
 */
- (void)addRoundedCornersWithRadius:(CGFloat)radius;
/**
 快速给View添加圆角
 radius:圆角半径
 corners:且那几个角
 类型共有以下几种:
 typedef NS_OPTIONS(NSUInteger, UIRectCorner) {
 UIRectCornerTopLeft,
 UIRectCornerTopRight ,
 UIRectCornerBottomLeft,
 UIRectCornerBottomRight,
 UIRectCornerAllCorners
 };
 使用案例:[self.mainView addRoundedCornersWithRadius:10 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight]; // 切除了左下 右下
 */
- (void)addRoundedCornersWithRadius:(CGFloat)radius byRoundingCorners:(UIRectCorner)corners;
/**
 给视图添加渐变
 
 @param frame 坐标布局
 @param colors 颜色组
 @param startPoint 开始位置 （0，1）左下角 （1，0）右上角
 @param endPoint 结束位置
 @return 基础视图
 */
- (UIView *)addGradientLayerWithFrame:(CGRect)frame Colors:(NSArray *)colors startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint;

/**
 添加默认渐变颜色

 @param frame 桌标
 @return 基础视图
 */
- (UIView *)addDefaultGradientLayerWithFrame:(CGRect)frame;
/**
 创建视图

 @param frame 坐标
 @param color 颜色
 @return 视图
 */
+ (UIView *)createViewWithFrame:(CGRect)frame color:(UIColor *)color;

/**
 创建视图

 @param frame 坐标
 @param color 颜色
 @param cornerRadius 圆角
 @return 视图
 */
+ (UIView *)createViewWithFrame:(CGRect)frame color:(UIColor *)color cornerRadius: (CGFloat)cornerRadius;
//灰色线
+(UIView *)createLineViewWithFrame:(CGRect)frame;
- (UIView *)addlineWithFrame:(CGRect)frame color:(UIColor *)color;
- (UIView *)addRedGradientLayerWithFrame:(CGRect)frame;
@end
