//
//  UIImageView+Create.m
//  KangDuKe
//
//  Created by xnj on 16/12/12.
//  Copyright © 2016年 MJ Science and Technology Ltd. All rights reserved.
//

#import "UIImageView+Create.h"
#import <objc/message.h>

@implementation UIImageView (Create)
static void *isVaildKey = &isVaildKey;

- (void)setIsVaild:(BOOL)isVaild {
    objc_setAssociatedObject(self, &isVaildKey, @(isVaild), OBJC_ASSOCIATION_ASSIGN);
}
- (BOOL)isVaild {
    return [objc_getAssociatedObject(self, &isVaildKey) boolValue];
}
+ (UIImageView *) createImageViewWithFrame:(CGRect)frame imageName:(NSString *)imageName{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    UIImage *image = [UIImage imageNamed:imageName];
    imageView.image = image;
    return imageView;
}
@end
