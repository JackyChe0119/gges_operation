//
//  UITextField+LenthAndTypeLimit.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/26.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "UITextField+LenthAndTypeLimit.h"
#import <objc/message.h>

@implementation UITextField (LenthAndTypeLimit)

static void *maxLenthKey = &maxLenthKey;
- (void)setMaxLenth:(NSInteger)maxLenth {
    objc_setAssociatedObject(self, &maxLenthKey, @(maxLenth), OBJC_ASSOCIATION_ASSIGN);
}
- (NSInteger)maxLenth {
    return [objc_getAssociatedObject(self, &maxLenthKey) integerValue];
}

static void *regularKey = &regularKey;
- (void)setRegular:(NSString *)regular {
    objc_setAssociatedObject(self, &regularKey, regular, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
- (NSString *)regular {
   return  objc_getAssociatedObject(self, &regularKey);
}

static void *textRangeKey = &textRangeKey;
- (void)setTetxRange:(NSRange)tetxRange {
    NSValue *value = [NSValue valueWithRange:tetxRange];
    objc_setAssociatedObject(self, &textRangeKey, value, OBJC_ASSOCIATION_ASSIGN);
}
- (NSRange )tetxRange {
    return [objc_getAssociatedObject(self, &textRangeKey) rangeValue];
}

/**********text光标位置的获取************/
- (NSRange) selectedRangeTextField {
    UITextPosition* beginning = self.beginningOfDocument;
    UITextRange* selectedRange = self.selectedTextRange;
    UITextPosition* selectionStart = selectedRange.start;
    UITextPosition* selectionEnd = selectedRange.end;
    const NSInteger location = [self offsetFromPosition:beginning toPosition:selectionStart];
    const NSInteger length = [self offsetFromPosition:selectionStart toPosition:selectionEnd];
    return NSMakeRange(location, length);
}
//光标的赋值
- (void) setSelectedRange:(NSRange) range  // 备注：UITextField必须为第一响应者才有效
{
    if (range.location >= self.maxLenth) {
        return;
    }
    UITextPosition* beginning = self.beginningOfDocument;
    UITextPosition* startPosition = [self positionFromPosition:beginning offset:range.location];
    UITextPosition* endPosition = [self positionFromPosition:beginning offset:range.location + range.length];
    UITextRange* selectionRange = [self textRangeFromPosition:startPosition toPosition:endPosition];
    [self setSelectedTextRange:selectionRange];
}
/*************************/
- (void)addObserverTextFieldEditChangeWithLenth:(NSInteger)maxLenth inputType:(ImportChatType)inpuType {
    self.maxLenth =maxLenth;
    self.regular = [self ImportChatTypeToRegular:inpuType];
    [self addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}
- (void)textFieldDidChange:(id)sender {
    self.tetxRange = [self selectedRangeTextField];
    NSLog(@"length:%ld    location:%ld",self.tetxRange.length,self.tetxRange.location);
    UITextField *textField = self;
    NSString *toBeString = textField.text;
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position1 = [textField positionFromPosition:selectedRange.start offset:0];
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position1 || !selectedRange) {
        if (self.regular.length) {
            toBeString = [self stringisMyNeed:self.text andPattern:self.regular];
            textField.text = toBeString;
        }
        if (toBeString.length > self.maxLenth) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:self.maxLenth];
            if (rangeIndex.length == 1) {
                textField.text = [toBeString substringToIndex:self.maxLenth];
            }
            else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.maxLenth)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
    [self setSelectedRange:self.tetxRange];
}
- (NSString *)ImportChatTypeToRegular:(ImportChatType)type{
    NSString *retuStr;
    switch (type) {
        case ImportChatTypeIDCard:
            retuStr = @"[0-9X]";
            break;
        case ImportChatTypeNumber:
            retuStr = @"[0-9]";
            break;
        case ImportChatTypeEnglish:
            retuStr = @"[A-Za-z]";
            break;
        case ImportChatTypeBigEnglish:
            retuStr = @"[A-Z|0-9]";
            break;
        case ImportChatTypeChinese:
            retuStr = @"[\u4e00-\u9fa5]";
            break;
        case ImportChatTypePunctuation:
            retuStr = @"[\\p{P}\\p{Z}+=^~<>$¥]";
            break;
        case ImportChatTypeNumberAndEnglish:
            retuStr = @"[0-9]|[A-Za-z]";
            break;
        case ImportChatTypeNumberAndEnglishXiahuaxian:
            retuStr = @"[0-9]|[A-Za-z]|[_]";
            break;
        case ImportChatTypeEmail:
            retuStr = @"[0-9]|[A-Za-z]|[.]|[@]";
            break;
        case ImportChatTypeNumberAndChinese:
            retuStr = @"[0-9]|[\u4e00-\u9fa5]";
            break;
        case ImportChatTypeNumberAndPunctuation:
            retuStr = @"[0-9]|[.]";
            break;
        case ImportChatTypeEnglishAndChinese:
            retuStr = @"[A-Za-z\u4e00-\u9fa5]";
            break;
        case ImportChatTypeEnglishAndPunctuation:
            retuStr = @"[A-Za-z]|[\\p{P}\\p{Z}+=^~<>$¥]";
            break;
        case ImportChatTypeChineseAndPunctuation:
            retuStr = @"[\u4e00-\u9fa5]|[\\p{P}\\p{Z}+=^~<>$¥]";
            break;
        case ImportChatTypeNumberEnglishChinese:
            retuStr = @"[0-9A-Za-z\u4e00-\u9fa5]";
            break;
        case ImportChatTypeNumberEnglishChinesePunctuation:
            retuStr = @"[0-9A-Za-z\u4e00-\u9fa5]|[\\p{P}\\p{Z}+=^~<>$¥]";
            break;
        case ImportChatTypeNumberAndBigEnglish:
            retuStr = @"[0-9]|[A-Z]";
            break;
        case ImportChatTypeAll:
            retuStr = @"";
            break;
            
    }
    return retuStr;
}
- (NSString *)stringisMyNeed:(NSString *)str andPattern:(NSString *)pattern{
    
    NSRange range;
    for(int i=0; i<str.length; i+=range.length){
        range = [str rangeOfComposedCharacterSequenceAtIndex:i];
        NSString *s = [str substringWithRange:range];
        BOOL isdel = [self isNeed:s WithPattern:pattern];
        if (!isdel) {
            str = [str stringByReplacingCharactersInRange:range withString:@""];
            i-=range.length;
        }
    }
    return str;
}

- (BOOL)isNeed:(NSString *)str WithPattern:(NSString *)pattern{
    //[0-9]|[A-Za-z]|[\u4e00-\u9fa5]
    NSRegularExpression *tLetterRegularExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger tLetterMatchCount = [tLetterRegularExpression numberOfMatchesInString:str options:NSMatchingReportProgress range:NSMakeRange(0, str.length)];
    if(tLetterMatchCount>=1){
        return YES;
    }else{
        self.tetxRange = NSMakeRange(self.tetxRange.location - 1, self.tetxRange.length);
        return NO;
    }
}

@end
