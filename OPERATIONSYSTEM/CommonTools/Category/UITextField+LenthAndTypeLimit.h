//
//  UITextField+LenthAndTypeLimit.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/26.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,ImportChatType) {
    ImportChatTypeIDCard,                           //身份证号
    ImportChatTypeNumber,                           //纯数字
    ImportChatTypeEnglish,                          //英文
    ImportChatTypeBigEnglish,                          //大写英文
    ImportChatTypeChinese,                          //汉字
    ImportChatTypePunctuation,                      //标点
    ImportChatTypeNumberAndEnglishXiahuaxian,       //数字加英文下划线
    ImportChatTypeNumberAndEnglish,                 //数字加英文
    ImportChatTypeEmail,                            //邮箱
    ImportChatTypeNumberAndChinese,                 //数字加中文
    ImportChatTypeNumberAndPunctuation,             //数字加标点
    ImportChatTypeEnglishAndChinese,                //英文加中文
    ImportChatTypeEnglishAndPunctuation,            //英文加标点
    ImportChatTypeChineseAndPunctuation,            //中文加标点
    ImportChatTypeNumberEnglishChinese,             //数字中文英文
    ImportChatTypeNumberEnglishChinesePunctuation,  //数字中文英文标点
    ImportChatTypeNumberAndBigEnglish,                 //数字加大i写英文
    ImportChatTypeAll,                              //不限
};
NS_ASSUME_NONNULL_BEGIN

@interface UITextField (LenthAndTypeLimit)
@property (nonatomic,assign)NSInteger maxLenth;//限制长度
@property (nonatomic,copy)NSString *regular;//输入限制的正则类型
@property (nonatomic,assign)NSRange tetxRange;//光标的位置

/**
 添加输入检测  限制长度和类型

 @param maxLenth 限制长度
 @param inpuType 限制类型
 */
- (void)addObserverTextFieldEditChangeWithLenth:(NSInteger)maxLenth inputType:(ImportChatType)inpuType;

@end

NS_ASSUME_NONNULL_END
