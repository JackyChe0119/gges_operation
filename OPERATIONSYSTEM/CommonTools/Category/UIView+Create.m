//
//  UIView+Create.m
//  H3CMagic
//
//  Created by mc on 15/12/14.
//  Copyright © 2015年 KongGeek. All rights reserved.
//

#import "UIView+Create.h"

@implementation UIView (Create)

#pragma mark - frame

#pragma mark - 快速添加阴影
- (void)addProjectionWithShadowOpacity:(CGFloat)shadowOpacity {
    self.layer.shadowColor = [UIColor blackColor].CGColor;//shadowColor阴影颜色
    self.layer.shadowOffset = CGSizeMake(10,10);//shadowOffset阴影偏移,x向右偏移2，y向下偏移6，默认(0, -3),这个跟shadowRadius配合使用
    self.layer.shadowOpacity = shadowOpacity;//阴影透明度，默认0
    self.layer.shadowRadius = 5;//阴影半径，默认5
}
- (void)addBorderWithWidth:(CGFloat)width {
    self.layer.borderWidth = width;
    self.layer.borderColor = [UIColor blackColor].CGColor;
}
- (void)addBorderWithWidth:(CGFloat)width borderColor:(UIColor *)borderColor {
    self.layer.borderWidth = width;
    self.layer.borderColor = borderColor.CGColor;
}
- (void)addRoundedCornersWithRadius:(CGFloat)radius {
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}
- (void)addRoundedCornersWithRadius:(CGFloat)radius byRoundingCorners:(UIRectCorner)corners{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}
+ (UIView *)createViewWithFrame:(CGRect)frame color:(UIColor *)color{
    
    UIView *view = [[UIView alloc]initWithFrame:frame];
    view.backgroundColor = color;
    return view;
}
+ (UIView *)createViewWithFrame:(CGRect)frame color:(UIColor *)color cornerRadius: (CGFloat)cornerRadius {
    UIView *view = [[UIView alloc]initWithFrame:frame];
    view.backgroundColor = color;
    view.layer.cornerRadius = cornerRadius;
    view.layer.masksToBounds = YES;
    return view;
}
//灰色线
+ (UIView *)createLineViewWithFrame:(CGRect)frame{
    UIView *lineView = [[UIView alloc] initWithFrame:frame];
    return lineView;
}
- (UIView *)addGradientLayerWithFrame:(CGRect)frame Colors:(NSArray *)colors startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = colors;
    gradient.startPoint = startPoint;
    gradient.endPoint = endPoint;
    [self.layer addSublayer:gradient];
    return self;
}
- (UIView *)addDefaultGradientLayerWithFrame:(CGRect)frame {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = @[(id)Color_Common.CGColor,(id)colorWithHexString(@"#69ADFF").CGColor];
    gradient.startPoint = POINT(0,0.5);
    gradient.endPoint = POINT(1,0.5);
    [self.layer addSublayer:gradient];
    return self;
}
- (UIView *)addRedGradientLayerWithFrame:(CGRect)frame {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = @[(id)colorWithHexString(@"#E1715B").CGColor,(id)colorWithHexString(@"#D65447").CGColor];
    gradient.startPoint = POINT(0,0.5);
    gradient.endPoint = POINT(1,0.5);
    [self.layer addSublayer:gradient];
    return self;
}
- (UIView *)addlineWithFrame:(CGRect)frame color:(UIColor *)color{
    CALayer *layer = [[CALayer alloc]init];
    layer.frame = frame;
    layer.backgroundColor = color.CGColor;
    [self.layer addSublayer:layer];
    return self;
}
@end
