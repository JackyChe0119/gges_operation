//
//  UITableView+NoDataView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/13.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (NoDataView)
@property (nonatomic,copy)NSString *noDataImageString;//无数据图片
@property (nonatomic,copy)NSString *noDataContent; //无数据内容
@property (nonatomic,copy)NSString *noNetImageString; //无网络图片
@property (nonatomic,copy)NSString *noNetContent; //无网络内容
@property (nonatomic,assign)NSInteger marginTop;//居上间距
@property (nonatomic,strong)NSMutableArray *dataArray;//数据源
@property (nonatomic,retain)UIView *baseView;//基础视图
@property (nonatomic,retain)UIImageView *noDataImageView;//无数据图片
@property (nonatomic,retain)UILabel *tipLabel;//无数据标签
//@property (nonatomic,retain)UIButton *operationButton; //无数据按钮
/*
 * 参数：无数据刷新
 */
- (void)reloadWithData;
@end

NS_ASSUME_NONNULL_END
