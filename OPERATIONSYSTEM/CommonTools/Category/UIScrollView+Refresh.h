//
//  UIScrollView+Refresh.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^RefreshBlock)(NSInteger pageIndex);
typedef void (^LoadMoreBlock)(NSInteger pageIndex);

@interface UIScrollView (Refresh)
@property (nonatomic,assign) NSInteger pageIndex;
@property (nonatomic,copy)RefreshBlock refreshBlcok;
@property (nonatomic,copy)LoadMoreBlock loadMoreBlock;
/*
 *  @brief 添加下拉刷新
 *  参数说明
 *  @param autoRefresh  是否自动刷新
 *  @param RefreshBlock  刷新回调
 *  @return  void
 */
- (void)addHeaderRefreshWithAutomaticallyRefresh:(BOOL)autoRefresh refreshBlock:(void (^)(NSInteger pageIndex))RefreshBlock;
/*
 *  @brief 添加上拉加载更多数据
 *  参数说明
 *  @param autoRefresh  是否自动加载
 *  @param RefreshBlock  刷新回调
 *  @return  void
 */
- (void)addFootLoadMoreWithAutomaticallyLoad:(BOOL)autoLoad loadMoreBlock:(void (^)(NSInteger pageIndex))LoadMoreBlock;
/*
 * 参数：结束刷新数据
 */
- (void)endHeaderRefresh;
/*
 * 参数：结束加载更多
 */
- (void)endFootLoadMore;
- (void)endLoading;
- (void)resetPageNum:(NSInteger)pageNum;
@end

NS_ASSUME_NONNULL_END
