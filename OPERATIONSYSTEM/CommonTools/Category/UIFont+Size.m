//
//  UIFont+Size.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/14.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "UIFont+Size.h"

@implementation UIFont (Size)
+(void)load {
//    Method newMethod = class_getClassMethod([self class], @selector(CJFontWithSize:));
//    Method systemMethod = class_getClassMethod([self class], @selector(systemFontOfSize:));
//    Method newBoldMethod = class_getClassMethod([self class], @selector(CJBoldFontWithSize:));
//    Method systemBoldMethod = class_getClassMethod([self class], @selector(boldSystemFontOfSize:));
//    method_exchangeImplementations(newMethod, systemMethod);
//    method_exchangeImplementations(newBoldMethod, systemBoldMethod);
}
+ (UIFont *)CJFontWithSize:(CGFloat)fontSize {
        UIFont *newFont = nil;
        if (IS_iPhone_5){
            newFont = [UIFont CJFontWithSize:(fontSize - 1)];
        }else if (IS_IPHONE_6_PLUS){
            newFont = [UIFont CJFontWithSize:fontSize + 1];
        }else if (IS_iPhoneXs_masX) {
            newFont = [UIFont CJFontWithSize:fontSize + 1];
        } else{
            newFont = [UIFont CJFontWithSize:fontSize];
        }
    return newFont;
}
+ (UIFont *)CJBoldFontWithSize:(CGFloat)fontSize {
    UIFont *newFont = nil;
    if (IS_iPhone_5){
        newFont = [UIFont CJBoldFontWithSize:(fontSize-1)];
    }else if (IS_IPHONE_6_PLUS){
        newFont = [UIFont CJBoldFontWithSize:fontSize + 1];
    }else if (IS_iPhoneXs_masX) {
        newFont = [UIFont CJBoldFontWithSize:fontSize + 1];
    } else{
        newFont = [UIFont CJBoldFontWithSize:fontSize];
    }
    return newFont;
}
@end
