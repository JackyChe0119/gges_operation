//
//  NotificationMacro.h
//  KongGeekSample
//
//  Created by Robin on 16/7/2.
//  Copyright © 2016年 KongGeek. All rights reserved.
//

#ifndef NotificationMacro_h

#define NotificationMacro_h

#define LOGIN_SUCCESSFUL @"loginOrRegisterSuccessful" //登录注册成功

#define REQUEST_SESSION_OUT @"requestSessionOut" //权限失效

#define DEFAULT_CARINFO_CHANGED @"defaultCarInfoChanged" //权限失效


#define MAINTAINDEATILNEEDREFRESH @"maintainDatailNeedRefresh" //权限失效



#endif /* NotificationMacro_h */
