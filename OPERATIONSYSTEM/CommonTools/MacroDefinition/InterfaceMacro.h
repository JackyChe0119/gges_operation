//
//  InterfaceMarco.h
//  KongGeekSample
//
//  Created by Robin on 16/7/2.
//  Copyright © 2016年 KongGeek. All rights reserved.
//

#ifndef InterfaceMarco_h
#define InterfaceMarco_h

#define URLAppendAPIServer(path) [NSString stringWithFormat:@"%@%@",URL_RootAPIServer,path]

#define REQUEST_SESSION @"sessionid"
#define REQUEST_VERSION  @"version"
#define REQUEST_ROUTE @"route"
#define REQUEST_TICKS @"ticks"
#define AUTO_LOGIN @"autoLogin"

//测试
//#define URL_RootAPIServer @"http://192.168.0.138:8850/"

//#define URL_RootAPIServer @"http://192.168.0.243:8830/"
//#define URL_LoginRootAPIServer @"http://192.168.0.243:8830/"

//#define URL_RootAPIServer @"http://192.168.0.121:8830/user/"
//#define URL_LoginRootAPIServer @"http://192.168.0.121:8830/"
//http://47.110.82.40:8840/dev
////录数据专用
//#define URL_RootAPIServer @"http://121.40.94.181:8830/user/"
//#define URL_LoginRootAPIServer @"http://121.40.94.181:8830/"
//正式环境
#define URL_LoginRootAPIServer @"http://gges.feiyuanenv.com:8830/"
#define URL_RootAPIServer @"http://gges.feiyuanenv.com:8830/"

//阿里云相关数据
#define ALIYUN_OSS_BUCKET_NAME @"gges"
#define ALIYUN_OSS_IMAGE_DOMAIN @"https://gges.oss-cn-hangzhou.aliyuncs.com"
#define ALIYUN_OSS_SERVER @"http://oss-cn-hangzhou.aliyuncs.com"
#define ALIYUN_OSS_GET_ACCESS_TOKEN @"v1_get_oss_token"
//
//#define ALIYUN_OSS_BUCKET_NAME @"boyioss"
//#define ALIYUN_OSS_IMAGE_DOMAIN @"http://boyioss.oss-cn-qingdao.aliyuncs.com"
//#define ALIYUN_OSS_SERVER @"http://oss-cn-qingdao.aliyuncs.com"
//#define ALIYUN_OSS_GET_ACCESS_TOKEN @"v1_get_oss_token"


#endif /* InterfaceMarco_h */
