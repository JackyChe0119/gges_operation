//
//  ColorMaco.h
//  KongGeekSample
//
//  Created by Robin on 16/7/2.
//  Copyright © 2016年 KongGeek. All rights reserved.
//

#ifndef ColorMaco_h
#define ColorMaco_h

// RGB
#define RGB_A(R,G,B,A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A];
// 16进制颜色赋值
#define colorWithHexString(hex) [UIColor colorWithHexString:hex]

#define Color_White colorWithHexString(@"#ffffff")

#define Color_Line colorWithHexString(@"#e5e5e5")

#define Color_999999 colorWithHexString(@"#999999")

#define Color_cccccc colorWithHexString(@"#cccccc")

#define Color_blue colorWithHexString(@"#7DA6DA")

#define Color_666666 colorWithHexString(@"#666666")

#define Color_333333 colorWithHexString(@"#333333")

#define Color_444444 colorWithHexString(@"#444444")

#define Color_3D3A39 colorWithHexString(@"#3D3A39")

#define Color_BG colorWithHexString(@"#F5F6F8")

#define Color_line_stackGray colorWithHexString(@"#EEEEEE")


#define Color_Green colorWithHexString(@"#33E433")

#define Color_Red_light colorWithHexString(@"#E483AE")

#define Color_Common colorWithHexString(@"#0C78FF")

#define Color_CommonLight colorWithHexString(@"#08a4ff")

#define Color_Money colorWithHexString(@"#D65447")

#define color_Red colorWithHexString(@"#FE647D")

#define color_chartLine colorWithHexString(@"#B8C2CE")


#endif /* ColorMaco_h */
