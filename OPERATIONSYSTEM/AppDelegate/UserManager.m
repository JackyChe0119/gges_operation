//
//  UserManager.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/26.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

+ (instancetype) shareManager {
    static  UserManager*_shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareManager = [super allocWithZone:NULL];
    });
    return _shareManager;
}
// 防止外部调用alloc 或者 new
+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [UserManager shareManager];
}
// 防止外部调用copy
- (id)copyWithZone:(nullable NSZone *)zone {
    return [UserManager shareManager];
}
// 防止外部调用mutableCopy
- (id)mutableCopyWithZone:(nullable NSZone *)zone {
    return [UserManager shareManager];
}
@end
