//
//  HYBaseModel.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

@implementation HYBaseModel
- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dictionary];
    }
    return self;
}
/**
 *  对特殊字段进行处理
 *
 *  @param value value description
 *  @param key   key description
 */
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]) {
        [self setValue:value forKey:@"Id"];
    }
}
+ (NSDictionary *)replacedKeyFromPropertyName {
    return @{@"Id":@"id"};
 }
@end
