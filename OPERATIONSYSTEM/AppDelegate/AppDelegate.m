//
//  AppDelegate.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "IQKeyboardManager.h" //键盘管理类
#import "AFNetworkReachabilityManager.h"
#import "LocationUtil.h"
#import "LoginViewController.h"
#import "ChooseSystemViewController.h"
// 引入JPush功能所需头文件
#import <AMapFoundationKit/AMapFoundationKit.h>
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self registerIQKeyboardManager];//键盘第三方

    [AMapServices sharedServices].apiKey = @"0c3287ce904abb8144722f9865392ad8";

    [self addNetWorkCheck];//添加网络状态监测
        
    [self requestAppInfo];//对个人信息单例话
    
    [application setApplicationIconBadgeNumber:0];
    
    [self requestLimit];
    
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIApplicationBackgroundFetchIntervalNever];
        [[UITableView appearance] setEstimatedRowHeight:0.f];
        [[UITableView appearance] setEstimatedSectionHeaderHeight:0.f];
        [[UITableView appearance] setEstimatedSectionFooterHeight:0.f];
    }
    
    [[UITabBar appearance] setTranslucent:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestSessionOut) name:REQUEST_SESSION_OUT object:nil];

    
    //非强制性登录 进入主页
    if ([UserManager shareManager].autoLogin) {
        if ([UserManager shareManager].funSystem.length==0&&![UserManager shareManager].funSystem) {
            ChooseSystemViewController *chooseSysVC = [ChooseSystemViewController new];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:chooseSysVC];
            self.window.rootViewController = nav;
        }else {
            MainViewController *mainVc = [[MainViewController alloc]init];
            self.window.rootViewController = mainVc;
        }
    }else {
        LoginViewController *mainVc = [[LoginViewController alloc]init];
        self.window.rootViewController = mainVc;
    }
    [self.window makeKeyAndVisible];
    return YES;
}
- (void)requestSessionOut {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UserManager shareManager].needLogin = NO;
    });
    LoginViewController *mainVc = [[LoginViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:mainVc];
    self.window.rootViewController = nav;
}
- (void)applicationWillResignActive:(UIApplication *)application {}
- (void)applicationDidEnterBackground:(UIApplication *)application {}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    [application setApplicationIconBadgeNumber:0]; //清除角标
    [[UIApplication sharedApplication] cancelAllLocalNotifications];//清除APP所有通知消息
    [self requestUpdate];
}
- (void)applicationDidBecomeActive:(UIApplication *)application {}
- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer  API_AVAILABLE(ios(10.0)){
    @synchronized (self) {
        if (_persistentContainer == nil) {
            if (@available(iOS 10.0, *)) {
                _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"OPERATIONSYSTEM"];
            } else {
                
            }
            if (@available(iOS 10.0, *)) {
                [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                    if (error != nil) {
                        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                        abort();
                    }
                }];
            } else {
            }
        }
    }
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}
- (void)requestLimit {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
        [params setValue:@"" forKey:@""];
        [RequestTool requestDataWithUrlSuffix:@"" method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        }];
    });
}
#pragma mark - 启动时需要添加的控件效果
- (void)registerIQKeyboardManager {
    //启动时导入 优化键盘使用效果
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES; // 控制整个功能是否启用。
    manager.shouldResignOnTouchOutside =YES; // 控制点击背景是否收起键盘
    manager.shouldToolbarUsesTextFieldTintColor =YES; // 控制键盘上的工具条文字颜色是否用户自定义
    manager.enableAutoToolbar =YES; // 控制是否显示键盘上的工具条
}
- (void)addNetWorkCheck {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"网络状态未知");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"当前网络不可用");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"当前网络状态为:WIFI");
                break;
             case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"当前网络状态为:移动网络WWAN");
                break;
            default:
                break;
        }
    }];
}
/**
 对个人信息 单例化
 */
- (void)requestAppInfo {
    UserManager *manager = [UserManager shareManager];
    manager.name = [CommonUtil getInfoWithKey:@"name"];
    manager.acountType = [ [CommonUtil getInfoWithKey:@"acountType"] integerValue];
    NSString *FUNSYSTEM =  [CommonUtil getInfoWithKey:@"FUNSYSTEM"];
    NSInteger acountType = [[NSUserDefaults standardUserDefaults] integerForKey:@"acountType"];
    if (acountType) {
        manager.acountType = acountType;
    }
    NSInteger roleGroup = [[NSUserDefaults standardUserDefaults] integerForKey:@"roleGroup"];
    if (roleGroup) {
        manager.roleGroup = roleGroup;
    }
    if (FUNSYSTEM) {
          manager.funSystem = FUNSYSTEM;
    }
    //    商务中心  001003001
    //    实施中心  001003002
    //    运维中心  001003003
    if ([FUNSYSTEM isEqualToString:@"implement"]) {
        manager.deptCode = @"001003002";
    }else if ([FUNSYSTEM isEqualToString:@"operation"]) {
        manager.deptCode = @"001003003";
    }else if ([FUNSYSTEM isEqualToString:@"business"]){
        manager.deptCode = @"001003001";
    }
    NSArray *array = [CommonUtil getInfoWithKey:@"routingArray"];
    manager.routingArray = array;
    manager.Acount = [CommonUtil getInfoWithKey:@"Acount"];
    manager.Id = [CommonUtil getInfoWithKey:@"userId"];
    NSString *psw = [CommonUtil getInfoWithKey:@"Password"];
    if (psw.length>0) {
        manager.Password = psw;
    }else {
        manager.Password = @"";
    }
    BOOL remember  = [[NSUserDefaults standardUserDefaults] boolForKey:@"rememberPsw"];
    if (remember||remember==NO) {
        manager.rememberPsw = remember;
    }else {
        manager.rememberPsw = YES;
    }
    NSString *session = [CommonUtil getInfoWithKey:REQUEST_SESSION];
    if (session.length>0) {
        manager.autoLogin = YES;
    }
}
- (void)requestUpdate {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *localVersion = [CommonUtil getInfoWithKey:@"localVersion"];
    if ([version isEqualToString:localVersion]&&localVersion) {
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/mobile/update/iosdev" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *versionInfo = responseMessage.data[@"data"];
                if (!versionInfo||![CommonUtil feiKong:version]) {
                    return ;
                }
                NSString *requestVersion = versionInfo[@"apkVersion"]; //请求的版本
                if (![version isEqualToString:requestVersion]) { //版本是否一致 不一致 执行是否更新操作
                    [CommonUtil setInfo:requestVersion forKey:@"localVersion"];
                    if ([versionInfo[@"prop"] integerValue]==1) { //有需要弹出
                        if ([versionInfo[@"forceUpdate"] integerValue]==1) { //强制更新
                            BOOL TemporarilyNotUpdate = [[NSUserDefaults standardUserDefaults] boolForKey:@"TemporarilyNotUpdate"];
                            if (TemporarilyNotUpdate) { //暂不更新是否点击过
                                return ;
                            }
                            [self showAlert:0];
                        }else { //不强制更新
                            [UserManager shareManager].isForce = YES;
                            [self showAlert:1];
                        }
                    }else {
                        //不需要弹出  不用处理
                    }
                }
            }else {
                if ([UserManager shareManager].isForce) {
                    [self showAlert:1];
                }
            }
        });
    }];
}
- (void)showAlert:(NSInteger)type {
    if (type==1) {
        [AlertViewUtil showCancelAlertViewWithVC:MainWindow.rootViewController Title:@"温馨提示！" Message:@"版本已更新，如不更新，当前版本暂时无法使用" LeftTitle:@"更新" callbackBlock:^{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TemporarilyNotUpdate"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1484906853"]];
        }];
    }else {
        [AlertViewUtil showSelectAlertViewWithVC:MainWindow.rootViewController Title:@"温馨提示！" Message:@"版本已更新，为了您的使用体验请前往更新" LeftTitle:@"暂不更新" RightTitle:@"去更新" callBack:^(NSInteger type) {
            if (type==2) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TemporarilyNotUpdate"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1484906853"]];
            }else {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TemporarilyNotUpdate"]; //暂不更新
            }
        }];
    }
}
#pragma mark - 消息推送相关
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    /// Required - 注册 DeviceToken
       if (![deviceToken isKindOfClass:[NSData class]]) return;
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    NSLog(@"deviceToken:%@",hexToken);
    [[NSUserDefaults standardUserDefaults] setObject:hexToken forKey:DEVICE_TOKEN];
    [UserManager shareManager].deveicedToken = hexToken;
}

@end
