//
//  HYBaseModel.h
//  OPERATIONSYSTEM
//  基础Model  可继承
//  Created by 车杰 on 2019/3/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYBaseModel : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
