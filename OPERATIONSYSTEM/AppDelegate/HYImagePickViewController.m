//
//  HYImagePickViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/18.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYImagePickViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "RSKImageCropViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/PHPhotoLibrary.h>
typedef NS_ENUM(NSUInteger, KJImagePickerMode) {
    KJImagePickerModeNomal = 1,
    KJImagePickerModeCircle,
    KJImagePickerModeSquare,
    KJImagePickerModeCustom,
};
@interface HYImagePickViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,RSKImageCropViewControllerDelegate>
@property (strong, nonatomic) UIViewController *upperViewController;
@property (assign, nonatomic) KJImagePickerMode imagePickerMode;
@property (assign, nonatomic) SourceType type;
@property (assign, nonatomic) float ratio;
@property (nonatomic,copy) void (^successBlock)(UIImage *image);
@property (nonatomic,copy) void (^cancelBlock)(void);
@end

@implementation HYImagePickViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
}

#pragma mark - Private Method

+ (id)presentViewController:(UIViewController *)upperViewController sourceType:(SourceType)sourceType {
    HYImagePickViewController *vc = [[HYImagePickViewController alloc] init];
    if (sourceType == SourceTypeCamera) {
        vc.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else if (sourceType == SourceTypePhotoLibrary) {
        vc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }else if (sourceType == SourceTypeSavedPhotosAlbum) {
        vc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [upperViewController presentViewController:vc animated:YES completion:nil];
    vc.upperViewController = upperViewController;
    return vc;
}

- (void)pushRSKViewControllerWithImage:(UIImage *)image {
    RSKImageCropViewController *imageCropVC;
    if (self.imagePickerMode == KJImagePickerModeCircle) {
        imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeCircle];
    }else if (self.imagePickerMode == KJImagePickerModeSquare) {
        imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeSquare];
    }else if (self.imagePickerMode == KJImagePickerModeCustom) {
        imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeCustom];
    }
    imageCropVC.delegate = self;
    imageCropVC.ratio = self.ratio;
    [imageCropVC setHidesBottomBarWhenPushed:YES];
    [self presentViewController:imageCropVC animated:YES completion:nil];
}

+ (id)showImagePickControllerIn:(UIViewController *)upperViewController sourceType:(SourceType)sourceType image:(void (^)(UIImage *))finish imagePickerControllerDidCancel:(void (^)(void))cancel {
    HYImagePickViewController *vc = [self presentViewController:upperViewController sourceType:sourceType];
    vc.successBlock = finish;
    vc.cancelBlock = cancel;
    vc.imagePickerMode = KJImagePickerModeNomal;
    return vc;
}

+ (id)showRSKCircleImagePickControllerIn:(UIViewController *)upperViewController sourceType:(SourceType)sourceType image:(void (^)(UIImage *))finish imagePickerControllerDidCancel:(void (^)(void))cancel {
    HYImagePickViewController *vc = [self presentViewController:upperViewController sourceType:sourceType];
    vc.successBlock = finish;
    vc.cancelBlock = cancel;
    vc.imagePickerMode = KJImagePickerModeCircle;
    if (sourceType==SourceTypeCamera) {
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusRestricted || status == PHAuthorizationStatusDenied) {
                NSString *errorStr = @"应用相机权限受限,请在设置中启用";
                [AlertViewUtil showCancelAlertViewWithVC:upperViewController Title:errorStr Message:@"开启步骤：设置-找到应用-开启权限" LeftTitle:@"知道了" callbackBlock:^{}];
            [AlertViewUtil showSelectAlertViewWithVC:upperViewController Title:errorStr Message:nil LeftTitle:@"知道了" RightTitle:@"去设置" callBack:^(NSInteger type) {
                if (type==1) {
                    NSLog(@"知道了，下回设置");
                }else {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }
            }];
        }
    }
    return vc;
}

+ (id)showRSKSquareImagePickControllerIn:(UIViewController *)upperViewController sourceType:(SourceType)sourceType image:(void (^)(UIImage *))finish imagePickerControllerDidCancel:(void (^)(void))cancel {
    HYImagePickViewController *vc = [self presentViewController:upperViewController sourceType:sourceType];
    vc.successBlock = finish;
    vc.cancelBlock = cancel;
    vc.imagePickerMode = KJImagePickerModeSquare;
    return vc;
}

+ (id)showRSKCustomImagePickControllerIn:(UIViewController *)upperViewController sourceType:(SourceType)sourceType withRatio:(float)ratio image:(void (^)(UIImage *))finish imagePickerControllerDidCancel:(void (^)(void))cancel {
    HYImagePickViewController *vc = [self presentViewController:upperViewController sourceType:sourceType];
    vc.successBlock = finish;
    vc.cancelBlock = cancel;
    vc.imagePickerMode = KJImagePickerModeCustom;
    vc.ratio = ratio;
    return vc;
}

#pragma mark - Delegate Method: UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (self.imagePickerMode == KJImagePickerModeNomal) {
        [picker dismissViewControllerAnimated:YES completion:^{
            if (_successBlock) {
                _successBlock(image);
            }
        }];
    }else {
        [self pushRSKViewControllerWithImage:image];
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:^{
        if (_cancelBlock) {
            _cancelBlock();
        }
    }];
}

#pragma mark - Delegate Method: RSKImageCropViewControllerDelegate

- (void)imageCropViewController:(RSKImageCropViewController *)controller didCropImage:(UIImage *)croppedImage usingCropRect:(CGRect)cropRect {
    [controller dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:YES completion:^{
            if (_successBlock) {
                _successBlock(croppedImage);
            }
        }];
    }];
    
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:YES completion:^{
            if (_cancelBlock) {
                _cancelBlock();
            }
        }];
    }];
}


@end
