//
//  FYBaseViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/6/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FYBaseViewController : UIViewController
@property (nonatomic,strong)UIView *shadowView;
@property (nonatomic,strong)UIView *customNavView;
@property (nonatomic,assign)BOOL noMoreData;//无更多数据状态
@property (nonatomic,assign) BOOL hasDecimal;//是否含有小数点

/**
 *   左侧按钮或返回按钮
 **/
- (void)setLeftButtonImage:(NSString *)imageName;
/**
 *   右侧按钮
 **/
- (void)setRightButtonImage:(NSString *)imageName;
/*
 * 参数：右侧按钮标题
 */
- (void)navgationRightButtonTitle:(NSString *)title color:(UIColor *)color;
/*
 * 参数：导航标题
 */
- (void)setNavTitle:(NSString *)title Color:(UIColor *)color;
/*
 * 参数：信息提示框
 */
- (void)showHUDMessage:(NSString *)content;
/*
 * 参数：不带文字的活动指示器
 */
- (void)showProgressHud;
/*
 * 参数：带文本的活动指示器
 */
- (void)showProgressHudWithTitle:(NSString *)title;
/*
 * 参数：隐藏活动指示器
 */
- (void)hiddenProgressHud;
/*
 * 参数：划线
 */
- (void )addLineViewWithFrame:(CGRect)frame Color:(UIColor *)color supView:(UIView *)supview;
/*
 * 参数：隐藏导航栏下划线
 */
- (void)hideNavBarLineView;
/*
 * 参数：重写使用 左侧按钮点击
 */
- (void)navgationLeftButtonClick;
/*
 * 参数：重写使用 右侧按钮点击
 */
- (void)navgationRightButtonClick;
/*
 * 参数：阴影遮盖成层
 */
- (void)showShadowView;
/*
 * 参数：隐藏阴影遮盖成层
 * callBack  回调
 */
- (void)hiddenShadowView:(BOOL)remove callBack:(void(^)(void))callBack;
- (void)setLeftButtonTitle:(NSString *)title;
/**
 点击蒙版层
 */
- (void)touchInShadowView;

/**
 视图跳转
 */
- (void)pushWithViewController:(UIViewController *)viewController hidden:(BOOL)hidden;
- (void)presentWithViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
