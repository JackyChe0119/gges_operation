//
//  FYBaseViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/6/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

@interface FYBaseViewController ()<UIGestureRecognizerDelegate,UINavigationBarDelegate>

@end

@implementation FYBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNavBarLineView]; //隐藏导航栏下的线条
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor = Color_White;
    self.navigationController.navigationBar.hidden = YES;
    if (@available(iOS 13.0, *)) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDarkContent;
    } else {
        // Fallback on earlier versions
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
    _customNavView = [[UIView alloc]initWithFrame:RECT(0, 0, ScreenWidth, StatusBarHeight+44)];
    _customNavView.backgroundColor = Color_White;
    [self addLineViewWithFrame:RECT(0, HEIGHT(_customNavView)-.5, WIDTH(_customNavView), .5) Color:Color_Line supView:_customNavView];
//    CAGradientLayer  *gradient = [CAGradientLayer layer];
//    [_customNavView.layer addSublayer:gradient];
//    gradient.frame = _customNavView.bounds;
//    gradient.colors = @[(id)Color_Common.CGColor,(id)Color_CommonLight.CGColor];
//    gradient.startPoint = POINT(0,0.5);
//    gradient.endPoint = POINT(1,0.5);
    [self.view addSubview:_customNavView];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
}
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // 注意：只有非根控制器才有滑动返回功能，根控制器没有。
    // 判断导航控制器是否只有一个子控制器，如果只有一个子控制器，肯定是根控制器
    if (self.navigationController.childViewControllers.count == 1) {
        // 表示用户在根控制器界面，就不需要触发滑动手势，
        return NO;
    }
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer*)otherGestureRecognizer {return[gestureRecognizer isKindOfClass:UIScreenEdgePanGestureRecognizer.class];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}


/**
 *   竖屏操作
 **/
-(BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)setLeftButtonImage:(NSString *)imageName {
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *leftImage = [UIImage imageNamed:imageName];
    leftButton.frame = RECT(5, StatusBarHeight, 44, 44);
    [leftButton setImage:leftImage forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(navgationLeftButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.customNavView addSubview:leftButton];
}
- (void)setLeftButtonTitle:(NSString *)title {
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = RECT(10, StatusBarHeight+9, 26, 26);
    [leftButton setTitle:title forState:UIControlStateNormal];
    [leftButton setTitleColor:Color_3D3A39 forState:UIControlStateNormal];
    [leftButton addBorderWithWidth:1 borderColor:Color_3D3A39];
    [leftButton addRoundedCornersWithRadius:13];
    leftButton.titleLabel.font = FONT(13);
    [leftButton addTarget:self action:@selector(navgationLeftButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.customNavView addSubview:leftButton];
}
- (void)setRightButtonImage:(NSString *)imageName {
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *rightImage = [UIImage imageNamed:imageName];
    rightButton.frame = CGRectMake(ScreenWidth-49, StatusBarHeight,44, 44);
    [rightButton setImage:rightImage forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(navgationRightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.customNavView addSubview:rightButton];
}
//布局导航条右侧标题按钮，颜色
- (void)navgationRightButtonTitle:(NSString *)title color:(UIColor *)color {
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:title forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(ScreenWidth-70, StatusBarHeight, 60, 44);
    [rightButton setTitleColor:color forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
//    rightButton.titleLabel.textAlignment = NSTextAlignmentRight;
    [rightButton addTarget:self action:@selector(navgationRightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.customNavView addSubview:rightButton];
}

- (void)setNavTitle:(NSString *)title Color:(UIColor *)color {
//    self.navigationItem.title = title;
//    [self.navigationController.navigationBar setTitleTextAttributes:
//     @{NSFontAttributeName:[UIFont boldSystemFontOfSize:18],
//       NSForegroundColorAttributeName:color}];
    
    UILabel *titleLabel = [UILabel new];
    [titleLabel rect:RECT(60, StatusBarHeight+2, ScreenWidth-120, 40) aligment:Center font:18 isBold:YES text:title textColor:color superView:self.customNavView];
    titleLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)showHUDMessage:(NSString *)content {
    [CommonToastHUD showTips:content];
}
- (void)showProgressHud {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
- (void)showProgressHudWithTitle:(NSString *)title {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES title:title];
}
- (void)hiddenProgressHud {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void )addLineViewWithFrame:(CGRect)frame Color:(UIColor *)color supView:(UIView *)supview {
    CALayer *layer = [CALayer layer];
    layer.frame = frame;
    layer.backgroundColor = color.CGColor;
    [supview.layer addSublayer:layer];
}
- (void)hideNavBarLineView{
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}
#pragma mark method 方法
- (void)navgationLeftButtonClick {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)navgationRightButtonClick {
    NSLog(@"右侧按钮点击了");
    //根据某个控制器 重写执行操作
}
- (void)showShadowView {
    if (!_shadowView) {
        _shadowView = [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, ScreenHeight) color:[[UIColor blackColor] colorWithAlphaComponent:.4]];
        _shadowView.userInteractionEnabled = YES;
        [_shadowView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchInShadowView)]];
        [MainWindow addSubview:_shadowView];
    }else {
        _shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        [MainWindow bringSubviewToFront:_shadowView];
    }
}
- (void)hiddenShadowView:(BOOL)remove callBack:(void(^)(void))callBack{
    if (_shadowView) {
        [UIView animateWithDuration:.3 animations:^{
            _shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
            [MainWindow sendSubviewToBack:_shadowView];
        }completion:^(BOOL finished) {
            if (remove) {
                [_shadowView removeFromSuperview];
                _shadowView = nil;
            }
            callBack();
        }];
    }
}
- (void)pushWithViewController:(UIViewController *)viewController hidden:(BOOL)hidden {
    if (hidden) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}
- (void)presentWithViewController:(UIViewController *)viewController animated:(BOOL)animated{
    UINavigationController *loginNav = [[UINavigationController alloc]initWithRootViewController:viewController];
    [self presentViewController:loginNav animated:animated completion:nil];
}
//重写 使用
- (void)touchInShadowView {
    
}

@end
