//
//  HY_TabbarView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HY_TabbarView.h"

@implementation HY_TabbarView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Color_White;
        _currentIndex = 0;
        
        UIButton *homeButton = [UIButton createButtonWithFrame:RECT(0, 0, ScreenWidth/2.0, 49) Title:@"首页" Target:self Selector:@selector(customTabbarClick:)];
        homeButton.tag = 100;
        [homeButton setTitleColor:colorWithHexString(@"#4F5D83") forState:UIControlStateNormal];
        homeButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [self addSubview:homeButton];
        
        UIButton *mineButton = [UIButton createButtonWithFrame:RECT(ScreenWidth/2.0, 0, ScreenWidth/2.0, 49) Title:@"我的" Target:self Selector:@selector(customTabbarClick:)];
        mineButton.tag = 200;
        [mineButton setTitleColor:Color_999999 forState:UIControlStateNormal];
        mineButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [self addSubview:mineButton];
    }
    return self;
}
- (void)customTabbarClick:(UIButton *)sender {
    if (sender.tag/100==_currentIndex+1) {
        NSLog(@"当前点击视图相同，无需切换");
        return;
    }
    UIButton *homeButton = (UIButton *)[self viewWithTag:100];
    UIButton *mineButton = (UIButton *)[self viewWithTag:200];
    if (sender.tag==100) {
        _currentIndex = 0;
        [homeButton setTitleColor:colorWithHexString(@"#4F5D83") forState:UIControlStateNormal];
        homeButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [mineButton setTitleColor:Color_999999 forState:UIControlStateNormal];
        mineButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    }else {
        _currentIndex = 1;
        [homeButton setTitleColor:Color_999999 forState:UIControlStateNormal];
        homeButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [mineButton setTitleColor:colorWithHexString(@"#4F5D83") forState:UIControlStateNormal];
        mineButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    }
    if (_delegate&&[_delegate respondsToSelector:@selector(HY_TabbarDidSelectIndex:)]) {
        [_delegate HY_TabbarDidSelectIndex:_currentIndex];
    }
}
- (void)setCurrentIndex:(NSInteger)currentIndex {
    _currentIndex = currentIndex;
    UIButton *homeButton = (UIButton *)[self viewWithTag:100];
    UIButton *mineButton = (UIButton *)[self viewWithTag:200];
    if (currentIndex==0) {
        [homeButton setTitleColor:colorWithHexString(@"#4F5D83") forState:UIControlStateNormal];
        homeButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [mineButton setTitleColor:Color_999999 forState:UIControlStateNormal];
        mineButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    }else {
        [homeButton setTitleColor:Color_999999 forState:UIControlStateNormal];
        homeButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [mineButton setTitleColor:colorWithHexString(@"#4F5D83") forState:UIControlStateNormal];
        mineButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    }
}
@end
