//
//  HY_TabbarView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//声明代理
@protocol HY_TabbarDelegate <NSObject>
// 可选方法
@optional

- (void)HY_TabbarDidSelectIndex:(NSInteger)index;

@end

@interface HY_TabbarView : UIView

@property (nonatomic,assign) id <HY_TabbarDelegate> delegate; //设置代理属性

@property (nonatomic,assign) NSInteger currentIndex;//当前索引值

@end

NS_ASSUME_NONNULL_END
