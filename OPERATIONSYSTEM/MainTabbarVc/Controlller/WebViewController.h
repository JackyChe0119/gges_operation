//
//  WebViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/4/19.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : FYBaseViewController
@property (nonatomic,copy)NSString *bannerUrl;//网页地址
@property (nonatomic,copy)NSString *navTitle;
@end

NS_ASSUME_NONNULL_END
