//
//  MainViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/11.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "MainViewController.h"
#import "MineViewController.h"
#import "TaskCenterViewController.h"
#import "ClientCenterViewController.h"
#import "ProjectListViewController.h"

#import "BCompactCenterViewController.h"
#import "BusinessClientViewController.h"
#import "BProjectCenterViewController.h"

#import "IProjectCenterViewController.h"
@interface MainViewController ()<UITabBarControllerDelegate> {
    NSInteger _currentIndex;
}
//@property (nonatomic,strong)HY_TabbarView *customTabbar;

@end

@implementation MainViewController

//初始化
- (instancetype)init {
   self = [super init];
    if (self) {
        if ([[UserManager shareManager].funSystem isEqualToString:@"operation"]) {//运维中心
            
            ClientCenterViewController *FindVC = [[ClientCenterViewController alloc]init];
            UINavigationController *FindNav = [[UINavigationController alloc]initWithRootViewController:FindVC];
            FindNav.tabBarItem.image=[UIImage imageNamed:@"icon_mineUnselect"];
            FindNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"icon_mineSelect"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            FindNav.tabBarItem.title=@"客户中心";
            [FindNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:Color_Common,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
            
            ProjectListViewController *alertVC = [[ProjectListViewController alloc]init];
            UINavigationController *alertNav = [[UINavigationController alloc]initWithRootViewController:alertVC];
            alertNav.tabBarItem.image=[UIImage imageNamed:@"icon_alertUnselect"];
            alertNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"icon_alertSelect"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            alertNav.tabBarItem.title=@"项目清单";
            [alertNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:Color_Common,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
            self.viewControllers = @[FindNav,alertNav];
            
        }else if ([[UserManager shareManager].funSystem isEqualToString:@"implement"]) {//实施中心
            TaskCenterViewController *HomeVC = [[TaskCenterViewController alloc]init];
            UINavigationController *HomeNav = [[UINavigationController alloc]initWithRootViewController:HomeVC];
            HomeNav.tabBarItem.image=[UIImage imageNamed:@"icon_task_unselect"];
            HomeNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"icon_task_select"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            HomeNav.tabBarItem.title=@"任务中心";
            [HomeNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:Color_Common,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
            
            IProjectCenterViewController *alertVC = [[IProjectCenterViewController alloc]init];
            UINavigationController *alertNav = [[UINavigationController alloc]initWithRootViewController:alertVC];
            alertNav.tabBarItem.image=[UIImage imageNamed:@"icon_alertUnselect"];
            alertNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"icon_alertSelect"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            alertNav.tabBarItem.title=@"项目清单";
            [alertNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:Color_Common,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
            self.viewControllers = @[HomeNav,alertNav];
        }else {//商务中心
            BCompactCenterViewController *HomeVC = [[BCompactCenterViewController alloc]init];
            UINavigationController *HomeNav = [[UINavigationController alloc]initWithRootViewController:HomeVC];
            HomeNav.tabBarItem.image=[UIImage imageNamed:@"icon_homeUnselect"];
            HomeNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"icon_homeSelect"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            HomeNav.tabBarItem.title=@"合同清单";
            [HomeNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:Color_Common,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
            
            BusinessClientViewController *FindVC = [[BusinessClientViewController alloc]init];
            UINavigationController *FindNav = [[UINavigationController alloc]initWithRootViewController:FindVC];
            FindNav.tabBarItem.image=[UIImage imageNamed:@"icon_companyUnselect"];
            FindNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"icon_companySelect"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            FindNav.tabBarItem.title=@"客户清单";
            [FindNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:Color_Common,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
            
            BProjectCenterViewController *alertVC = [[BProjectCenterViewController alloc]init];
            UINavigationController *alertNav = [[UINavigationController alloc]initWithRootViewController:alertVC];
            alertNav.tabBarItem.image=[UIImage imageNamed:@"icon_alertUnselect"];
            alertNav.tabBarItem.selectedImage = [[UIImage imageNamed:@"icon_alertSelect"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            alertNav.tabBarItem.title=@"项目清单";
            [alertNav.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:Color_Common,NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
            self.viewControllers = @[FindNav,HomeNav,alertNav];
        }
        self.selectedIndex = 0;
        self.delegate = self;
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
#pragma HY_TabbarViewd 代理方法
#pragma mark - UITabBarController代理方法 点击事件
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    //点击tabBarItem动画
    if (self.selectedIndex != _currentIndex)[self tabBarButtonClick:[self getTabBarButton]];
}
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    return YES;
}
-(BOOL)shouldAutorotate{
    return NO;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.selectedViewController supportedInterfaceOrientations];
}
- (UIControl *)getTabBarButton{
    NSMutableArray *tabBarButtons = [[NSMutableArray alloc] initWithCapacity:0];
    for (UIView *tabBarButton in self.tabBar.subviews) {
        if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            [tabBarButtons addObject:tabBarButton];
        }
    }
    UIControl *tabBarButton = [tabBarButtons objectAtIndex:self.selectedIndex];
    return tabBarButton;
}
- (void)tabBarButtonClick:(UIControl *)tabBarButton {
    for (UIView *imageView in tabBarButton.subviews) {
        if ([imageView isKindOfClass:NSClassFromString(@"UITabBarSwappableImageView")]) {
            //需要实现的帧动画,这里根据需求自定义
            CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
            animation.keyPath = @"transform.scale";
            animation.values = @[@1.0,@1.1,@0.9,@1.0];
            animation.duration = 0.3;
            animation.calculationMode = kCAAnimationCubic;
            //把动画添加上去就OK了
            [imageView.layer addAnimation:animation forKey:nil];
        }
    }
    _currentIndex = self.selectedIndex;
}

@end
