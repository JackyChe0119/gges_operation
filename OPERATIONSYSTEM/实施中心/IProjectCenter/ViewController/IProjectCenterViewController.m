//
//  IProjectCenterViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "IProjectCenterViewController.h"
#import "MineViewController.h"
#import "AlertListSelectView.h"
#import "SearchView.h"
#import "TaskCenterListCell.h"
#import "ProjectDeatilViewController.h"
#import "CompactModel.h"
#import "HYLocationView.h"
#import "AreaSelectViewController.h"
#import "LegenSelectViewController.h"
static NSString * const TASK_LIST = @"TaskCenterListCell";

@interface IProjectCenterViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)AlertListSelectView *areaView;//区域
@property (nonatomic,strong)AlertListSelectView *chargerView;//负责人
@property (nonatomic,strong)AlertListSelectView *statusView;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)SearchView *searchView;
@property (nonatomic,strong)NSMutableArray *listArray;
@end

@implementation IProjectCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
        __weak typeof(self)weakSelf = self;
    
    _listArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    [self setLeftButtonTitle:@"实"];

    [self setNavTitle:@"项目清单" Color:Color_3D3A39];
    
    [self addLineViewWithFrame:RECT(0, NavHeight, ScreenWidth, 4) Color:Color_BG supView:self.view];
    
    _areaView = [[AlertListSelectView alloc]initWithFrame:RECT(0, NavHeight+8, ScreenWidth/2.0, 40)];
    _areaView.title = @"区域：";
    _areaView.info = @"全部";
    _areaView.operationBlock = ^{
        NSArray  *array = [UserManager shareManager].areaJosn;
        if (!array) {
            [weakSelf requestDivisionCode:YES];
        }else {
           if (array.count==0) {
              [weakSelf showHUDMessage:@"您当前账号暂无区域可选"];
              return ;
            }
            [weakSelf showCityPickerView];
        }
    };
    [self.view addSubview:_areaView];
    
    _chargerView = [[AlertListSelectView alloc]initWithFrame:RECT(ScreenWidth/2.0, NavHeight+8, ScreenWidth/2.0, 40)];
    _chargerView.title = @"实施负责人：";
    _chargerView.info = @"全部";
    _chargerView.leftWidth = 85;
    _chargerView.operationBlock = ^{
        LegenSelectViewController *vc = [[LegenSelectViewController alloc]init];
        vc.url = @"devops/api/v1/contract/applyLeader/list";
        vc.selectWithIdBlock = ^(NSString * _Nonnull name, NSString * _Nonnull Id) {
            weakSelf.chargerView.info = name;
            weakSelf.chargerView.lineId = Id;
            [weakSelf.tableView resetPageNum:1];
            [weakSelf requestBusinessList:1 show:YES];
        };
        vc.navTitle = @"实施负责人";
        [weakSelf presentWithViewController:vc animated:YES];
    };
    [self.view addSubview:_chargerView];
    
    _statusView = [[AlertListSelectView alloc]initWithFrame:RECT(0, BOTTOM(_chargerView), ScreenWidth/2.0, 32)];
    _statusView.title = @"实施状态:";
    _statusView.info = @"全部";
    _statusView.operationBlock = ^{
        [weakSelf showActionSheetWithArray:@[@"全部",@"未分配",@"进行中",@"已完成"] title:@"实施状态"];
    };
    [self.view addSubview:_statusView];
    
    _searchView = [[SearchView alloc]initWithFrame:RECT(ScreenWidth/2.0, BOTTOM(_chargerView), ScreenWidth/2.0, 32) type:1];
    _searchView.placeHold = @"请输入";
    _searchView.searchBlock = ^(NSString * _Nonnull search) {
        [weakSelf.tableView resetPageNum:1];
        [weakSelf requestBusinessList:1 show:YES];
    };
    [self.view addSubview:_searchView];
    
    [self addLineViewWithFrame:RECT(0, BOTTOM(_statusView)+5, ScreenWidth, 4) Color:Color_BG supView:self.view];
    
    
    _tableView = [[UITableView alloc]initWithFrame:RECT(0, BOTTOM(_searchView)+9, ScreenWidth, ScreenHeight-BOTTOM(_searchView)-9-TabBarHeight) style:0];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 64;
    _tableView.noDataContent = @"暂无项目清单";
    _tableView.marginTop = HEIGHT(_tableView)/2.0-80;
    [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0 )];
    [_tableView registerNib:[UINib nibWithNibName:@"TaskCenterListCell" bundle:nil] forCellReuseIdentifier:TASK_LIST];
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
    
    NSArray  *array = [UserManager shareManager].areaJosn;
    if (!array.count) {
        dispatch_queue_t queue = dispatch_queue_create("loadAreaTree", DISPATCH_QUEUE_CONCURRENT);
        dispatch_async(queue, ^{
            [self requestDivisionCode:NO];
        });
    }
    [_tableView addHeaderRefreshWithAutomaticallyRefresh:YES refreshBlock:^(NSInteger pageIndex) {
          [weakSelf requestBusinessList:pageIndex show:NO];
      }];
      [_tableView addFootLoadMoreWithAutomaticallyLoad:YES loadMoreBlock:^(NSInteger pageIndex) {
          if (weakSelf.noMoreData) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [weakSelf showHUDMessage:@"暂无更多数据了"];
                  [weakSelf.tableView endFootLoadMore];
              });
              return;
          }
          [weakSelf requestBusinessList:pageIndex show:NO];
      }];
      [weakSelf requestBusinessList:1 show:YES];
    
}
#pragma mark ---------->>> method
- (void)change:(UISegmentedControl *)seg {
    
}
- (void)navgationLeftButtonClick {
    MineViewController *vc = [MineViewController new];
    [self pushWithViewController:vc hidden:YES];
}
#pragma mark ---------->>> delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskCenterListCell *cell = [tableView dequeueReusableCellWithIdentifier:TASK_LIST];
    CompactModel *model = self.listArray[indexPath.row];
    cell.typeLabel.text = [NSString stringWithFormat:@"实施负责人：%@",[CommonUtil fixNullTextWithLine:model.applyLeader]];
    if (model.applyStatus==0) {
        cell.legenLabel.text = @"实施状态：未分配";
    }else if (model.applyStatus==1) {
        cell.legenLabel.text = @"实施状态：进行中";
    }else {
        cell.legenLabel.text = @"实施状态：已完成";
    }
    cell.nameLabel.text = model.companyName;
    cell.areaLabel.text = [NSString stringWithFormat:@"区域：%@",model.division];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ProjectDeatilViewController *vc = [ProjectDeatilViewController new];
    CompactModel *model = self.listArray[indexPath.row];
    vc.taskId = model.Id;
    [self pushWithViewController:vc hidden:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, .5) color:Color_Line];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([UserManager shareManager].roleGroup==1) {
        return YES;
    }else {
        return NO;
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}
//修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}
//点击删除
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //在这里实现删除操作
    [AlertViewUtil showSelectAlertViewWithVC:self Title:@"删除该项目？" Message:@"删除后该项目将无法查看，是否确定删除？" LeftTitle:@"确定" RightTitle:@"取消" callBack:^(NSInteger type) {
        if (type==1) {
            [self showProgressHud];
            CompactModel *model = self.listArray[indexPath.row];
            NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
            [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/contract//project/%@",model.Id] method:DELETE params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hiddenProgressHud];
                    if ([responseMessage isRequestSuccessful]) {
                        [self showHUDMessage:@"删除成功"];
                        [self.listArray removeObjectAtIndex:indexPath.row];
                        _tableView.dataArray = _listArray;
                        [_tableView reloadWithData];
                    }else {
                        [self showHUDMessage:responseMessage.errorMessage];
                    }
                });
            }];
        }
    }];
}
#pragma mark ---------->>> request
- (void)requestBusinessList:(NSInteger)pageNum show:(BOOL)show {
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[NSNumber numberWithInteger:pageNum] forKey:@"pageNum"];
    [params setValue:[NSNumber numberWithInteger:20] forKey:@"pageSize"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    if (_areaView.lineId&&![_areaView.lineId isEqualToString:@"0"]) {
        [params setValue:_areaView.lineId forKey:@"division"];
    }
    if (![_chargerView.info isEqualToString:@"全部"]) {
        [params setValue:_chargerView.lineId forKey:@"applyLeader"];
    }
    if (![_statusView.info isEqualToString:@"全部"]) {
        if ([_statusView.info isEqualToString:@"未分配"]) {
            [params setValue:[NSNumber numberWithInteger:0] forKey:@"applyStatus"];
        }else if ([_statusView.info isEqualToString:@"进行中"]){
            [params setValue:[NSNumber numberWithInteger:1] forKey:@"applyStatus"];
        }else {
            [params setValue:[NSNumber numberWithInteger:2] forKey:@"applyStatus"];
        }
    }
    if (_searchView.searchTextField.text.length>0) {
            [params setValue:_searchView.searchTextField.text forKey:@"companyName"];
    }
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/apply/project" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
                if (show) {
                     [self hiddenProgressHud];
                 }else {
                     [_tableView endLoading];
                 }
                 if ([responseMessage isRequestSuccessful]) {
                     if (pageNum==1) {
                         [self.listArray removeAllObjects];
                     }
                     NSDictionary *result = responseMessage.data;
                     NSArray *array = result[@"data"];
                     [_listArray addObjectsFromArray:[CompactModel mj_objectArrayWithKeyValuesArray:array]];
                     if (array.count==0) {
                        [_tableView resetPageNum:pageNum];
                         self.noMoreData = YES;
                     }else {
                         self.noMoreData = NO;
                     }
                     _tableView.dataArray = _listArray;
                     [_tableView reloadWithData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestDivisionCode:(BOOL)show {
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[UserManager shareManager].deptCode forKey:@"deptCode"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/division/divisionTreeByDeptCode" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }
            if ([responseMessage isRequestSuccessful]) {
                 NSArray *data = responseMessage.data[@"data"];
                             if (data.count>0) {
                                      NSArray *treeArray = data;
                                      NSMutableArray *totalArray = [[NSMutableArray alloc]initWithCapacity:0];
                                      NSMutableDictionary *all = [[NSMutableDictionary alloc]initWithCapacity:0];
                                      [all setValue:@"全部" forKey:@"name"];
                                      [all setValue:@"0" forKey:@"parentId"];
                                      [totalArray addObject:all];
                                      [totalArray addObjectsFromArray:treeArray];
                                      [UserManager shareManager].areaJosn = (NSArray *)totalArray;
                                      if (show) {
                                         [self showCityPickerView];
                                      }
                             }else{
                                 if (show) {
                                     [self showHUDMessage:@"您当前账号暂无区域可选"];
                                 }
                             }
            }else {
                if (show) {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            }
        });
    }];
}
- (void)showCityPickerView {
    __weak typeof(self)weakSelf = self;
    AreaSelectViewController *vc = [[AreaSelectViewController alloc]init];
    vc.titleStr = @"选择区域";
    vc.parentName = @"全部";
    vc.parentCode = @"0";
    vc.areaBlock = ^(NSString * _Nonnull name, NSString * _Nonnull code) {
        weakSelf.areaView.info = name;
        weakSelf.areaView.lineId= code;
        [weakSelf.tableView resetPageNum:1];
        [weakSelf requestBusinessList:1 show:YES];
    };
    [self presentWithViewController:vc animated:YES];
}
#pragma mark ---------->>> 弹出视图
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    _statusView.info = title;
    [self.tableView resetPageNum:1];
    [self requestBusinessList:1 show:YES];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}
@end
