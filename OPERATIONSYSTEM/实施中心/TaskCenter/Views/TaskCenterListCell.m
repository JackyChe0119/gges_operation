//
//  TaskCenterListCell.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TaskCenterListCell.h"

@implementation TaskCenterListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.centerWidth.constant = (ScreenWidth-30)/3.0;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
