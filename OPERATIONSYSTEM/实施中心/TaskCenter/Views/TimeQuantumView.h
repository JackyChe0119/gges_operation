//
//  TimeQuantumView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeSelectView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TimeQuantumView : UIView
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *centerLabel;
@property (nonatomic,strong)TimeSelectView *fromTimeView,*toTimeView;
@property (nonatomic,assign)double leftWidth;
@property (nonatomic,assign)NSInteger serviceYear;
@property (nonatomic,strong) void (^timeBlock)(void);
- (void)setToTime:(NSString *)fromTime;
@end

NS_ASSUME_NONNULL_END
