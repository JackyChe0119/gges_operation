//
//  LocationView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <MAMapKit/MAMapKit.h>
#import "CustomTitleInputView.h"
NS_ASSUME_NONNULL_BEGIN

@interface LocationView : UIView<MAMapViewDelegate,UITextViewDelegate>
@property (nonatomic,strong)MAMapView *mapView;
@property (nonatomic,strong)CustomTitleInputView *jdView;//经度
@property (nonatomic,strong)CustomTitleInputView *wdView;//纬度
@property (nonatomic,strong)UITextView *addressView;
@property (nonatomic,strong)NSMutableArray *mapTitleArray;
@property (nonatomic,assign)CLLocationCoordinate2D currentLocation;
@property (nonatomic,copy)NSString *address;
@property (nonatomic,assign)BOOL needlocation;
@property (nonatomic,strong)UIImageView *locaImage;
@property (nonatomic,strong)MAPointAnnotation *pointAnnotation;
@property (nonatomic,assign)BOOL canEdit;
- (void)addPoint:(CLLocationCoordinate2D)location title:(NSString *)title;
- (void)removePoint;
@end

NS_ASSUME_NONNULL_END
