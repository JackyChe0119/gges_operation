//
//  ProductInfoView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ProductInfoView.h"

@implementation ProductInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
               
        _tyxydmView = [[CustomTitleInputView alloc]initWithFrame:RECT(10,0, WIDTH(self)-20, 40)];
        _tyxydmView.tipTitle = @"*统一信用识别代码：";
        _tyxydmView.placeHoldTtitle = @"请输入企业统一信用代码";
        _tyxydmView.titleLabel.textAlignment = Left;
        _tyxydmView.leftMargin = 130;
        [_tyxydmView.inputTextfield addObserverTextFieldEditChangeWithLenth:18 inputType:ImportChatTypeNumberAndBigEnglish];
        [self addSubview:_tyxydmView];
        
        _fzView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_tyxydmView), WIDTH(self)-20, 40)];
        _fzView.tipTitle = @"*法      人：";
        _fzView.placeHoldTtitle = @"请输入法人姓名";
        _fzView.titleLabel.textAlignment = Left;
        [self addSubview:_fzView];
        
            _banciView = [[CustomTitleInputView alloc]initWithFrame:RECT(10,BOTTOM(_fzView), (WIDTH(self)-20)/2.0, 40)];
            _banciView.tipTitle = @"*生产班次：";
            _banciView.placeHoldTtitle = @"请输入";
            _banciView.titleLabel.textAlignment = Left;
            _banciView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
            [self addSubview:_banciView];
               
            _timeView= [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_fzView), (WIDTH(self)-20)/2.0, 40)];
            _timeView.tipTitle = @"*生产时间：";
            _timeView.placeHoldTtitle = @"请输入";
            _timeView.titleLabel.textAlignment = Left;
            _timeView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
            _timeView.showRightTitle = YES;
            _timeView.rightMargin = 30;
            _timeView.titleLabel.textAlignment = Right;
            _timeView.titleLabel.textAlignment = Right;
            _timeView.subTitleLabel.text = @"小时";
            [self addSubview:_timeView];
               
            _dianweiView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_timeView), (WIDTH(self)-20)/2.0, 40)];
            _dianweiView.tipTitle = @"*点位数量：";
            _dianweiView.placeHoldTtitle = @"请输入";
            _dianweiView.titleLabel.textAlignment = Left;
            _dianweiView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
               
            [self addSubview:_dianweiView];
               
            _sjslView= [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_timeView), (WIDTH(self)-20)/2.0, 40)];
            _sjslView.tipTitle = @"*实际数量：";
            _sjslView.placeHoldTtitle = @"请输入";
            _sjslView.titleLabel.textAlignment = Right;
            _sjslView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
                [self addSubview:_sjslView];
        
        
        _nczView = [[CustomTitleInputView alloc]initWithFrame:RECT(10,BOTTOM(_sjslView), (WIDTH(self)-20)/2.0, 40)];
        _nczView.tipTitle = @"年  产  值：";
        _nczView.placeHoldTtitle = @"请输入";
        _nczView.titleLabel.textAlignment = Left;
        _nczView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        _nczView.showRightTitle = YES;
        _nczView.rightMargin = 30;
        _nczView.subTitleLabel.text = @"万元";
        [self addSubview:_nczView];
           
        _nclView= [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_sjslView), (WIDTH(self)-20)/2.0, 40)];
        _nclView.tipTitle = @"年  产  量：";
        _nclView.placeHoldTtitle = @"请输入";
        _nclView.titleLabel.textAlignment = Right;
        _nclView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [self addSubview:_nclView];
           
        _npflView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_nclView), WIDTH(self)-20, 40)];
        _npflView.tipTitle = @"VOCs年排放量：";
        _npflView.placeHoldTtitle = @"请输入";
        _npflView.titleLabel.textAlignment = Left;
        _npflView.leftMargin = 110;
        _npflView.showRightTitle = YES;
        _npflView.rightMargin = 20;
        _npflView.subTitleLabel.text = @"升";
        _npflView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
           
        [self addSubview:_npflView];
           
        _nydlView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_npflView), WIDTH(self)-20, 40)];
        _nydlView.tipTitle = @"年用电量：";
        _nydlView.placeHoldTtitle = @"请输入";
        _nydlView.titleLabel.textAlignment = Left;
        _nydlView.showRightTitle = YES;
        _nydlView.rightMargin = 30;
        _nydlView.subTitleLabel.text = @"万度";
        _nydlView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [self addSubview:_nydlView];
        
        CGRect rect = self.frame;
        rect.size.height = BOTTOM(_nydlView);
        self.frame = rect;
    }
    return self;
}
- (void)hiddenBottomView {
    _nczView.hidden = YES;
    _nclView.hidden = YES;
    _npflView.hidden = YES;
    _nydlView.hidden = YES;
    CGRect rect = self.frame;
    rect.size.height = BOTTOM(_sjslView);
    self.frame = rect;
}
@end
