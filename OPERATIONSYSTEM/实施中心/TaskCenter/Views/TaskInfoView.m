//
//  TaskInfoView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TaskInfoView.h"

@implementation TaskInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        __weak typeof(self)weakSelf = self;
        _principalLabel = [UILabel new];
        [_principalLabel rect:RECT(10, 10, (WIDTH(self)-20)/2.0, 20) aligment:Left font:13 isBold:NO text:@"区域负责人：--" textColor:Color_333333 superView:self];
        
        _implementView = [[AlertListSelectView alloc]initWithFrame:RECT(ScreenWidth/2.0, 7, WIDTH(_principalLabel), 26)];
        _implementView.titleLabel.textColor = Color_333333;
        _implementView.title = @"实施负责人：";
        _implementView.leftWidth = 80;
        _implementView.titleLabel.textAlignment = Left;
        _implementView.operationBlock = ^{
            if (weakSelf.operationBlock) {
                weakSelf.operationBlock(0);
            }
        };
        [self addSubview:_implementView];
        
        _hetongView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_principalLabel)+10, WIDTH(self)-20, 40)];
        _hetongView.tipTitle = @"*合同名称：";
        _hetongView.placeHoldTtitle = @"请输入合同名称";
        _hetongView.inputTextfield.text = @"涉VOCs企业环保用电监管运营合同";
        _hetongView.titleLabel.textAlignment = Left;
        [self addSubview:_hetongView];
        
        _htbhView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_hetongView), WIDTH(self)-20, 40)];
          _htbhView.tipTitle = @"合同编号：";
          _htbhView.placeHoldTtitle = @"请输入合同编号";
          _htbhView.titleLabel.textAlignment = Left;
        _htbhView.inputTextfield.text = @"FYYDHZ2019-190";
          [self addSubview:_htbhView];
        
        _yiFangView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_htbhView), WIDTH(self)-20, 40)];
        _yiFangView.tipTitle = @"*乙方公司：";
        _yiFangView.placeHoldTtitle = @"请输入乙方公司名称";
        _yiFangView.inputTextfield.text = @"浙江飞源环境工程有限公司";
        _yiFangView.titleLabel.textAlignment = Left;
        [self addSubview:_yiFangView];
        
        _serviceTimeView = [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_yiFangView), WIDTH(_principalLabel), 40)];
        _serviceTimeView.titleLabel.textColor = Color_333333;
        _serviceTimeView.title = @"*服务年限：";
        _serviceTimeView.info = @"";
        _serviceTimeView.leftWidth = 80;
        _serviceTimeView.titleLabel.textAlignment = Left;
        _serviceTimeView.operationBlock = ^{
            if (weakSelf.operationBlock) {
                weakSelf.operationBlock(1);
            }
        };
        [self addSubview:_serviceTimeView];
        
        _timeQuanTum = [[TimeQuantumView alloc]initWithFrame:RECT(10, BOTTOM(_serviceTimeView), WIDTH(self)-20, 40)];
        _timeQuanTum.timeBlock = ^{
            if (weakSelf.operationBlock) {
                weakSelf.operationBlock(2);
            }
        };
        [self addSubview:_timeQuanTum];
    }
    return self;
}
- (void)setPrincipal:(NSString *)principal {
    _principal = principal;
    self.principalLabel.text = principal;
}
- (void)setPrincipalId:(NSString *)principalId {
    _principalId = principalId;
}
- (void)setPrincipalName:(NSString *)principalName {
    _principalName = principalName;
}
- (void)hiddenImplement {
    _implementView.hidden = YES;
}
@end
