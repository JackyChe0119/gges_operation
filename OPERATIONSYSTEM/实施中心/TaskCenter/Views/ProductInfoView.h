//
//  ProductInfoView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTitleInputView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProductInfoView : UIView
@property (nonatomic,strong)CustomTitleInputView *tyxydmView;//统一信用代码
@property (nonatomic,strong)CustomTitleInputView *fzView;//法人
@property (nonatomic,strong)CustomTitleInputView *banciView;//生产班次
@property (nonatomic,strong)CustomTitleInputView *timeView;//生成时间
@property (nonatomic,strong)CustomTitleInputView *dianweiView;//点位数量
@property (nonatomic,strong)CustomTitleInputView *sjslView;//实际数量
@property (nonatomic,strong)CustomTitleInputView *nczView;//年产值
@property (nonatomic,strong)CustomTitleInputView *nclView;//年产量
@property (nonatomic,strong)CustomTitleInputView *npflView;//年排放量
@property (nonatomic,strong)CustomTitleInputView *nydlView;//年用电量
- (void)hiddenBottomView;
@end

NS_ASSUME_NONNULL_END
