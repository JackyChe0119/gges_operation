//
//  LocationView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "LocationView.h"

@implementation LocationView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.canEdit = YES;
      UILabel *titleLabel = [UILabel new];
                  [titleLabel rect:RECT(10, 10, 50, 20) aligment:Left font:13 isBold:NO text:@"位置：" textColor:Color_333333 superView:self];
        
        _jdView = [[CustomTitleInputView alloc]initWithFrame:RECT(60, 0,(WIDTH(self)-80)/2.0, 40)];
                   _jdView.tipTitle = @"经度：";
                   _jdView.placeHoldTtitle = @"经度";
                   _jdView.titleLabel.textAlignment = Left;
        _jdView.inputTextfield.font = FONT(12);
        _jdView.leftMargin = 40;
                   [self addSubview:_jdView];
        
        _wdView = [[CustomTitleInputView alloc]initWithFrame:RECT(RIGHT(_jdView)+10, 0, WIDTH(_jdView), 40)];
                      _wdView.tipTitle = @"纬度：";
                      _wdView.placeHoldTtitle = @"纬度";
                      _wdView.titleLabel.textAlignment = Left;
        _wdView.inputTextfield.font = FONT(12);
        _wdView.leftMargin = 40;
                      [self addSubview:_wdView];
        
        _mapView = [[MAMapView alloc] initWithFrame:RECT(10, 50, WIDTH(self)-20, 200)];
        _mapView.showsCompass = NO;
        _mapView.showsScale = NO;
        _mapView.delegate = self;
        _mapView.zoomLevel = 13;
        _mapView.showsUserLocation = YES;
        _mapView.userTrackingMode = MAUserTrackingModeFollow;
        [self addSubview:_mapView];
        
        _locaImage = [UIImageView createImageViewWithFrame:RECT(_mapView.center.x-20, _mapView.center.y-36, 40, 40) imageName:@"icon_location"];
        [self addSubview:_locaImage];
        
        UIButton *nav = [UIButton createButtonWithFrame:RECT(WIDTH(self)-60, 60, 40, 40) Target:self Selector:@selector(navButtonClick) Image:@"iocn_daohang"];
        [nav setBackgroundColor:Color_BG];
        [nav addRoundedCornersWithRadius:2];
        [self addSubview:nav];
        
        UIButton *relocation = [UIButton createButtonWithFrame:RECT(20, 205, 20, 20) Target:self Selector:@selector(relocationClick) Image:@"icon_reLocation"];
        [relocation setBackgroundColor:Color_BG];
        [relocation addRoundedCornersWithRadius:2];
        [self addSubview:relocation];
        
        UILabel *titleLabel2 = [UILabel new];
                         [titleLabel2 rect:RECT(10, 270, 75, 20) aligment:Left font:13 isBold:NO text:@"详细地址：" textColor:Color_333333 superView:self];
        
          _addressView = [[UITextView alloc]initWithFrame:RECT(85,270 , WIDTH(self)-95, 60)];
          _addressView.text = @"请输入详细地址";
          _addressView.textColor = Color_999999;
          _addressView.font = FONT(14);
          _addressView.delegate = self;
          _addressView.backgroundColor = Color_BG;
        [_addressView addBorderWithWidth:.5 borderColor:Color_Line];
          [self addSubview:_addressView];
        
        CGRect rect   = self.frame;
        rect.size.height = BOTTOM(_addressView)+10;
        self.frame = rect;
        
    }
    return self;
}
#pragma mark ---------->>> textViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@"请输入详细地址"]) {
        _addressView.text = @"";
        _addressView.textColor = Color_666666;
    }
    return self.canEdit;
}
- (void)setCanEdit:(BOOL)canEdit {
    _canEdit = canEdit;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    if (_addressView.text.length==0) {
        _addressView.text = @"请输入详细地址";
        _addressView.textColor = Color_999999;
    }
}
- (void)addPoint:(CLLocationCoordinate2D)location title:(NSString *)title {
    _currentLocation = location;
    _address = title;
    _locaImage.hidden = YES;
    _pointAnnotation = [[MAPointAnnotation alloc] init];
    _pointAnnotation.coordinate = location;
    _pointAnnotation.title = title;
    [_mapView addAnnotation:_pointAnnotation];
}
- (void)removePoint {
    [_mapView removeAnnotation:_pointAnnotation];
    _pointAnnotation = nil;
    _locaImage.hidden = NO;
}
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MAPointAnnotation class]])
    {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
        MAPinAnnotationView*annotationView = (MAPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil)
        {
            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.canShowCallout= YES;       //设置气泡可以弹出，默认为NO
        annotationView.animatesDrop = YES;        //设置标注动画显示，默认为NO
        annotationView.draggable = YES;        //设置标注可以拖动，默认为NO
        annotationView.pinColor = MAPinAnnotationColorPurple;
        return annotationView;
    }
    return nil;
}
- (void)navButtonClick {
   [self.mapTitleArray removeAllObjects];
    _mapTitleArray = (NSMutableArray *)[CommonUtil getInstalledMapAppWithEndLocation:CLLocationCoordinate2DMake(self.currentLocation.latitude, self.currentLocation.longitude) title:_address];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    [_mapTitleArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *string = obj[@"title"];
        NSLog(@"-------%@",string);
        [array addObject:string];
    }];
    [array addObject:@"取消"];
    [AlertViewUtil showSlectSheetViewWithVC:[UIApplication sharedApplication].delegate.window.rootViewController Title:@"选择导航方式" array:array callBack:^(NSInteger type) {
        if (type==0) {
            [CommonUtil navAppleMapWithEndLocation:CLLocationCoordinate2DMake(self.currentLocation.latitude, self.currentLocation.longitude) title:_address];
        }else if (type==array.count-1) {
            //取消 不做操作
        }else {
            NSDictionary *dic = self.mapTitleArray[type];
            NSString *urlString = dic[@"url"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
    }];
}
- (void)setNeedlocation:(BOOL)needlocation {
    _needlocation = needlocation;
    if (_needlocation) {
        _currentLocation = _mapView.centerCoordinate;
//        _jdView.inputTextfield.text = [NSString stringWithFormat:@"%.6f",_currentLocation.longitude];
//        _wdView.inputTextfield.text = [NSString stringWithFormat:@"%.6f",_currentLocation.latitude];
//        [self setinfoBy:centerLocation];
    }
}
- (void)relocationClick {
    [_mapView setCenterCoordinate:_mapView.userLocation.location.coordinate animated:YES];
}
#pragma mark ---------->>> mapviewDelegate
- (void)mapView:(MAMapView *)mapView mapDidMoveByUser:(BOOL)wasUserAction {
    if (_needlocation) {
        [self setinfoBy:mapView.centerCoordinate];
    }
}
- (void)setinfoBy:(CLLocationCoordinate2D)location {
    _currentLocation = location;
    _jdView.inputTextfield.text = [NSString stringWithFormat:@"%.6f",location.longitude];
    _wdView.inputTextfield.text = [NSString stringWithFormat:@"%.6f",location.latitude];
    CLGeocoder *clGeoCoder = [[CLGeocoder alloc] init];
    CLLocation *loca = [[CLLocation alloc]initWithLatitude:location.latitude longitude:location.longitude];
    [clGeoCoder reverseGeocodeLocation:loca completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        for (CLPlacemark* mark in placemarks) {
            NSDictionary *addressDic = mark.addressDictionary;
            NSLog(@"%@",addressDic);
            NSArray *array = [addressDic objectForKey:@"FormattedAddressLines"];
            if (array.count>0) {
                _addressView.text = array[0];
                _address = array[0];
            }
        }
    }];
}
@end
