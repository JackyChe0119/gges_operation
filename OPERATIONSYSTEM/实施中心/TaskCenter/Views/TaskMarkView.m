//
//  TaskMarkView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TaskMarkView.h"

@implementation TaskMarkView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.canEdit = YES;
          _titleLabel = [UILabel new];
          [_titleLabel rect:RECT(10, 10, 100, 20) aligment:Left font:15 isBold:NO text:@"现场情况备注:" textColor:Color_333333 superView:self];
          
          _textView = [[UITextView alloc]initWithFrame:RECT(10,40 , WIDTH(self)-20, HEIGHT(self)-45)];
        self.tipStr = @"详细的情况备注(如回款、实施过程中遇到的问题)";
          _textView.textColor = Color_999999;
          _textView.font = FONT(14);
          _textView.delegate = self;
          _textView.backgroundColor = Color_BG;
        [_textView addBorderWithWidth:.5 borderColor:Color_Line];
          [self addSubview:_textView];
    }
    return self;
}
- (void)setTipStr:(NSString *)tipStr {
    _tipStr = tipStr;
    _textView.text = tipStr;

}
- (void)setCanEdit:(BOOL)canEdit {
    _canEdit = canEdit;
}
#pragma mark ---------->>> textViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:_tipStr]) {
        _textView.text = @"";
        _textView.textColor = Color_666666;
    }
    return self.canEdit;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    if (_textView.text.length==0) {
        _textView.text =_tipStr;
        _textView.textColor = Color_999999;
    }
}
@end
