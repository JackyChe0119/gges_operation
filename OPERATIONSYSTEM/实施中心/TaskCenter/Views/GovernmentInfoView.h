//
//  GovernmentInfoView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertListSelectView.h"
#import "CustomTitleInputView.h"
NS_ASSUME_NONNULL_BEGIN

@interface GovernmentInfoView : UIView
@property (nonatomic,strong)UILabel *titleLabel;

@property (nonatomic,strong)AlertListSelectView *dwmcView;//公司名称
@property (nonatomic,strong)CustomTitleInputView *lxrView;//联系人
@property (nonatomic,strong)CustomTitleInputView *sjhmView;//手机号码
@property (nonatomic,strong)CustomTitleInputView *bylxrView;//备用联系人
@property (nonatomic,strong)CustomTitleInputView *bysjhmView;//备用手机号码
@property (nonatomic,strong)AlertListSelectView *xzqhView;//行政区划
- (instancetype)initWithFrame:(CGRect)frame type:(NSInteger)addType;
@end

NS_ASSUME_NONNULL_END
