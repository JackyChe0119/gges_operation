//
//  GovernmentInfoView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "GovernmentInfoView.h"

@implementation GovernmentInfoView

- (instancetype)initWithFrame:(CGRect)frame type:(NSInteger)addType {
    self = [super initWithFrame:frame];
    if (self) {
               _titleLabel = [UILabel new];
               [_titleLabel rect:RECT(10, 10, 100, 20) aligment:Left font:13 isBold:NO text:@"甲方信息" textColor:Color_333333 superView:self];
               CGFloat topSet = BOTTOM(_titleLabel)+17;
               if (addType==2) {
                   _titleLabel.hidden = YES;
                   topSet = 10;
               }
               
               _dwmcView = [[AlertListSelectView alloc]initWithFrame:RECT(10, topSet, WIDTH(self)-20, 26)];
               _dwmcView.titleLabel.textColor = Color_333333;
               _dwmcView.title = @"*单位名称：";
               _dwmcView.leftWidth = 80;
               _dwmcView.info = @"湖州市环境生态局";
               _dwmcView.titleLabel.textAlignment = Left;
               [self addSubview:_dwmcView];
               
               _lxrView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_dwmcView)+7, WIDTH(self)-20, 40)];
                    _lxrView.tipTitle = @"*联  系  人：";
                    _lxrView.placeHoldTtitle = @"请输入联系人姓名";
                    _lxrView.titleLabel.textAlignment = Left;
                    [self addSubview:_lxrView];
               
               _sjhmView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_lxrView), WIDTH(self)-20, 40)];
                    _sjhmView.tipTitle = @"*手机号码：";
                    _sjhmView.placeHoldTtitle = @"请输入联系人号码";
                    _sjhmView.titleLabel.textAlignment = Left;
                    [self addSubview:_sjhmView];
               
               _bylxrView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_sjhmView), WIDTH(self)-20, 40)];
               _bylxrView.tipTitle = @"*备用联系人：";
               _bylxrView.placeHoldTtitle = @"请输入备用联系人";
               _bylxrView.titleLabel.textAlignment = Left;
               _bylxrView.leftMargin = 90;
               [self addSubview:_bylxrView];
               
               _bysjhmView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_bylxrView), WIDTH(self)-20, 40)];
               _bysjhmView.tipTitle = @"*备用手机号码：";
               _bysjhmView.placeHoldTtitle = @"请输入联系人号码";
               _bysjhmView.titleLabel.textAlignment = Left;
               _bysjhmView.leftMargin = 100;
               [self addSubview:_bysjhmView];
               
               _xzqhView = [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_bysjhmView)+7, WIDTH(self)-20, 26)];
               _xzqhView.titleLabel.textColor = Color_333333;
               _xzqhView.title = @"*行政区划：";
               _xzqhView.info = @"湖州市";
               _xzqhView.leftWidth = 80;
               _xzqhView.titleLabel.textAlignment = Left;
               [self addSubview:_xzqhView];
        
        CGRect rect = self.frame;
          rect.size.height = BOTTOM(_xzqhView)+10;
          self.frame = rect;
               
    }
    return self;
}

@end
