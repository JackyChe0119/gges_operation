//
//  TaskCenterListCell.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaskCenterListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *legenLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerWidth;

@end

NS_ASSUME_NONNULL_END
