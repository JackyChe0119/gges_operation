//
//  TaskMarkView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaskMarkView : UIView<UITextViewDelegate>
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UITextView *textView;
@property (nonatomic,copy)NSString *tipStr;
@property (nonatomic,assign)BOOL canEdit;
@end

NS_ASSUME_NONNULL_END
