//
//  TaskCompanyInfoView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TaskCompanyInfoView.h"

@implementation TaskCompanyInfoView

- (instancetype)initWithFrame:(CGRect)frame type:(NSInteger)addType {
    self = [super initWithFrame:frame];
    if (self) {
        __weak typeof(self)weakSelf = self;
        _titleLabel = [UILabel new];
        [_titleLabel rect:RECT(10, 10, 100, 20) aligment:Left font:13 isBold:NO text:@"甲方信息：" textColor:Color_333333 superView:self];
        CGFloat topSet = BOTTOM(_titleLabel)+17;
        if (addType>=2) {
            _titleLabel.hidden = YES;
            topSet = 10;
        }
        if (addType==3) {
            _qymcIView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, topSet, WIDTH(self)-20, 40)];
                 _qymcIView.tipTitle = @"*企业名称：";
                 _qymcIView.placeHoldTtitle = @"请输入企业名称";
                 _qymcIView.titleLabel.textAlignment = Left;
                [_qymcIView.inputTextfield addObserverTextFieldEditChangeWithLenth:50 inputType:ImportChatTypeAll];
                 _qymcIView.inputTextfield.delegate = self;
                 [self addSubview:_qymcIView];
            topSet = BOTTOM(_qymcIView);
        }else {
            _qymcView = [[AlertListSelectView alloc]initWithFrame:RECT(10, topSet, WIDTH(self)-10, 26)];
            _qymcView.titleLabel.textColor = Color_333333;
            _qymcView.title = @"*企业名称：";
            _qymcView.info = @"请选择";
            _qymcView.leftWidth = 80;
            _qymcView.titleLabel.textAlignment = Left;
            _qymcView.operationBlock = ^{
                if (weakSelf.operationBlock) {
                    weakSelf.operationBlock(0);//选择企业
                }
            };
            [self addSubview:_qymcView];
            topSet = BOTTOM(_qymcView)+7;
        }
        
        _tyxydmView = [[CustomTitleInputView alloc]initWithFrame:RECT(10,topSet, WIDTH(self)-20, 40)];
        _tyxydmView.tipTitle = @"统一信用识别代码：";
        _tyxydmView.placeHoldTtitle = @"请输入企业统一信用代码";
        _tyxydmView.titleLabel.textAlignment = Left;
        _tyxydmView.leftMargin = 130;
        [_tyxydmView.inputTextfield addObserverTextFieldEditChangeWithLenth:18 inputType:ImportChatTypeNumberAndBigEnglish];
        [self addSubview:_tyxydmView];
        
        _fzView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_tyxydmView), WIDTH(self)-20, 40)];
        _fzView.tipTitle = @"法    人：";
        _fzView.placeHoldTtitle = @"请输入法人姓名";
        _fzView.titleLabel.textAlignment = Left;
        [_fzView.inputTextfield addObserverTextFieldEditChangeWithLenth:20 inputType:ImportChatTypeAll];
        [self addSubview:_fzView];
        
        _lxrView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_fzView), WIDTH(self)-20, 40)];
        _lxrView.tipTitle = @"*联  系  人：";
        _lxrView.placeHoldTtitle = @"请输入联系人姓名";
        [_lxrView.inputTextfield addObserverTextFieldEditChangeWithLenth:20 inputType:ImportChatTypeAll];
        _lxrView.titleLabel.textAlignment = Left;
        [self addSubview:_lxrView];
        
        _sjhmView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_lxrView), WIDTH(self)-20, 40)];
        _sjhmView.tipTitle = @"*手机号码：";
        _sjhmView.placeHoldTtitle = @"请输入联系人号码";
        _sjhmView.titleLabel.textAlignment = Left;
        [_sjhmView.inputTextfield addObserverTextFieldEditChangeWithLenth:11 inputType:ImportChatTypeNumber];
        _sjhmView.inputTextfield.keyboardType = UIKeyboardTypeNumberPad;
        [self addSubview:_sjhmView];
        
        _bylxrView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_sjhmView), WIDTH(self)-20, 40)];
        _bylxrView.tipTitle = @"备用联系人：";
        _bylxrView.placeHoldTtitle = @"请输入备用联系人";
        _bylxrView.titleLabel.textAlignment = Left;
        [_bylxrView.inputTextfield addObserverTextFieldEditChangeWithLenth:20 inputType:ImportChatTypeAll];
        _bylxrView.leftMargin = 90;
        [self addSubview:_bylxrView];
        
        _bysjhmView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_bylxrView), WIDTH(self)-20, 40)];
        _bysjhmView.tipTitle = @"备用手机号码：";
        _bysjhmView.placeHoldTtitle = @"请输入联系人号码";
        _bysjhmView.titleLabel.textAlignment = Left;
        [_bysjhmView.inputTextfield addObserverTextFieldEditChangeWithLenth:11 inputType:ImportChatTypeNumber];
        _bysjhmView.inputTextfield.keyboardType = UIKeyboardTypeNumberPad;
        _bysjhmView.leftMargin = 100;
        [self addSubview:_bysjhmView];
        
        _xzqhView = [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_bysjhmView)+7, WIDTH(self)-10, 26)];
        _xzqhView.titleLabel.textColor = Color_333333;
        _xzqhView.title = @"*行政区划：";
        _xzqhView.info = @"";
        _xzqhView.leftWidth = 80;
        _xzqhView.titleLabel.textAlignment = Left;
        [self addSubview:_xzqhView];
        
        _hylbView = [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_xzqhView)+14, WIDTH(self)-10, 26)];
        _hylbView.titleLabel.textColor = Color_333333;
        _hylbView.title = @"*行业类别：";
        _hylbView.info = @"";
        _hylbView.leftWidth = 80;
        _hylbView.titleLabel.textAlignment = Left;
        [self addSubview:_hylbView];
        
        CGFloat offset = BOTTOM(_hylbView)+7;
        if (addType>=2) {
            _zczjView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, offset, WIDTH(self)-20, 40)];
            _zczjView.tipTitle = @"注册资金(万元)：";
            _zczjView.placeHoldTtitle = @"请输入注册资金";
            _zczjView.titleLabel.textAlignment = Left;
            _zczjView.leftMargin = 105;
            [self addSubview:_zczjView];
            offset = BOTTOM(_zczjView);
        }
        
        _pwxkzView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, offset, WIDTH(self)-20, 40)];
        _pwxkzView.tipTitle = @"排污许可证编号：";
        _pwxkzView.placeHoldTtitle = @"请输入排污许可证编号";
        _pwxkzView.titleLabel.textAlignment = Left;
        _pwxkzView.leftMargin = 120;
        [self addSubview:_pwxkzView];
        
        _pwjyhView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_pwxkzView), WIDTH(self)-20, 40)];
        _pwjyhView.tipTitle = @"排污权交易文件号：";
        _pwjyhView.placeHoldTtitle = @"请输入排污权交易文件号";
        _pwjyhView.titleLabel.textAlignment = Left;
        _pwjyhView.leftMargin = 130;
        [self addSubview:_pwjyhView];
        
        _hppfView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_pwjyhView), WIDTH(self)-20, 40)];
        _hppfView.tipTitle = @"环评批复文件号：";
        _hppfView.placeHoldTtitle = @"请输入环评批复文件号";
        _hppfView.titleLabel.textAlignment = Left;
        _hppfView.leftMargin = 120;
        [self addSubview:_hppfView];
        
        CGRect rect = self.frame;
        rect.size.height = BOTTOM(_hppfView)+10;
        self.frame = rect;
    }
    return self;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (_operationBlock) {
        _operationBlock(3);//输入企业名称
    }
}

@end
