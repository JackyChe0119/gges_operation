//
//  TimeSelectView.h
//  GEESYSTEM
//
//  Created by 车杰 on 2019/9/4.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TimeSelectView : UIView
@property (nonatomic,strong)UILabel *infoLabel;
@property (nonatomic,copy)NSString *info;
@property (nonatomic,copy)NSString *lineId;
@property (nonatomic,strong) void (^block)(void);
@end

NS_ASSUME_NONNULL_END
