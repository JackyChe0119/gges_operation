//
//  TimeQuantumView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TimeQuantumView.h"

@implementation TimeQuantumView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        __weak typeof(self)weakSelf = self;
        _titleLabel = [UILabel new];
        [_titleLabel rect:RECT(0, HEIGHT(self)/2.0-10, 75, 20) aligment:Left font:13 isBold:NO text:@"*服务时间：" textColor:Color_333333 superView:self];
        self.serviceYear = 3;
        
        _fromTimeView = [[TimeSelectView alloc]initWithFrame:RECT(RIGHT(_titleLabel)+5, HEIGHT(self)/2.0-13,(WIDTH(self)-RIGHT(_titleLabel)-15)/2.0, 26)];
        _fromTimeView.block = ^{
            if (weakSelf.timeBlock) {
                weakSelf.timeBlock();
            }
        };
        _fromTimeView.info = [CommonUtil getStringForDate:[NSDate date] format:@"yyyy-MM-dd"];
        _fromTimeView.lineId = [NSString stringWithFormat:@"%@ 00:00:00",[CommonUtil getStringForDate:[NSDate date] format:@"yyyy-MM-dd"]];
           [self addSubview:_fromTimeView];
        
        _centerLabel = [UILabel new];
        [_centerLabel rect:RECT(RIGHT(_fromTimeView)+2, HEIGHT(self)/2.0-10, 8, 20) aligment:Left font:10 isBold:NO text:@"~" textColor:Color_333333 superView:self];
        
        
        _toTimeView = [[TimeSelectView alloc]initWithFrame:RECT(RIGHT(_fromTimeView)+10, HEIGHT(self)/2.0-13,(WIDTH(self)-RIGHT(_titleLabel)-15)/2.0, 26)];
        _toTimeView.block = ^{
            [CommonToastHUD showTips:@"只支持修改起始时间"];
        };
        [self setToTime:_fromTimeView.info];
        [self addSubview:_toTimeView];
        
    }
    return self;
}
- (void)setServiceYear:(NSInteger)serviceYear {
    _serviceYear = serviceYear;
    [self setToTime:_fromTimeView.info];
}
- (void)setToTime:(NSString *)fromTime {
    NSString *year = [fromTime substringWithRange:RANGE(0, 4)];
    NSInteger toYear = [year integerValue]+_serviceYear;
    NSString *month = [fromTime substringWithRange:RANGE(5, 2)];
    NSString *day = [fromTime substringWithRange:RANGE(8, 2)];
    _fromTimeView.lineId = [NSString stringWithFormat:@"%@ 00:00:00",fromTime];
    if (toYear%4!=0) {
        if ([month isEqualToString:@"02"]&&[day isEqualToString:@"29"]) {
            _toTimeView.info = [fromTime stringByReplacingCharactersInRange:RANGE(0, 4) withString:[NSString stringWithFormat:@"%ld",toYear]];
            day = @"28";
            _toTimeView.info = [_toTimeView.info stringByReplacingCharactersInRange:RANGE(8, 2) withString:@"28"];
        }else {
            if ([day isEqualToString:@"01"]) {
                if ([month isEqualToString:@"02"]||[month isEqualToString:@"04"]||[month isEqualToString:@"06"]||[month isEqualToString:@"09"]||[month isEqualToString:@"11"]) {//
                    day = @"31";
                    month = [NSString stringWithFormat:@"%02ld",[month integerValue]-1];
                }else if ([month isEqualToString:@"03"]) {
                    day = @"28";
                    month = [NSString stringWithFormat:@"%02ld",[month integerValue]-1];
                }else if ([month isEqualToString:@"05"]||[month isEqualToString:@"07"]||[month isEqualToString:@"08"]||[month isEqualToString:@"10"]||[month isEqualToString:@"12"]) {
                    day = @"30";
                    month = [NSString stringWithFormat:@"%02ld",[month integerValue]-1];
                }else {
                    toYear = toYear-1;
                    month = @"12";
                    day = @"31";
                }
            }else {
                NSInteger  lastDay= [day integerValue];
                day =  [NSString stringWithFormat:@"%02ld",lastDay-1];
            }
            NSString *final = [NSString stringWithFormat:@"%ld-%@-%@",toYear,month,day];
            _toTimeView.info = final;
    
        }
    }else {

            if ([day isEqualToString:@"01"]) {
                     if ([month isEqualToString:@"02"]||[month isEqualToString:@"04"]||[month isEqualToString:@"06"]||[month isEqualToString:@"09"]||[month isEqualToString:@"11"]) {//
                             day = @"31";
                             month = [NSString stringWithFormat:@"%02ld",[month integerValue]-1];
                     }else if ([month isEqualToString:@"03"]) {
                             day = @"29";
                            month = [NSString stringWithFormat:@"%02ld",[month integerValue]-1];
                     }else if ([month isEqualToString:@"05"]||[month isEqualToString:@"07"]||[month isEqualToString:@"08"]||[month isEqualToString:@"10"]||[month isEqualToString:@"12"]) {
                             day = @"30";
                            month = [NSString stringWithFormat:@"%02ld",[month integerValue]-1];
                     }else {
                             toYear = toYear-1;
                             month = @"12";
                             day = @"31";
                     }
                 }else {
                         NSInteger  lastDay= [day integerValue];
                         day =  [NSString stringWithFormat:@"%02ld",lastDay-1];
                 }
    
     
        NSString *final = [NSString stringWithFormat:@"%ld-%@-%@",toYear,month,day];
                _toTimeView.info = final;
    }
    _toTimeView.lineId = [NSString stringWithFormat:@"%ld-%@-%@ 23:59:59",toYear,month,day];
}
@end
