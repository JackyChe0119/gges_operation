//
//  TimeSelectView.m
//  GEESYSTEM
//
//  Created by 车杰 on 2019/9/4.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TimeSelectView.h"

@implementation TimeSelectView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIView *baseView = [UIView createViewWithFrame:RECT(0, 0, WIDTH(self), HEIGHT(self)) color:Color_BG];
        [baseView addRoundedCornersWithRadius:4];
        baseView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [baseView addGestureRecognizer:tap];
        [self addSubview:baseView];
        
        _infoLabel = [UILabel new];
        [_infoLabel rect:RECT(5, 0,WIDTH(baseView)-15, HEIGHT(baseView)) aligment:Left font:13 isBold:NO text:@"全部" textColor:Color_333333 superView:baseView];
        
        UIImageView *imageView = [UIImageView createImageViewWithFrame:RECT(WIDTH(baseView)-12, HEIGHT(baseView)-12, 24, 20) imageName:@"iocn_sanjiao"];
        [baseView addSubview:imageView];
    }
    return self;
}
- (void)setInfo:(NSString *)info {
    _info = info;
    self.infoLabel.text = info;
}
- (void)tap:(UITapGestureRecognizer *)tap {
    tap.view.userInteractionEnabled = NO;
    if (_block) {
        _block();
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        tap.view.userInteractionEnabled = YES;
    });
}
@end
