//
//  TaskCompanyInfoView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertListSelectView.h"
#import "CustomTitleInputView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TaskCompanyInfoView : UIView<UITextFieldDelegate>
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)AlertListSelectView *qymcView;//公司名称
@property (nonatomic,strong)CustomTitleInputView *qymcIView;//企业名称
@property (nonatomic,strong)CustomTitleInputView *tyxydmView;//企业代码
@property (nonatomic,strong)CustomTitleInputView *fzView;//法人
@property (nonatomic,strong)CustomTitleInputView *lxrView;//联系人
@property (nonatomic,strong)CustomTitleInputView *sjhmView;//手机号码
@property (nonatomic,strong)CustomTitleInputView *bylxrView;//备用联系人
@property (nonatomic,strong)CustomTitleInputView *bysjhmView;//备用手机号码
@property (nonatomic,strong)AlertListSelectView *xzqhView;//行政区划
@property (nonatomic,strong)AlertListSelectView *hylbView;//行业类别
@property (nonatomic,strong)CustomTitleInputView *zczjView;//注册资金
@property (nonatomic,strong)CustomTitleInputView *pwxkzView;//排污许可证
@property (nonatomic,strong)CustomTitleInputView *pwjyhView;//排污交易号
@property (nonatomic,strong)CustomTitleInputView *hppfView;//环评批复
@property (nonatomic,strong) void (^operationBlock)(NSInteger type);
- (instancetype)initWithFrame:(CGRect)frame type:(NSInteger)addType;
@end

NS_ASSUME_NONNULL_END
