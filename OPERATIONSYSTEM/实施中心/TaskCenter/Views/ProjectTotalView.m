//
//  ProjectTotalView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ProjectTotalView.h"

@implementation ProjectTotalView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _swfzrLabel = [YYLabel new];
        _swfzrLabel.frame = RECT(10, 0, (WIDTH(self)-20)/2.0, 30);
        _swfzrLabel.textColor = Color_333333;
        _swfzrLabel.font = FONT(13);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(call:)];
        _swfzrLabel.userInteractionEnabled = YES;
        [_swfzrLabel addGestureRecognizer:tap];
        _swfzrLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _swfzrLabel.tag = 100;
        [self addSubview:_swfzrLabel];
        
        _ssfzrLabel = [YYLabel new];
         _ssfzrLabel.frame = RECT(10, BOTTOM(_swfzrLabel), (WIDTH(self)-20)/2.0, 30);
        _ssfzrLabel.textColor = Color_333333;
          _ssfzrLabel.font = FONT(13);
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(call:)];
        _ssfzrLabel.userInteractionEnabled = YES;
        [_ssfzrLabel addGestureRecognizer:tap2];
        _ssfzrLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _ssfzrLabel.tag = 200;
         [self addSubview:_ssfzrLabel];
        
        _ywfzrLabel = [YYLabel new];
        _ywfzrLabel.frame = RECT(10, BOTTOM(_ssfzrLabel), (WIDTH(self)-20)/2.0, 30);
        _ywfzrLabel.textColor = Color_333333;
        _ywfzrLabel.font = FONT(13);
         UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(call:)];
        _ywfzrLabel.userInteractionEnabled = YES;
        [_ywfzrLabel addGestureRecognizer:tap3];
        _ywfzrLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _ywfzrLabel.tag = 300;
        [self addSubview:_ywfzrLabel];
        
        _hkztLabel = [YYLabel new];
            _hkztLabel.frame = RECT(WIDTH(self)/2.0, 0, (WIDTH(self)-20)/2.0, 30);
        _hkztLabel.textColor = Color_333333;
        _hkztLabel.font = FONT(13);
            [self addSubview:_hkztLabel];
            
        _ssztLabel = [YYLabel new];
        _ssztLabel.frame = RECT(WIDTH(self)/2.0, BOTTOM(_hkztLabel), (WIDTH(self)-20)/2.0, 30);
        _ssztLabel.textColor = Color_333333;
        _ssztLabel.font = FONT(13);
             [self addSubview:_ssztLabel];
            
        _ywztLabel = [YYLabel new];
        _ywztLabel.frame = RECT(WIDTH(self)/2.0, BOTTOM(_ssztLabel), (WIDTH(self)-20)/2.0, 30);
        _ywztLabel.textColor = Color_333333;
        _ywztLabel.font = FONT(13);
        [self addSubview:_ywztLabel];
    }
    return self;
}
- (void)setSwfzr:(NSString *)swfzr {
    _swfzr = swfzr;
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:swfzr];
    if (![_swfzr containsString:@"--"]) {
           NSMutableAttributedString *attachment = nil;
               //是boss才可以修改
           UIImage *iamge = IMAGE(@"icon_call");
           attachment = [NSMutableAttributedString yy_attachmentStringWithContent:iamge contentMode:UIViewContentModeCenter attachmentSize:iamge.size alignToFont:FONT(13) alignment:YYTextVerticalAlignmentCenter];
           [att appendAttributedString:attachment];
       }
    _swfzrLabel.attributedText = att;
}

- (void)setSsfzr:(NSString *)ssfzr {
    _ssfzr = ssfzr;
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:ssfzr];
   if (![_ssfzr containsString:@"--"]) {
          NSMutableAttributedString *attachment = nil;
              //是boss才可以修改
          UIImage *iamge = IMAGE(@"icon_call");
          attachment = [NSMutableAttributedString yy_attachmentStringWithContent:iamge contentMode:UIViewContentModeCenter attachmentSize:iamge.size alignToFont:FONT(13) alignment:YYTextVerticalAlignmentCenter];
          [att appendAttributedString:attachment];
      }
    _ssfzrLabel.attributedText = att;
}
- (void)setYwfzr:(NSString *)ywfzr {
    _ywfzr = ywfzr;
    NSMutableAttributedString *att = [[NSMutableAttributedString alloc]initWithString:ywfzr];
    if (![_ywfzr containsString:@"--"]) {
        NSMutableAttributedString *attachment = nil;
            //是boss才可以修改
        UIImage *iamge = IMAGE(@"icon_call");
        attachment = [NSMutableAttributedString yy_attachmentStringWithContent:iamge contentMode:UIViewContentModeCenter attachmentSize:iamge.size alignToFont:FONT(13) alignment:YYTextVerticalAlignmentCenter];
        [att appendAttributedString:attachment];
    }
    _ywfzrLabel.attributedText = att;
}
- (void)setHkzt:(NSString *)hkzt {
    _hkzt = hkzt;
    _hkztLabel.text = hkzt;
}
- (void)setSszt:(NSString *)sszt {
    _sszt = sszt;
    _ssztLabel.text = sszt;
}
- (void)setYwzt:(NSString *)ywzt{
    _ywzt = ywzt;
    _ywztLabel.text = ywzt;
}
- (void)hiddenRightInfo {
    _hkztLabel.hidden = YES;
    _ssztLabel.hidden = YES;
    _ywztLabel.hidden = YES;
}
- (void)call:(UITapGestureRecognizer *)tap {
    if (tap.view.tag == 100&&![_swfzr containsString:@"--"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_swPhone]]];
    }else if (tap.view.tag == 200&&![_ssfzr containsString:@"--"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_ssPhone]]];
    }else if(tap.view.tag == 300&&![_ywfzr containsString:@"--"]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_ywPhone]]];
    }
}
@end
