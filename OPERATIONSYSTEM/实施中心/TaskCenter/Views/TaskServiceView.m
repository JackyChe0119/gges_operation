//
//  TaskServiceView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TaskServiceView.h"

@implementation TaskServiceView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = Color_BG;
        
        _fuwuView = [[AlertListSelectView alloc]initWithFrame:RECT(10, 7, WIDTH(self)-10, 26)];
        _fuwuView.titleLabel.textColor = Color_333333;
        _fuwuView.title = @"服务选择：";
        _fuwuView.leftWidth = 75;
        _fuwuView.info = @"用电监管";
        _fuwuView.baseView.backgroundColor = Color_White;
        _fuwuView.titleLabel.textAlignment = Left;
        [self addSubview:_fuwuView];
        
        _banciView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_fuwuView)+10, (WIDTH(self)-20)/2.0, 40)];
        _banciView.tipTitle = @"*生产班次：";
        _banciView.placeHoldTtitle = @"请输入";
        _banciView.titleLabel.textAlignment = Left;
        [_banciView.inputTextfield addObserverTextFieldEditChangeWithLenth:3 inputType:ImportChatTypeNumber];
        _banciView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        _banciView.inputTextfield.keyboardType = UIKeyboardTypeNumberPad;
        [self addSubview:_banciView];
        
         _timeView= [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_fuwuView)+10, (WIDTH(self)-20)/2.0, 40)];
        _timeView.tipTitle = @"*生产时间：";
        _timeView.placeHoldTtitle = @"请输入";
        _timeView.titleLabel.textAlignment = Left;
        _timeView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        _timeView.showRightTitle = YES;
        _timeView.rightMargin = 25;
        _timeView.titleLabel.textAlignment = Right;
        _timeView.subTitleLabel.text = @"小时";
        [_timeView.inputTextfield addObserverTextFieldEditChangeWithLenth:5 inputType:ImportChatTypeNumberAndPunctuation];
         _timeView.inputTextfield.keyboardType = UIKeyboardTypeDecimalPad;
        [self addSubview:_timeView];
        
        _dianweiView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_timeView), (WIDTH(self)-20)/2.0, 40)];
        _dianweiView.tipTitle = @"*点位数量：";
        _dianweiView.placeHoldTtitle = @"请输入";
        _dianweiView.titleLabel.textAlignment = Left;
        _dianweiView.inputTextfield.keyboardType = UIKeyboardTypeNumberPad;
        [_dianweiView.inputTextfield addObserverTextFieldEditChangeWithLenth:5 inputType:ImportChatTypeNumber];
        _dianweiView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        
        [self addSubview:_dianweiView];
        
       _htjeView= [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_timeView), (WIDTH(self)-20)/2.0, 40)];
        _htjeView.tipTitle = @"*合同金额：";
        _htjeView.placeHoldTtitle = @"请输入";
        _htjeView.titleLabel.textAlignment = Right;
        _htjeView.showRightTitle = YES;
        _htjeView.rightMargin = 25;
        [_htjeView.inputTextfield addObserverTextFieldEditChangeWithLenth:10 inputType:ImportChatTypeNumberAndPunctuation];
        _htjeView.subTitleLabel.text = @"万元";
        _htjeView.inputTextfield.keyboardType = UIKeyboardTypeDecimalPad;
        _htjeView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [self addSubview:_htjeView];
        
        _uploadView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_dianweiView)+10, WIDTH(self), 140)  max:9];
        [self addSubview:_uploadView];
        
        _markView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_uploadView)+10, WIDTH(self), 190)];
        [self addSubview:_markView];
        
        CGRect rect = self.frame;
        rect.size.height = BOTTOM(_markView)+10;
        self.frame = rect;
        
        
    }
    return self;
}
- (void)addMarView {
    _markView2 = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_markView)+10, WIDTH(self), 190)];
    _markView2.titleLabel.text = @"回款备注：";
    _markView2.tipStr = @"请输入回款状态备注";
    [self addSubview:_markView2];
    CGRect rect = self.frame;
    rect.size.height = BOTTOM(_markView2)+10;
    self.frame = rect;
}
- (void)hiddenHTJE {
    _htjeView.hidden = YES;
}
@end
