//
//  TaskServiceView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertListSelectView.h"
#import "CustomTitleInputView.h"
#import "ImageUploadView.h"
#import "TaskMarkView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TaskServiceView : UIView
@property (nonatomic,strong)AlertListSelectView *fuwuView;//服务类型
@property (nonatomic,strong)CustomTitleInputView *banciView;//生产班次
@property (nonatomic,strong)CustomTitleInputView *timeView;//生成时间
@property (nonatomic,strong)CustomTitleInputView *dianweiView;//点位数量
@property (nonatomic,strong)CustomTitleInputView *htjeView;//合同金额
@property (nonatomic,strong)ImageUploadView *uploadView;
@property (nonatomic,strong)TaskMarkView *markView;
@property (nonatomic,strong)TaskMarkView *markView2;
- (void)hiddenHTJE;
- (void)addMarView;
@end

NS_ASSUME_NONNULL_END
