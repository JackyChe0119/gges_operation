//
//  ProjectTotalView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/24.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYText.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProjectTotalView : UIView
@property (nonatomic,strong)YYLabel *swfzrLabel;
@property (nonatomic,strong)YYLabel *ssfzrLabel;
@property (nonatomic,strong)YYLabel *ywfzrLabel;
@property (nonatomic,strong)YYLabel *hkztLabel;
@property (nonatomic,strong)YYLabel *ssztLabel;
@property (nonatomic,strong)YYLabel *ywztLabel;
@property (nonatomic,copy)NSString *swfzr,*ssfzr,*ywfzr,*hkzt,*sszt,*ywzt;
@property (nonatomic,copy)NSString *swPhone,*ssPhone,*ywPhone;
- (void)hiddenRightInfo;
@end

NS_ASSUME_NONNULL_END
