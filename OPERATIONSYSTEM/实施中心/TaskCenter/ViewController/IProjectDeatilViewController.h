//
//  IProjectDeatilViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/10.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface IProjectDeatilViewController : FYBaseViewController
@property (nonatomic,copy)NSString *taskId;
@property (nonatomic,strong) void (^addBlock)(void);
@property (nonatomic,strong) void (^senderBlock)(NSString *name);

@end

NS_ASSUME_NONNULL_END
