//
//  TaskSenderViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TaskSenderViewController.h"
#import "TaskInfoView.h"
#import "TaskServiceView.h"
#import "TaskCompanyInfoView.h"
#import "LocationView.h"
#import "ProjectTotalView.h"
#import "LegenSelectViewController.h"
@interface TaskSenderViewController ()
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)TaskInfoView *taskView;
@property (nonatomic,strong)TaskServiceView *serviceView;
@property (nonatomic,strong)TaskCompanyInfoView *companyInfoView;
@property (nonatomic,strong)LocationView *locationView;
@property (nonatomic,strong)ProjectTotalView *totalView;
@property (nonatomic,copy)NSString *divisionLeader,*divisionId;
@property (nonatomic,strong)CustomGraButton *commitButton;
@end

@implementation TaskSenderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"任务分配" Color:Color_333333];
    [self setLeftButtonImage:@"icon_backUp_black"];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [self.view addSubview:_baseScrollView];
    
    _taskView = [[TaskInfoView alloc]initWithFrame:RECT(0, 0, ScreenWidth, 240)];
    _taskView.implementView.operationBlock = ^{
        if (!weakSelf.divisionId) {
            [weakSelf showHUDMessage:@"获取区域信息出错，请返回重试"];
            return;
        }
        LegenSelectViewController *vc = [[LegenSelectViewController alloc]init];
        vc.url = @"basics/api/v1/operation/user/underlingUser";
        vc.selectWithIdBlock = ^(NSString * _Nonnull name, NSString * _Nonnull Id) {
            weakSelf.taskView.implementView.info = name;
            weakSelf.taskView.implementView.lineId = Id;
        };
        vc.navTitle = @"实施负责人";
        vc.userId = weakSelf.divisionLeader;
        vc.divisionId = weakSelf.divisionId;
        [weakSelf presentWithViewController:vc animated:YES];
    };
    [_baseScrollView addSubview:_taskView];
    
    _totalView = [[ProjectTotalView alloc]initWithFrame:RECT(0, BOTTOM(_taskView)+10, ScreenWidth, 90)];
    _totalView.swfzr = @"商务负责人：-- ";
    _totalView.ssfzr = @"实施负责人：-- ";
    _totalView.ywfzr = @"运维负责人：-- ";
    _totalView.hkzt = @"回款状态：-- ";
    _totalView.sszt = @"实施状态：未分配";
    _totalView.ywzt = @"运维状态：--";
    [_baseScrollView addSubview:_totalView];
    
    _serviceView = [[TaskServiceView alloc]initWithFrame:RECT(10, BOTTOM(_totalView)+10, ScreenWidth-20, 300)];
    [_serviceView hiddenHTJE];//隐藏合同金额
    [_baseScrollView addSubview:_serviceView];
    
    _companyInfoView = [[TaskCompanyInfoView alloc]initWithFrame:RECT(0, BOTTOM(_serviceView)+20, ScreenWidth, 500) type:1];
    [_baseScrollView addSubview:_companyInfoView];
    
    _locationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(_companyInfoView), ScreenWidth, 340)];
    [_baseScrollView addSubview:_locationView];
    
    _commitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0-80, BOTTOM(_locationView)+30,160, 40) target:self action:@selector(commitButtonClick) title:@"提  交" color:Color_White font:17 type:2];
    [_commitButton addRoundedCornersWithRadius:5];
    
    [_baseScrollView addSubview:_commitButton];
    _commitButton.hidden = YES;
    [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(_commitButton)+20)];
    [self setEditStatus];
    [self requestDatail];
    
}
- (void)commitButtonClick {
    if (!_divisionLeader) {
        [self showHUDMessage:@"区域负责人出问题，请稍后重试"];
        return;
    }
    if ([_taskView.implementView.info isEqualToString:@"未选择"]) {
        [self showHUDMessage:@"请选择实施负责人"];
        return;
    }
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:_divisionLeader forKey:@"divisionLeader"];
    [params setValue:_taskView.implementView.lineId forKey:@"applyLeader"];
    [params setValue:self.divisionId forKey:@"divisionId"];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/user/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"分配成功"];
                if (_senderBlock) {
                    _senderBlock(_taskView.implementView.info);
                }
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestDatail {
    if (self.isNoDivision) {
        [self showProgressHud];
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
        [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
                dispatch_async(dispatch_get_main_queue(), ^{
                        if ([responseMessage isRequestSuccessful]) {
                                   NSDictionary *result = responseMessage.data[@"data"];
                                   [self relayoutWithDeatil:result];
                                   NSMutableDictionary *params2 = [[NSMutableDictionary alloc]initWithCapacity:0];
                                   [params2 setValue:[UserManager shareManager].deptCode forKey:@"deptCode"];
                                   [params2 setValue:result[@"division"] forKey:@"divisionId"];
                                   _divisionId = result[@"division"];
                                   [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/operation/user/chargeUser" method:GET params:params2 callBack:^(RequestResponseMessage * _Nonnull responseMessage1) {
                                       [self hiddenProgressHud];
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           if ([responseMessage1 isRequestSuccessful]) {
                                               NSDictionary *dic = responseMessage1.data[@"data"];
                                               if (!dic||[dic isKindOfClass:[NSNull class]]) {
                                                   [AlertViewUtil showCancelAlertViewWithVC:self Title:@"温馨提示！" Message:@"该区域暂无负责人，可能无法指派任务，如有需要请联系管理员" LeftTitle:@"好的" callbackBlock:^{
                                                       _taskView.implementView.userInteractionEnabled = NO;
                                                       [_commitButton setUnSelectState];
                                                   }];
                                               }else {
                                                   _divisionLeader = dic[@"id"];
                                                   _taskView.principal = [NSString stringWithFormat:@"区域负责人：%@",[CommonUtil fixNullTextWithLine:dic[@"name"]]];
                                               }
                                           }else {
                                               [self showHUDMessage:responseMessage1.errorMessage];
                                               [self.navigationController popViewControllerAnimated:YES];
                                           }
                                       });
                                    }];
                        }else {
                            [self hiddenProgressHud];
                            [self showHUDMessage:responseMessage.errorMessage];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                });
        }];
    }else {
        [self showProgressHud];
           NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
           [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
               dispatch_async(dispatch_get_main_queue(), ^{
                   [self hiddenProgressHud];
                   if ([responseMessage isRequestSuccessful]) {
                       NSDictionary *result = responseMessage.data[@"data"];
                       [self relayoutWithDeatil:result];
                   }else {
                       [self showHUDMessage:responseMessage.errorMessage];
                       [self.navigationController popViewControllerAnimated:YES];
                   }
               });
           }];
    }
}
- (void)relayoutWithDeatil:(NSDictionary *)result {
    _companyInfoView.qymcView.info= result[@"companyName"];
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.tyxydmView.inputTextfield.text = result[@"creditCode"];
    _companyInfoView.fzView.inputTextfield.text = result[@"legalRepresentative"];
    _companyInfoView.lxrView.inputTextfield.text = result[@"linkman"];
    _companyInfoView.sjhmView.inputTextfield.text = result[@"linkmanPhone"];
    _companyInfoView.bylxrView.inputTextfield.text = result[@"linkmanTwo"];
    _companyInfoView.bysjhmView.inputTextfield.text = result[@"linkmanTwoPhone"];
    _companyInfoView.xzqhView.info = result[@"divisionName"];
    _companyInfoView.xzqhView.lineId = result[@"division"];
    _companyInfoView.hylbView.info = result[@"industryClass"];
    _companyInfoView.pwxkzView.inputTextfield.text = result[@"licenseNumber"];
    _companyInfoView.pwjyhView.inputTextfield.text = result[@"emissionTradingDocuments"];
    _companyInfoView.hppfView.inputTextfield.text = result[@"eiaBatchNumber"];
    _locationView.jdView.inputTextfield.text = result[@"longitude"];
    _locationView.wdView.inputTextfield.text = result[@"latitude"];
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([result[@"latitude"] doubleValue], [result[@"longitude"] doubleValue]);
    [_locationView.mapView setCenterCoordinate:location animated:YES];
    [_locationView addPoint:location title:result[@"companyName"]];
    _locationView.addressView.text = result[@"addressDetail"];
    _taskView.principal = [NSString stringWithFormat:@"区域负责人：%@",@"--"];//无区域负责人  需要请求之后展示
    _taskView.implementView.info = @"未选择";
    _taskView.hetongView.inputTextfield.text = result[@"name"];
    _taskView.htbhView.inputTextfield.text = [CommonUtil fixNullText:result[@"number"]];
    _taskView.yiFangView.inputTextfield.text = result[@"bcompany"];
    _taskView.serviceTimeView.info = [NSString stringWithFormat:@"%@",[CommonUtil fixNullText:result[@"serviceYear"]]];
    _taskView.timeQuanTum.fromTimeView.info = [result[@"serviceStart"] substringWithRange:RANGE(0, 10)];
    _taskView.timeQuanTum.toTimeView.info = [result[@"serviceEnd"] substringWithRange:RANGE(0, 10)];
    _totalView.swfzr =[NSString stringWithFormat:@"商务负责人：%@",[CommonUtil fixNullTextWithLine:result[@"businessLeaderName"]]];
    if ([result[@"moneyBackStatus"] integerValue]==0) {
           _totalView.hkzt = @"回款状态：未回款 ";
    }else if ([result[@"moneyBackStatus"] integerValue]==1){
        _totalView.hkzt = @"回款状态：回款中 ";
    }else {
        _totalView.hkzt = @"回款状态：已回款 ";
    }
    _totalView.swPhone = result[@"businessLeaderPhone"];
    if ([result[@"applyStatus"] integerValue]==1) {
        _totalView.sszt = @"进行中";
        _totalView.ssPhone = result[@"applyLeaderPhone"];
    }else if ([result[@"applyStatus"] integerValue]==2){
        _totalView.sszt = @"已完成";
        _totalView.ssPhone = result[@"applyLeaderPhone"];
    }else {
        _commitButton.hidden = NO;
    }
    _serviceView.banciView.inputTextfield.text = result[@"productShifts"];
    _serviceView.fuwuView.info = result[@"service"];
    _serviceView.timeView.inputTextfield.text = result[@"productTime"];
    _serviceView.dianweiView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"pointCount"]];
    if (![result[@"comment"] isKindOfClass:[NSNull class]]) {
        _serviceView.markView.textView.text = result[@"comment"];
    }else {
        _serviceView.markView.textView.text = @"暂无现场情况备注";
    }
    _taskView.hetongView.inputTextfield.enabled = NO;
    _taskView.htbhView.inputTextfield.enabled = NO;
    _taskView.yiFangView.inputTextfield.enabled = NO;
    _taskView.serviceTimeView.userInteractionEnabled = NO;
    _taskView.timeQuanTum.fromTimeView.userInteractionEnabled = NO;
    _serviceView.fuwuView.userInteractionEnabled = NO;
    _serviceView.banciView.inputTextfield.enabled = NO;
    _serviceView.timeView.inputTextfield.enabled = NO;
    _serviceView.dianweiView.inputTextfield.enabled = NO;
    _serviceView.htjeView.inputTextfield.enabled = NO;
    _serviceView.markView.canEdit = NO;
    _serviceView.markView2.canEdit = NO;
    _serviceView.uploadView.isCommit = YES;
    [_serviceView.uploadView removeUploadButton];//提交状态图片不可编辑
    NSArray *array = result[@"pointImageAttachment"];
    if (array&&array.count>0) {
         _serviceView.uploadView.imageUrlArray = [array mutableCopy];
     }
}
- (void)setEditStatus {
    _companyInfoView.qymcView.userInteractionEnabled = NO;//甲方不可选择
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.fzView.inputTextfield.enabled = NO;
    _companyInfoView.lxrView.inputTextfield.enabled = NO;
    _companyInfoView.sjhmView.inputTextfield.enabled = NO;
    _companyInfoView.bylxrView.inputTextfield.enabled = NO;
    _companyInfoView.bysjhmView.inputTextfield.enabled = NO;
    _companyInfoView.xzqhView.baseView.userInteractionEnabled = NO;
    _companyInfoView.hylbView.baseView.userInteractionEnabled = NO;
    _companyInfoView.zczjView.inputTextfield.enabled = NO;
    _companyInfoView.pwxkzView.inputTextfield.enabled = NO;
    _companyInfoView.pwjyhView.inputTextfield.enabled = NO;
    _companyInfoView.hppfView.inputTextfield.enabled = NO;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    _locationView.needlocation = NO;
    [_locationView setCanEdit:NO];
    _locationView.addressView.editable = NO;
}
@end
