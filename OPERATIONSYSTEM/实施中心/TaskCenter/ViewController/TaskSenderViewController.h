//
//  TaskSenderViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/23.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskSenderViewController : FYBaseViewController
@property (nonatomic,copy)NSString *taskId;
@property (nonatomic,assign)BOOL isNoDivision;//无区域负责人
@property (nonatomic,strong) void (^senderBlock)(NSString *name);
@end

NS_ASSUME_NONNULL_END
