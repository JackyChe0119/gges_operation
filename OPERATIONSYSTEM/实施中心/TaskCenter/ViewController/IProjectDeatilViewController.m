//
//  IProjectDeatilViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/10.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "IProjectDeatilViewController.h"

#import "TaskCompanyInfoView.h"
#import "LocationView.h"
#import "TaskInfoView.h"
#import "TaskServiceView.h"
#import "ProjectTotalView.h"
#import "ProductInfoView.h"
#import "TaskMarkView.h"
#import "ImageUploadView.h"
#import "EquipmentManagerViewController.h"
#import "LegenSelectViewController.h"
@interface IProjectDeatilViewController ()
@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)UIScrollView *xcScrollView;
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)TaskCompanyInfoView *companyInfoView;
@property (nonatomic,strong)LocationView *locationView;
@property (nonatomic,strong)TaskInfoView *taskView;
@property (nonatomic,strong)TaskServiceView *serviceView;
@property (nonatomic,strong)ProjectTotalView *totalView;

@property (nonatomic,strong)ProductInfoView *productView;
@property (nonatomic,strong)ImageUploadView *dwfhtView;//点位复核图
@property (nonatomic,strong)ImageUploadView *czwgxtView;//产治污关系图
@property (nonatomic,strong)ImageUploadView *czwsjwdView;//产治污设计文档
@property (nonatomic,strong)TaskMarkView *scgyView;//生产工艺
@property (nonatomic,strong)TaskMarkView *zywrwView;//主要污染物
@property (nonatomic,strong)TaskMarkView *zysbView;//主要设备
@property (nonatomic,strong)LocationView *xcLocationView;
@property (nonatomic,assign)BOOL isloadXCInfo;//是否已请求现场概况
@property (nonatomic,copy)NSString *divisionLeader,*divisionId;

@end

@implementation IProjectDeatilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftButtonImage:BACKUP];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    [self navgationRightButtonTitle:@"设备管理" color:Color_3D3A39];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.backgroundColor = Color_White;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.scrollEnabled = NO;
    [_scrollView setContentSize:CGSizeMake(ScreenWidth*2, HEIGHT(_scrollView))];
    [self.view addSubview:_scrollView];
    
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, 0, ScreenWidth, ScreenHeight-NavHeight)];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [_scrollView addSubview:_baseScrollView];
    
        //先生成存放标题的数据
    NSArray *array = [NSArray arrayWithObjects:@"任务概况",@"现场概况",nil];
        //初始化UISegmentedControl
    _segment = [[UISegmentedControl alloc]initWithItems:array];
        //设置frame
    _segment.frame = CGRectMake(ScreenWidth/2.0-80, StatusBarHeight+9, 160, 26);
        //添加到视图
    _segment.tintColor = Color_CommonLight;
    [_segment setSelectedSegmentIndex:0];
    [_segment addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_segment];

    _taskView = [[TaskInfoView alloc]initWithFrame:RECT(0, 0, ScreenWidth, 240)];
    _taskView.implementView.operationBlock = ^{
        if (!weakSelf.divisionId) {
             [weakSelf showHUDMessage:@"获取区域信息出错，请返回重试"];
             return;
         }
         LegenSelectViewController *vc = [[LegenSelectViewController alloc]init];
         vc.url = @"basics/api/v1/operation/user/underlingUser";
         vc.selectWithIdBlock = ^(NSString * _Nonnull name, NSString * _Nonnull Id) {
             weakSelf.taskView.implementView.info = name;
             weakSelf.taskView.implementView.lineId = Id;
             [weakSelf commitButtonClick];
         };
         vc.navTitle = @"实施负责人";
         vc.userId = weakSelf.divisionLeader;
         vc.divisionId = weakSelf.divisionId;
         [weakSelf presentWithViewController:vc animated:YES];
    };
    [_baseScrollView addSubview:_taskView];
    
    _totalView = [[ProjectTotalView alloc]initWithFrame:RECT(0, BOTTOM(_taskView)+10, ScreenWidth, 90)];
    _totalView.swfzr = @"商务负责人：-- ";
    _totalView.ssfzr = @"实施负责人：-- ";
    _totalView.ywfzr = @"运维负责人：-- ";
    _totalView.hkzt = @"回款状态：--";
    _totalView.sszt = @"实施状态：--";
    _totalView.ywzt = @"运维状态：--";
    [_baseScrollView addSubview:_totalView];
    
    _serviceView = [[TaskServiceView alloc]initWithFrame:RECT(10, BOTTOM(_totalView)+10, ScreenWidth-20, 300)];
    [_serviceView hiddenHTJE];//隐藏合同金额
    [_baseScrollView addSubview:_serviceView];
    
    _companyInfoView = [[TaskCompanyInfoView alloc]initWithFrame:RECT(0, BOTTOM(_serviceView)+10, ScreenWidth, 500) type:2];
    [_baseScrollView addSubview:_companyInfoView];
        
    _locationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(_companyInfoView), ScreenWidth, 340)];
    [_baseScrollView addSubview:_locationView];
        
    [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(_locationView)+20)];

    //现场概况
    _xcScrollView = [[UIScrollView alloc]initWithFrame:RECT(ScreenWidth, 0, ScreenWidth, ScreenHeight-NavHeight)];
    _xcScrollView.showsVerticalScrollIndicator = NO;
    _xcScrollView.backgroundColor = Color_White;
    [self.view addSubview:_xcScrollView];
    [_scrollView addSubview:_xcScrollView];
    
    _productView = [[ProductInfoView alloc]initWithFrame:RECT(0, 10, ScreenWidth, 240)];

    [_xcScrollView addSubview:_productView];
    
    _dwfhtView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_productView)+10, ScreenWidth, 140) max:9];
    _dwfhtView.titleLabel.text = @"*点位复核图：";
    [_xcScrollView addSubview:_dwfhtView];
    
    _czwgxtView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_dwfhtView), ScreenWidth, 140) max:9];
    _czwgxtView.titleLabel.text = @"*产治污关系图：";
    [_xcScrollView addSubview:_czwgxtView];
    
    _czwsjwdView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_czwgxtView), ScreenWidth, 140) max:30];
    _czwsjwdView.titleLabel.text = @"产治污设计文档：";
    _czwsjwdView.maxCount = 40;
    [_xcScrollView addSubview:_czwsjwdView];
    
    _scgyView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_czwsjwdView)+10, ScreenWidth, 150)];
    _scgyView.titleLabel.text = @"生产工艺：";
    _scgyView.textView.text = @"请输入生产工艺";
    _scgyView.tipStr = @"请输入主要工艺";
    [_xcScrollView addSubview:_scgyView];
    
    _zywrwView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_scgyView)+10, ScreenWidth, 150)];
    _zywrwView.titleLabel.text = @"主要污染物：";
    _zywrwView.textView.text = @"请输入主要污染物";
    _zywrwView.tipStr = @"请输入主要污染物";
    [_xcScrollView addSubview:_zywrwView];
    
    _zysbView = [[TaskMarkView alloc]initWithFrame:RECT(0, BOTTOM(_zywrwView)+10, ScreenWidth, 150)];
    _zysbView.titleLabel.text = @"主要设备：";
    _zysbView.textView.text = @"请输入主要设备";
    _zysbView.tipStr = @"请输入主要设备";
    [_xcScrollView addSubview:_zysbView];
    
   UILabel *titleLabel = [UILabel new];
   [titleLabel rect:RECT(10, BOTTOM(_zysbView)+10, 100, 20) aligment:Left font:15 isBold:NO text:@"地址复核：" textColor:Color_333333 superView:_xcScrollView];
    
    _xcLocationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(titleLabel), ScreenWidth, 340)];
    [_xcScrollView addSubview:_xcLocationView];
    
    UIButton *zcCommitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0-150, BOTTOM(_xcLocationView)+30,140, 40) target:self action:@selector(zcCommitButtonClick) title:@"暂    存" color:Color_White font:17 type:2];
     [zcCommitButton addRoundedCornersWithRadius:5];
    [_xcScrollView addSubview:zcCommitButton];
    
    UIButton *bcCommitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0+10, BOTTOM(_xcLocationView)+30,140, 40) target:self action:@selector(bcCommitButtonClick) title:@"提    交" color:Color_White font:17 type:2];
     [bcCommitButton addRoundedCornersWithRadius:5];
    [_xcScrollView addSubview:bcCommitButton];
    
    [_xcScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(bcCommitButton)+20)];
    
    [self setEditStatus];
    [self requestDetail];
    
}
#pragma mark ---------->>> method
- (void)change:(UISegmentedControl *)seg {
    [_scrollView setContentOffset:CGPointMake(ScreenWidth*seg.selectedSegmentIndex, 0) animated:YES];
    if (!_isloadXCInfo) {
        [self requestXcDetail];
    }
}
- (void)commitButtonClick {
    if (!_divisionLeader) {
        [self showHUDMessage:@"区域负责人出问题，请稍后重试"];
        return;
    }
    if ([_taskView.implementView.info isEqualToString:@"未选择"]||_taskView.implementView.info.length==0) {
        [self showHUDMessage:@"请选择实施负责人"];
        return;
    }
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:_divisionLeader forKey:@"divisionLeader"];
    [params setValue:_taskView.implementView.lineId forKey:@"applyLeader"];
    [params setValue:self.divisionId forKey:@"divisionId"];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/user/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"分配成功"];
                if (_senderBlock) {
                    _senderBlock(_taskView.implementView.info);
                }
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)saveInfoWithcallBack:(void(^)(void))callBackBlock{
    if (_productView.tyxydmView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入统一信用识别代码"];return;
    }
    if (_productView.fzView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入法人姓名"];return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    if (_dwfhtView.imageArray.count>0) {
             __block NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_dwfhtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"pointReviewAttachment"];
            }else {
                [self showHUDMessage:@"请上传点位复核图"];return;
            }
        }
    if (_czwgxtView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_czwgxtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"relationAttachment"];
            }else {
                [self showHUDMessage:@"请上传产治污关系图"];return;
            }
        }
    if (_czwsjwdView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_czwsjwdView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"designAttachment"];
            }
        }
    if (_productView.nczView.inputTextfield.text.length>0) {
            [params setValue:_productView.nczView.inputTextfield.text forKey:@"annualOutputValue"];
    }
    if (_productView.nclView.inputTextfield.text.length>0) {
            [params setValue:_productView.nclView.inputTextfield.text forKey:@"annualOutput"];
    }
    if (_productView.npflView.inputTextfield.text.length>0) {
            [params setValue:_productView.npflView.inputTextfield.text forKey:@"vocs"];
    }
    if (_productView.nydlView.inputTextfield.text.length>0) {
            [params setValue:_productView.nydlView.inputTextfield.text forKey:@"yearPower"];
    }
    [params setValue:_productView.tyxydmView.inputTextfield.text forKey:@"creditCode"];
    [params setValue:_productView.fzView.inputTextfield.text forKey:@"legalRepresentative"];
    if (![_scgyView.textView.text isEqualToString:@"请输入生产工艺"]) {
        [params setValue:_scgyView.textView.text  forKey:@"productProcess"];
    }
    if (![_zywrwView.textView.text isEqualToString:@"请输入主要污染物"]) {
        [params setValue:_zywrwView.textView.text  forKey:@"mainPollute"];
    }
    if (![_zysbView.textView.text isEqualToString:@"请输入主要设备"]) {
        [params setValue:_zysbView.textView.text  forKey:@"mainEquip"];
    }
    [params setValue:_xcLocationView.jdView.inputTextfield.text forKey:@"longitudeReal"];
    [params setValue:_xcLocationView.wdView.inputTextfield.text forKey:@"latitudeReal"];
    [params setValue:_xcLocationView.addressView.text forKey:@"addressDetailReal"];
    [params setValue:[NSNumber numberWithInteger:1] forKey:@"sceneSurveyStatus"];//暂存
    [self showProgressHud];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/survey/%@",self.taskId] method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [self hiddenProgressHud];
               if ([responseMessage isRequestSuccessful]) {
                   [self showHUDMessage:@"保存成功"];
                   if (_addBlock) {
                       _addBlock();
                   }
                   [self.navigationController popViewControllerAnimated:YES];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
               }
           });
       }];
}
//现场概况 保存
- (void)bcCommitButtonClick {
    if (_productView.tyxydmView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入统一信用识别代码"];return;
    }
    if (_productView.fzView.inputTextfield.text.length==0) {
        [self showHUDMessage:@"请输入法人姓名"];return;
    }
    if (_dwfhtView.imageArray.count>0) {
            __block BOOL haiPicture = NO;
            [_dwfhtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    haiPicture = YES;
                }
            }];
            if (!haiPicture) {
                [self showHUDMessage:@"请上传点位复核图"];return;
            }
        }
    if (_czwgxtView.imageArray.count>0) {
            __block BOOL haiPicture = NO;
            [_czwgxtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    haiPicture = YES;
                }
            }];
            if (!haiPicture) {
                [self showHUDMessage:@"请上传产治污关系图"];return;
            }
        }
    [AlertViewUtil showSelectAlertViewWithVC:self Title:@"温馨提示" Message:@"提交现场概况前，请确认设备及数采仪已录入无误，一旦提交将无法再次添加" LeftTitle:@"取消" RightTitle:@"确定" callBack:^(NSInteger type) {
        if (type==2) {
            [self saveInfoWithcallBack:^{}];
        }
    }];
}
//现场概况 暂存
- (void)zcCommitButtonClick {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    if (_productView.nczView.inputTextfield.text.length>0) {
        [params setValue:_productView.nczView.inputTextfield.text forKey:@"annualOutputValue"];
    }
    if (_productView.nclView.inputTextfield.text.length>0) {
        [params setValue:_productView.nclView.inputTextfield.text forKey:@"annualOutput"];
    }
    if (_productView.npflView.inputTextfield.text.length>0) {
        [params setValue:_productView.npflView.inputTextfield.text forKey:@"vocs"];
    }
    if (_productView.nydlView.inputTextfield.text.length>0) {
        [params setValue:_productView.nydlView.inputTextfield.text forKey:@"yearPower"];
    }
    if (_dwfhtView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_dwfhtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"pointReviewAttachment"];
            }
        }
    if (_czwgxtView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_czwgxtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"relationAttachment"];
            }
        }
    if (_czwsjwdView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_czwsjwdView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"designAttachment"];
            }
        }
    if (![_scgyView.textView.text isEqualToString:@"请输入生产工艺"]) {
        [params setValue:_scgyView.textView.text  forKey:@"productProcess"];
    }
    if (![_zywrwView.textView.text isEqualToString:@"请输入主要污染物"]) {
        [params setValue:_zywrwView.textView.text  forKey:@"mainPollute"];
    }
    if (![_zysbView.textView.text isEqualToString:@"请输入主要设备"]) {
        [params setValue:_zysbView.textView.text  forKey:@"mainEquip"];
    }
    [params setValue:_xcLocationView.jdView.inputTextfield.text forKey:@"longitudeReal"];
    [params setValue:_xcLocationView.wdView.inputTextfield.text forKey:@"latitudeReal"];
    [params setValue:_xcLocationView.addressView.text forKey:@"addressDetailReal"];
    [params setValue:[NSNumber numberWithInteger:0] forKey:@"sceneSurveyStatus"];//暂存
    [params setValue:_productView.tyxydmView.inputTextfield.text forKey:@"creditCode"];
    [params setValue:_productView.fzView.inputTextfield.text forKey:@"legalRepresentative"];
    [self showProgressHud];
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/survey/%@",self.taskId] method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [self hiddenProgressHud];
               if ([responseMessage isRequestSuccessful]) {
                   [self showHUDMessage:@"暂存成功"];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
               }
           });
       }];
}
- (void)navgationRightButtonClick {
    EquipmentManagerViewController *vc = [EquipmentManagerViewController new];
    vc.Id = _companyInfoView.qymcView.lineId;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)requestXcDetail {
    [self showProgressHud];
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/survey/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [self hiddenProgressHud];
               if ([responseMessage isRequestSuccessful]) {
                   self.isloadXCInfo = YES;
                   NSDictionary *result = responseMessage.data[@"data"];
                   [self relayoutWithXcDeatil:result];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
                   [self.navigationController popViewControllerAnimated:YES];
               }
           });
       }];
}
#pragma mark ---------->>> request
- (void)requestDetail {
    [self showProgressHud];
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/%@",self.taskId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               [self hiddenProgressHud];
               if ([responseMessage isRequestSuccessful]) {
                   NSDictionary *result = responseMessage.data[@"data"];
                   _divisionId = result[@"division"];
                   [self relayoutWithDeatil:result];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
                   [self.navigationController popViewControllerAnimated:YES];
               }
           });
       }];
}
- (void)relayoutWithDeatil:(NSDictionary *)result {
    _companyInfoView.qymcView.info= result[@"companyName"];
    _companyInfoView.qymcView.lineId = result[@"companyId"];
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    if (![result[@"creditCode"] isKindOfClass:[NSNull class]]) {
        _companyInfoView.tyxydmView.inputTextfield.text = result[@"creditCode"];
    }
    if (![result[@"legalRepresentative"] isKindOfClass:[NSNull class]]) {
        _companyInfoView.fzView.inputTextfield.text = result[@"legalRepresentative"];
    }
    _companyInfoView.lxrView.inputTextfield.text = result[@"linkman"];
    _companyInfoView.sjhmView.inputTextfield.text = result[@"linkmanPhone"];
    if (![result[@"linkmanTwo"] isKindOfClass:[NSNull class]]) {
        _companyInfoView.bylxrView.inputTextfield.text = result[@"linkmanTwo"];
    }
    if (![result[@"linkmanTwoPhone"] isKindOfClass:[NSNull class]]) {
        _companyInfoView.bysjhmView.inputTextfield.text = result[@"linkmanTwoPhone"];
    }
    _companyInfoView.xzqhView.info = result[@"divisionName"];
    _companyInfoView.xzqhView.lineId = result[@"division"];
    _companyInfoView.hylbView.info = result[@"industryClass"];
    if (![result[@"licenseNumber"] isKindOfClass:[NSNull class]]) {
         _companyInfoView.pwxkzView.inputTextfield.text = result[@"licenseNumber"];
     }
    if (![result[@"emissionTradingDocuments"] isKindOfClass:[NSNull class]]) {
         _companyInfoView.pwjyhView.inputTextfield.text = result[@"emissionTradingDocuments"];
     }
    if (![result[@"eiaBatchNumber"] isKindOfClass:[NSNull class]]) {
         _companyInfoView.hppfView.inputTextfield.text = result[@"eiaBatchNumber"];
     }
    _locationView.jdView.inputTextfield.text = result[@"longitude"];
    _locationView.wdView.inputTextfield.text = result[@"latitude"];
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([result[@"latitude"] doubleValue], [result[@"longitude"] doubleValue]);
    [_locationView.mapView setCenterCoordinate:location animated:YES];
    [_locationView addPoint:location title:result[@"companyName"]];
    _locationView.addressView.text = result[@"addressDetail"];

    _xcLocationView.jdView.inputTextfield.text = result[@"longitude"];
     _xcLocationView.wdView.inputTextfield.text = result[@"latitude"];
     [_xcLocationView.mapView setCenterCoordinate:location];
     [_xcLocationView addPoint:location title:result[@"addressDetail"]];
     _xcLocationView.currentLocation = location;
     _xcLocationView.addressView.text = result[@"addressDetail"];
    
    _taskView.principal = [NSString stringWithFormat:@"区域负责人：%@",result[@"divisionLeaderName"]];
    _divisionLeader = result[@"divisionLeader"];
    _taskView.implementView.info = [CommonUtil fixNullTextWithLine:result[@"applyLeaderName"]];

    if (![result[@"name"] isKindOfClass:[NSNull class]]) {
        _taskView.hetongView.inputTextfield.text = result[@"name"];
    }
    _taskView.htbhView.inputTextfield.text = [CommonUtil fixNullText:result[@"number"]];
     
    if (![result[@"bcompany"] isKindOfClass:[NSNull class]]) {
         _taskView.yiFangView.inputTextfield.text = result[@"bcompany"];
     }
    _taskView.serviceTimeView.info = [NSString stringWithFormat:@"%@",result[@"serviceYear"]];
    _taskView.timeQuanTum.fromTimeView.info = [result[@"serviceStart"] substringWithRange:RANGE(0, 10)];
    _taskView.timeQuanTum.toTimeView.info = [result[@"serviceEnd"] substringWithRange:RANGE(0, 10)];

    _totalView.swfzr =[NSString stringWithFormat:@"商务负责人：%@",[CommonUtil fixNullTextWithLine:result[@"businessLeaderName"]]];
    if ([result[@"moneyBackStatus"] integerValue]==0) {
           _totalView.hkzt = @"回款状态：未回款 ";
    }else if ([result[@"moneyBackStatus"] integerValue]==1){
        _totalView.hkzt = @"回款状态：回款中 ";
    }else {
        _totalView.hkzt = @"回款状态：已回款 ";
    }
    _totalView.swPhone = result[@"businessLeaderPhone"];
    
    if ([result[@"applyStatus"] integerValue]==1) {
        _totalView.sszt = @"实施状态：进行中";
        _totalView.ssfzr = [NSString stringWithFormat:@"实施负责人：%@",[CommonUtil fixNullTextWithLine:result[@"applyLeaderName"]]];
        _totalView.ssPhone = [CommonUtil fixNullText:result[@"applyLeaderPhone"]];
    }else if ([result[@"applyStatus"] integerValue]==2){
        _totalView.sszt = @"实施状态：已完成";
        _totalView.ssfzr = [NSString stringWithFormat:@"实施负责人：%@",[CommonUtil fixNullTextWithLine:result[@"applyLeaderName"]]];
        _totalView.ssPhone = [CommonUtil fixNullText:result[@"applyLeaderPhone"]];
    }
    
    if (![result[@"productShifts"] isKindOfClass:[NSNull class]]) {
         _serviceView.banciView.inputTextfield.text = result[@"productShifts"];
     }
    if (![result[@"service"] isKindOfClass:[NSNull class]]) {
         _serviceView.fuwuView.info = result[@"service"];
     }
    if (![result[@"productTime"] isKindOfClass:[NSNull class]]) {
           _serviceView.timeView.inputTextfield.text = result[@"productTime"];
       }
    if (![result[@"pointCount"] isKindOfClass:[NSNull class]]) {
            _serviceView.dianweiView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"pointCount"]];
        }
    if (![result[@"comment"] isKindOfClass:[NSNull class]]) {
              _serviceView.markView.textView.text = result[@"comment"];
    }else {
        _serviceView.markView.textView.text = @"无";
    }

    NSArray *array = result[@"pointImageAttachment"];
    if (array&&array.count>0) {
         _serviceView.uploadView.imageUrlArray = [array mutableCopy];
     }
}
- (void)setEditStatus {
    _companyInfoView.qymcView.userInteractionEnabled = NO;//甲方不可选择
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.fzView.inputTextfield.enabled = NO;
    _companyInfoView.lxrView.inputTextfield.enabled = NO;
    _companyInfoView.sjhmView.inputTextfield.enabled = NO;
    _companyInfoView.bylxrView.inputTextfield.enabled = NO;
    _companyInfoView.bysjhmView.inputTextfield.enabled = NO;
    _companyInfoView.xzqhView.baseView.userInteractionEnabled = NO;
    _companyInfoView.hylbView.baseView.userInteractionEnabled = NO;
    _companyInfoView.zczjView.inputTextfield.enabled = NO;
    _companyInfoView.pwxkzView.inputTextfield.enabled = NO;
    _companyInfoView.pwjyhView.inputTextfield.enabled = NO;
    _companyInfoView.hppfView.inputTextfield.enabled = NO;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    _locationView.needlocation = NO;
    [_locationView setCanEdit:NO];
    _locationView.addressView.editable = NO;
    _taskView.hetongView.inputTextfield.enabled = NO;
    _taskView.htbhView.inputTextfield.enabled = NO;
    _taskView.yiFangView.inputTextfield.enabled = NO;
    _taskView.serviceTimeView.userInteractionEnabled = NO;
    _taskView.timeQuanTum.fromTimeView.userInteractionEnabled = NO;
    _serviceView.fuwuView.userInteractionEnabled = NO;
    _serviceView.banciView.inputTextfield.enabled = NO;
    _serviceView.timeView.inputTextfield.enabled = NO;
    _serviceView.dianweiView.inputTextfield.enabled = NO;
    _serviceView.htjeView.inputTextfield.enabled = NO;
    _serviceView.markView.canEdit = NO;
    _serviceView.markView2.canEdit = NO;
    _serviceView.uploadView.isCommit = YES;
    [_serviceView.uploadView removeUploadButton];//提交状态图片不可编辑
}
- (void)relayoutWithXcDeatil:(NSDictionary *)result {
    _productView.banciView.inputTextfield.text = result[@"productShifts"];
    _productView.banciView.inputTextfield.enabled = NO;
    _productView.timeView.inputTextfield.text = result[@"productTime"];
    _productView.timeView.inputTextfield.enabled = NO;
    _productView.dianweiView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"pointCount"]];
    _productView.dianweiView.inputTextfield.enabled = NO;
    _productView.sjslView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"realPointCount"]];
    _productView.sjslView.inputTextfield.enabled = NO;
    if (![result[@"annualOutputValue"] isKindOfClass:[NSNull class]]) {
        _productView.nczView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"annualOutputValue"]];
    }
    if (![result[@"annualOutput"] isKindOfClass:[NSNull class]]) {
        _productView.nclView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"annualOutput"]];
    }
    if (![result[@"vocs"] isKindOfClass:[NSNull class]]) {
        _productView.npflView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"vocs"]];
    }
    if (![result[@"yearPower"] isKindOfClass:[NSNull class]]) {
        _productView.nydlView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"yearPower"]];
    }
    if (![result[@"creditCode"] isKindOfClass:[NSNull class]]) {
        _productView.tyxydmView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"creditCode"]];
    }
    if (![result[@"legalRepresentative"] isKindOfClass:[NSNull class]]) {
        _productView.fzView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"legalRepresentative"]];
    }
    NSArray *array = result[@"pointReviewAttachment"];
    if (array&&array.count>0) {
         _dwfhtView.imageUrlArray = [array mutableCopy];
     }
    NSArray *array2 = result[@"relationAttachment"];
    if (array2&&array2.count>0) {
         _czwgxtView.imageUrlArray = [array2 mutableCopy];
     }
    NSArray *array3 = result[@"designAttachment"];
     if (array3&&array3.count>0) {
          _czwsjwdView.imageUrlArray = [array3 mutableCopy];
      }
    _scgyView.textView.text = result[@"productProcess"];
    _zywrwView.textView.text = result[@"mainPollute"];
    _zysbView.textView.text = result[@"mainEquip"];


    if ([CommonUtil feiKong:result[@"longitudeReal"]]) {
        _xcLocationView.jdView.inputTextfield.text = result[@"longitudeReal"];
        _xcLocationView.wdView.inputTextfield.text = result[@"latitudeReal"];
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake([result[@"latitudeReal"] doubleValue], [result[@"longitudeReal"] doubleValue]);
        [_xcLocationView.mapView setCenterCoordinate:location];
        [_xcLocationView addPoint:location title:result[@"addressDetailReal"]];
        _xcLocationView.currentLocation = location;
        _xcLocationView.addressView.text = result[@"addressDetail"];
    }
    _xcLocationView.needlocation = YES;
    _xcLocationView.locaImage.hidden = NO;
}
@end
