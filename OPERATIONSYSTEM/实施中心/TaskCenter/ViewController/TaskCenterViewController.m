//
//  TaskCenterViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/20.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "TaskCenterViewController.h"
#import "MineViewController.h"
#import "AlertListSelectView.h"
#import "SearchView.h"
#import "TaskCenterListCell.h"
#import "TaskSenderViewController.h"
#import "AreaSelectViewController.h"
#import "TaskModel.h"
#import "IProjectDeatilViewController.h"
#import "ProjectDeatilViewController.h"
static NSString * const TASK_LIST = @"TaskCenterListCell";

@interface TaskCenterViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UISegmentedControl *segment;//筛选视图
@property (nonatomic,strong)AlertListSelectView *areaView;
@property (nonatomic,strong)AlertListSelectView *taskTypeView;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)SearchView *searchView;
@property (nonatomic,strong)NSMutableArray *listArray;
@end

@implementation TaskCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    __weak typeof(self)weakSelf = self;
    
    _listArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    [self setLeftButtonTitle:@"实"];

    [self setNavTitle:@"任务中心" Color:Color_3D3A39];
    
    [self addLineViewWithFrame:RECT(0, NavHeight, ScreenWidth, 4) Color:Color_BG supView:self.view];
    
    _searchView = [[SearchView alloc]initWithFrame:RECT(0, NavHeight+8, ScreenWidth, 40) type:1];
    _searchView.placeHold = @"请输入搜索内容";
    _searchView.searchBlock = ^(NSString * _Nonnull search) {
       [weakSelf.tableView resetPageNum:1];
         [weakSelf reqestTaskList:1 show:YES];
    };
    [self.view addSubview:_searchView];
    
    _areaView = [[AlertListSelectView alloc]initWithFrame:RECT(0, BOTTOM(_searchView), ScreenWidth/2.0, 40)];
    _areaView.title = @"区    域：";
    _areaView.info = @"全部";
    _areaView.operationBlock = ^{
        NSArray  *array = [UserManager shareManager].areaJosn;
        if (array.count==0) {
            [weakSelf showHUDMessage:@"您当前账号暂无区域可选"];
        }else {
            [weakSelf showCityPickerView];
        }
    };
    [self.view addSubview:_areaView];
    
    _taskTypeView = [[AlertListSelectView alloc]initWithFrame:RECT(ScreenWidth/2.0, BOTTOM(_searchView), ScreenWidth/2.0, 40)];
    _taskTypeView.title = @"任务状态：";
    _taskTypeView.info = @"全部";
    _taskTypeView.operationBlock = ^{
        [weakSelf showActionSheetWithArray:@[@"全部",@"未分配",@"进行中",@"已完成"] title:@"任务状态"];
    };
    [self.view addSubview:_taskTypeView];
    
    [self addLineViewWithFrame:RECT(0, BOTTOM(_taskTypeView), ScreenWidth, 4) Color:Color_BG supView:self.view];
    
    _tableView = [[UITableView alloc]initWithFrame:RECT(0, BOTTOM(_taskTypeView)+4, ScreenWidth, ScreenHeight-BOTTOM(_taskTypeView)-4-TabBarHeight) style:0];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 64;
    _tableView.noDataContent = @"暂无任务列表";
    _tableView.marginTop = HEIGHT(_tableView)/2.0-80;
    [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0 )];
    [_tableView registerNib:[UINib nibWithNibName:@"TaskCenterListCell" bundle:nil] forCellReuseIdentifier:TASK_LIST];
    _tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_tableView];
    
    NSArray  *array = [UserManager shareManager].areaJosn;
    if (!array.count) {
        dispatch_queue_t queue = dispatch_queue_create("loadAreaTree", DISPATCH_QUEUE_CONCURRENT);
        dispatch_async(queue, ^{
            [self requestDivisionCode:NO];
        });
    }
    [_tableView addHeaderRefreshWithAutomaticallyRefresh:YES refreshBlock:^(NSInteger pageIndex) {
          [weakSelf reqestTaskList:pageIndex show:NO];
      }];
      [_tableView addFootLoadMoreWithAutomaticallyLoad:YES loadMoreBlock:^(NSInteger pageIndex) {
          if (weakSelf.noMoreData) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  [weakSelf showHUDMessage:@"暂无更多数据了"];
                  [weakSelf.tableView endFootLoadMore];
              });
              return;
          }
          [weakSelf reqestTaskList:pageIndex show:NO];
      }];
      [self reqestTaskList:1 show:YES];
    [self requestUpdate];

}
#pragma mark ---------->>> method

- (void)navgationLeftButtonClick {
    MineViewController *vc = [MineViewController new];
    [self pushWithViewController:vc hidden:YES];
}
#pragma mark ---------->>> delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskCenterListCell *cell = [tableView dequeueReusableCellWithIdentifier:TASK_LIST];
    TaskModel *model = self.listArray[indexPath.row];
    cell.nameLabel.text  = model.companyName;
    if (model.applyStatus==0) {
        cell.typeLabel.text = @"任务状态：未分配";
        cell.legenLabel.text = [NSString stringWithFormat:@"区域负责人：%@",[CommonUtil fixNullTextWithLine:model.divisionLeaderName]];
    }else if (model.applyStatus==1) {
        cell.typeLabel.text = @"任务状态：进行中";
        cell.legenLabel.text = [NSString stringWithFormat:@"实施负责人：%@",[CommonUtil fixNullTextWithLine:model.applyLeaderName]];
    }else {
        cell.typeLabel.text = @"任务状态：已完成";
        cell.legenLabel.text = [NSString stringWithFormat:@"实施负责人：%@",[CommonUtil fixNullTextWithLine:model.applyLeaderName]];
    }
    cell.nameLabel.text = model.companyName;
    cell.areaLabel.text = [NSString stringWithFormat:@"区域：%@",model.division];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskModel *model = self.listArray[indexPath.row];
    if (model.applyStatus==0) {
        TaskSenderViewController *vc = [TaskSenderViewController new];//点击跳转至任务分配
        vc.taskId = model.Id;
        vc.senderBlock = ^(NSString *name){
            model.applyStatus = 1;
            model.applyLeaderName = name;
            [tableView reloadData];
        };
        vc.isNoDivision = YES;
        [self pushWithViewController:vc hidden:YES];
    }else if (model.applyStatus==1) {
        IProjectDeatilViewController *vc = [IProjectDeatilViewController new];
        vc.taskId = model.Id;
        vc.senderBlock = ^(NSString *name){
            model.applyStatus = 1;
            model.applyLeaderName = name;
            [tableView reloadData];
        };
        [self pushWithViewController:vc hidden:YES];
    }else {
        ProjectDeatilViewController *vc = [ProjectDeatilViewController new];
        vc.taskId = model.Id;
        if ([UserManager shareManager].roleGroup==1) {
            vc.isFromFZR = YES;
        }
        [self pushWithViewController:vc hidden:YES];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return .5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView createViewWithFrame:RECT(0, 0, ScreenWidth, .5) color:Color_Line];
}
- (void)requestDivisionCode:(BOOL)show {
//    商务中心  001003001
//    实施中心  001003002
//    运维中心  001003003
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[UserManager shareManager].deptCode forKey:@"deptCode"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/division/divisionTreeByDeptCode" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }
            if ([responseMessage isRequestSuccessful]) {
                NSArray *data = responseMessage.data[@"data"];
                             if (data.count>0) {
                                      NSArray *treeArray = data;
                                      NSMutableArray *totalArray = [[NSMutableArray alloc]initWithCapacity:0];
                                      NSMutableDictionary *all = [[NSMutableDictionary alloc]initWithCapacity:0];
                                      [all setValue:@"全部" forKey:@"name"];
                                      [all setValue:@"0" forKey:@"parentId"];
                                      [totalArray addObject:all];
                                      [totalArray addObjectsFromArray:treeArray];
                                      [UserManager shareManager].areaJosn = (NSArray *)totalArray;
                                      if (show) {
                                         [self showCityPickerView];
                                      }
                             }else{
                                 if (show) {
                                     [self showHUDMessage:@"您当前账号暂无区域可选"];
                                 }
                             }
            }else {
                if (show) {
                    [self showHUDMessage:responseMessage.errorMessage];
                }
            }
        });
    }];
}
- (void)showCityPickerView {
    __weak typeof(self)weakSelf = self;
    AreaSelectViewController *vc = [[AreaSelectViewController alloc]init];
    vc.titleStr = @"选择区域";
    vc.parentName = @"全部";
    vc.parentCode = @"0";
    vc.areaBlock = ^(NSString * _Nonnull name, NSString * _Nonnull code) {
        weakSelf.areaView.info = name;
        weakSelf.areaView.lineId= code;
        [weakSelf.tableView resetPageNum:1];
        [weakSelf reqestTaskList:1 show:YES];
    };
    [self presentWithViewController:vc animated:YES];
}
#pragma mark ---------->>> 弹出视图
- (void)showActionSheetWithArray:(NSArray *)array title:(NSString *)title{
    GYZActionSheet *sheet = [[GYZActionSheet alloc]initSheetWithTitle:title style:GYZSheetStyleWeiChat itemTitles:array];
    sheet.delegate = self;
    sheet.titleTextColor = Color_999999;
    sheet.itemTextColor = Color_333333;
    sheet.itemTextFont = FONT(17);
    sheet.cancleTextColor = color_Red;
    sheet.cancleTextFont = FONT(17);
    [sheet show];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
 title:(NSString *)title
                         sender:(GYZActionSheet *)actionSheet {
    _taskTypeView.info = title;
    [self.tableView resetPageNum:1];
    [self reqestTaskList:1 show:YES];
}
- (void)sheetViewDidSelectIndex:(NSInteger)index
                          title:(NSString *)title {
    
}
#pragma mark ---------->>> request
- (void)reqestTaskList:(NSInteger)pageNum show:(BOOL)show {
    if (show) {
        [self showProgressHud];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:[NSNumber numberWithInteger:pageNum] forKey:@"pageNum"];
    [params setValue:[NSNumber numberWithInteger:20] forKey:@"pageSize"];
    [params setValue:[UserManager shareManager].Id forKey:@"userId"];
    if (_areaView.lineId&&![_areaView.lineId isEqualToString:@"0"]) {
        [params setValue:_areaView.lineId forKey:@"division"];
    }
    if (![_taskTypeView.info isEqualToString:@"全部"]) {
        if ([_taskTypeView.info isEqualToString:@"未分配"]) {
            [params setValue:[NSNumber numberWithInteger:0] forKey:@"applyStatus"];
        }else if ([_taskTypeView.info isEqualToString:@"进行中"]){
            [params setValue:[NSNumber numberWithInteger:1] forKey:@"applyStatus"];
        }else {
            [params setValue:[NSNumber numberWithInteger:2] forKey:@"applyStatus"];
        }
    }
    if (_searchView.searchTextField.text.length>0) {
            [params setValue:_searchView.searchTextField.text forKey:@"companyName"];
    }
    [RequestTool requestDataWithUrlSuffix:@"devops/api/v1/apply/task" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (show) {
                [self hiddenProgressHud];
            }else {
                [_tableView endLoading];
            }
            if ([responseMessage isRequestSuccessful]) {
                if (pageNum==1) {
                    [self.listArray removeAllObjects];
                }
                NSDictionary *result = responseMessage.data;
                NSArray *array = result[@"data"];
                [_listArray addObjectsFromArray:[TaskModel mj_objectArrayWithKeyValuesArray:array]];
                    if (array.count==0) {
                        [_tableView resetPageNum:pageNum];
                        self.noMoreData = YES;
                    }else {
                        self.noMoreData = NO;
                    }
                _tableView.dataArray = _listArray;
                [_tableView reloadWithData];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)requestUpdate {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *localVersion = [CommonUtil getInfoWithKey:@"localVersion"];
    if ([version isEqualToString:localVersion]&&localVersion) {
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestDataWithUrlSuffix:@"basics/api/v1/mobile/update/iosdev" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *versionInfo = responseMessage.data[@"data"];
                if (!versionInfo||![CommonUtil feiKong:version]) {
                    return ;
                }
                NSString *requestVersion = versionInfo[@"apkVersion"]; //请求的版本
                if (![version isEqualToString:requestVersion]) { //版本是否一致 不一致 执行是否更新操作
                    [CommonUtil setInfo:requestVersion forKey:@"localVersion"];
                    if ([versionInfo[@"prop"] integerValue]==1) { //有需要弹出
                        if ([versionInfo[@"forceUpdate"] integerValue]==1) { //强制更新
                            BOOL TemporarilyNotUpdate = [[NSUserDefaults standardUserDefaults] boolForKey:@"TemporarilyNotUpdate"];
                            if (TemporarilyNotUpdate) { //暂不更新是否点击过
                                return ;
                            }
                            [self showAlert:0];
                        }else { //不强制更新
                            [UserManager shareManager].isForce = YES;
                            [self showAlert:1];
                        }
                    }else {
                        //不需要弹出  不用处理
                    }
                }
            }else {
                if ([UserManager shareManager].isForce) {
                    [self showAlert:1];
                }
            }
        });
    }];
}
- (void)showAlert:(NSInteger)type {
    if (type==1) {
        [AlertViewUtil showCancelAlertViewWithVC:MainWindow.rootViewController Title:@"温馨提示！" Message:@"版本已更新，如不更新，当前版本暂时无法使用" LeftTitle:@"更新" callbackBlock:^{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TemporarilyNotUpdate"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1484906853"]];
        }];
    }else {
        [AlertViewUtil showSelectAlertViewWithVC:MainWindow.rootViewController Title:@"温馨提示！" Message:@"版本已更新，为了您的使用体验请前往更新" LeftTitle:@"暂不更新" RightTitle:@"去更新" callBack:^(NSInteger type) {
            if (type==2) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TemporarilyNotUpdate"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1484906853"]];
            }else {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TemporarilyNotUpdate"]; //暂不更新
            }
        }];
    }
}
@end
