//
//  TaskModel.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/9.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskModel : HYBaseModel
//applyLeaderName = "商务主管";
//              applyStatus = 0;
//              companyName = "呵呵文化广场";
//              division = "杭州/西湖";
//              divisionLeaderName = "商务主管";
//              id = 341746382421495808;
@property (nonatomic,copy)NSString *applyLeaderName,*companyName,*division,*divisionLeaderName,*Id;
@property (nonatomic,assign)NSInteger applyStatus;
@end

NS_ASSUME_NONNULL_END
