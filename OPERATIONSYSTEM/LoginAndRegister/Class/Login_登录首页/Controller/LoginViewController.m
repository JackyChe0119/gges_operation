//
//  LoginViewController.m
//  GEESYSTEM
//
//  Created by 车杰 on 2019/3/15.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "LoginViewController.h"
#import "CustomInputView.h"
#import "MainViewController.h"
#import "ForgetPswViewController.h"
// 引入JPush功能所需头文件
#import "ChooseSystemViewController.h"
@interface LoginViewController ()
@property (nonatomic,strong)CustomInputView *phoneNumView;
@property (nonatomic,strong)CustomInputView *loginPswView;
@property (nonatomic,strong)CustomInputView *phoneView;
@property (nonatomic,strong)CustomInputView *codeView;
@property (nonatomic,strong)CustomGraButton *loginButton;
@property (nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong)UIButton *eyesButton;//是否加密
@property (nonatomic,strong)UIButton *keepPswButton;//记住密码
@property (nonatomic,strong)UIButton *forgetPSWButton;//自动登录
@property (nonatomic,strong)UILabel *countDownLabel;//验证码
@property (nonatomic,assign)BOOL isCodeLogin;//是验证码登录
@end

@implementation LoginViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layoutUI];
    
    self.customNavView.hidden = YES;
}
#pragma mark 布局视图
- (void)layoutUI {
    
    UIImageView *bgImageView = [UIImageView createImageViewWithFrame:RECT(0, 0, ScreenWidth, ScreenHeight) imageName:@"icon_login_bgview"];
    [self.view addSubview:bgImageView];
    
    UIImageView *logo = [UIImageView createImageViewWithFrame:RECT(ScreenWidth/2.0-80, ScreenHeight/2.0-220, 160, 50) imageName:@"icon_logo_bg"];
    [self.view addSubview:logo];
    
    UILabel *nameLabel = [UILabel new];
    [nameLabel rect:RECT(20, ScreenHeight/2.0-170, ScreenWidth-40, 30) aligment:Center font:25 isBold:YES text:@"绿金运营平台" textColor:Color_333333 superView:self.view];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:RECT(0,  ScreenHeight/2.0-80, ScreenWidth, 120)];
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.scrollEnabled = NO;
    [self.view addSubview:_scrollView];
    
    _phoneNumView = [[CustomInputView alloc]initWithFrame:RECT(40, 0, ScreenWidth-80, 60)];
    _phoneNumView.showLine = YES;
    _phoneNumView.tipTitle = @"请输入账号";
    [_phoneNumView.inputTextField addObserverTextFieldEditChangeWithLenth:32 inputType:ImportChatTypeNumberAndEnglishXiahuaxian];
    _phoneNumView.inputTextField.keyboardType = UIKeyboardTypeASCIICapable;
    [_phoneNumView.inputTextField addTarget:self action:@selector(inputChanged) forControlEvents:UIControlEventEditingChanged];
    [_scrollView addSubview:_phoneNumView];
    
    _loginPswView = [[CustomInputView alloc]initWithFrame:RECT(40, BOTTOM(_phoneNumView), ScreenWidth-80, 60)];
    _loginPswView.showLine = YES;
    _loginPswView.tipTitle = @"请输入登录密码";
    [_loginPswView.inputTextField addObserverTextFieldEditChangeWithLenth:16 inputType:ImportChatTypeNumberAndEnglish];
    _loginPswView.inputTextField.keyboardType = UIKeyboardTypeASCIICapable;
    _loginPswView.inputTextField.secureTextEntry = YES;
    [_loginPswView.inputTextField addTarget:self action:@selector(inputChanged) forControlEvents:UIControlEventEditingChanged];
    _loginPswView.inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _loginPswView.inputTextField.frame = RECT(0, 20, WIDTH(_loginPswView)-40, 30);
    [_scrollView addSubview:_loginPswView];
    
    _eyesButton = [UIButton createButtonWithFrame:RECT(WIDTH(_loginPswView)-35, 20, 30, 30) Target:self Selector:@selector(eyesButtonClick) Image:@"icon_eyesClose"];
    _eyesButton.selected = NO;
    [_loginPswView addSubview:_eyesButton];
    
    _phoneView = [[CustomInputView alloc]initWithFrame:RECT(40+ScreenWidth, 0, ScreenWidth-80, 60)];
    _phoneView.showLine = YES;
    _phoneView.tipTitle = @"请输入手机号码";
    [_phoneView.inputTextField addObserverTextFieldEditChangeWithLenth:11 inputType:ImportChatTypeNumber];
    _phoneView.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_phoneView.inputTextField addTarget:self action:@selector(inputChanged2) forControlEvents:UIControlEventEditingChanged];
    [_scrollView addSubview:_phoneView];
    
    _codeView = [[CustomInputView alloc]initWithFrame:RECT(ScreenWidth+40, BOTTOM(_phoneNumView), ScreenWidth-80, 60)];
    _codeView.showLine = YES;
    _codeView.tipTitle = @"请输入验证码";
    [_codeView.inputTextField addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumber];
    [_codeView.inputTextField addTarget:self action:@selector(inputChanged2) forControlEvents:UIControlEventEditingChanged];
    _codeView.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    _codeView.inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _codeView.inputTextField.frame = RECT(0, 20, WIDTH(_loginPswView)-100, 30);
    [_scrollView addSubview:_codeView];
    
    
    _countDownLabel = [UILabel new];
    [_countDownLabel rect:RECT(WIDTH(_codeView)-100, 20, 100, 30) aligment:Center font:13 isBold:NO text:@"获取验证码" textColor:Color_Common superView:_codeView];
    _countDownLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(getCode)];
    [_countDownLabel addGestureRecognizer:tap];
    [_countDownLabel addBorderWithWidth:.5 borderColor:Color_Common];
    
    
    _keepPswButton = [UIButton createButtonWithFrame:RECT(36, BOTTOM(_scrollView)+10,80, 30) Target:self Selector:@selector(keepPswButtonClick) Image:@"icon_unselect"];
    [_keepPswButton setTitle:@" 记住密码" forState:UIControlStateNormal];
    [_keepPswButton setTitleColor:Color_666666 forState:UIControlStateNormal];
    _keepPswButton.titleLabel.font = FONT(13);
    [_keepPswButton setImage:IMAGE(@"icon_select") forState:UIControlStateSelected];
    _keepPswButton.selected = YES;
    [self.view addSubview:_keepPswButton];
    
    _forgetPSWButton = [UIButton createButtonWithFrame:RECT(ScreenWidth-110,BOTTOM(_scrollView)+10, 80, 30) Target:self Selector:@selector(forgetPswButtonClick) Image:@""];
    [_forgetPSWButton setTitle:@" 忘记密码？" forState:UIControlStateNormal];
    [_forgetPSWButton setTitleColor:Color_Common forState:UIControlStateNormal];
    _forgetPSWButton.titleLabel.font = FONT(13);
    _forgetPSWButton.selected = YES;
    [self.view addSubview:_forgetPSWButton];
    
    _loginButton = [[CustomGraButton alloc]initWithFrame:RECT(40, BOTTOM(_scrollView)+100, ScreenWidth-80, 44) target:self action:@selector(LoginButtonClick) title:@"登  录" color:Color_White font:17 type:1];
    [_loginButton addRoundedCornersWithRadius:22];
    
    [self.view addSubview:_loginButton];
    
    UIButton *exchangeButton = [UIButton createButtonWithFrame:RECT(ScreenWidth/2.0-60,BOTTOM(_loginButton)+10, 120, 30) Target:self Selector:@selector(scxchangeButtonClick:) Image:@"icon_exChangeLogin"];
    [exchangeButton setTitle:@" 验证码登录" forState:UIControlStateNormal];
    [exchangeButton setTitleColor:Color_Common forState:UIControlStateNormal];
    exchangeButton.titleLabel.font = FONT(14);
    [self.view addSubview:exchangeButton];
    
    if ([UserManager shareManager].Acount.length>0) {
        _phoneNumView.inputTextField.text = [UserManager shareManager].Acount;
    }
    if ([UserManager shareManager].rememberPsw==NO) {
        _keepPswButton.selected = NO;
    }else {
        if ([UserManager shareManager].Password.length>0) {
            _loginPswView.inputTextField.text = [UserManager shareManager].Password;
        }
        _keepPswButton.selected = YES;
    }
    [self inputChanged];

}
- (void)inputChanged {
    if (_phoneNumView.inputTextField.text.length>0&&_loginPswView.inputTextField.text.length>=6) {
        [_loginButton setSlectState];
    }else {
        [_loginButton setUnSelectState];
    }
}
- (void)inputChanged2 {
    
}
#pragma mark private
- (void)getCode { //获取登录验证码
    if (![CommonUtil isValueMobile:_phoneView.inputTextField.text]) {
        [self showHUDMessage:@"无效的手机号码"];
        return;
    }
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestLoginWithUrlSuffix:[NSString stringWithFormat:@"sso/sso/sendSms?phone=%@&type=%@",_phoneView.inputTextField.text,@"1"] method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"短信已发至您的手机，请注意查看"];
                [_countDownLabel countdown:^{
                    //已完成
                } :^{
                    //未完成
                }];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)eyesButtonClick {
    _eyesButton.selected = !_eyesButton.selected;
    if (_eyesButton.selected) {
        [_eyesButton setImage:IMAGE(@"icon_eyes") forState:UIControlStateNormal];
    }else {
        [_eyesButton setImage:IMAGE(@"icon_eyesClose") forState:UIControlStateNormal];
    }
    _loginPswView.inputTextField.secureTextEntry = !_eyesButton.selected;
}
- (void)keepPswButtonClick {
    _keepPswButton.selected = !_keepPswButton.selected;
    if (_keepPswButton.selected) {
        [_keepPswButton setImage:IMAGE(@"icon_select") forState:UIControlStateNormal];
    }else {
        [_keepPswButton setImage:IMAGE(@"icon_unselect") forState:UIControlStateNormal];
    }
}
- (void)scxchangeButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        _isCodeLogin = YES;
        [_scrollView setContentOffset:CGPointMake(ScreenWidth, 0) animated:YES];
        [sender setTitle:@" 账号密码登录" forState:UIControlStateNormal];
        _keepPswButton.hidden = YES;
        _forgetPSWButton.hidden = YES;
    }else {
        _isCodeLogin = NO;
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [sender setTitle:@" 验证码登录" forState:UIControlStateNormal];
        _keepPswButton.hidden = NO;
        _forgetPSWButton.hidden = NO;
    }
}
- (void)forgetPswButtonClick {
    ForgetPswViewController *vc = [[ForgetPswViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
static void extracted(LoginViewController *object, NSMutableDictionary *params, NSString *url) {
    [RequestTool requestLoginWithUrlSuffix:url method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                NSDictionary *userInfo = responseMessage.data[@"info"];
                [[NSUserDefaults standardUserDefaults] setObject:userInfo[@"token"] forKey:REQUEST_SESSION];
                [[NSUserDefaults standardUserDefaults] setObject:userInfo[@"token"] forKey:AUTO_LOGIN];
                [UserManager shareManager].autoLogin = YES;
                NSInteger type = [userInfo[@"type"] integerValue];
                if (type==1||type==7) {
                    [object requestjurisdiction:userInfo];
                    [[NSUserDefaults standardUserDefaults]setInteger:type forKey:@"acountType"];
                    [[NSUserDefaults standardUserDefaults] setInteger:[userInfo[@"roleGroup"] integerValue] forKey:@"roleGroup"];
                    [UserManager shareManager].acountType =type;
                    [UserManager shareManager].roleGroup = [userInfo[@"roleGroup"] integerValue];
                }else {
                    [object hiddenProgressHud];
                    [object showHUDMessage:@"当前账户无操作权限"];
                }
            }else {
                [object hiddenProgressHud];
                if ([responseMessage.responseCode isEqualToString:@"0000003"]||[responseMessage.responseCode isEqualToString:@"0000002"]) {
                    [object showHUDMessage:@"登录-账号或密码错误"];
                }else {
                    [object showHUDMessage:responseMessage.errorMessage];
                }
            }
        });
    }];
}
//登录点击
- (void)LoginButtonClick {
    NSArray *array = @[];
    [UserManager shareManager].industry = array;//清空区域数据
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    NSString *url;
    if (!_isCodeLogin) {//账号密码登录
        [params setValue:_phoneNumView.inputTextField.text forKey:@"username"];
        [params setValue:[CommonUtil md5:_loginPswView.inputTextField.text] forKey:@"password"];
        url = @"sso/sso/login";
    }else { //验证码登录
        if (![CommonUtil isValueMobile:_phoneView.inputTextField.text]) {
            [self showHUDMessage:@"无效的手机号码"];
            return;
        }
        if (_codeView.inputTextField.text.length!=6) {
            [self showHUDMessage:@"验证码为6位数字"];
            return;
        }
        url = [NSString stringWithFormat:@"sso/sso/login/code?phone=%@&code=%@",_phoneView.inputTextField.text,_codeView.inputTextField.text];
    }
    [params setValue:[NSNumber numberWithInteger:1] forKey:@"system"];
    [self showProgressHud];
    extracted(self, params, url);
}
- (void)requestjurisdiction:(NSDictionary *)result {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [params setValue:result[@"id"] forKey:@"loginUserId"];
    [params setValue:@"operational_system" forKey:@"serverCode"];
    [RequestTool requestLoginWithUrlSuffix:@"basics/api/v1/system/app/subSystemResource" method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [[NSUserDefaults standardUserDefaults] setObject:self.phoneNumView.inputTextField.text forKey:@"Acount"]; //记录账号
                [UserManager shareManager].Acount = self.phoneNumView.inputTextField.text;
                UserManager  *manager  = [UserManager shareManager];
                [[NSUserDefaults standardUserDefaults] setObject:result[@"name"] forKey:@"name"];
                manager.name = result[@"name"];
                [[NSUserDefaults standardUserDefaults] setObject:result[@"id"] forKey:@"userId"];
                manager.Id = result[@"id"];
                [[NSUserDefaults standardUserDefaults] setObject:self.loginPswView.inputTextField.text forKey:@"Password"];
                [UserManager shareManager].Password = self.loginPswView.inputTextField.text;
                if (self.keepPswButton.selected) { //保存密码
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"rememberPsw"];
                    [UserManager shareManager].rememberPsw = YES;
                }else {
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"rememberPsw"];
                    [UserManager shareManager].rememberPsw = NO;
                }
                NSDictionary *dic = responseMessage.data;
                NSArray *array = dic[@"data"];
                [[NSUserDefaults standardUserDefaults] setValue:array forKey:@"routingArray"];
                manager.routingArray = array;
                if (array.count==1) {
                    NSDictionary *result = array[0];
                    if ([result[@"routing"] isEqualToString:@"business_center"]) {
                        [CommonUtil setInfo:@"business" forKey:@"FUNSYSTEM"];
                        [UserManager shareManager].funSystem = @"business";
                        [UserManager shareManager].deptCode = @"001003001";
                    }else if ([result[@"routing"] isEqualToString:@"implement_center"]) {
                        [CommonUtil setInfo:@"implement" forKey:@"FUNSYSTEM"];
                        [UserManager shareManager].funSystem = @"implement";
                        [UserManager shareManager].deptCode = @"001003002";
                    }else if ([result[@"routing"] isEqualToString:@"operation_center"]) {
                        [CommonUtil setInfo:@"operation" forKey:@"FUNSYSTEM"];
                        [UserManager shareManager].funSystem = @"operation";
                        [UserManager shareManager].deptCode = @"001003003";
                    }
                    MainViewController *mainVc = [[MainViewController alloc]init];
                    [UIApplication sharedApplication].delegate.window.rootViewController = mainVc;
                }else {
                    ChooseSystemViewController *vc = [ChooseSystemViewController new];
                    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
                    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
                }
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
                [CommonUtil removeInfoWithKey:REQUEST_SESSION];
                [CommonUtil removeInfoWithKey:AUTO_LOGIN];
                [UserManager shareManager].autoLogin = NO;
            }
        });
    }];
}
@end
