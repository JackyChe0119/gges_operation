//
//  ChooseSystemViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/20.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChooseSystemViewController : FYBaseViewController
@property (nonatomic,assign)BOOL isChange;
@end

NS_ASSUME_NONNULL_END
