//
//  ForgetPswViewController.m
//  GEESYSTEM
//
//  Created by 车杰 on 2019/9/2.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ForgetPswViewController.h"
#import "CustomGraButton.h"
#import "MineListView.h"
@interface ForgetPswViewController ()
@property (nonatomic,strong)MineListView *phoneView;//手机号码
@property (nonatomic,strong)MineListView *codeView;//验证码
@property (nonatomic,strong)MineListView *newsPswView;
@property (nonatomic,strong)MineListView *surePswView;
@property (nonatomic,strong)UILabel *getCodeLabel;
@property (nonatomic,strong)CustomGraButton *sureButton;
@property (nonatomic,assign)BOOL isGettingCode;
@end

@implementation ForgetPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLeftButtonImage:BACKUP];
    [self setNavTitle:@"忘记密码" Color:Color_White];
    [self layoutUI];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}
- (void)layoutUI{
    UIScrollView *baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    baseScrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:baseScrollView];
    
    _phoneView = [[MineListView alloc]initWithFrame:RECT(26, 20, ScreenWidth-52, 40)];
    _phoneView.inputTextField.placeholder = @"请输入手机号";
    _phoneView.leftImageView.image = IMAGE(@"icon_mobileNum");
    _phoneView.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_phoneView.inputTextField addObserverTextFieldEditChangeWithLenth:11 inputType:ImportChatTypeNumber];
    [_phoneView.inputTextField addTarget:self action:@selector(PhoneinputChanged) forControlEvents:UIControlEventEditingChanged];
    [baseScrollView addSubview:_phoneView];
    
    _codeView = [[MineListView alloc]initWithFrame:RECT(26, BOTTOM(_phoneView)+20, ScreenWidth-52-CJSizeTransform(100)-10, 40)];
    _codeView.inputTextField.placeholder = @"请输入验证码";
    _codeView.leftImageView.image = IMAGE(@"icon_codeNum");
    _codeView.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    [_codeView.inputTextField addObserverTextFieldEditChangeWithLenth:6 inputType:ImportChatTypeNumber];
    [_codeView.inputTextField addTarget:self action:@selector(inputChanged) forControlEvents:UIControlEventEditingChanged];
    [baseScrollView addSubview:_codeView];
    
    _getCodeLabel = [UILabel new];
    [_getCodeLabel rect:RECT(ScreenWidth-CJSizeTransform(100)-26, BOTTOM(_phoneView)+25, CJSizeTransform(100), 30) aligment:Center font:13 isBold:NO text:@"发送验证码" textColor:Color_666666 superView:baseScrollView];
    _getCodeLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(getCode)];
    [_getCodeLabel addGestureRecognizer:tap];
    [_getCodeLabel addRoundedCornersWithRadius:15];
    _getCodeLabel.backgroundColor = Color_BG;
    
    
    _newsPswView = [[MineListView alloc]initWithFrame:RECT(26, BOTTOM(_codeView)+20, ScreenWidth-52, 40)];
    _newsPswView.inputTextField.placeholder = @"请输入新密码(6-16位英文或数字)";
    _newsPswView.inputTextField.secureTextEntry = YES;
    [_newsPswView.inputTextField addObserverTextFieldEditChangeWithLenth:16 inputType:ImportChatTypeNumberAndEnglish];
    [_newsPswView.inputTextField addTarget:self action:@selector(inputChanged) forControlEvents:UIControlEventEditingChanged];
    [baseScrollView addSubview:_newsPswView];
    
    _surePswView = [[MineListView alloc]initWithFrame:RECT(26, BOTTOM(_newsPswView)+20, ScreenWidth-52, 40)];
    _surePswView.inputTextField.placeholder = @"请再次确认新密码";
    _surePswView.inputTextField.secureTextEntry = YES;
    [_surePswView.inputTextField addObserverTextFieldEditChangeWithLenth:16 inputType:ImportChatTypeNumberAndEnglish];
    [_surePswView.inputTextField addTarget:self action:@selector(inputChanged) forControlEvents:UIControlEventEditingChanged];
    [baseScrollView addSubview:_surePswView];
    
    _sureButton = [[CustomGraButton alloc]initWithFrame:RECT(40, BOTTOM(_surePswView)+50, ScreenWidth-80, 44) target:self action:@selector(commitButtonClick) title:@"提  交" color:Color_White font:17 type:1];
    [_sureButton addRoundedCornersWithRadius:22];
    [baseScrollView addSubview:_sureButton];
    
    [baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(_sureButton)+10)];
    
}
#pragma mark private
- (void)getCode {
    if (![CommonUtil isValueMobile:_phoneView.inputTextField.text]) {
        [self showHUDMessage:@"无效的手机号码"];
        return;
    }
    [self showProgressHud];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    [RequestTool requestLoginWithUrlSuffix:[NSString stringWithFormat:@"sso/sso/sendSms?phone=%@&type=%@",_phoneView.inputTextField.text,@"2"]  method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"短信已发至您的手机，请注意查看"];
                self.isGettingCode = YES;
                [_getCodeLabel countdown:^{
                } :^{
                    //未完成状态下
                }];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
- (void)inputChanged{
    if (_phoneView.inputTextField.text.length==11&&_codeView.inputTextField.text.length==6&&_newsPswView.inputTextField.text.length>=6&&_surePswView.inputTextField.text.length>=6) {
        [_sureButton setSlectState];
    }else {
        [_sureButton setUnSelectState];
    }
}
- (void)PhoneinputChanged {
    if (self.isGettingCode) {
        
    }else {
        if (_phoneView.inputTextField.text.length==11) {
            _getCodeLabel.backgroundColor = Color_CommonLight;
            _getCodeLabel.textColor = Color_White;
        }else {
            _getCodeLabel.backgroundColor = Color_BG;
            _getCodeLabel.textColor = Color_666666;
        }
        _getCodeLabel.text = @"获取验证码";
    }
}
//确认按钮点击
- (void)commitButtonClick {
    if (![CommonUtil isValueMobile:_phoneView.inputTextField.text]) {
        [self showHUDMessage:@"无效的手机号码"];
        return;
    }
    if (_codeView.inputTextField.text.length!=6) {
        [self showHUDMessage:@"验证码为6位数字"];
        return;
    }
    if (![_newsPswView.inputTextField.text isEqualToString:_surePswView.inputTextField.text]) {
        [self showHUDMessage:@"新密码与确认密码不相同"];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
    NSString *url = [NSString stringWithFormat:@"sso/sso/modifyPasswordByCode?phone=%@&code=%@&password=%@",_phoneView.inputTextField.text,_codeView.inputTextField.text,[CommonUtil md5:_newsPswView.inputTextField.text]];
    [RequestTool requestLoginWithUrlSuffix:url method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([responseMessage isRequestSuccessful]) {
                [self showHUDMessage:@"修改成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
@end
