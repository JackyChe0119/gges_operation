//
//  ChooseSystemViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/20.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ChooseSystemViewController.h"
#import "CustomGraButton.h"
#import "MainViewController.h"
#import "LoginViewController.h"
@interface ChooseSystemViewController ()

@end

@implementation ChooseSystemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
- (void)navgationLeftButtonClick {
    if (self.isChange) {
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        LoginViewController *mainVc = [[LoginViewController alloc]init];
        [UIApplication sharedApplication].delegate.window.rootViewController = mainVc;
    }
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    [self setLeftButtonImage:BACKUP];

    [self setNavTitle:@"绿金运营平台" Color:Color_3D3A39];
    
    UILabel *tipLabel = [UILabel new];
    [tipLabel rect:RECT(10, NavHeight+10, 100, 20) aligment:Left font:14 isBold:NO text:@"系统选择：" textColor:Color_333333 superView:self.view];
    [self addLineViewWithFrame:RECT(0, NavHeight+40, ScreenHeight, .5) Color:Color_Line supView:self.view];
    
    NSArray *array = [UserManager shareManager].routingArray;
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CustomGraButton *businessButton = [[CustomGraButton alloc]initWithFrame:RECT(40, NavHeight+65+70*idx, ScreenWidth-80, 50) target:self action:@selector(ButtonClick:) title:obj[@"name"] color:Color_White font:17 type:2];
        [businessButton addRoundedCornersWithRadius:4];
        [self.view addSubview:businessButton];
    }];
}
#pragma mark ---------->>> method
- (void)ButtonClick:(UIButton *)sender {
    NSArray *array = [NSArray array];
    [UserManager shareManager].areaJosn = array;
    if ([sender.titleLabel.text isEqualToString:@"商务中心"]) {
        [CommonUtil setInfo:@"business" forKey:@"FUNSYSTEM"];
        [UserManager shareManager].funSystem = @"business";
        [UserManager shareManager].deptCode = @"001003001";
    }else if ([sender.titleLabel.text isEqualToString:@"实施中心"]) {
        [CommonUtil setInfo:@"implement" forKey:@"FUNSYSTEM"];
        [UserManager shareManager].funSystem = @"implement";
        [UserManager shareManager].deptCode = @"001003002";
    }else {
        [CommonUtil setInfo:@"operation" forKey:@"FUNSYSTEM"];
        [UserManager shareManager].funSystem = @"operation";
        [UserManager shareManager].deptCode = @"001003003";

    }
    MainViewController *mainVc = [[MainViewController alloc]init];
    [UIApplication sharedApplication].delegate.window.rootViewController = mainVc;
}
@end
