//
//  CustomTitleInputView.h
//  CarPrimeMinister
//
//  Created by 车杰 on 2019/3/18.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomTitleInputView : UIView
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *subTitleLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,copy)NSString *tipTitle;//标题标签
@property (nonatomic,assign) BOOL showLine;//是否展示线条
@property (nonatomic,assign)CGFloat rightMargin;//右侧边距
@property (nonatomic,assign)CGFloat leftMargin;//左侧边距
@property (nonatomic,copy)NSString *placeHoldTtitle;//占位符
@property (nonatomic,strong)UITextField *inputTextfield;//输入框
@property (nonatomic,assign)BOOL showRightTitle;//右侧标题
@property (nonatomic,assign)UIColor *titleColor;//右侧标题颜色

@end

NS_ASSUME_NONNULL_END
