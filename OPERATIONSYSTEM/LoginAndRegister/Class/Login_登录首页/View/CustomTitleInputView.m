//
//  CustomTitleInputView.m
//  CarPrimeMinister
//
//  Created by 车杰 on 2019/3/18.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "CustomTitleInputView.h"
@interface CustomTitleInputView()<UITextFieldDelegate>

@end
@implementation CustomTitleInputView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor= Color_White;
        self.rightMargin = 100;
        _titleLabel = [UILabel new];
        _leftMargin = 75;
        [_titleLabel rect:RECT(0, HEIGHT(self)/2.0-15, 75, 30) aligment:Left font:13 isBold:NO text:@"" textColor:Color_333333 superView:self];
        
        _inputTextfield = [[UITextField alloc]initWithFrame:RECT(75, HEIGHT(self)/2.0-15, WIDTH(self)-75, 30)];
        _inputTextfield.borderStyle = 0;
        _inputTextfield.textColor = Color_333333;
        _inputTextfield.delegate = self;
        _inputTextfield.font = FONT(14);
        _inputTextfield.borderStyle =  UITextBorderStyleRoundedRect;
        _inputTextfield.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:_inputTextfield];
        
    }
    return self;
}
- (void)setShowLine:(BOOL)showLine {
    _showLine = showLine;
    self.lineView.hidden = !showLine;
}
- (void)setShowRightTitle:(BOOL)showRightTitle {
    _showRightTitle = showRightTitle;
    if (showRightTitle) {
        if (!_subTitleLabel) {
            _subTitleLabel = [UILabel new];
            [_subTitleLabel rect:RECT(WIDTH(self)-56, HEIGHT(self)/2.0-15, 40, 30) aligment:Right font:10 isBold:NO text:@"公里" textColor:Color_333333 superView:self];
            _inputTextfield.frame = RECT(self.leftMargin, HEIGHT(self)/2.0-15, WIDTH(self)-self.self.leftMargin-56, 30);
        }
    }
}
- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    self.titleLabel.textColor = titleColor;
}
- (void)setTipTitle:(NSString *)tipTitle {
    _tipTitle = tipTitle;
    self.titleLabel.text = tipTitle;
}
- (void)setPlaceHoldTtitle:(NSString *)placeHoldTtitle {
    _placeHoldTtitle = placeHoldTtitle;
    self.inputTextfield.placeholder = placeHoldTtitle;
}
- (void)setRightMargin:(CGFloat)rightMargin {
    _rightMargin = rightMargin;
    _inputTextfield.frame = RECT(_leftMargin, HEIGHT(self)/2.0-15, WIDTH(self)-rightMargin-_leftMargin, 30);
    _subTitleLabel.frame =  RECT(WIDTH(self)-rightMargin, HEIGHT(self)/2.0-15,rightMargin, 30);
}
- (void)setLeftMargin:(CGFloat)leftMargin {
    _leftMargin = leftMargin;
    _titleLabel.frame = RECT(0, HEIGHT(self)/2.0-15, leftMargin, 30);
    _inputTextfield.frame = RECT(leftMargin, HEIGHT(self)/2.0-15, WIDTH(self)-leftMargin, 30);
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView createViewWithFrame:RECT(16, HEIGHT(self)-.5, WIDTH(self)-16, .5) color:Color_Line];
        [self addSubview:_lineView];
    }
    return _lineView;
}
@end
