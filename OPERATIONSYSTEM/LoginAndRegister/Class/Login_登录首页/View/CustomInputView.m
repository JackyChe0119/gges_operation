//
//  CustomInputView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/15.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "CustomInputView.h"
@interface CustomInputView ()
@property (nonatomic,strong)UIView *lineView;
@end
@implementation CustomInputView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.inputTextField];
//        [self addSubview:self.tipLabel];
    }
    return self;
}
- (void)setShowLine:(BOOL)showLine {
    _showLine = showLine;
    self.lineView.hidden = !showLine;
}
- (void)setTipTitle:(NSString *)tipTitle {
    _tipTitle = tipTitle;
    self.inputTextField.placeholder = tipTitle;
}
- (void)setRightMargin:(CGFloat)rightMargin {
    _rightMargin = rightMargin;
//    _inputTextField.frame = RECT(0, HEIGHT(self)-32, WIDTH(self)-rightMargin, 30);
//    //给提示文本重新赋值
//     CGRect rect = _tipLabel.frame;
//    rect.size.width = rect.size.width-rightMargin;
//    if (rightMargin<=0) {
//        rect.size.width = WIDTH(self);
//    }
//    _tipLabel.frame = rect;
}
#pragma mark 布局视图
- (UITextField *)inputTextField {
    if (!_inputTextField) {
        _inputTextField = [[UITextField alloc]initWithFrame:RECT(0, HEIGHT(self)-40, WIDTH(self), 30)];
        _inputTextField.borderStyle = 0;
        _inputTextField.textColor = Color_333333;
        _inputTextField.font = FONT(16);
        _inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _inputTextField;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView createViewWithFrame:RECT(0, HEIGHT(self)-.5, WIDTH(self), .5) color:Color_Line];
        [self addSubview:_lineView];
    }
    return _lineView;
}
- (void)beginToInput {
    [self.inputTextField becomeFirstResponder];
}

@end
