//
//  CustomInputView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/3/15.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomInputView : UIView
@property (nonatomic,copy)NSString *tipTitle;//提示标签
@property (nonatomic,assign) BOOL showLine;//展示线条
@property (nonatomic,assign)CGFloat rightMargin;//右侧间距
@property (nonatomic,strong)UITextField *inputTextField;//输入框
@end

NS_ASSUME_NONNULL_END
