//
//  ServiceDetailView.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertListSelectView.h"
#import "TimeQuantumView.h"
#import "CustomTitleInputView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ServiceDetailView : UIView
@property (nonatomic,strong)AlertListSelectView *serviceTimeView;//服务时间
@property (nonatomic,strong)TimeQuantumView *timeQuanTum;
@property (nonatomic,strong)AlertListSelectView *fuwuView;//服务类型
@property (nonatomic,strong)CustomTitleInputView *banciView;//生产班次
@property (nonatomic,strong)CustomTitleInputView *timeView;//生成时间
@property (nonatomic,strong)CustomTitleInputView *dianweiView;//点位数量
@end

NS_ASSUME_NONNULL_END
