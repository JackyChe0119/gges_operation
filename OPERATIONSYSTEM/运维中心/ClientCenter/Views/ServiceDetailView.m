//
//  ServiceDetailView.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "ServiceDetailView.h"

@implementation ServiceDetailView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _serviceTimeView = [[AlertListSelectView alloc]initWithFrame:RECT(10, 0, WIDTH(self)-10, 40)];
        _serviceTimeView.titleLabel.textColor = Color_333333;
        _serviceTimeView.title = @"*服务年限：";
        _serviceTimeView.leftWidth = 80;
        _serviceTimeView.titleLabel.textAlignment = Left;
        _serviceTimeView.info = @"";
        [self addSubview:_serviceTimeView];
        
        _timeQuanTum = [[TimeQuantumView alloc]initWithFrame:RECT(10, BOTTOM(_serviceTimeView), WIDTH(self)-20, 40)];
        [self addSubview:_timeQuanTum];
        
        _fuwuView = [[AlertListSelectView alloc]initWithFrame:RECT(10, BOTTOM(_timeQuanTum), WIDTH(self)-10, 40)];
        _fuwuView.titleLabel.textColor = Color_333333;
        _fuwuView.title = @"服务选择：";
        _fuwuView.leftWidth = 75;
        _fuwuView.info = @"用电监管";
        _fuwuView.titleLabel.textAlignment = Left;
        [self addSubview:_fuwuView];
        
        _banciView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_fuwuView), (WIDTH(self)-20)/2.0, 40)];
        _banciView.tipTitle = @"*生产班次：";
        _banciView.placeHoldTtitle = @"请输入";
        _banciView.titleLabel.textAlignment = Left;
        _banciView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [self addSubview:_banciView];
        
         _timeView= [[CustomTitleInputView alloc]initWithFrame:RECT(WIDTH(self)/2.0, BOTTOM(_fuwuView), (WIDTH(self)-20)/2.0, 40)];
        _timeView.tipTitle = @"*生产时间：";
        _timeView.placeHoldTtitle = @"请输入";
        _timeView.titleLabel.textAlignment = Left;
        _timeView.showRightTitle = YES;
        _timeView.rightMargin = 30;
        _timeView.titleLabel.textAlignment = Right;
        _timeView.subTitleLabel.text = @"小时";
        [self addSubview:_timeView];
        
        _dianweiView = [[CustomTitleInputView alloc]initWithFrame:RECT(10, BOTTOM(_timeView), (WIDTH(self)-20)/2.0+30, 40)];
        _dianweiView.tipTitle = @"*实际点位数量：";
        _dianweiView.placeHoldTtitle = @"请输入";
        _dianweiView.titleLabel.textAlignment = Left;
        _dianweiView.leftMargin = 100;
        _dianweiView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        
        [self addSubview:_dianweiView];
        
        CGRect rect = self.frame;
        rect.size.height = BOTTOM(_dianweiView);
        self.frame = rect;
    }
    return self;
}

@end
