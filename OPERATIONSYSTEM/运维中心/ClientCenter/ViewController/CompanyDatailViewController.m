//
//  CompanyDatailViewController.m
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "CompanyDatailViewController.h"
#import "ServiceDetailView.h"
#import "ProjectTotalView.h"
#import "ImageUploadView.h"
#import "TaskCompanyInfoView.h"
#import "LocationView.h"
#import "EquipmentManagerViewController.h"
@interface CompanyDatailViewController ()
@property (nonatomic,strong)UIScrollView *baseScrollView;
@property (nonatomic,strong)ServiceDetailView *serviewView;
@property (nonatomic,strong)ProjectTotalView *totalView;
@property (nonatomic,strong)ImageUploadView *dwfhtView;
@property (nonatomic,strong)ImageUploadView *czwgxtView;
@property (nonatomic,strong)ImageUploadView *czwsjwdView;
@property (nonatomic,strong)TaskCompanyInfoView *companyInfoView;
@property (nonatomic,strong)LocationView *locationView;
@property (nonatomic,copy)NSString *maintainId;
@end

@implementation CompanyDatailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
}
#pragma mark ---------->>> 布局视图
- (void)layoutUI {
    [self setLeftButtonImage:BACKUP];
    [self setNavTitle:@"企业概况" Color:Color_3D3A39];
    
    [self navgationRightButtonTitle:@"设备管理" color:Color_333333];
    
    _baseScrollView = [[UIScrollView alloc]initWithFrame:RECT(0, NavHeight, ScreenWidth, ScreenHeight-NavHeight)];
    _baseScrollView.showsVerticalScrollIndicator = NO;
    _baseScrollView.backgroundColor = Color_White;
    [self.view addSubview:_baseScrollView];
    
    _totalView = [[ProjectTotalView alloc]initWithFrame:RECT(0, 10, ScreenWidth, 90)];
    _totalView.swfzr = @"商务负责人：-- ";
    _totalView.ssfzr = @"实施负责人：-- ";
    _totalView.ywfzr = @"运维负责人：-- ";
    _maintainId = @"";
    [_totalView hiddenRightInfo];
    [_baseScrollView addSubview:_totalView];
    
    _serviewView = [[ServiceDetailView alloc]initWithFrame:RECT(0, BOTTOM(_totalView), ScreenWidth, 200)];
    [_baseScrollView addSubview:_serviewView];
    
    _dwfhtView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_serviewView), ScreenWidth, 140) max:9];
    _dwfhtView.titleLabel.text = @"点位复核图：";
    [_baseScrollView addSubview:_dwfhtView];
    
    _czwgxtView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_dwfhtView), ScreenWidth, 140) max:9];
    _czwgxtView.titleLabel.text = @"产治污关系图：";
      [_baseScrollView addSubview:_czwgxtView];
    
    _czwsjwdView = [[ImageUploadView alloc]initWithFrame:RECT(0, BOTTOM(_czwgxtView), ScreenWidth, 140) max:30];
    _czwsjwdView.titleLabel.text = @"产治污设计文档：";
      [_baseScrollView addSubview:_czwsjwdView];
    
    _companyInfoView = [[TaskCompanyInfoView alloc]initWithFrame:RECT(0, BOTTOM(_czwsjwdView)+20, ScreenWidth, 500) type:1];
    [_baseScrollView addSubview:_companyInfoView];
    
    _locationView = [[LocationView alloc]initWithFrame:RECT(0, BOTTOM(_companyInfoView), ScreenWidth, 340)];
    [_baseScrollView addSubview:_locationView];
    
//   CustomGraButton *commitButton = [[CustomGraButton alloc]initWithFrame:RECT(ScreenWidth/2.0-80, BOTTOM(_locationView)+30,160, 40) target:self action:@selector(commitButtonClick) title:@"保    存" color:Color_White font:17 type:2];
//     [commitButton addRoundedCornersWithRadius:5];
//    [_baseScrollView addSubview:commitButton];
    
    [_baseScrollView setContentSize:CGSizeMake(ScreenWidth, BOTTOM(_locationView)+20)];
    
    [self setEditStatus];
    
    [self doRequestOperation:YES];
}
- (void)navgationRightButtonClick {
    EquipmentManagerViewController *vc = [EquipmentManagerViewController new];
    vc.Id = self.companyId;
    [self pushWithViewController:vc hidden:YES];
}
- (void)doRequestOperation:(BOOL)show {
    [self showProgressHud];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_group_t group = dispatch_group_create();
    if (self.projectId) {
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            [self requestXcDetail:group];
        });
        dispatch_group_enter(group);
        dispatch_group_async(group, queue, ^{
            [self requestDetail:group];
        });
    }
    dispatch_group_enter(group);
          dispatch_group_async(group, queue, ^{
              [self requestCompanyDetail:group];
          });
    dispatch_group_notify(group, queue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];//刷新UI
        });
    });
}
- (void)requestXcDetail:(dispatch_group_t)group {
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/survey/%@",self.projectId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               if ([responseMessage isRequestSuccessful]) {
                   NSDictionary *result = responseMessage.data[@"data"];
                   [self relayoutWithXcDeatil:result];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
               }
               if (group) {
                   dispatch_group_leave(group);
               }
           });
       }];
}
#pragma mark ---------->>> request
- (void)requestDetail:(dispatch_group_t)group  {
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/apply/%@",self.projectId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               if ([responseMessage isRequestSuccessful]) {
                   NSDictionary *result = responseMessage.data[@"data"];
                   [self relayoutWithDeatil:result];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
               }
               if (group) {
                    dispatch_group_leave(group);
               }
           });
       }];
}
- (void)requestCompanyDetail:(dispatch_group_t)group {
       NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
       [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"basics/api/v1/company/%@",self.companyId] method:GET params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
           dispatch_async(dispatch_get_main_queue(), ^{
               if ([responseMessage isRequestSuccessful]) {
                   NSDictionary *result = responseMessage.data[@"data"];
                   [self layoutCompanyDetail:result];
               }else {
                   [self showHUDMessage:responseMessage.errorMessage];
               }
               if (group) {
                   dispatch_group_leave(group);
               }
           });
       }];
}
- (void)layoutCompanyDetail:(NSDictionary *)result {
    _companyInfoView.qymcView.info= result[@"companyName"];
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    if ([CommonUtil feiKong:result[@"creditCode"]]) {
        _companyInfoView.tyxydmView.inputTextfield.text = result[@"creditCode"];
    }
    if ([CommonUtil feiKong:result[@"legalRepresentative"]]) {
        _companyInfoView.fzView.inputTextfield.text = result[@"legalRepresentative"];
    }
    NSDictionary *leadCadre = result[@"leadCadre"];
    _companyInfoView.lxrView.inputTextfield.text = leadCadre[@"name"];
    _companyInfoView.sjhmView.inputTextfield.text = leadCadre[@"phone"];
    
    NSDictionary *industryClass = result[@"industryClass"];
    if ([CommonUtil feiKong:industryClass]) {
      _companyInfoView.hylbView.info = industryClass[@"name"];
    }
    NSDictionary *basDivision = result[@"basDivision"];
    NSString *first = basDivision[@"name"];
    NSDictionary *level = basDivision[@"level"];
    NSString *second;
    if (level) {
        second = level[@"name"];
    }
    if (second) {
        _companyInfoView.xzqhView.info = [NSString stringWithFormat:@"%@/%@",first,second];
    }else {
        _companyInfoView.xzqhView.info = first;
    }
    if ([CommonUtil feiKong:result[@"licenseNumber"]]) {
         _companyInfoView.pwxkzView.inputTextfield.text = result[@"licenseNumber"];
     }
    if ([CommonUtil feiKong:result[@"emissionTradingDocuments"]]) {
         _companyInfoView.pwjyhView.inputTextfield.text = result[@"emissionTradingDocuments"];
     }
    if ([CommonUtil feiKong:result[@"eIABatchNumber"]]) {
         _companyInfoView.hppfView.inputTextfield.text = result[@"eIABatchNumber"];
     }
    if ([CommonUtil feiKong:result[@"longitude"]]) {
        _locationView.jdView.inputTextfield.text = result[@"longitude"];
        _locationView.wdView.inputTextfield.text = result[@"latitude"];
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake([result[@"latitude"] doubleValue], [result[@"longitude"] doubleValue]);
        [_locationView.mapView setCenterCoordinate:location animated:YES];
        [_locationView addPoint:location title:result[@"companyName"]];
        _locationView.currentLocation = location;
        _locationView.addressView.text = result[@"addressDetail"];
    }
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    

}
- (void)relayoutWithDeatil:(NSDictionary *)result {
    
    if (![result[@"service"] isKindOfClass:[NSNull class]]) {
         _serviewView.fuwuView.info = result[@"service"];
     }
    _serviewView.serviceTimeView.info = [NSString stringWithFormat:@"%@",result[@"serviceYear"]];
    _serviewView.timeQuanTum.fromTimeView.info = [result[@"serviceStart"] substringWithRange:RANGE(0, 10)];
    _serviewView.timeQuanTum.toTimeView.info = [result[@"serviceEnd"] substringWithRange:RANGE(0, 10)];
    _totalView.swfzr =[NSString stringWithFormat:@"商务负责人：%@",result[@"businessLeaderName"]];
    _totalView.swPhone = result[@"businessLeaderPhone"];
    _totalView.ssfzr = [NSString stringWithFormat:@"实施负责人：%@",result[@"applyLeaderName"]];
    _totalView.ssPhone = result[@"applyLeaderPhone"];
    _totalView.ywfzr = [NSString stringWithFormat:@"运维负责人：%@",result[@"maintainLeaderName"]];
    _totalView.ywPhone = result[@"maintainLeaderPhone"];
    _maintainId = result[@"maintainLeader"];

}
- (void)setEditStatus {
    _serviewView.serviceTimeView.userInteractionEnabled = NO;
    _serviewView.timeQuanTum.fromTimeView.userInteractionEnabled = NO;
    _serviewView.timeQuanTum.toTimeView.userInteractionEnabled = NO;
    _serviewView.banciView.inputTextfield.enabled = NO;
    _serviewView.timeView.inputTextfield.enabled = NO;
    _serviewView.dianweiView.inputTextfield.enabled = NO;
    _companyInfoView.qymcView.userInteractionEnabled = NO;//甲方不可选择
    _companyInfoView.tyxydmView.inputTextfield.enabled = NO;
    _companyInfoView.fzView.inputTextfield.enabled = NO;
    _companyInfoView.lxrView.inputTextfield.enabled = NO;
    _companyInfoView.sjhmView.inputTextfield.enabled = NO;
    _companyInfoView.bylxrView.inputTextfield.enabled = NO;
    _companyInfoView.bysjhmView.inputTextfield.enabled = NO;
    _companyInfoView.xzqhView.baseView.userInteractionEnabled = NO;
    _companyInfoView.hylbView.baseView.userInteractionEnabled = NO;
    _companyInfoView.zczjView.inputTextfield.enabled = NO;
    _companyInfoView.pwxkzView.inputTextfield.enabled = NO;
    _companyInfoView.pwjyhView.inputTextfield.enabled = NO;
    _companyInfoView.hppfView.inputTextfield.enabled = NO;
    _locationView.jdView.inputTextfield.enabled = NO;
    _locationView.wdView.inputTextfield.enabled = NO;
    _locationView.needlocation = NO;
    [_locationView setCanEdit:NO];
    _locationView.addressView.editable = NO;
    
    [_dwfhtView setIsCommit:YES];
    [_dwfhtView removeUploadButton];
    
    [_czwgxtView setIsCommit:YES];
    [_czwgxtView removeUploadButton];
    
    [_czwsjwdView setIsCommit:YES];
    [_czwsjwdView removeUploadButton];
}
- (void)relayoutWithXcDeatil:(NSDictionary *)result {
    if (![result[@"productShifts"] isKindOfClass:[NSNull class]]) {
         _serviewView.banciView.inputTextfield.text = result[@"productShifts"];
     }
    if (![result[@"productTime"] isKindOfClass:[NSNull class]]) {
           _serviewView.timeView.inputTextfield.text = result[@"productTime"];
       }
    if (![result[@"pointCount"] isKindOfClass:[NSNull class]]) {
            _serviewView.dianweiView.inputTextfield.text = [NSString stringWithFormat:@"%@",result[@"pointCount"]];
    }

    NSArray *array = result[@"pointReviewAttachment"];
    if (array&&array.count>0) {
         _dwfhtView.imageUrlArray = [array mutableCopy];
     }

    NSArray *array2 = result[@"relationAttachment"];
    if (array2&&array2.count>0) {
         _czwgxtView.imageUrlArray = [array2 mutableCopy];
     }

    NSArray *array3 = result[@"designAttachment"];
     if (array3&&array3.count>0) {
          _czwsjwdView.imageUrlArray = [array3 mutableCopy];
      }
}
- (void)commitButtonClick { //暂不可修改状态
    [self showProgressHud];
        if (_companyInfoView.qymcIView.inputTextfield.text.length==0) {[self showHUDMessage:@"请输入企业名称"];return;}
        if (_companyInfoView.tyxydmView.inputTextfield.text.length==0) {[self showHUDMessage:@"请输入企业统一信用识别代码"];return;}
        if (_companyInfoView.fzView.inputTextfield.text.length==0) {[self showHUDMessage:@"请输入法人姓名"];return;}
        if (_companyInfoView.lxrView.inputTextfield.text.length==0) {[self showHUDMessage:@"请输入联系人姓名"];return;}
        if ( _companyInfoView.xzqhView.info.length==0) {[self showHUDMessage:@"请选择行政区划"];return;}
       if ( _companyInfoView.hylbView.info.length==0) {[self showHUDMessage:@"请选择行业类别"];return;}
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithCapacity:0];
        [params setValue:_companyInfoView.qymcIView.inputTextfield.text forKey:@"companyName"];
        [params setValue:_companyInfoView.tyxydmView.inputTextfield.text forKey:@"creditCode"];
        [params setValue:_companyInfoView.fzView.inputTextfield.text forKey:@"legalRepresentative"];
        [params setValue:_companyInfoView.lxrView.inputTextfield.text forKey:@"linkman"];
        [params setValue:_companyInfoView.sjhmView.inputTextfield.text forKey:@"linkmanPhone"];
        if (_companyInfoView.bylxrView.inputTextfield.text.length>0) {
            [params setValue:_companyInfoView.bylxrView.inputTextfield.text forKey:@"linkmanTwo"];
        }
        if (_companyInfoView.bysjhmView.inputTextfield.text .length>0) {
             [params setValue:_companyInfoView.bysjhmView.inputTextfield.text  forKey:@"linkmanTwoPhone"];
        }
        [params setValue:_companyInfoView.xzqhView.info forKey:@"divisionName"];
        [params setValue:_companyInfoView.xzqhView.lineId forKey:@"division"];
        [params setValue:_companyInfoView.hylbView.info  forKey:@"industryClassName"];
        [params setValue:_companyInfoView.hylbView.lineId  forKey:@"industryClass"];
        if (_companyInfoView.zczjView.inputTextfield.text.length>0) {
            [params setValue:_companyInfoView.zczjView.inputTextfield.text forKey:@"registerMoney"];
        }
        if (_companyInfoView.pwxkzView.inputTextfield.text.length>0) {
            [params setValue:_companyInfoView.pwxkzView.inputTextfield.text forKey:@"licenseNumber"];
        }
        if (_companyInfoView.pwjyhView.inputTextfield.text.length>0) {
            [params setValue:_companyInfoView.pwjyhView.inputTextfield.text forKey:@"emissionTradingDocuments"];
        }
        if (_companyInfoView.hppfView.inputTextfield.text.length>0) {
            [params setValue:_companyInfoView.hppfView.inputTextfield.text forKey:@"eiaBatchNumber"];
        }
        [params setValue:_locationView.jdView.inputTextfield.text forKey:@"longitude"];
        [params setValue:_locationView.wdView.inputTextfield.text forKey:@"latitude"];
        [params setValue:_locationView.addressView.text forKey:@"addressDetail"];
        [params setValue:[NSNumber numberWithInteger:1] forKey:@"status"];
    if (_dwfhtView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_dwfhtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"pointReviewAttachment"];
            }
        }
    if (_czwgxtView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_czwgxtView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"relationAttachment"];
            }
        }
    if (_czwsjwdView.imageArray.count>0) {
            NSMutableArray *uploadImageArray = [NSMutableArray arrayWithCapacity:0];
            [_czwsjwdView.imageArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if (obj.length>0) {
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithCapacity: 0];
                       [dic setValue:[obj substringToIndex:obj.length-4] forKey:@"name"];
                       [dic setValue:obj forKey:@"ossName"];
                       [uploadImageArray addObject:dic];
                }
            }];
            if (uploadImageArray.count>0) {
                [params setValue:uploadImageArray forKey:@"designAttachment"];
            }
        }
    [RequestTool requestDataWithUrlSuffix:[NSString stringWithFormat:@"devops/api/v1/maintain/companyInfo/%@",self.projectId] method:POST params:params callBack:^(RequestResponseMessage * _Nonnull responseMessage) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hiddenProgressHud];
            if ([responseMessage isRequestSuccessful]) {
                
            }else {
                [self showHUDMessage:responseMessage.errorMessage];
            }
        });
    }];
}
@end
