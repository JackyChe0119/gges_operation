//
//  CompanyDatailViewController.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/9/25.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "FYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CompanyDatailViewController : FYBaseViewController
@property (nonatomic,copy)NSString *projectId;
@property (nonatomic,copy)NSString *companyId;
@end

NS_ASSUME_NONNULL_END
