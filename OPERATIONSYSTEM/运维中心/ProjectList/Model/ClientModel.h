//
//  ClientModel.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ClientModel : HYBaseModel
@property (nonatomic,copy)NSString *applyLeader,*businessLeader,*companyName,*division,*industryClass,*Id,*leadCadre,*maintain,*userId,*maintainLeader,*messageTime,*userName,*projectId;
@property (nonatomic,assign)NSInteger manageStatus;
@end

NS_ASSUME_NONNULL_END
