//
//  MaintainModel.h
//  OPERATIONSYSTEM
//
//  Created by 车杰 on 2019/10/12.
//  Copyright © 2019 Jacky Che. All rights reserved.
//

#import "HYBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MaintainModel : HYBaseModel
//applyEndTime = "2019-10-10 18:30:58";
//applyLeaderName = "商务主管";
//businessLeaderName = "商务主管";
//companyName = Eone;
//division = "湖州/吴兴区";
//id = 290013016496410624;
//maintainLeaderName = "商务主管";
//maintainStatus = 1;
//service = "<null>";
@property (nonatomic,copy)NSString *applyEndTime,*applyLeaderName,*businessLeaderName,*companyName,*division,*Id,*maintainLeaderName,*service,*maintainLeader,*maintainDivisionLeaderName;
@property (nonatomic,assign)NSInteger maintainStatus;
@end

NS_ASSUME_NONNULL_END
